:-dynamic no/6.

:-dynamic linha/5.

:-dynamic segment/4.

:- dynamic horariosPath/2.

:-dynamic horario/3. %horario(Path,Sequencial Id ,List_of_Time)

:-dynamic tripId/2. %tripId(Sequencial Id,DB Id)

:-dynamic workblock/6.  % workblock(Sequencial Id, List_of_Trips,StartTime, EndTime,Start node, end node)

:-dynamic workblockId/2. %workblockId(Sequencial Id,DB Id)

:-dynamic vehicleduty/2. % vehicleduty(VehicleDuty, List_of_WorkBlocks)

:-dynamic geracoes/1. %numero de geracoes

:-dynamic populacao/1. %numero de elementos em cada populacao

:-dynamic prob_cruzamento/1. %probabilidade de haver cruzamento

:-dynamic prob_mutacao/1.  %probabilidade de haver mutacao

:-dynamic service/1. %Id do servico de viatura em estudo

:-dynamic tamanho/1. %numero dos work blocks do servico de viatura em estudo

:-dynamic veryHardConst/1. %peso da constraint mais pesada, 10. Apenas usada se trabalhar mais de 4h seguidas.

:-dynamic hardConst/1. %peso da constraint pesadas, 8.

:-dynamic softConst/1. %peso das constraints leves, 1.

:-dynamic horas4/1.  %numero de segundos de 4 horas.

:-dynamic horas8/1.  %numero de segundos de 8 horas.

:-dynamic hora/1.  %numero de segundos de 4 horas.

:-dynamic t/3. %tripletos do tipo (Tempo Inicio, Tempo Fim, Id Condutor)

:-dynamic maxTime/1. %Tempo para o criterio de finalizacao: Acabar por tempo.

:-dynamic almoco/2. %Agenda temporal para o almoco das [39600,54000]

:-dynamic jantar/2.  %Agenda temporal para o jantar  das [64800,79200]

:-dynamic resposta/1. %guarda a melhor solucao da ultima geracao criada

:-dynamic pref_horario/1. %lista de tripletos do tipo (Id,TempoInicio,TempoFim), que representa a preferencia de horario do motorista

:-dynamic avaliacao_termino/1. %Critério de paragem aquando de um respetivo valor de avaliacao é inferior a este

:-dynamic termino_estabiliza/1.

:-dynamic obrigacao_horario/1.

:-dynamic melhor_escalonamento/2. % melhor escalonamento para cada vehicleduty gerado pelo algoritmo genético

:-dynamic motoristas_restantes/1. %lista de tuplos que ficaram disponiveis depois da afetação

:-dynamic tu/4. %tu(Hora Inicio ,horario fim, tempo de trabalho, Id).

:-dynamic lista_motoristas_nworkblocks/2.

:-dynamic  lista_wb_motorista/2.

:-dynamic melhor_escalonamento/2.

%correcao sobreposicao 

:-dynamic proposta_sobreposicao/3.

:-dynamic faltou_afetar_sobreposicao/1.

:-dynamic flag_sobreposicao/1.

:-dynamic tentativas_sobreposicao/1.

%correcao 4 horas
:-dynamic proposta_mudanca_4_horas/3.

:-dynamic flag_4_horas/1.

:-dynamic tentativas_4_horas/1.

:-dynamic faltou_corrigir_4_horas/3.

%correcao almoco jantar
:-dynamic tentativas_almoco_jantar/1.

:-dynamic flag_almoco_jantar/1.

:-dynamic proposta_mudanca_almoco_jantar/3.

:-dynamic faltou_corrigir_almoco_jantar/3.

%correcao 8 horas 
:-dynamic tentativas_8_horas/1.

:-dynamic flag_8_horas/1.

:-dynamic proposta_mudanca_8_horas/3.

:-dynamic faltou_corrigir_8_horas/1.

%correcao tempo deslocacao

:-dynamic tentativas_deslocacao/1.

:-dynamic flag_deslocacao/1.

:-dynamic proposta_mudanca_deslocamento/3.

:-dynamic faltou_corrigir_deslocamento/1.
