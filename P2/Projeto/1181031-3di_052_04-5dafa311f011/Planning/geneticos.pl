% PREDICADOS
hora(3600).
horas4(14400).
horas8(28800).
populacao(1).
geracoes(1).
prob_cruzamento(60).
prob_mutacao(30).
veryHardConst(10).
hardConst(8).
softConst(2).
maxTime(20000).
almoco(39600,54000).
jantar(64800,79200).
avaliacao_termino(-5).
termino_estabiliza(10).

pref_horario([(276,36000,72000), (5188,32400,68400),(16690,36000,68400),(18107,36000,64800)]).
%obrigacao_horario([(276,0,54000), (5188,54000,86400),(16690,36000,59200),(18107,36000,64800)]).

%COMO SERVIDOR
		
get_genetic(Request):-http_parameters(Request,
                    [duty(_, []),
                      nrGenerations(NrGenerations,[]),
                      dimension(Dimension,[]),
                      crossing(Crossing,[]),
					  mutation(Mutation,[])
                    ]), atom_number(NrGenerations,NrGenerations1),
					atom_number(NrGenerations,NrGenerations1), atom_number(Dimension,Dimension1),
					atom_number(Crossing,Crossing1),atom_number(Mutation,Mutation1),
					retractall(geracoes(_)),	retractall(populacao(_)),retractall(prob_cruzamento(_)),retractall(prob_mutacao(_)),
					asserta(geracoes(NrGenerations1)), asserta(populacao(Dimension1)), asserta(prob_cruzamento(Crossing1)), asserta(prob_mutacao(Mutation1)), 
					gera(), resposta(RF*_), prolog_to_json(RF,X), reply_json(X).


:-set_prolog_flag(answer_write_options,[max_depth(0)]).

gera_lista(V,R):-lista_motoristas_nworkblocks(V,A), gera_lista1(A,X), flatten(X,R).
gera_lista1([],[]).
gera_lista1([(H,A)|T],[R|T1]):-gera_lista1(T,T1), gera_aux(H,A,R).
gera_aux(_,0,[]):-!.
gera_aux(H,A,[H|T1]):-A1 is A-1, gera_aux(H,A1,T1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

insereA(X,1,L,[X|L]):-!.
insereA(X,N,[Y|L],[Y|L1]):- N1 is N-1,insereA(X,N1,L,L1).

gera_pop_heur(R):-populacao(TamPop), service(V),gera_lista(V,ListaDrivers), length(ListaDrivers,NumT), %gera individuo através da heuristica
        retractall(tamanho(_)), asserta(tamanho(NumT)), gera_pop_aux_heur(TamPop,ListaDrivers,NumT,R),!.

gera_pop_aux_heur(0,_,_,[]):-!.

gera_pop_aux_heur(TamPop,ListaDrivers,NumT,[Ind|Resto]):-!,
	TamPop1 is TamPop-1,
	gera_pop_aux_heur(TamPop1,ListaDrivers,NumT,Resto),!,
	NumT1 is NumT+1,
	gera_ind_heur(ListaDrivers,NumT1,Ind),
	not(member(Ind,Resto)).


verifica_comeco_heur(G,V):-obrigacao_horario(L), nth0(_,L,(G,I,_)),service(S), vehicleduty(S,L1), nth0(0,L1,WB),!,
 workblock(WB,_,S1,F1,_,_), (((S1>I,I<F1),!, V is 1); V is 0).

gera_ind_heur(L,N,R):-aux3(L,N,[],R,0).
aux3(_,1,R1,R,_):-!, reverse(R1,R).
aux3(Lista,Num,T,R,Count):- tamanho(Tam), Count1 is Count+1, Temp is Num - 1, ((Temp==1,!,random(1,2,N)); random(1,Temp,N)) , retira(N,Lista,G,NovaLista),!,verifica_comeco_heur(G,V),
(((V==1,!;Count1=Tam), !, aux3(NovaLista,Temp,[G|T],R,0)); aux3(Lista,Num,T,R,Count1)). 


gera():-
	%inicializa,
	get_time(Ti),
	gera_populacao(Pop),!,
    %gera_pop_heur(Pop), !, para obter gerações através de heuristica é necessario decomentar esta lista e comentar a de cima
	avalia_populacao(Pop,PopAv),
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao(0,NG,PopOrd,Ti,0,0),!.

gera_populacao(Pop):-
	populacao(TamPop),
	service(VS),
	gera_lista(VS,ListaDrivers),
	length(ListaDrivers,NumT),
        retractall(tamanho(_)), asserta(tamanho(NumT)),
	gera_populacao(TamPop,ListaDrivers,NumT,Pop),!.

gera_populacao(0,_,_,[]):-!.

gera_populacao(TamPop,ListaDrivers,NumT,[Ind|Resto]):-!,
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaDrivers,NumT,Resto),!,
	gera_individuo(ListaDrivers,NumT,Ind),
	not(member(Ind,Resto)).


gera_individuo([G],1,[G]):-!.

gera_individuo(ListaDrivers,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaDrivers,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

%AVALIA GERACAO

wbTimes(L,R):-wbTimes1(L,R). %transforma uma lista de work blocks numa lista com os tempos de passagem de cada viagem sua.
wbTimes1([],[]).
wbTimes1([H|T],[R|T1]):-wbTimes1(T,T1), workblock(H,LT,_,_,_,_), tripTimes(LT,R).

tripTimes(L,R):-tripTimes1(L,A), flatten(A,R). %transforma uma lista de trips numa lista com os tempos de passagem de cada elemento.
tripTimes1([],[]).
tripTimes1([H|T],[R|T1]):-tripTimes1(T,T1), horario(_,H,R).


pack([],[]). %compacta em listas os elementos iguais consecutivos.
pack([X|Xs],[Z|Zs]) :- transfer(X,Xs,Ys,Z), !,pack(Ys,Zs).
transfer(X,[],[],[X]).
transfer(X,[Y|Ys],[Y|Ys],[X]) :- X \= Y.
transfer(X,[X|Xs],Ys,[X|Zs]) :- transfer(X,Xs,Ys,Zs).


constroiAvaliacao([],[],_):-retractall(t(_,_,_)). %constroi tripletos do tipo (tempoInicio,tempoFim,Motorista) para um dado gene.
constroiAvaliacao([H|T],[(S,E,H1)|T1],[H1|T2]):-constroiAvaliacao(T,T1,T2), workblock(H,_,S,E,_,_), asserta(t(S,E,H1)).

constroiSuperAvaliacao(L,R,T):-constroiSuperAvaliacao(L,R,T,0). %Transforma tripletos consecutivos referentes a um motorista num so tripleto.
constroiSuperAvaliacao([],[],_,_).
constroiSuperAvaliacao([H|T],[(A1,A2,H1)|T1],L,C):-
nth0(0,H,H1), length(H,Len), nth0(C,L,(A1,_,_)), C1 is C +Len , Aux1 is C1 - 1 ,  nth0(Aux1,L,(_,A2,_)),
constroiSuperAvaliacao(T,T1,L,C1).


avalia_tempo_continuo(L,P):-avalia_tempo_continuo1(L,0,P). %avalia a penalizacao do tempo continuo m�ximo de trabalho ser 4h.
avalia_tempo_continuo1([],P,P).
avalia_tempo_continuo1([(I,F,_)|T],P,R):- Aux is F - I, horas4(H),
((Aux>=H,!,veryHardConst(X),Aux1 is X * Aux, P1 is P + Aux1); (P1 is P)), avalia_tempo_continuo1(T,P1,R).

constroiListaTempoMaximo([],[]).
constroiListaTempoMaximo([H|T],[(H,LT)|T1]):-constroiListaTempoMaximo(T,T1),findall(t(I,F,H),t(I,F,H),LT).

avalia_tempo_maximo(M,P):-avalia_tempo_maximo1(M,0,P). %avalia a penalizacao do tempo total de trabalho ser 8h.
avalia_tempo_maximo1([],P,P).
avalia_tempo_maximo1([H|T],P,R):-calculaPenMotorista(H,Paux), P1 is P + Paux, avalia_tempo_maximo1(T,P1,R).

calculaPenMotorista((_,LT),P):- calculaTempoMotorista(LT,T), horas8(X), ((T>X,!,Aux is T-X, 	(C), P is Aux *C); P is 0). %avalia a penalizacao do tempo total de trabalho ser 8h.

calculaTempoMotorista([],0).
calculaTempoMotorista([t(B,E,_)|T],P):-calculaTempoMotorista(T,P1),Aux is E-B, P is P1 + Aux. %calcula tempo q um motorista trabalhou

avalia_descanso(L,P):-avalia_descanso1(L,0,P).   %devolve em P as penalizações associadas ao descanso dos motoristas
avalia_descanso1([],P,P).
avalia_descanso1([(S,E,I)|T],P,R):- auxiliar_descanso(S,E,I,T,A), P1 is P + A,avalia_descanso1(T,P1,R).

auxiliar_descanso(S,E,I,L,P):-Aux is E-S,horas4(X),((Aux>X,!, proxima(I,L,E,P)); P is 0). %metodo auxiliar do calculo da penalizacao por tempo de descanso

primeira_ocr(_,[],(-1,_,_)):-!.    %encontra a primeira ocorrencia de um motorista numa lista de tripletos
primeira_ocr(Id,[(S,E,Id)|_],(S,E,Id)):-!.
primeira_ocr(Id,[_|T],G):-primeira_ocr(Id,T,G).


proxima(_,[],_,0):-!.
proxima(Id,L,E,P):-primeira_ocr(Id,L,(S,_,_)),
((S \== -1, !,Aux is S-E, hora(X), veryHardConst(HC), ((Aux<X,! , Aux1 is X-Aux, P is Aux1 * HC ); P is 0)); P is 0). %calcula a penalizacao caso o motorista nao cumpra a hora de descanso.

encontra_tempos([],[],_,_).   %encontra tripletos que estejam a ser executados entre um dado intrevalo
encontra_tempos([(Id,L)|T],[(Id,R)|T1],I,F):-encontra_tempos(T,T1,I,F), constroi_tempos(L,I,F,R).

constroi_tempos(L,I,F,R):-constroi_tempos1(L,I,F,[],R).  %constroi tripletos que estejam a ser executados entre um dado intrevalo
constroi_tempos1([],_,_,R,R1):- reverse(R,R1).  
constroi_tempos1([t(S,E,Id)|T],I,F,X,R):-((verifica(S,E,I,F),!,constroi_tempos1(T,I,F,[t(S,E,Id)|X],R));constroi_tempos1(T,I,F,X,R)).

verifica(S,E,I,F):-(bet(I,F,S),!;bet(I,F,E),!;(S<I,E>F)). %verifica se executou viagens num dado intrevalo

avalia_almoco(L,P):-almoco(I,F), encontra_tempos(L,R1,I,F), avalia_comer(I,F,R1,P).  %devolve a penalizacao total do individuo referente à hora de almoco

avalia_jantar(L,P):-jantar(I,F), encontra_tempos(L,R1,I,F), avalia_comer(I,F,R1,P).  %devolve a penalizacao total do individuo referente à hora de jantar

avalia_comer(_,_,[],0). 
avalia_comer(I,F,[(_,L)|T],P):-avalia_comer(I,F,T,P1), length(L,Laux), 
((Laux==0,!, P is 0); (nth0(0,L,X), verifica_todo_comer(I,F,X,P9), ((P9==0, !, avalia_comer_mot(I,F,L,Paux,1), P is P1 + Paux); P is P1 + P9))).

verifica_todo_comer(I1,F1,t(I,F,_),P):- hardConst(Pen), ((I<I1,F>F1,!,P is 3600*Pen); P is 0).  %verifica o motorista trabalha durante toda a hora de almoco

avalia_comer_mot(_,_,_,P,0):-!,P is 0. 						%Devolve a penalizacao de cada motorista em relacao às horas de almoco/jantar  
avalia_comer_mot(I,F,[],P,_):-!,Aux is F-I, hora(H), ((Aux>H, !, avalia_comer_mot(1,1,1,P,0)) ; hardConst(C), P is 36000*C).
avalia_comer_mot(I,F,[t(S,E,_)|T],P,A):-((S<I, !,avalia_comer_mot(E,F,T,P,A)); (Aux is S-I, hora(H), ((Aux>H,!, avalia_comer_mot(1,1,1,P,0)); 
avalia_comer_mot(E,F,T,P,A)))). 


avalia_populacao([],[]).  %avalia toda a populacao
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia_preferencia([],_,0). %avalia todos os motoritas em relacao às suas preferencias de horario
avalia_preferencia([(Id,I,F)|T],Mot,P):-avalia_preferencia(T,Mot,P1),nth0(X,Mot,(Id,_)),!, nth0(X,Mot,(_,L)),!,softConst(C),avalia_pref_aux((Id,I,F),L,Paux,C), P is P1 + Paux.

avalia_obrigacao([],_,0). %avalia todos os motoritas em relacao às suas preferencias de horario
avalia_obrigacao([(Id,L)|T],Obrigacoes,P):-avalia_obrigacao(T,Obrigacoes,P1), findall((Id,I,F), nth0(_,Obrigacoes,(Id,I,F)),R),!, avalia_obrigacao_motorista(L,R,P2), P is P1+ P2. 

avalia_obrigacao_motorista(_,[],0).
avalia_obrigacao_motorista(L,[H|T],P):-avalia_obrigacao_motorista(L,T,P1) ,avalia_pref_aux(H,L,PA,1),!, P is P1 + PA.



avalia_pref_aux((_,_,_),[],0,_):-!. %avalia um motorista em relacao à preferencia de horario
avalia_pref_aux((Id,I,F),[t(I1,F1,Id)|T],P,C):-avalia_pref_aux((Id,I,F),T,P1,C),
((((I1<I,F1<I);(I1>F,F1>F)),!, Paux is (F1-I1)*C, P is P1 + Paux); 
((I1<I),!, Paux is (I-I1)*C, (((F1>F),!, Paux1 is (F1-F)*C); Paux1 is 0), P is P1 + Paux + Paux1); ((((F1>F),!,Paux is (F1-F)*C); Paux is 0), P is P1 + Paux)).

avalia(Seq,V):-service(VS),
	vehicleduty(VS,ListaBlocks),
	constroiAvaliacao(ListaBlocks,R,Seq), %constroi tripletos do tipo (tempoInicio,tempoFim,Motorista) para um dado gene.
        pack(Seq,T1),                          %cria uma lista de listas dos elementos consecutivos.
        constroiSuperAvaliacao(T1,R1,R), %Transforma tripletos consecutivos referentes a um motorista num so tripleto.
        avalia_tempo_continuo(R1,V1),  %avalia a penalizacao do tempo continuo m�ximo de trabalho ser 4h.
        sort(Seq,Motoristas),					%cria lista de motoristas sem repetidos
        constroiListaTempoMaximo(Motoristas,L),  %constroi a lista de tempos de cada condutor ([(id,[tripletos])])
		avalia_tempo_maximo(L,V2), %avalia a penalizaicao do tempo total de trabalho ser 8h.
        avalia_descanso(R1,V3),    %avalia se o condutor descansa pelo menos 1h depois de realizar pelo menos 4h seguidas
		avalia_almoco(L,V4),       %avalia se o condutor tem pelo menos 1h de pausa para almoço
		avalia_jantar(L,V5), 	   %avalia se o condutor tem pelo menos 1h de pausa para jantar
		obrigacao_horario(LM1),		   %lista de motoristas com obrigacoes de horario 
		avalia_obrigacao(L,LM1,V7),   %avalia se as obrigacoes de horarios dos condutores sao respetidas
		V is V1+V2+V3+V4+V5+V7.

%ORDENA POR AVALIACAO

ordena_populacao(PopAv,PopAvOrd):-
bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).

btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).

take(Src,N,L) :- findall(E, (nth1(I,Src,E), I =< N), L). % coloca em L os N primeiros elementos de Src

remove_list([], _, []). %remove os elementos repetidos de duas listas
remove_list([X|Tail], L2, Result):- member(X, L2), !, remove_list(Tail, L2, Result).
remove_list([X|Tail], L2, [X|Result]):- remove_list(Tail, L2, Result).

multiplicaAvaliacoes(L,R1,R):-multiplicaAvaliacoes1(L,[],[],R1,R).
multiplicaAvaliacoes1([],R1,R,X,X1):-reverse(R,X), reverse(R1,X1).
multiplicaAvaliacoes1([F*N|T],R1,R,X,X1):-random(0,1.0,Pc), A is N*Pc, multiplicaAvaliacoes1(T,[F*Pc|R1],[F*A|R],X,X1).


multiplicaInverso(L,R,R1):-multiplicaInverso1(L,R,R1,[]).
multiplicaInverso1([],_,R1,R2):-!,reverse(R2,R1),!.
multiplicaInverso1([A*N|T], L, R1, R2):-nth0(X,L,A*_),nth0(X,L,_*Aux),F is N * 1/Aux, round(F,F1),!, multiplicaInverso1(T,L,R1,[A*F1|R2]).


%GERAR GERACAO
:- discontiguous gera_geracao/6.

gera_geracao(G,G,Pop,_,_,_):-!,
	%write('Geracao '), write(G), write(':'), nl, write(Pop), nl,!,
	nth0(0,Pop,R),retractall(resposta(_)),service(A), asserta(melhor_escalonamento(A,R)).

gera_geracao(_,_,Pop,_,1,_):-!,
	%write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	nth0(0,Pop,R), retractall(resposta(_)), service(A), asserta(melhor_escalonamento(A,R)).

nao_elitista(Pop,NPopAv,NPopOrd):-
        append(Pop,NPopAv,NPopM),		  % junta a popul��o aos seus descendentes
		sort(NPopM, Aux),				  %remove repetidos
        ordena_populacao(Aux,T),                  %ordena por avaliacao
		length(Pop,Len),					%tamanho da populacao
		round(Len*0.3,Tamanho),				%30% do tamanho da lista total
		take(T,Tamanho,P),					%pega os primeiros Tamanho elementos da list T colocando-os em P
		remove_list(T,P,A),				%remove os primeiros Tamanho elementos da lista
		multiplicaAvaliacoes(A,L1,R1),         %multiplica as avaliacoes dos elementos pelo numero aleatorio
		ordena_populacao(L1,AR1), 			%ordena pela avaliacao fake
		multiplicaInverso(AR1,R1,R2),			%volta a colocar as avaliações corretas
		L2 is Len - Tamanho,					%descobre quantos elementos faltam para completar a populacao
        take(R2,L2,PopulacaoFinal),				%pega nos L2 melhores elementos ordenados pela avaliacao fake
        append(P,PopulacaoFinal,PopulacaoFinal01), %junta os "Tamanho" melhores da junção dos progenitores com os filhos com os melhores resultantes do fake
		ordena_populacao(PopulacaoFinal01,NPopOrd).   %ordena a solucao final

termino_tempo(Ti,F):-get_time(C), Aux is C - Ti,maxTime(M), ((Aux>M, !, F is 1); F is -1).

termino_avaliacao(A,F):-avaliacao_termino(AT), ((A=<AT, !, F is 1); F is -1).


gera_geracao(N,G,Pop,Ti,_,Conta):-
	((N==0, !,get_time(T1));T1 is Ti), % guarda o instante do começo das geracoes 
	%write('Geracao '), write(N), write(':'), nl, write(Pop), nl, nl, nl,nl,
	random_permutation(Pop,Pop1), %Evitar que a sequencia de cruzamento seja sempre entre elementos em sequencias ja definidas 2 a 2
	cruzamento(Pop1,NPop1), %cruzamento da populacao 
	mutacao(NPop1,NPop),    %mutacao da populacao
	avalia_populacao(NPop,NPopAv), %avaliacao da populacao
    nao_elitista(Pop,NPopAv,NPopOrd), %obriga ao algoritmo a não ser elitista
	ordena_populacao(NPopOrd,NPopOrd1), %ordena a populacao gerada
	((Pop==NPopOrd1,!, Conta1 is Conta + 1); Conta1 is 0),
	nth0(0,NPopOrd1,_*A),
	termino_tempo(T1,F),              %verifica se o tempo excedeu ao parameterizado como total de duracao do algoritmo
	termino_avaliacao(A,F1),	
	termino_estabiliza(X),       
	(((F==1,!;F1==1,!;Conta1==X),!, E1 is 1); E1 is 0),    %verifica se o termino por tempo tem que ser cumprido
	N1 is N+1,
	gera_geracao(N1,G,NPopOrd1,T1,E1,Conta1).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tamanho(N),

	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tamanho(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tamanho(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,F):-
	sublista(Ind1,P1,P2,Sub1),
	tamanho(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
        gera_filho(Sub1,Ind21,P1,P2,F).

gera_filho(Sub1,Ind21,P1,P2,F1):-tamanho(T),
constroi(Sub1,Ind21,T,P2,F), Aux is P1-1,constroi(F,Ind21,Aux,0,F1).

constroi(Sub1,_,T,T,Sub1):-!.
constroi(L,[H|T],LM,P2,Sub1):-ocorrencias(H,N), elementOcLista(H,L,Oc),
((Oc<N,!, replace(P2,L,H,L1), P3 is P2 + 1, constroi(L1,T,LM,P3,Sub1)); constroi(L,T,LM,P2,Sub1)).

elementOcLista(_,[],0):-!.%conta as ocorrencias de um elemento numa lista
elementOcLista(H,[H|T],N):-!,elementOcLista(H,T,N1),N is N1+1.
elementOcLista(H,[_|T],N):-elementOcLista(H,T,N1), N is N1.

ocorrencias(H,N):- service(S), lista_motoristas_nworkblocks(S,LMotorista),!,member((H,N), LMotorista),!. %coloca em N o numero de ocorrencias de um motorista H no servi�o de veiculo em estudo.

replace(I,L,E,K):-
  nth0(I, L, _, R),
  nth0(I, K, E, R).


	%elimina(Ind21,Sub1,Sub2),
	%P3 is P2 + 1,
	%insere(Sub2,Sub1,P3,NInd1),
	%eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,

	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).


