%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------- TRATAMENTO DE MOTORISTAS SOBREPOSTOS--------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:-retractall(tentativas_sobreposicao(_)).
:-asserta(tentativas_sobreposicao(10)). % número de tentaivas que o sistema tenta tratar dos motoristas sobrepostos

corrige_motoristas_sobrepostos_exaustivamente():-tentativas_sobreposicao(N),corrige_motoristas_sobrepostos_exaustivamente(N).
corrige_motoristas_sobrepostos_exaustivamente(0):-!.
corrige_motoristas_sobrepostos_exaustivamente(N):-corrige_motoristas_sobrepostos(),findall(A,faltou_afetar_sobreposicao(A),R),
 ((R=0,!, corrige_motoristas_sobrepostos_exaustivamente(0)); N1 is N-1, corrige_motoristas_sobrepostos_exaustivamente(N1)).  


corrige_motoristas_sobrepostos():-cria_e_ordena_duplos_motorista(R),corrige_motoristas_sobrepostos1(R),corrige_fora_de_horas(),cria_lista_wb_motorista. %corrige falhas de motoristas com workblocks simulataneos 

cria_e_ordena_duplos_motorista(R2):-cria_duplos_horario_motorista(R),ordena_horario_motorista(R,R1),sortLista_wb_Motoristas(R1,R2).

cria_duplos_horario_motorista(R):-findall((Id,Lista), (lista_wb_motorista(Id,Lista), length(Lista,Aux), Aux>0) ,R).

corrige_motoristas_sobrepostos1([]):-!. %corrige falhas de motoristas com workblocks simulataneos 
corrige_motoristas_sobrepostos1([(Id,L)|T]):- corrige_sobreposicoes_mesmo_horario(Id,L),corrige_motoristas_sobrepostos1(T).

corrige_sobreposicoes_mesmo_horario(_,[]):-!. %itera cada motorista    
corrige_sobreposicoes_mesmo_horario(Id,[H|T]):- length(T,Aux), ((Aux>0,!, procura_sobreposicoes(H,T,R1), delMember(0,R1,R2), flatten(R2,R3), length(R3,Len), 
((Len>0,!,cria_consecutivos(R3,R4),corrige(R4));  corrige_sobreposicoes_mesmo_horario(Id,T))); corrige_sobreposicoes_mesmo_horario(Id,T)).
%corrige(Id,R4),
procura_sobreposicoes(_,[],[]):-!. %procura sobreposições
procura_sobreposicoes((Vei,I,F),[(Vei1,I1,F1)|T],[R|T1]):-((verifica(I1,F1,I,F),!, clone([(Vei1,I1,F1)],R), procura_sobreposicoes((Vei,I,F),T,T1) ) ; R is 0 ,procura_sobreposicoes((Vei,I,F),T,T1)).

corrige(R1):-corrige1(R1,0). %corrige sobreposições
corrige1([],_):-!.
corrige1([H|T],C):- C1 is C + 1, motoristas_restantes(P), tenta_corrigir(P,H),corrige1(T,C1).

tenta_corrigir(P,H):-procura_tuplo(P,H,-1,Res), ((Res=0,!, asserta(faltou_afetar_sobreposicao(H))); true).

procura_tuplo([],_,0,R):-!,R is 0. %procura se algum motorista pode substituir 
procura_tuplo(_,_,1,R):-!, R is 1.
procura_tuplo([tu(I1,F2,Dur,Id)|T],H,_,R):-length(H,Len), nth1(1,H,(Vei,Iworkblock,_)),!,nth1(Len,H,(Vei,_,Fworkblock)),!,
(((Iworkblock>=I1,Fworkblock=<F2),!, nova_afetacao(Vei,H,tu(I1,F2,Dur,Id)) ,!, procura_tuplo(T,H,1,R),!);  procura_tuplo(T,H,0,R),!).


nova_afetacao(Vei,H,tu(I1,F2,Dur,Id)):-encontra_workblocks_a_substituir(Vei,H,R), flatten(R,R1), substitui_motorista(Id,Vei,H,R1), limpaListaMotorista, separa_wb_posicao(R1,Wbs),
remove_tuplo(tu(I1,F2,Dur,Id)), length(Wbs,Len), ((Len=1,!, nth0(0,Wbs,XI),!, workblock(XI,_,I,F,_,_), cria_tuplo(I,I1,F,F2,Id),!) ;cria_tuplos(Wbs,I1,F2,Id)). %cria afetações quando é possível substituir por motoristas com horário de trabalho nos workblocks a corrigir

cria_tuplos([_],I1,F2,Id):-!, Dur is F2-I1, atualiza_motoristas_restantes(tu(I1,F2,Dur,Id)). %cria os novos tuplos
cria_tuplos([H|T],I1,F2,Id):-workblock(H,_,I,F,_,_), Dur is I-I1,(Dur>0,!, atualiza_motoristas_restantes(tu(I1,I,Dur,Id))), cria_tuplos(T,F,F2,Id). 

cria_tuplo(I,I1,F,F2,Id):-DurI is I-I1, DurF is F2-F,  (DurI>0,!, atualiza_motoristas_restantes(tu(I1,I,DurI,Id)),!), (DurF>0,!, atualiza_motoristas_restantes(tu(F,F2,DurF,Id)),!). %cria novo tuplo

remove_tuplo(tu(I1,F2,Dur,Id)):-motoristas_restantes(P), nth1(Pos,P,tu(I1,F2,Dur,Id)),!, away(P,Pos,P1), ordena_tuplos(P1,R), retractall(motoristas_restantes(_)), asserta(motoristas_restantes(R)). %remove tuplo do predicado motoristas restantes

remove_faltou(H):-retract(faltou_afetar_sobreposicao(H)),!.

atualiza_motoristas_restantes(tu(I1,NovoFI,IAux,Id)):-motoristas_restantes(P), append([tu(I1,NovoFI,IAux,Id)],P,R), retract(motoristas_restantes(P)), ordena_tuplos(R,R1),asserta(motoristas_restantes(R1)). %atualiza o predicado motoristas restantes

substitui_motorista(_,_,[],[]):-!. %substitui motorista no melhor escalonamento, repercutindo as mudanças na lista motorista
substitui_motorista(Id,Vei,[(_,_,_)|T],[(Pos,_)|T1]):-
melhor_escalonamento(Vei,Escalonamento*_), nth0(Pos,Escalonamento,MotoristaAntigo),!,asserta(proposta_sobreposicao(Vei,MotoristaAntigo,Id)), replace(Pos,Escalonamento,Id,NovoEscalonamento),substitui_base_conhecimento(Vei,NovoEscalonamento,MotoristaAntigo,Id),substitui_motorista(Id,Vei,T,T1).

substitui_base_conhecimento(Vei,NovoEscalonamento,MotoristaAntigo,Id):-replace_melhor_escalonamento(Vei,NovoEscalonamento), atualiza_lista_motorista(Vei,MotoristaAntigo,-1),  atualiza_lista_motorista(Vei,Id,1).

replace_melhor_escalonamento(I,Novo):-retractall(melhor_escalonamento(I,_)),!,retractall(service(_)), asserta(service(I)),avalia(Novo,Pen), asserta(melhor_escalonamento(I,Novo*Pen)).

encontra_workblocks_a_substituir(_,[],[]):-!. %encontra workblocks pelo vehicleduty,Inicio e fim
encontra_workblocks_a_substituir(Id,[(_,I,F)|T],[H1|T1]):-
findall((P,W), (workblock(W,_,I,F,_,_), vehicleduty(Id,L), member(W,L),!, nth0(P,L,W),!),H1), encontra_workblocks_a_substituir(Id,T,T1). 

corrige_fora_de_horas():-pega_motoristas_livres(M), findall(R,faltou_afetar_sobreposicao(R),P),corrige_fora_de_horas1(P,M).

corrige_fora_de_horas1([],_).
corrige_fora_de_horas1([H|T],[tu(I,F,Dur,Id)|T1]):-
getIdByH(H,Vei),encontra_workblocks_a_substituir(Vei,H,R), flatten(R,R1), substitui_motorista(Id,Vei,H,R1), remove_tuplo(tu(I,F,Dur,Id)), remove_faltou(H), corrige_fora_de_horas1(T,T1).

pega_falta_sobreposicao(P):-findall(R,faltou_afetar_sobreposicao(R),P).

pega_motoristas_livres(M):-findall(tu(I,Fim,D,A), (motoristas_restantes(P), lista_wb_motorista(A,F), length(F,Aux), Aux=0, nth0(_,P,(tu(I,Fim,D,A)))) ,M). %descobre quais os motoristas que estão livres 
getIdByH([(Id,_,_)|_],Id). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------TRATAMENTO DE CONSTRAINT 4 HORAS--------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-retractall(tentativas_4_horas(_)).
:-asserta(tentativas_4_horas(10)). % número de tentaivas que o sistema tenta tratar de problemas com + de 4 h consecutivas
:-asserta(flag_4_horas(1)).

procura_falha_4_horas():-procura_falha_4_horas(10).
procura_falha_4_horas(0):-!.
procura_falha_4_horas(N):-N1 is N-1, cria_duplos_horario_motorista(R) ,procura_falha_4_horas_motorista(R), flag_4_horas(F), ((F=1,!,procura_falha_4_horas(0), retractall(faltou_corrigir_4_horas(_,_,_)));procura_falha_4_horas(N1)).

procura_falha_4_horas_motorista([]).
procura_falha_4_horas_motorista([(_,L)|T]):-flatten(L,[H1|T1]),length(T1,Len), ((Len>1,!,corrige_consecutivos_temporais(H1,T1,[[H1]],L1),verifica_falha_4_horas(L1,Falhas),length(Falhas,Len1),
((Len1>0,!,flatten(Falhas,F1),corrige_4_horas(_,F1));true)); true) , procura_falha_4_horas_motorista(T).

%cria listas de tripletos consecutivos
corrige_consecutivos_temporais((Id,_,F),[(Id,F,F1)],R,Laux1):-!,length(R,Len), nth1(Len,R,Lista),!, append(Lista,[(Id,F,F1)],Lista1), substituiAux(R,Lista1,Len,Laux1),!.
corrige_consecutivos_temporais((_,_,_),[(Id1,I1,F1)],R,Laux1):-!,length(R,Len), Len1 is Len+1,clone([(Id1,I1,F1)],NovaLista),inserePos(NovaLista,Len1,R,Laux1),!.
corrige_consecutivos_temporais((Id,_,F),[(Id,F,F1)|T],R,R1):-!,length(R,Len), nth1(Len,R,Lista),!,append(Lista,[(Id,F,F1)],Lista1),substituiAux(R,Lista1,Len,Laux1), corrige_consecutivos_temporais((Id,F,F1),T,Laux1,R1).
corrige_consecutivos_temporais((_,_,_),[(Id1,I1,F1)|T],R,R1):-!,length(R,Len),Len1 is Len+1,clone([(Id1,I1,F1)],NovaLista), inserePos(NovaLista,Len1,R,Laux1),corrige_consecutivos_temporais((Id1,I1,F1),T,Laux1,R1),!.

verifica_falha_4_horas(L,R):-!,verifica_falha_4_horas1(L,R1), delMember(0,R1,R). %descobre falhas de um motorista trabalhar mais de 4h seguidas
verifica_falha_4_horas1([],[]):-!.
verifica_falha_4_horas1([H|T],[Falha|T1]):-nth1(1,H,_),!, length(H,Len), nth1(1,H,(_,I,_)),!, nth1(Len,H,(_,_,F)),!, Dur is F-I ,horas4(Horas4),
((Dur>Horas4,!, clone(H,Falha)); Falha is 0), verifica_falha_4_horas1(T,T1).

corrige_4_horas(Id,Falhas):-nth0(0,Falhas,(Vei,_,_)), encontra_workblocks_a_substituir(Vei,Falhas,L),flatten(Falhas,F1),corrige_4_horas1(Vei,Id,F1,L). %corrige falhas das 4h

 corrige_4_horas1(Vei,IdMotorista,F1,L):-flatten(L,L1),vehicleduty(Vei,DutyList), length(DutyList,DutyLen),length(L1,Len),((DutyLen=Len,!,corrige(F1)); nth1(1,L1,(Pos,_)),!,((Pos=0,!, nth1(Len,L1,(Pos0,_)),!, PosAux is Pos0+ 1, troca_motorista_mesmo_wb(Vei,IdMotorista,Pos0,PosAux)); PosAux is Pos-1,troca_motorista_mesmo_wb(Vei,IdMotorista,Pos,PosAux))).  %corrige falhas das 4h

troca_motorista_mesmo_wb(Vei,IdMotorista,Pos,PosAux):-melhor_escalonamento(Vei,Escalonamento*_),nth0(PosAux,Escalonamento,IdMotorista1),!,Pos1 is Pos + 1, PosAux1 is PosAux +1, substituiAux(Escalonamento,IdMotorista1,Pos1,E1), substituiAux(E1,IdMotorista,PosAux1,E2), verifica_corrigiu(Vei,E2,Escalonamento,IdMotorista,IdMotorista1). %troca o motoristas na posicao Pos pelo na PosAux num dado vehicleduty duty

verifica_corrigiu(Vei,E2,Escalonamento,IdMotorista,MotoristaAntigo):-retract(service(_)), asserta(service(Vei)), vehicleduty(Vei,V),constroiAvaliacao(V,R,E2), pack(E2,T1),constroiSuperAvaliacao(T1,R1,R), avalia_tempo_continuo(R1,P), asserta(proposta_mudanca_4_horas(Vei,Escalonamento,E2)),substitui_base_conhecimento(Vei,E2,MotoristaAntigo,IdMotorista), cria_lista_wb_motorista, retractall(flag_4_horas(_)),
((P>0,!,asserta(faltou_corrigir_4_horas(Vei,Escalonamento,E2)), asserta(flag_4_horas(0)));asserta(flag_4_horas(1))). %verifica se foi possivel corrigir totalmente a falha e persiste as mudanças nos predicados necessários

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------TRATAMENTO DE CONSTRAINT HORA ALMOCO/JANTAR------------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%SETUP
:-retractall(tentativas_almoco_jantar(_)).
:-asserta(tentativas_almoco_jantar(10)). % número de tentaivas que o sistema tenta tratar dos motoristas sobrepostos
:-asserta(flag_almoco_jantar(1)).

%ALMOCO
procura_falha_hora_almoco():-tentativas_almoco_jantar(N),procura_falha_hora_almoco(N). %metodo automatico que tenta tratar de falhas de almoco
procura_falha_hora_almoco(0):-!.
procura_falha_hora_almoco(N):-N1 is N-1, cria_e_ordena_duplos_motorista(R), procura_falha_hora_almoco_motorista(R),flag_almoco_jantar(F), ((F=1,!,procura_falha_hora_almoco(0), retractall(faltou_corrigir_almoco_jantar(_,_,_)));procura_falha_hora_almoco(N1)).

procura_falha_hora_almoco_motorista([]). %procura falhas de almoco para cada motorista 
procura_falha_hora_almoco_motorista([(Id,L)|T]):-almoco(I,F),descobre_blocos_num_periodo(I,F,L,P),flatten(P,P1),delMember(0,P1,P2), length(P2,Len), 
((Len>0,!,verifica_descanso(I,F,P2,Ver,1), ((Ver=1,!,corrige_almoco_jantar(Id,P2)); true));true),
procura_falha_hora_almoco_motorista(T).

%JANTAR
procura_falha_hora_jantar():-tentativas_almoco_jantar(N),procura_falha_hora_jantar(N). %metodo automatico que tenta tratar de falhas de jantar
procura_falha_hora_jantar(0):-!.
procura_falha_hora_jantar(N):-N1 is N-1, cria_duplos_horario_motorista(R), procura_falha_hora_jantar_motorista(R),flag_almoco_jantar(F), ((F=1,!,procura_falha_hora_jantar(0), retractall(faltou_corrigir_almoco_jantar(_,_,_)));procura_falha_hora_jantar(N1)).


procura_falha_hora_jantar_motorista([]). %procura falhas de jantar para cada motorista 
procura_falha_hora_jantar_motorista([(Id,L)|T]):-jantar(I,F),descobre_blocos_num_periodo(I,F,L,P),flatten(P,P1),delMember(0,P1,P2), length(P2,Len), 
((Len>0,!,verifica_descanso(I,F,P2,Ver,1), ((Ver=1,!,corrige_almoco_jantar(Id,P2)); true));true),
procura_falha_hora_jantar_motorista(T).
 
%CODIGO COMUM

descobre_blocos_num_periodo(I,F,L,P):-verifica_hora_comer(L,P,I,F).  %devolve a penalizacao total do individuo referente à hora de almoco

verifica_hora_comer([],[],_,_):-!.   %encontra tripletos que estejam a ser executados entre um dado intrevalo
verifica_hora_comer([(Id,S,E)|T],[R|T1],I,F):-((verifica(S,E,I,F),!, clone([(Id,S,E)],R)); R is 0 ), verifica_hora_comer(T,T1,I,F).

verifica_descanso(_,_,_,P,0):-!,P is 0. 						%verifica se houve descanso ou nao 
verifica_descanso(I,F,[],P,_):-!,Aux is F-I, hora(H), ((Aux>H, !, verifica_descanso(1,1,1,P,0)) ;  P is 1).
verifica_descanso(I,F,[(_,S,E)|T],P,A):-((S<I, !,verifica_descanso(E,F,T,P,A)); (Aux is S-I, hora(H), ((Aux>H,!, verifica_descanso(1,1,1,P,0)); 
verifica_descanso(E,F,T,P,A)))). 

corrige_almoco_jantar(Id,Falhas):-nth0(0,Falhas,(Vei,_,_)), encontra_workblocks_a_substituir(Vei,Falhas,L),flatten(Falhas,F1),corrige_almoco_jantar1(Vei,Id,F1,L). %corrige falha de almoco e jantar

%C
 corrige_almoco_jantar1(Vei,IdMotorista,F1,L):-flatten(L,L1),vehicleduty(Vei,DutyList), length(DutyList,DutyLen),length(L1,Len),((DutyLen=Len,!,corrige(F1)); nth1(1,L1,(Pos,_)),!,((Pos=0,!, nth1(Len,L1,(Pos0,_)),!, PosAux is Pos0+ 1, corrige_almoco_jantar2(Vei,IdMotorista,Pos0,PosAux)); PosAux is Pos-1,corrige_almoco_jantar2(Vei,IdMotorista,Pos,PosAux))).  %corrige falhas de almoco ou jantar

corrige_almoco_jantar2(Vei,IdMotorista,Pos,PosAux):-melhor_escalonamento(Vei,Escalonamento*_),nth0(PosAux,Escalonamento,IdMotorista1),!,Pos1 is Pos + 1, PosAux1 is PosAux +1, substituiAux(Escalonamento,IdMotorista1,Pos1,E1), substituiAux(E1,IdMotorista,PosAux1,E2),verifica_corrigiu_almoco_jantar(Vei,E2,Escalonamento,IdMotorista,IdMotorista1). %troca o motoristas na posicao Pos pelo na PosAux num dado vehicleduty duty

verifica_corrigiu_almoco_jantar(Vei,E2,Escalonamento,IdMotorista,MotoristaAntigo):-retract(service(_)), asserta(service(Vei)), vehicleduty(Vei,V),constroiAvaliacao(V,_,E2),sort(E2,Motoristas),constroiListaTempoMaximo(Motoristas,L), avalia_almoco(L,P), asserta(proposta_mudanca_almoco_jantar(Vei,Escalonamento,E2)),substitui_base_conhecimento(Vei,E2,MotoristaAntigo,IdMotorista), cria_lista_wb_motorista, retractall(flag_almoco_jantar(_)),
((P>0,!,asserta(faltou_corrigir_almoco_jantar(Vei,Escalonamento,E2)),asserta(flag_almoco_jantar(0)));retractall(faltou_corrigir_almoco_jantar(_,_,_)),asserta(flag_almoco_jantar(1))). %verifica se conseguiu corrigir totalmente o almoco / jantar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------TRATAMENTO DE CONSTRAINT 8 HORAS-------------------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%SETUP
 :-retractall(tentativas_8_horas(_)).
 :-asserta(tentativas_8_horas(10)). % número de tentaivas que o sistema tenta tratar dos motoristas sobrepostos
 :-asserta(flag_8_horas(1)).

 %ALMOCO
 procura_falha_8_horas():-tentativas_8_horas(N),procura_falha_8_horas(N). %metodo automatico que tenta tratar de falhas de 8h
 procura_falha_8_horas(0):-!.
 procura_falha_8_horas(N):-N1 is N-1, cria_e_ordena_duplos_motorista(R), procura_falha_8_horas_motorista(R),flag_8_horas(F), ((F=1,!,procura_falha_8_horas(0), retractall(faltou_corrigir_8_horas(_)));procura_falha_8_horas(N1)).

 procura_falha_8_horas_motorista([]). %procura falhas de um motorista  trabalhar mais de 8 horas
 procura_falha_8_horas_motorista([(Id,L)|T]):-cria_consecutivos(L,L1), calcula_tempo_trabalho(L1,Total), horas8(Hora), ((Total>Hora, !, corrige_8_horas(Id,L1)); true), procura_falha_8_horas_motorista(T).

calcula_tempo_trabalho(L,P):-calcula_tempo_trabalho1(L,0,P). %calcula quanto tempo é que o motorista trabalhou
calcula_tempo_trabalho1([],R,R):-!.
calcula_tempo_trabalho1([H|T],P,R):-nth0(0,H,(Vei,_,_)),!,encontra_workblocks_a_substituir(Vei,H,Lista),!,flatten(Lista,H1),descobre_wbs(H1,L1),list_sum1(L1,Total),!, P1 is P + Total, calcula_tempo_trabalho1(T,P1,R).

descobre_wbs([],[]).
descobre_wbs([(_,Id)|T],[Id|T1]):-descobre_wbs(T,T1).

corrige_8_horas(Id,L):-flatten(L,[H|T]),corrige_consecutivos_temporais(H,T,[[H]],L1), menorLista(L1,Menor), length(Menor,Len), 
((Len=1,!, nth0(0,Menor,Apaga),!); length(L,Len1), nth1(Len1,L,Apaga),!), pega_motoristas_livres(M),encontra_moto_diferente(Id,M,Elem),
((Elem=0,!, asserta(faltou_corrigir_8_horas(Id)), asserta(flag_8_horas(0)));  substitui_por_motorista_livre(Elem,Apaga)).

encontra_moto_diferente(_,[],0).
encontra_moto_diferente(Id,[tu(_,_,_,Id)|T],T1):-!,encontra_moto_diferente(Id,T,T1).
encontra_moto_diferente(_,[tu(I,F,Dur,Id1)|_],tu(I,F,Dur,Id1)):-!.

substitui_por_motorista_livre(tu(I1,F1,Dur,Id1),(Vei,I,F)):-encontra_workblocks_a_substituir(Vei,[(Vei,I,F)],R),!,
flatten(R,R1), substitui_motorista1(Id1,Vei,R1), remove_tuplo(tu(I1,F1,Dur,Id1)). 

substitui_motorista1(Id,Vei,[(Pos,_)]):-asserta(flag_8_horas(1)), melhor_escalonamento(Vei,Escalonamento*_), nth0(Pos,Escalonamento,MotoristaAntigo),!, asserta(proposta_mudanca_8_horas(MotoristaAntigo,Id,Vei)), replace(Pos,Escalonamento,Id,NovoEscalonamento), substitui_base_conhecimento(Vei,NovoEscalonamento,MotoristaAntigo,Id),cria_lista_wb_motorista.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------TRATAMENTO DE CONSTRAINT DO TEMPO DE DESLOCAÇÃO-----------------------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


 :-retractall(tentativas_deslocacao(_)).
 :-asserta(tentativas_deslocacao(10)). % número de tentaivas que o sistema tenta tratar dos motoristas sobrepostos
 :-asserta(flag_deslocacao(1)).

 %ALMOCO
 procura_falhas_tempo_deslocacao():-tentativas_deslocacao(N),procura_falhas_tempo_deslocacao(N). %metodo automatico que tenta tratar de falhas de 8h
 procura_falhas_tempo_deslocacao(0):-!.
 procura_falhas_tempo_deslocacao(N):-N1 is N-1, cria_e_ordena_duplos_motorista(R), procura_falhas_tempo_deslocacao_motorista(R),flag_deslocacao(F), ((F=1,!,procura_falha_8_horas(0), retractall(faltou_corrigir_deslocamento(_)));procura_falhas_tempo_deslocacao(N1)).
 
procura_falhas_tempo_deslocacao_motorista([]):-!.
procura_falhas_tempo_deslocacao_motorista([(Id,L)|T]):-!,flatten(L,[H1|T1]), length(L,Len), (( Len>1, !,corrige_consecutivos_temporais(H1,T1,[[H1]],L1),!, length(L1,Len1),
((Len1>0,!,lista_para_pares1(L1,L2),processa_tempo_deslocacao(Id,L2));true)); !,true),!, procura_falhas_tempo_deslocacao_motorista(T).

  processa_tempo_deslocacao(_,[]). %encontra falhas de tempo de deslocação
  processa_tempo_deslocacao(Id,[H|T]):-nth0(0,H,Elem1), nth0(1,H,Elem2), length(Elem1,Len), nth1(Len,Elem1,(Id1,I1,F1)), nth1(1,Elem2,(Id2,I2,F2)), 
  encontra_workblocks_a_substituir(Id1,[(Id1,I1,F1)],[[(_,WB1)]]), 
  encontra_workblocks_a_substituir(Id2,[(Id2,I2,F2)],[[(_,WB2)]]),
  workblock(WB1,_,_,_,_,NoF1),
  workblock(WB2,_,_,_,NoI2,_),
  ((NoF1=NoI2,!, true); 
  findBest(F1,NoF1,NoI2,_,W),
((W=86400,!, true); Dur is I2 - F1, 
(((Dur<0,!;Dur<W),!,
 pega_motoristas_livres(M),encontra_moto_diferente(Id,M,Elem),
((Elem=0,!, asserta(faltou_corrigir_deslocamento(Id)), asserta(flag_deslocacao(0))); substitui_por_motorista_livre1(Elem,(Id1,I1,F1)))); true))),
   processa_tempo_deslocacao(Id,T).

   substitui_por_motorista_livre1(tu(I1,F1,Dur,Id1),(Vei,I,F)):-encontra_workblocks_a_substituir(Vei,[(Vei,I,F)],R),!, %substitui motorista pelo da falha
flatten(R,R1), substitui_motorista2(Id1,Vei,R1),!, remove_tuplo(tu(I1,F1,Dur,Id1)). 

substitui_motorista2(Id,Vei,[(Pos,_)]):-asserta(flag_deslocacao(1)),melhor_escalonamento(Vei,Escalonamento*_), nth0(Pos,Escalonamento,MotoristaAntigo),!, asserta(proposta_mudanca_deslocamento(MotoristaAntigo,Id,Vei)), replace(Pos,Escalonamento,Id,NovoEscalonamento), substitui_base_conhecimento(Vei,NovoEscalonamento,MotoristaAntigo,Id),cria_lista_wb_motorista.



  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------AUXILIARES--------------------------------------------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

menorLista([X],X):-!.
menorLista([X|L],M):- menorLista(L,M1),length(X,X1), length(M1,M2),  ((X1<M2,!, clone(X,M)) ; clone(M1,M)).

ordena_horario_motorista([],[]). %ordena os blocos de trabalho de um motorista
ordena_horario_motorista([(Id,L)|T],[(Id,R)|T1]):-ordena_horario_motoristaAux(L,R), ordena_horario_motorista(T,T1).

ordena_horario_motoristaAux(InputList,SortList) :- %auxiliar de ordenação dos blocos de trabalho de um motorista
swapHM(InputList,List) , ! ,
ordena_horario_motoristaAux(List,SortList).
ordena_horario_motoristaAux(SortList,SortList).

swapHM([(Id,I,F),(IdY,IY,FY)|List],[(IdY,IY,FY),(Id,I,F)|List]) :- I > IY.
swapHM([Z|List],[Z|List1]) :- swapHM(List,List1). %auxiliar de ordenação

separa_wb_posicao(R,W):-findall(H, nth0(_,R,(_,H)), W).

bet(N, M, K) :- N < M, K = N,!. %verifica se K está entre N e M exclusive
bet(N, M, K) :- N < M, N1 is N+1, bet(N1, M, K),!.

delMember(_, [], []) :- !. %apaga os elementos X da lista
delMember(X, [X|Xs], Y) :- !, delMember(X, Xs, Y).
delMember(X, [T|Xs], Y) :- !, delMember(X, Xs, Y2), append([T], Y2, Y).

%transforma uma lista de tripletos em listas de tripletos consecutivos
cria_consecutivos([],[]):-!. 
cria_consecutivos(L,R):-findall(Id, nth0(_,L,(Id,_,_)),X), pack(X,X1), cria_consecutivos1(L,X1,R).

cria_consecutivos1(L,X,R):-!,cria_consecutivos1(L,X,R,1).
cria_consecutivos1(_,[],[],_):-!.
cria_consecutivos1(L,[H|T],[R|T1],C):-length(H,A), A1 is C+A, ((A=1, !, nth1(C,L,X), clone([X],R),! ); valores_index(L,C,A1,R)), C1 is C + A, cria_consecutivos1(L,T,T1,C1).

valores_index(L,C,A,R):-valores_index1(L,C,A,R).
valores_index1(_,C,C,[]):-!.
valores_index1(L,C,A,[R|T]):-nth1(C,L,R),!, C1 is C +1,valores_index1(L,C1,A,T).

sortLista_wb_Motoristas(InputList,SortList) :- %auxiliar de ordenação da lista de wb motrositas
swapX1(InputList,List) , ! ,
sortLista_wb_Motoristas(List,SortList).
sortLista_wb_Motoristas(SortList,SortList).

swapX1([(X,L),(Y,L1)|List],[(Y,L1),(X,L)|List]) :- nth0(0,L,(_,S,_)), nth0(0,L1,(_,S1,_)), S > S1.
swapX1([Z|List],[Z|List1]) :- swapX1(List,List1). %auxiliar de ordenação

index([V|_],V,0).
index([_|T],V,s(I)) :- index(T,V,I).

lista_para_pares(L,R):-findall(X,u(L,X),R). % u[a,b,c,d,e,f], X) dá X=[[a,b],[b,c],[c,d]]
u(L, [A,B]) :- %u([a,b,c,d,e,f], X) dá X=[a,b], X=[b,c], X=[c,d]
    append(_, [A,B|_], L).

lista_para_pares1(L,R):-findall(X,v(L,X),R). 
v(L, X) :-
     append([A,B], L1,L),
    (   X = [A,B]; v(L1, X)).