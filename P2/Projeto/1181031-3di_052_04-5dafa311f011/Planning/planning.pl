% COMO CLIENTE
:-json_object no(name:string, shortName:string, isDepot:boolean, isReliefPoint:boolean, latitude:string, longitude:string).
:-json_object linha(name:string, code:string, listNodes:list(string), totalTime:integer, totalDist:integer).

:-dynamic liga/3.
:-dynamic ligaD/4.

geraPlanning():-findall(_,
			((no(_,No1,true,false,_,_);no(_,No1,false,true,_,_)),
			(no(_,No2,true,false,_,_);no(_,No2,false,true,_,_)),
			 No1\==No2,
			 linha(_,N,LNos,_,_),
			 ordem_membros(No1,No2,LNos),
			 assertz(liga(No1,No2,N))
			),_).

pathDist(No1,No2,LNos,Dist):-nth1(Pos1,LNos,No1),nth1(Pos2,LNos,No2), pathDist1(Pos1,Pos2,LNos,Dist),!.

pathDist1(Pos2,Pos2,_,0):-!.
pathDist1(Pos1,Pos2,LNos,Dist1):-nth1(Pos1,LNos,No1), Pos1A is Pos1 + 1,  nth1(Pos1A ,LNos,No2), segment(No1,No2,D,_), pathDist1(Pos1A,Pos2,LNos,Dist), Dist1 is Dist + D.


gera1():-findall(_,
			((no(_,No1,true,false,_,_);no(_,No1,false,true,_,_)),
			(no(_,No2,true,false,_,_);no(_,No2,false,true,_,_)),
			 No1\==No2,
			 linha(_,N,LNos,_,_),
			 ordem_membros(No1,No2,LNos),
             pathDist(No1,No2,LNos,Dist),
			 assertz(ligaD(No1,No2,N,Dist))
			),_).   % Gera ligações com a distância associada.

ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).


getNodes():- retractall(no(_,_,_,_,_,_)),
        http_get('https://opt52.herokuapp.com/api/node', ListNodes, [json_object(dict)]), importNodes(ListNodes).

importNodes([]).
importNodes([H|T]):- importNodes(T), assertz(no(H.nodeName, H.shortName, H.isDepot, H.isReliefPoint, H.latitude, H.longitude)).
      

getLines():- retractall(line(_,_,_,_,_)),
        http_get('https://opt52.herokuapp.com/api/line', ListLines, [json_object(dict)]), importLines(ListLines).

importLines([]).
importLines([H|T]):- importLines(T),findRouteByLineId(H.lineId,H.name).

findRouteByLineId(L,N):-string_concat('https://opt52.herokuapp.com/api/route?lineId=', L, X), http_get(X,ListRoutes, [json_object(dict)]), importRoute(ListRoutes,L,N).

importRoute([],_,_).
importRoute([H|T],L,N):-importRoute(T,L,N), processSegment(H.networkSegments,Lista), processSegmentV2(H.networkSegments,D,R), 
atom_number(H.routeId,X),
assertz(linha(N,X,Lista,D,R)).

%processa os segmentos criando a lista ordenada de passagem pelos nós.
processSegment(L,R):-processSegment1(L,[],0,R).
processSegment1([],Lista,_,Y):-reverse(Lista,Y).
processSegment1([H|T],Lista,C,Y):-(C == 0, (!,C1 is C+1, processSegment1(T,[H.idNo2,H.idNo1|Lista],C1,Y));(processSegment1(T,[H.idNo2|Lista],C,Y))).

%processa os segmentos criando o predicado segment.
processSegmentV2([],0,0).
processSegmentV2([H|T],D,R):-processSegmentV2(T,D1,R1),atom_number(H.distance, X),atom_number(H.deslocationTime, Y),
(not(call(segment(H.idNo1,H.idNo2,X,Y))),!, assertz(segment(H.idNo1,H.idNo2,X,Y)); true),
D is D1 + X, R is R1 + Y.


%segment("SOBRO","PARED",2000,10).
%linha("ALBUQUERQUE",23, ["CETE","AGUIA","BESTR","LORDL", "SOBRO", "PARED"], 31, 15700).

:-getNodes(),getLines(), geraPlanning(),gera1().

               
caminho(Noi,Nof,LCaminho):-caminho(Noi,Nof,[],LCaminho). %gera todos os caminhos

caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).
caminho(No1,Nof,Lusadas,Lfinal):- 
	liga(No1,No2,N),
        \+member([_,_,N],Lusadas),
	\+member([No2,_,_],Lusadas),
	\+member([_,No2,_],Lusadas),
	caminho(No2,Nof,[[No1,No2,N]|Lusadas],Lfinal).

rendicao(Noi):-(no(_,Noi,true,false,_,_);no(_,Noi,false,true,_,_);no(_,Noi,true,true,_,_)),!.

% Gerador de soluções sem findall

:-dynamic melhor_sol_ntrocas/2.

findBest(Horario,Noi,Nof,LShortestTimePath,WastedTime):-
rendicao(Noi),rendicao(Nof),
(bestPath(Horario,Noi,Nof);true),
retract(melhor_sol_ntrocas(LShortestTimePath,WastedTime)).

bestPath(Horario,Noi,Nof):-
asserta(melhor_sol_ntrocas(_,86400)),  %segundos que tem um dia
caminho(Noi,Nof,LCaminho),
timePath(Horario,LCaminho,WastedTime),
newBest(WastedTime,LCaminho),
fail.

newBest(WastedTime,LCaminho):-
melhor_sol_ntrocas(_,N),
WastedTime<N,retract(melhor_sol_ntrocas(_,_)),
asserta(melhor_sol_ntrocas(LCaminho,WastedTime)).

timePath(Horario,H,R):-timePath1(Horario,H,Aux), R is Aux - Horario. %Calcula o tempo total de um percurso desde o nó inicio até ao fim.
timePath1(Horario,[],Horario):-!.
timePath1(Horario,[H|T],Time):-
nth1(3,H,Id), nth1(1,H,No1), nth1(2,H,No2),
getTimePath(Horario,No1,No2,Id,WastedTime),
CurrentTime is Horario + WastedTime,
timePath1(CurrentTime,T,Time).

getTimePath(Horario,No1,No2,Id,R):-linha(_,Id,L,_,_), nth1(Pos1,L,No1), nth1(Pos2,L,No2),processaHorario(Horario,Id,Pos1,Res)
,processPathTime(Horario,Pos1,Pos2,Res,R),!. %devolve o tempo gasto num determinado trecho do caminho. (sem fazer mudança).

processPathTime(Horario,Pos1,Pos2,L,R):- nth1(Pos1,L,Temp1), nth1(Pos2,L,Temp2), WaitingTime is Temp1 - Horario, R is Temp2 - Temp1+ WaitingTime. %processa o tempo que o crewmember demora naquele trecho do caminho. (sem fazer mudança).

processaHorario(Horario,Id,Pos,Res):-horariosPath(Id,R),processaLista(Horario,R,Pos,0,Res).

processaLista(_,[],_,0,_):-!,false.
processaLista(_,[],_,_,_):-!.   % encontra o menor horario de cada path.
processaLista(Horario,[H|T],Pos,Aux,Res):-
nth1(Pos,H,HorarioAux), 
((Aux is 0,HorarioAux>Horario), !, Res = H, processaLista(Horario,T,Pos,1,Res); processaLista(Horario,T,Pos,Aux,Res)).

:-retractall(horariosPath(_,_)).
:-findall(_,(horario(X,_,_), findall(_,(horario(X,_,_), (not(horariosPath(X,_)), !, setof(Z,horario(X,_,Z),Lista), sort(Lista,Lista1),assertz(horariosPath(X,Lista1))
;true)),_)),_). %cria o preficado horario Path do tipo horariosPath(NºPath,[[Horario viagem]])

gerador(Horario,No1,No2,R):-findall(LCaminho,caminho(No1,No2,LCaminho),LLCaminho),  bestTime(Horario,LLCaminho,R).

bestTime(_,[H],H):-!. %descobre o melhor caminho tendo em conta os horarios.
bestTime(Horario,[H|T],Hmenor):-bestTime(Horario,T,L1),timePath(Horario,H,C),timePath(Horario,L1,C1),((C<C1,!,Hmenor=H);Hmenor=L1).

%aStar
%findall(_,(horario(X,_,_), findall(_,(horario(X,_,_), (not(horariosPath(X,_)), !, setof(Z,horario(X,_,Z),Lista), sort(Lista,Lista1),write(Lista1),nl;true)),_)),_).
%Calcula maior velocidade media de todas as linhas
maior_velocidade_media:-
            velocidade_media_geral(List),
            maior(List, Res),
            assertz(velocidade_media(Res)).

velocidade_media_geral(List):-findall(Res,(
    linha(_,_,_,Temp,Dist),
    velocidade_media_linha(Dist, Temp, Res)),List).

velocidade_media_linha(Dist, Temp, Res):- Res is Dist / (Temp * 60).

maior([H|[]],H).
maior([H|T],M):-maior(T,M), M > H,!.
maior([H|_],H).

:-dynamic velocidade_media/1.
:-maior_velocidade_media.

aStar(Orig,Dest,Hora,Cam,Custo):-
    aStar2(Dest,[(_,0,Hora,[Orig])],Cam,Custo).

aStar2(Dest,[(_,Custo,_,[Dest|T])|_],Cam,Custo):- reverse([Dest|T],Cam).

aStar2(Dest,[(_,Ca,HoraAct,Visited)|Outros],Cam,Custo):-
    Visited=[Act|_],
    findall((CEX,CaX,Hora,[X|Visited]),
        (Dest\==Act, liga(Act,X,NLinha), \+ member(X,Visited),
        getNextSchedules(Act,X,NLinha,HoraAct,(CustoV,CustoE)),
        CaX is CustoV + CustoE + Ca, estimativa(X,Dest,EstX),
        CEX is CaX + EstX,
        Hora is HoraAct + CustoV + CustoE),
        Novos),
    append(Outros,Novos,Todos),
    sort(Todos,TodosOrd),
    aStar2(Dest,TodosOrd,Cam,Custo).

%Calcula uma estimativa do tempo entre dois pontos do globo, recorrendo a distancia e tempo medio
estimativa(No1,No2,Estimativa):-
    no(_,No1,_,_,LongA,LatA),
    no(_,No2,_,_,LongB,LatB),
    calcular_distancia(LatA, LongA, LatB, LongB, Dist),
    velocidade_media(Vel),
    Estimativa is Dist / Vel.

estimativaDistancia(No1,No2,Dist):-
    no(_,No1,_,_,LongA,LatA),
    no(_,No2,_,_,LongB,LatB),
    calcular_distancia(LatA, LongA, LatB, LongB, Dist).

%Calcula distancia entre dois pontos no globo
calcular_distancia(LatA, LongA, LatB, LongB, Dist):- 
    T1 is LatA * pi / 180,
    T2 is LatB * pi / 180,
    D1 is (LatB - LatA) * pi / 180,
    D2 is (LongB - LongA) * pi / 180,

    A is sin(D1 / 2) * sin(D1 / 2) + cos(T1) * cos(T2) * sin(D2 / 2) * sin(D2 / 2),
    Dist is 2 * atan2(sqrt(A), sqrt(1 - A)) * 6371000.

%Dados 2 nos, uma linha e uma hora, devolve o tempo de espera e o tempo de viagem
getNextSchedules(NoInicio,NoFim,NLinha,Hora,(CustoViagem,CustoEspera)):-
                                linha(_,NLinha,ListNos,_,_), %Procura a lista de nos da linha
                                nth0(PosInicio,ListNos,NoInicio), %Procura a posicao do no
                                nth0(PosFim,ListNos,NoFim), %Procura a posicao do no
                                horariosPath(NLinha,LHorario), %Procura os horarios da linha
                                getSchedudesAfter(LHorario,PosInicio,Hora,ListProximos),
                                getNearestSchedule(ListProximos,PosInicio,ProximoHorario),
                                calcularCustoViagem(ProximoHorario,PosInicio,PosFim,CustoViagem),
                                calcularCustoEspera(ProximoHorario,PosInicio,Hora,CustoEspera).

%Dados os horarios de uma linha, um no e uma hora, devolve todos os horarios depois dessa hora daquela linha naquele no
getSchedudesAfter([],_,_,[]).
getSchedudesAfter([H|T],Pos,Hora,[H|Res]):-nth0(Pos,H,Tempo),Tempo > Hora,!,getSchedudesAfter(T,Pos,Hora,Res).
getSchedudesAfter([_|T],Pos,Hora,Res):-getSchedudesAfter(T,Pos,Hora,Res).

%Dado um lista de horarios, devolve o que mais proximo
getNearestSchedule(LHorario,Pos,Res):-getNearestSchedule1(LHorario,Pos,_,Res).

getNearestSchedule1([H|[]],Pos,Menor,H):-nth0(Pos,H,Menor),!.
getNearestSchedule1([H|T],Pos,Menor,H):-getNearestSchedule1(T,Pos,Menor,_),nth0(Pos,H,Tempo), Tempo < Menor,!.
getNearestSchedule1([_|T],Pos,Menor,Res):-getNearestSchedule1(T,Pos,Menor,Res).

%Dado um Horario e dois nós, calcula o tempo de viagem entre esses dois nós
calcularCustoViagem(Horario,PosInicio,PosFim,Custo):-
            nth0(PosInicio,Horario,TempoInicio),
            nth0(PosFim,Horario,TempoFim),
            Custo is TempoFim - TempoInicio.

%Dado uma hora, um horario e um no, calcula o tempo de espera
calcularCustoEspera(ProximoHorario,PosInicio,Hora,CustoEspera):-
            nth0(PosInicio,ProximoHorario,Tempo),
            CustoEspera is Tempo - Hora.



% BFS 

bestfs(Horario,Orig,Dest,Cam,Custo):-
bestfs2(Horario,Dest,(0,[Orig]),Cam,Custo).

bestfs2(_,Dest,(Custo,[Dest|T]),Cam,Custo):-!,reverse([Dest|T],Cam). %calculo do melhor caminho através da heuristica de minimização da distância, tendo em conta os horarios.

bestfs2(Horario,Dest,(Ca,LA),Cam,Custo):-
LA=[Act|_],
findall((EstX,CaX,[X|LA]),
(ligaD(Act,X,Line,Distance), haveSchedules(Act,Line,Horario), \+member(X,LA),
estimativaDistancia(X,Dest,EstX),
CaX is Ca+Distance),Novos),
sort(Novos,NovosOrd),
NovosOrd = [(_,CM,Melhor)|_],
bestfs2(Horario,Dest,(CM,Melhor),Cam,Custo).

haveSchedules(Act,Line,Horario):-horariosPath(Line,LH), linha(_,Line,Nodes,_,_), nth1(Pos,Nodes,Act), getSchedudesAfter(LH,Pos,Horario,R),!, length(R,Aux), (Aux is 0, !,fail; true). %verifica se é possível partir de uma dada paragem, numa dada linha a partir de uma certa hora.


%COMO SERVIDOR
				

get_solution(Request):-http_parameters(Request,
                    [horario(Horario, []),
                      no1(No1,[]),
                      no2(No2,[]),
                      tipo(Type,[])
                    ]),	
        atom_string(No1, N1),atom_string(No2, N2), atom_string(Type, T), atom_number(Horario,H),
        (T == "astar", aStar(N1,N2,H,R1,_), prolog_to_json(R1, X1),reply_json(X1),!;
        T == "gerador", findBest(H,N1,N2,R,_), prolog_to_json(R, X),reply_json(X),!;
        T == "bestfirst", bestfs(H,N1,N2,R), prolog_to_json(R, X),reply_json(X)).
		