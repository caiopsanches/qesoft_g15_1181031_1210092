 :-[baseConhecimento].
 :-[setUpMDV]. %importa base de conhecimento do MDV
 :-[horariomotoristas]. %consulta o ficheiro que contém os horarios dos motoristas e gera os tuplos dos seus blocos
 :-[escalonador].
 :-[geneticos]. %algoritmos geneticos
 :-[afetacoes]. %algoritmos afetação
 :-[tratamentoConstraints].
 :-[planning].

% Bibliotecas HTTP
:-use_module(library(http/thread_httpd)).
:-use_module(library(http/http_dispatch)).
:-use_module(library(http/http_parameters)).
:-use_module(library(http/http_client)).
:-use_module(library(http/http_cors)).
:-set_setting(http:cors, [*]).
:-http_handler('/plan/genetic', get_genetic, []).
:-http_handler('/plan/solution', get_solution, []).		
:-http_handler('/plan/affect', get_affect, []).	
% Bibliotecas JSONx
:-use_module(library(http/json_convert)).
:-use_module(library(http/http_json)).
:-use_module(library(http/json)).
:-use_module(library(ssl)).
:-use_module(library(socket)).
:-use_module(library(http/http_open)).
:-use_module(library(crypto)).
:-use_module(library(http/http_ssl_plugin)).
:-dynamic horariosPath/2.
:- set_setting(http:cors, [*]).


:-dynamic some_fact/1.
:-asserta(some_fact(6)).

:- http_handler(/, root_handler, []).
:- http_handler('/db', db, []).

root_handler(_):-
        format('Content-Type: text/html~n~n', []),
        findall(Fact, some_fact(Fact), Facts),
        atomic_list_concat(Facts, ', ', Atom),
        format('Hello from Prolog: ~w', [Atom]).

db(_):-
        heroku_db_connect(Connection),
        format('Content-Type: text/html~n~n', []),        
        format('We have a connection: ~q', [Connection]).

server(Port) :-cors_enable,					
        http_server(http_dispatch, [port(Port)]).
	
inicia():-
write('1. Gera os motoristas no sistema '), nl,
write('2. Afetar motoristas a veiculos escalonando-os através do algoritmo genético'),nl,
write('3. Tratar sobreposições '), nl,
write('4. Tratar 4 horas consecutivas'),nl,
write('5. Tratar 8 horas consecutivas'),nl,
write('6. Tratar hora de descanso almoço'),nl,
write('7. Tratar hora de descanso jantar'),nl,
write('8. Tratar tempo de deslocação'),nl,
read(X), X>0 , X<9,
doit(X).

doit(1):-nl,gera_disponibilidade,cria_obrigacoes, write('Motoristas e os seus horários gerados no sistema'),nl,nl,inicia. %gera tuplos do tipo tu e gera obrigações de horario para os motoristas

doit(2):-nl,afeta_motorista_a_veiculos,escalonador_motoristas,cria_lista_wb_motorista,write('Motoristas afetados a veículos'),ml,nl,inicia. %afeta motoristas a veiculos chamando o algoritmo genetico

doit(3):-nl,((not(corrige_motoristas_sobrepostos_exaustivamente),!,
write('Nao foi possível corrigir todas as sobreposições, por favor corriga manualmente as seguintes:'),nl,
findall(A,motoristas_restantes(A),R),flatten(R,R1),
write(R1),nl); write('Todas as sobreposições foram corrigidas na base de conhecimento.'),nl),nl,nl,inicia.

doit(4):-nl,procura_falha_4_horas,
findall((Id,E1,E2),proposta_mudanca_4_horas(Id,E1,E2),R),!,
length(R,Len),
((Len>0,!,
escreve_mudancas(R)); !, write('Não existe nenhuma falha relacionada'),nl),
findall(Id1,(faltou_corrigir_4_horas(Id1,_,_)),R1),
flatten(R1,R2), sort(R2,R3),
length(R3,Len2),
((Len2>0,!,escreve_problema(R3)); true), nl,nl,inicia.

doit(5):-nl,procura_falha_8_horas,
findall((Id,Id1,Vei),proposta_mudanca_8_horas(Id,Id1,Vei),R),!,
length(R,Len),
((Len>0,!,
escreve_mudancas_motorista(R)); !, write('Não existe nenhuma falha relacionada'),nl),
findall(Id1,(faltou_corrigir_8_horas(Id1)),R1),
flatten(R1,R2), sort(R2,R3),
length(R3,Len2),
((Len2>0,!,write('Necessário corrigir manualmente os seguintes motoristas '), write(R3)); true), nl,inicia.


doit(6):-nl, procura_falha_hora_almoco,
flag_almoco_jantar(F), 
((F=1,!,
findall((Id,E1,E2),proposta_mudanca_almoco_jantar(Id,E1,E2),R),
escreve_mudancas(R)
); 
findall(Id,(faltou_corrigir_almoco_jantar(Id,_,_)),R),
escreve_problema(R)),nl,nl,inicia.

doit(7):-nl, procura_falha_hora_jantar,
flag_almoco_jantar(F), 
((F=1,!,
findall((Id,E1,E2),proposta_mudanca_almoco_jantar(Id,E1,E2),R),
escreve_mudancas(R)
); 
findall(Id,(faltou_corrigir_almoco_jantar(Id,_,_)),R),
escreve_problema(R)),nl,nl,inicia.

doit(8):-nl, procura_falhas_tempo_deslocacao,
flag_deslocacao(F), 
((F=1,!,
findall((Id,Id1,V),proposta_mudanca_deslocamento(Id,Id1,V),R),
escreve_mudancas_motorista(R)
); 
findall(Id,(faltou_corrigir_deslocamento(Id,_,_)),R),
escreve_problema(R)),nl,nl,inicia.

escreve_problema(R):-
flatten(R,R1),
pack(R1,I),
flatten(I,R2),
write('Problemas no/s veiculo/s '), write(R2), write(' '),
write('Corrigir manualmente.'),nl.


escreve_mudancas_motorista([]):-nl.
escreve_mudancas_motorista([(Id1,Id,Vei)|T]):-nl,write('mudança do motorista ') , write(Id1), write(' para o '), write(Id), write(' no veiculo '), write(Vei),nl, escreve_mudancas_motorista(T).

escreve_mudancas([]):-nl.
escreve_mudancas([(Id,E1,E2)|T]):-nl,nl,write('Mudança no veiculo: '), write(Id),nl, write('De: '), write(E1), nl, write('Para '), write(E2),nl,escreve_mudancas(T).
% :-doit(1).
% :-doit(2).
% %:-doit(3).
% %:-doit(4).

