% horariomotoristas(id Motorista, Hora que pode começar, hora q pode acabar,  duração total, [duração de cada bloco de trabalho]).
horariomotorista(276,25200,61200,28800, [21600,7200]). %6h + 2h
horariomotorista(527, 25200, 61200, 28800, [21600,7200]). %6h + 2h     7h - 17h
horariomotorista(889, 25200, 61200, 28800, [21600,7200]). %6h + 2h
horariomotorista(1055, 25200, 61200, 28800, [14400,14400]). %4h + 4h
horariomotorista(1461, 25200, 61200, 28800, [14400,14400]). %4h + 4h
horariomotorista(1640, 25200, 61200, 28800, [21600,7200]). %6h + 2h
horariomotorista(2049, 25200, 61200, 28800, [21600,7200]).%6h + 2h
horariomotorista(5188,33300,69300, 28800, [7200,21600]). %2h + 6h
horariomotorista(6616,33300, 69300, 28800, [14400,14400]). %4h + 4h
horariomotorista(6697,33300, 69300, 28800, [21600,7200]). %6h + 2h
horariomotorista(11018,41400,77400, 28800, [21600,7200]).%6h + 2h
horariomotorista(11692,41400, 77400, 28800, [21600,7200]). %6h + 2h
horariomotorista(14893,45000,81000, 28800, [10800,18000]). %5h + 5h
horariomotorista(16458,50400,86400, 28800, [14400,14400]). %4h + 4h
horariomotorista(16690, 50400, 86400, 28800, [7200,21600]).  %6h +2h
horariomotorista(16763, 50400, 86400, 28800, [14400,14400]). %4h + 4h
horariomotorista(17015, 50400, 86400, 28800, [10800,18000]).  %5h + 5h
horariomotorista(17552,54000,90000,28800, [10800,18000]).  %5h + 5h
horariomotorista(17623,25200, 61200, 28800, [21600,7200]).  %6h + 2h
horariomotorista(17630,25200, 61200, 28800, [21600,7200]). %6h + 2h
horariomotorista(17639,27000,48600,21600,[21600]).  %6h 
horariomotorista(17673,25200, 61200, 28800, [21600,7200]).   %6h + 2h
horariomotorista(18009,50400, 86400, 28800, [7200,21600]).  %2h + 6h
horariomotorista(18050,54000,90000,28800, [21600,7200]). %6h + 2h
horariomotorista(18097,57600,79200,21600,[21600]).  %6h 
horariomotorista(18105,57600,79200,21600,[21600]). %6h 
horariomotorista(18107,57600,79200,21600,[21600]). %6h 
horariomotorista(18119,59400,81000,21600,[21600]). %6h 
horariomotorista(18131,66600,88200,21600,[21600]). %6h 


gera_disponibilidade:-gera_lista_horarios(L), retractall(tu(_,_,_,_)), processa_lista_horarios(L). %gera tuplos do tipo tu

gera_lista_horarios(R):-findall(horariomotorista(Id,I,F,Duracao,L), horariomotorista(Id,I,F,Duracao,L),R). %gera uma lista com todos os horariomotorista/5.

processa_lista_horarios([]).  %processa a lista com todos os horariomotorista/5.
processa_lista_horarios([H|T]):-processa_lista_horarios(T), processa_motorista(H).

processa_motorista(horariomotorista(Id,I,F,Duracao,L)):-descobre_blocos(Id,I,F,Duracao,L). %processa cada motorista 

descobre_blocos(Id,I,F,Duracao,L):-length(L,A), ((A==1,!,asserta(tu(I,F,Duracao,Id))); Total is F-I, Sobra is Total - Duracao, Margem is Sobra / A, processa_blocos(Id,I,F,L,Margem)). %descobre quantos blocos tem um motorista, caso o motorista tenha apenas 1 bloco de trabalho então cria o tu/4 correspondente.

processa_blocos(Id,I,F,[L],_):-!,asserta(tu(I,F,L,Id)). %caso o motorista tenha mais que 1 bloco então processa-os criando os tuplos tu/4.
processa_blocos(Id,I,F,[H|T],Margem):- IAux is I + H + Margem, asserta(tu(I,IAux,H,Id)), !,processa_blocos(Id,IAux,F,T,Margem). 



ordena_tuplos(R):-findall(tu(A,B,C,D), tu(A,B,C,D), R1), ordena_tuplos(R1,R). %ordena tuplos

ordena_tuplos(InputList,SortList) :- %auxiliar de ordenação
swapz(InputList,List) , ! ,
ordena_tuplos(List,SortList).
ordena_tuplos(SortList,SortList).

swapz([tu(Q,W,E,R),tu(Q1,W1,E1,R1)|List],[tu(Q1,W1,E1,R1),tu(Q,W,E,R)|List]) :-Q > Q1.
swapz([Z|List],[Z|List1]) :- swapz(List,List1). %auxiliar de ordenação

cria_obrigacoes():-retractall(obrigacao_horario(_)), findall((Id,I,F), tu(I,F,_,Id),R), asserta(obrigacao_horario(R)).

