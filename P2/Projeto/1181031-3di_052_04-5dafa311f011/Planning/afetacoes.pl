get_affect(_):-findall([Id,L],melhor_escalonamento(Id,L*_),R),
                     prolog_to_json(R,R1), reply_json(R1).


% afeta, se  possível, motoristas a todos os veichle duties 
afeta_motorista_a_veiculos():-verifica_carga(F), ((F==1,!,sortVehicleDuties(V), ordena_tuplos(T), afeta_moto_vei(V,T),afeta(V,T,0),limpaListaMotorista()); false).

afeta_moto_vei(V,_):-retractall(lista_motoristas_nworkblocks(_,_)), cria_lista_motoristas_nworkblocksAuxiliar(V).

%metodos  que fazem a afetação entre blocos de trabalho de um vehicle duty e um tuplo de um motorista.

afeta(_,Tuplos,1):-retractall(motoristas_restantes(_)),asserta(motoristas_restantes(Tuplos)), !.  %verifica se ainda existem veiculos a serem afetados
afeta(V,T,0):-apaga_vazios(V,New1), length(New1,I), ((I>0,!,afeta1(New1,T,New2,NT), afeta(New2,NT,0)); afeta(New1,T,1)). 

afeta1([],NT,[],NT).  %itera os vehicle duties que ainda tenham workblocks a afetar
afeta1([vehicleduty(I,L)|T], Tuplos, [vehicleduty(I,R)|T1],RX):- afeta2(I,L,Tuplos,R,NewTuplos),afeta1(T,NewTuplos,T1,RX).

calcula_aproximacao_blocos(D,N,R):-R is round(D/N). %calcula uma aproximação 

verifica_numero_workblocks(NBlocosA,Laux,NBlocos):-((NBlocosA>Laux,!, NBlocos is Laux); NBlocos is NBlocosA). %verifica se o condutor pode 

afeta2(I,L,Tuplos,R,NewTuplos):-nth0(0,Tuplos,tu(_,_,Duracao,Id)), length(L,Laux), encontra_nblocks_real(L,Duracao,Laux,NBlocos),    ((NBlocos==0, !, away(Tuplos,1,NewTuplos), clone(L,R)); take(L,NBlocos,L1), real_afetacao(NBlocos,I,Duracao,Id,L1,Tuplos,NewTuplos) ,remove_n(L,NBlocos,R),!). %afeta motorista a um dado vehicle duty


%encontra o numero de blocos que o motorista pode executar
encontra_nblocks_real(L,Duracao,Len,BReal):-list_sum1(L,Tempo),((Tempo>Duracao,!, Aux is Len - 1, ((Aux>0,!,take(L,Aux,NovaLista), encontra_nblocks_real(NovaLista,Duracao,Aux,BReal) ); !,BReal is 0) ); !, BReal is Len).

 %atualiza os tuplos dos motoristas em ordem às afetações criadas
real_afetacao(BReal,I,Duracao,Id,L,Tuplos,NewTuplos):- remove_n(Tuplos,1,TuplosAux), atualiza_lista_motorista(I,Id,BReal), list_sum1(L,Tempo), Adicionar is Duracao - Tempo, ((nth1(P,TuplosAux,tu(HI,HF,Dur,Id)), NewDur is Dur+Adicionar, away(TuplosAux,P,TuplosAux1), inserePos(tu(HI,HF,NewDur,Id),P,TuplosAux1,NewTuplos)); clone(TuplosAux,NewTuplos)).

nao_consegue_afetar(Tuplos,NT):-away(Tuplos,1,NT). %quando nao é possível afetar o motorista em causa aos workblocks do vehicle duty

%adiciona um numero de workblocks executados por um dado motorista na lista de afetações
atualiza_lista_motorista(I,Id,Len):- lista_motoristas_nworkblocks(I,ListaMotoristas), ((nth0(_,ListaMotoristas,(Id,Number)),!, NewNumber is Number + Len, substitui(Number,NewNumber,ListaMotoristas,Id,NovaLista),!); 
append([(Id,Len)],ListaMotoristas,NovaLista)), retract(lista_motoristas_nworkblocks(I,ListaMotoristas)), asserta(lista_motoristas_nworkblocks(I,NovaLista)). 


capacidade_sistema(R):-findall(L,horariomotorista(_,_,_,L,_),R1), list_sum(R1,R). %calcula a capacidade do sistema através dos horarios dos motoristas

carga_sistema(R):-findall(A,vehicleduty(_,A),L), flatten(L,R1), list_sum1(R1,R). %calcula a carga do sistema através da duração de todos os workblocks dos vehicle duties

verifica_carga(F):-capacidade_sistema(S), carga_sistema(C), ((C<S, !, F is 1);F is 0). %verifica se a capacidade do sistema é maior do que a carga


limpaListaMotorista():-findall((I,L),lista_motoristas_nworkblocks(I,L),R), apaga_motoristas_nao_usados(R). %limpa toda a lista de motoristas que não estão afetados a vehicle duties

apaga_motoristas_nao_usados([]). %apaga motoristas não usados 

apaga_motoristas_nao_usados([(Id,H)|T]):- apaga_vazios1(H,NovaLista),  retract(lista_motoristas_nworkblocks(Id,H)), asserta(lista_motoristas_nworkblocks(Id,NovaLista)),apaga_motoristas_nao_usados(T).

verifica_fim_afetacao([],1,R):-!,R is 1.  %verifica se ainda existem veiculos duties a serem associados
verifica_fim_afetacao([vehicleduty(_,L)|T],_,R):-length(L,A), ((A>0, !, R is 0); !,verifica_fim_afetacao(T,1,R)). 

list_sum([],0). %soma valores numa lista
list_sum([Head|Tail], TotalSum):-
list_sum(Tail, Sum1),
TotalSum is Head+Sum1.

list_sum1([],0). %soma duração de lista de workblocks
list_sum1([Head|Tail], TotalSum):-
list_sum1(Tail, Sum1),
workblock(Head,_,S,E,_,_), Aux is E-S, TotalSum is Aux + Sum1.

clone([],[]).
clone([H|T],[H|Z]):- clone(T,Z).

sortVehicleDuties(R1):-findall(vehicleduty(I,L), vehicleduty(I,L),R), sortVehicleDuties(R,R1).  %ordena serviço de veiculos por ordem crescente de hora de inicio

sortVehicleDuties(InputList,SortList) :- %auxiliar de ordenação
swap(InputList,List) , ! ,
sortVehicleDuties(List,SortList).
sortVehicleDuties(SortList,SortList).

swap([vehicleduty(X,L),vehicleduty(Y,L1)|List],[vehicleduty(Y,L1),vehicleduty(X,L)|List]) :- workblock(X,_,S,_,_,_), workblock(Y,_,S1,_,_,_),S > S1.
swap([Z|List],[Z|List1]) :- swap(List,List1). %auxiliar de ordenação

encontra_tempo_de_bloco_maior_duracao(L,Tempo):-encontra_bloco_maior_duracao(L,Bloco), workblock(Bloco,_,BI,BF,_,_), Tempo is BF-BI.
encontra_bloco_maior_duracao([H|[]],H). %encontra workblock com maior duração a partir de uma lista deles
encontra_bloco_maior_duracao([H|T],M):-encontra_bloco_maior_duracao(T,M), workblock(M,_,I,F,_,_), workblock(H,_,I1,F1,_,_), M1 is F-I, H1 is F1-I1 , M1>H1,!.
encontra_bloco_maior_duracao([H|_],H).

cria_lista_motorista(M):-findall((I,0), horariomotorista(I,_,_,_,_), M).
cria_lista_motoristas_nworkblocksAuxiliar([]). %cria os predicados lista_motoristas_nworkblocks/2.
cria_lista_motoristas_nworkblocksAuxiliar([vehicleduty(I,_)|T]):-cria_lista_motorista(M), asserta(lista_motoristas_nworkblocks(I,M)), cria_lista_motoristas_nworkblocksAuxiliar(T).

remove_n(List, N, ShorterList) :- %remove os primeiros N da lista e coloca na shorted os elementos Length - N.
    length(Prefix, N),
    append(Prefix, ShorterList, List).

 away([_|H],1,H):-!. %remove elemento de uma lista a uma dada posição
   away([G|H],N,[G|L]):- N > 1, Nn is N - 1,!,away(H,Nn,L).

substitui(_,_,[],_,[]):-!. %substitui numero de workblocks de um motorista no predicado lista_motoristas_nworkblocks/2
substitui(X,Y,[(Id,_)|L],Id,[(Id,Y)|L1]):- !, substitui(X,Y,L,Id,L1).
substitui(X,Y,[Z|L],Id,[Z|L1]):- !, substitui(X,Y,L,Id,L1).

inserePos(X,1,L,[X|L]):-!. %insere um elemento numa lista a uma dada posição
inserePos(X,N,[Y|L],[Y|L1]):- N1 is N-1,inserePos(X,N1,L,L1).

apaga_vazios([],[]). %apaga de uma lista de vehicle duties aqueles que já não têm mais workblocks por afetar
apaga_vazios([vehicleduty(_,[])|T], T1):-!, apaga_vazios(T,T1).
apaga_vazios([vehicleduty(Id,L)|T], [vehicleduty(Id,L)|T1]):-apaga_vazios(T,T1).

apaga_vazios1([],[]). %apaga da lista de motoristas de um serviço de veiculo aqueles que não farão parte dele
apaga_vazios1([(_,0)|T], T1):-!, apaga_vazios1(T,T1).
apaga_vazios1([(Id,L)|T], [(Id,L)|T1]):-apaga_vazios1(T,T1).


