escalonador_motoristas():-findall(lista_motoristas_nworkblocks(A,V), lista_motoristas_nworkblocks(A,V), R), itera_escalonamentos(R). %junta o escalonamento da afetação com o algoritmo genético

itera_escalonamentos([]). %para cada lista de motoristas afetado a um vehicle duty chama o algoritmo genético 
itera_escalonamentos([lista_motoristas_nworkblocks(A,L)|T]):-retractall(service(_)), asserta(service(A)), length(L,Len), ((Len=1,!, gera_lista(A,Aux),  asserta(melhor_escalonamento(A,Aux)) ); gera), itera_escalonamentos(T).

cria_lista_wb_motorista():-retractall(lista_wb_motorista(_,_)), findall(melhor_escalonamento(A,L), melhor_escalonamento(A,L*_),R) ,sortMelhorEscalonamento(R,X), cria_lista_de_motorista(M),itera_cria_lista_wb_motorista(X,M,V),adiciona_lista_wb_motorista_bc(V).

cria_lista_de_motorista(M):-findall((I,[]), horariomotorista(I,_,_,_,_), M).

itera_cria_lista_wb_motorista([],R,R):-!.
itera_cria_lista_wb_motorista([melhor_escalonamento(A,L)|T],M,R):- cria_tuplos_wb_motoristas(A,L,M,A1), itera_cria_lista_wb_motorista(T,A1,R).

cria_tuplos_wb_motoristas(A,L,M,R):-vehicleduty(A,Wbs), constroiAvaliacao(Wbs,Tripletos,L),itera_tripletos(A,Tripletos,M,R).

itera_tripletos(_,[],R,R):-!.
itera_tripletos(V,[(I,F,Id)|T],M,R):-nth1(P,M,(Id,Lista)),!, length(Lista,Len),LenA is Len + 1,  inserePos((V,I,F),LenA,Lista,Res),substituiAux(M,(Id,Res),P,Aux), !, itera_tripletos(V,T,Aux,R).

adiciona_lista_wb_motorista_bc([]).
adiciona_lista_wb_motorista_bc([(I,L)|T]):-asserta(lista_wb_motorista(I,L)), adiciona_lista_wb_motorista_bc(T).

substituiAux(L,Q,P,R):-away(L,P,L1),inserePos(Q,P,L1,R).

sortMelhorEscalonamento(InputList,SortList) :- %auxiliar de ordenação
swapX(InputList,List) , ! ,
sortMelhorEscalonamento(List,SortList).
sortMelhorEscalonamento(SortList,SortList).

swapX([melhor_escalonamento(X,L),melhor_escalonamento(Y,L1)|List],[melhor_escalonamento(Y,L1),melhor_escalonamento(X,L)|List]) :- workblock(X,_,S,_,_,_), workblock(Y,_,S1,_,_,_),S > S1.
swapX([Z|List],[Z|List1]) :- swapX(List,List1). %auxiliar de ordenação

:-cria_lista_wb_motorista.

