% Bibliotecas HTTP
:-use_module(library(http/thread_httpd)).
:-use_module(library(http/http_dispatch)).
:-use_module(library(http/http_parameters)).
:-use_module(library(http/http_client)).
:-use_module(library(http/http_cors)).
:-set_setting(http:cors, [*]).
:-http_handler('/plan/genetic', get_solution, []).
% Bibliotecas JSONx
:-use_module(library(http/json_convert)).
:-use_module(library(http/http_json)).
:-use_module(library(http/json)).
:-use_module(library(ssl)).
:-use_module(library(socket)).
:-use_module(library(http/http_open)).
:-use_module(library(crypto)).
:-use_module(library(http/http_ssl_plugin)).
:-dynamic horariosPath/2.
:- set_setting(http:cors, [*]).

:-dynamic horario/3. %horario(Path,Sequencial Id ,List_of_Time)

:-dynamic tripId/2. %tripId(Sequencial Id,DB Id)

:-dynamic workblock/6.  % workblock(Sequencial Id, List_of_Trips,StartTime, EndTime, Start Node, Finish Node)

:-dynamic workblockId/2. %workblockId(Sequencial Id,DB Id)

:-dynamic vehicleduty/2. % vehicleduty(VehicleDuty, List_of_WorkBlocks)
%SETUP
getTrips():-retractall(horario(_,_,_)),retractall(tripId(_,_)), http_get('https://mdv52.herokuapp.com/mdv/Trips', ListTrips, [json_object(dict),cert_verify_hook(cert_accept_any)]), importTrips(ListTrips). %importa as trips do MDV

getBlocks():-retractall(workblock(_,_,_,_,_,_)),retractall(workblockId(_,_)),http_get('https://mdv52.herokuapp.com/mdv/WorkBlock', ListBlocks, [json_object(dict),cert_verify_hook(cert_accept_any)]), importBlocks(ListBlocks). %importa os workblocks do MDV

getVehicles():-retractall(vehicleduty(_,_)),http_get('https://mdv52.herokuapp.com/mdv/VehicleDuty', ListVehicles, [json_object(dict),cert_verify_hook(cert_accept_any)]), importVehicles(ListVehicles). %importa os vehicle duties do MDV

getAll:-getTrips(),getBlocks(),getVehicles().

importTrips([]).
importTrips([H|T]):-importTrips(T), asserta(tripId(H.tripName.name,H.id.value)), getTimes(H.passingTimes,R), sort(R,R1), atom_number(H.routeId.routeId,RouteId), asserta(horario(RouteId,H.tripName.name,R1)). %constroi o predicado tripId e horario.

importBlocks([]).
importBlocks([H|T]):- importBlocks(T), asserta(workblockId(H.name,H.id)), getTripsInWB(H.trips,A), busortTrips(A,R),  asserta(workblock(H.name,R,H.startTime,H.finishTime, H.beginNode, H.finishNode)).  %constroi o predicado workblockId e workblock.

importVehicles([]). %constroi o predicado vehicleduty
importVehicles([H|T]):-importVehicles(T), getWorkblocksFromTripIds(H.tripList,P,L), asserta(vehicleduty(P,L)).

getWorkblocksFromTripIds(L1,P,L2):-getWorkblocksFromTripIds1(L1,LAux),flatten(LAux,LAux1), sort(LAux1,L2), nth0(0,L2,P). %devolve a lista de workblocks a partir de uma lista de ids de trips

getWorkblocksFromTripIds1([],[]). 
getWorkblocksFromTripIds1([H|T], [R|T1]):-getWorkblocksFromTripIds1(T,T1), findWorkblockByTripid(H,R). %descobre os workblocks referentes a um conjunto de viagens

findWorkblockByTripid(H,R):-findall(I,(workblock(I,L,_,_,_,_), member(H,L)),R). %encontra um workblock através de uma trip que seja associado


getTimes(L,R):-getTimes1(L,R).  %constroi uma lista com os tempos de passagem de uma determinada trip.
getTimes1([],[]).
getTimes1([H|T],[H.time.time|T1]):-getTimes1(T,T1).

getTripsInWB(L,R):-getTripsInWB1(L,R). %constroi uma lista de viagens com os Ids sequênciais
getTripsInWB1([],[]).
getTripsInWB1([H|T],[R|T1]):-getTripsInWB1(T,T1),tripId(R,H).


busortTrips(L,S):-        %adaptacao do bubble sort para ordenar uma lista de trips por ordem temporal
swap1(L,LS), !, busortTrips(LS,S).
busortTrips(S,S).
swap1([X,Y|T],[Y,X|T]):- horario(_,X,L1), horario(_,Y,L2), nth0(0,L1,X1),nth0(0,L2,Y2), X1>Y2.
swap1([Z|T],[Z|TT]):- swap1(T,TT).

:-getAll.
:-retractall(vehicleduty(11,_)). %removemos alguns veiculos para dar uma maior margem e ficarmos com alguns motoristas sem trabalhar para dar cobertura as hard consts
 :-retractall(vehicleduty(5,_)).
 :-retractall(vehicleduty(4,_)).
 :-retractall(vehicleduty(9,_)).
 :-retractall(vehicleduty(1,_)).


 