# Aplicação OPT

## 1. Desenvolvimento

Neste projeto, foi usado `node` na versão `12.18.4`

Para instalar o node, corra os seguintes comandos:

```
nvm install 12.18.4
```

```
nvm use 12.18.4
```
### 1.1 MDR

A primeira vez, é necessário correr os estes comandos:

```
npm install
```

```
npm fund
```

```
npm audit fix --force
```

Posteriormente, traduz-se o TypeScript em JavaScript:

```
npx tsc
```

E, finalmente, correr um dos seguintes comandos para iniciar o servidor.

```
npm run start
```
ou
```
node ./build/server.js
```

### 1.2 SPA

A primeira vez, é necessário correr os estes comandos:

```
npm install
```

```
npm fund
```

```
npm audit fix --force
```
Posteriormente, traduz-se o TypeScript em JavaScript:

```
npx tsc
```
E, finalmente, correr o seguinte comando para iniciar o SPA.

```
ng serve --open
```

### 1.3 Planning

Para colocar o planning em execução correr o seguinte comando
```
swipl planning.pl
```
E posteriormente já no SWI-Prolog correr a seguinte linha
```
server(5000)
```

## 2. Design Arquitetural

### 2.1 Nível 1: Vista Lógica e de Cenário

![logicViewLevel1](./Diagrams/logicViewLevel1.png)

### 2.2 Nível 2: Vista Lógica, de processo de implementação e física

#### 2.2.1 Vista Lógica 

![logicViewLevel2](./Diagrams/logicViewLevel2.png)

#### 2.2.2 Vista de Processo

Decidiu-se criar um artefacto de nível dois da vista de processo para o caso de uso seguinte:

- Como gestor pretendo visualizar as soluções do algoritmo de mudança de motorista.

Achamos relevante apresentar este diagrama, pois quando se iniciou a projeção arquitetural do mesmo surgiram distintas soluções para resolver o problema. Soluções estas que poderiam mudar o rumo e abordagem da equipa quanto à implementação do projeto como um todo.

A possível resolução do problema dividiu-se nas duas seguintes propostas:

1. Aquando da iniciação do planning, ele vai ao MDR e a o MDV buscar a informação relativa a nós/linhas/viagens.

2. Aquando da iniciação do planning, ele não tem qualquer informação na sua base de conhecimento, porém, ao ser adicionado um nó/linha ou viagem no MDR ou MDV, respetivamente, estes iriam enviar a informação para o planning.

Vantagens:

1.  
- API Rest implementada anteriormente para o MDR manter-se-ia a mesma, não havendo necessidade de adicionar novas funcionalidades ao MDR.
- O planning não teria que estar constantemente ligado para estar atualizado.

2. 
- Quando algo era adicionado no MDR/MDV essa alteração era imediatamente repercutida na base de conhecimento do planning, poupando-se a necessidade de estar constantemente a consultar ambos para obter informação.

Desvantagens:

1.   
- Necessidade de fazer um pedido ao planning para ir buscar toda a informação ao MDR/MDV sempre que precisamos de calcular uma nova solução.

2.  
- Necessidade de refazer toda a API implementada para o MDR.
- Ter o planning sempre ligado apôs o inicio do MDR/MDV.

Analisados os pros e contras de ambas as abordagens a equipa resolveu optar pela primeira opção apresentada. 
Esta escolha deve-se maioritariamente à vantagem de não ser necessário fazer qualquer mudança no MDR. 

![processLevel2](./Diagrams/processLevel2.jpg)

### 2.3 Nível 3: Master Data Rede: Vista Lógica, de processo e de implementação com adoção de padrões: (Cliente/Servidor, DDD, Onion (DI,IoC), DTO)

#### 2.3.1 OPT API
![logicViewLevel3](./Diagrams/logicViewLevel3.png)
===========================================
![logicViewLevel3_DI_IoC](./Diagrams/logicViewLevel3_DI_IoC.png)


#### 2.3.2 SPA API
![logicViewLevel3](./Diagrams/level3SPA.png)


### 2.4 Modelo de Domínio
![ClassDiagram](./Diagrams/ClassDiagram.jpg)

### 2.5 Model de Domínio com DDD
![DomainModel](./Diagrams/DomainModel.jpg)

### 2.6 Design dos US de criar/definir

![PostDiagram](./Diagrams/PostDiagram.jpg)

### 2.7 Design dos US de listar

![GetDiagram](./Diagrams/GetDiagram.jpg)