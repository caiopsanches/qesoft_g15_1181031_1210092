import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest, HttpEvent } from '@angular/common/http';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {
  fileToUpload: File

  constructor(private http: HttpClient) { }
  result: string;
  aux: number;
  ngOnInit(): void {
    document.title = "Import"
  }


  InputChange(fileInputEvent: any) {
    this.result = ''
    const endpointMDR = '/api/import';
    const endpointMDV = '/mdv/Import';

    if (fileInputEvent.target.files[0].type != "text/xml") {
      this.result = "Unable to import the file, please review the file!";
      return -1;
    } else {
      const formData: FormData = new FormData();
      formData.append('fileKey', fileInputEvent.target.files[0], fileInputEvent.target.files[0].name);
      return this.http.post(endpointMDR, formData).subscribe((response) => {
        this.http.post(endpointMDV, formData).subscribe((response) => {
          this.result = "Sucessfully imported!";
        },
          (error) => {
            this.result = "Sucess only importing to Network Master Data!";
          }
        )
      },
        (error) => {
          this.aux = -1
          this.http.post(endpointMDV, formData).subscribe((response) => {
            this.result = "Sucess only importing to Trip Master Data!";
          },
            (error) => {
              this.result = "Unsucess importing data, please try again!";

            }
          )
        },

      )
    }
  }
}