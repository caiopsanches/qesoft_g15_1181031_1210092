import { DriverService } from './driver.service';
import { CrewmemberService } from '../crewmember/crewmember.service';
import DriverDTO from "../dto/IDriverDTO"
import ICrewmemberDTO from '../dto/ICrewmemberDTO'
import { Output, Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'app-driver',
    templateUrl: './driver.component.html',
    styleUrls: ['./driver.component.css']
})

export class DriverComponent implements OnInit {

    driverDto: DriverDTO
    constructor(private driverService: DriverService, private crewmemberService: CrewmemberService) { }

    resultado: string

    @Input() driverCC: string;
    @Input() driverDate: string;
    @Input() driverEntry: string;
    @Input() driverExit: string;
    @Input() driverMecNumber: string;
    @Input() driverName: string;
    @Input() driverNIF: string;
    @Input() driverLicenseExpiry: string;
    @Input() driverLicenseNumber: string;
    @Input() code: string;
    listDriverType: string[] = [];

    driversDisplay = new Array();

    ngOnInit(): void {

        document.title = "Driver"

        this.crewmemberService.getCrewmembers().subscribe(crewmembers => {
            for (let crewmember of crewmembers) {
                this.driversDisplay.push(crewmember.code)
            }
            console.log(this.driversDisplay)
        })

    }

    addCrewmembers() {
        this.listDriverType.push(
          this.code);
          this.code = '';
    }
    
    addDriver() {
        this.driverService.addDriver({
            driverCC: this.driverCC,
            driverDate: this.driverDate,
            driverEntry: this.driverEntry,
            driverExit: this.driverExit,
            driverMecNumber: this.driverMecNumber,
            driverName: this.driverName,
            driverNIF: this.driverNIF,
            driverLicenseExpiry: this.driverLicenseExpiry,
            driverLicenseNumber: this.driverLicenseNumber,
            listDriverType: this.listDriverType

        } as DriverDTO).subscribe((response) => {
            this.resultado = "Tripulante definido com sucesso!"
        },
            (error) => {                              //Error callback
                this.resultado = "Não foi possível definir o tripulante, reveja os dados!";
            })
    }

}