import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import IDriverDTO from '../dto/IDriverDTO';

import { DriverService } from './driver.service';

describe('DriverService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let driverService: DriverService;

    beforeEach(() => {
        //Configures testing app module
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DriverService
            ]
        });

        //Instantaites HttpClient, HttpTestingController and EmployeeService
        httpClient = TestBed.inject(HttpClient);
        httpTestingController = TestBed.inject(HttpTestingController);
        driverService = TestBed.inject(DriverService);
    });

    afterEach(() => {
        httpTestingController.verify(); //Verifies that no requests are outstanding.
    });

    it('should be created', () => {
        expect(driverService).toBeTruthy();
    });

    //Test case 1
    it('should add an Driver and return it', () => {
        const newDriver: IDriverDTO = {
            driverCC: "12345678",
            driverDate: "20-11-1990",
            driverEntry: "20-11-2018",
            driverExit: "10-05-2020",
            driverMecNumber: "abcde1234",
            driverName: "Vigario",
            driverNIF: "123456789",
            driverLicenseExpiry: "20-11-2030",
            driverLicenseNumber: "P-1234567 8",
            listDriverType: ["12", "13"]
        };

        driverService.addDriver(newDriver).subscribe(
            data => expect(data).toEqual(newDriver, 'should return the Driver'),
            fail
        );

        // addEmploye should have made one request to POST employee
        const req = httpTestingController.expectOne(driverService.rootURL + "/driver");
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toEqual(newDriver);

        // Expect server to return the employee after POST
        const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newDriver });
        req.event(expectedResponse);
    });

    it('get all drivers', () => {
        const drivers = [
            {
            driverCC: "12345678",
            driverDate: "20-11-1990",
            driverEntry: "20-11-2018",
            driverExit: "10-05-2020",
            driverMecNumber: "abcde1234",
            driverName: "Vigario",
            driverNIF: "123456789",
            driverLicenseExpiry: "20-11-2030",
            driverLicenseNumber: "P-1234567 8",
            listDriverType: ["12", "13"]
            },
            {
            driverCC: "12355698",
            driverDate: "24-08-1995",
            driverEntry: "10-05-2017",
            driverExit: "10-05-2035",
            driverMecNumber: "abtgw1739",
            driverName: "Lizando",
            driverNIF: "198456329",
            driverLicenseExpiry: "09-12-2030",
            driverLicenseNumber: "P-1234107 2",
            listDriverType: ["12", "13"]
            }
        ];

        driverService.getDrivers().subscribe(g => {
            expect(g.length).toBe(2)
            expect(g).toEqual(drivers)
        })

        const req = httpTestingController.expectOne(`${driverService.rootURL}/driver`)
        expect(req.request.method).toBe("GET")
        req.flush(drivers)
    })

});