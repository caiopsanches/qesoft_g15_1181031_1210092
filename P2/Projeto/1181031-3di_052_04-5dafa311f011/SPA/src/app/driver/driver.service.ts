import { Injectable } from '@angular/core';
import IDriverDTO from '../dto/IDriverDTO';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DriverService {
    constructor(private http: HttpClient) { }
    rootURL: string = "/mdv"


    addDriver(data: IDriverDTO) {
        console.log(data);
        return this.http.post(this.rootURL + '/driver', data)
    }

    getDrivers(): Observable<IDriverDTO[]> {
        return this.http.get<IDriverDTO[]>(this.rootURL + '/driver').pipe(map(response => { return response }))
    }

}