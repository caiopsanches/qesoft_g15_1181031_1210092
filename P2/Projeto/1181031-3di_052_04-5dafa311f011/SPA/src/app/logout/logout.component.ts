import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../Homepage/homepage/login.service';


@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

    constructor(private loginService: LoginService, private router: Router) { }

    ngOnInit() {
    }

    logout(): void {
        this.loginService.Clientelogout();
        this.loginService.AdminLogout();
        this.router.navigateByUrl('/login');
    }

}