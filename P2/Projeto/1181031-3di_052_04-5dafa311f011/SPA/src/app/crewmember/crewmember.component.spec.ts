import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs/internal/observable/of';
import { CrewmemberComponent } from './crewmember.component';
import { CrewmemberService } from './crewmember.service';

describe('CrewmemberComponent', () => {
  let component: CrewmemberComponent;
  let crewmemberService: CrewmemberService;
  let fixture: ComponentFixture<CrewmemberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrewmemberComponent ],

      providers: [
        {
          provide: CrewmemberService, useValue: {
            addCrewmember: () => of({
              code: "crew62",
              description: "desc1"
            })
          }
        }],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrewmemberComponent);
    component = fixture.componentInstance;
    crewmemberService = TestBed.get(CrewmemberService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});