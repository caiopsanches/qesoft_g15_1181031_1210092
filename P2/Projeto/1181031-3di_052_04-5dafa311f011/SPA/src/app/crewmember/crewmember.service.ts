import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import ICrewmemberDTO from '../dto/ICrewmemberDTO';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class CrewmemberService {

  constructor(private http: HttpClient) { }
  rootURL = '/api';

  addCrewmember(data: ICrewmemberDTO) {
    return this.http.post(this.rootURL + '/crewmember', data)
  }

  getCrewmembers(): Observable<ICrewmemberDTO[]> {
    return this.http.get<ICrewmemberDTO[]>(this.rootURL + '/crewmember').pipe(map((response: any) => { return response }))
  }
}