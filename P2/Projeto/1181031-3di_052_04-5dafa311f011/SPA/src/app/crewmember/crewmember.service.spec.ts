import { TestBed } from '@angular/core/testing';
import { CrewmemberService } from './crewmember.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import  ICrewmemberDTO  from '../dto/ICrewmemberDTO';

describe('CrewmemberService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let crewmemberservice: CrewmemberService;

  beforeEach(() => {
    //Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CrewmemberService
      ]
    });

    //Instantaites HttpClient, HttpTestingController and EmployeeService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    crewmemberservice = TestBed.inject(CrewmemberService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(crewmemberservice).toBeTruthy();
  });

  //Test case 1
  it('should add an crewmember and return it', () => {
    const newCrewmember: ICrewmemberDTO = {
      code: "crew62",
      description: "desc1"
    };

    crewmemberservice.addCrewmember(newCrewmember).subscribe(
      data => expect(data).toEqual(newCrewmember, 'should return the crewmember'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(crewmemberservice.rootURL+"/crewmember");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(newCrewmember);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newCrewmember });
    req.event(expectedResponse);
  });

  it('get all crewmembers', () => {
    const crewmembers = [
      {
        code: "crew1",
        description: "desc1",
      },
      {
        code: "crew1",
        description: "desc2",
      }
    ];

    crewmemberservice.getCrewmembers().subscribe(g => {
      expect(g.length).toBe(2)
      expect(g).toEqual(crewmembers)
    })

    const req = httpTestingController.expectOne(`${crewmemberservice.rootURL}/crewmember`)
    expect(req.request.method).toBe("GET")
    req.flush(crewmembers)
  })
});