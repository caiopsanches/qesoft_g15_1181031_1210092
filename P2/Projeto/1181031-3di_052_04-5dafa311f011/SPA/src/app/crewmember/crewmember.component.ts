import { Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, NgControlStatus } from '@angular/forms';
import { CrewmemberService } from './crewmember.service'
import CrewmemberDTO from '../dto/ICrewmemberDTO'

@Component({
  selector: 'app-crewmember',
  templateUrl: './crewmember.component.html',
  styleUrls: ['./crewmember.component.css']
})

export class CrewmemberComponent implements OnInit {

  crewmember: CrewmemberDTO
  constructor(private crewmemberService: CrewmemberService) {
  }
  result: string;

  @Input() code: string;
  @Input() description: string;

  ngOnInit(): void {
    document.title = "Crewmember"
  }

  addCrewmember() {
    this.crewmemberService.addCrewmember({
      code: this.code,
      description: this.description
    } as CrewmemberDTO).subscribe((response) => {
      this.result = "Crewmember added!"
    },
      (error) => {                              //Error callback
        this.result = "Unable to add the crewmember, please review the data!";
      })
  }
}