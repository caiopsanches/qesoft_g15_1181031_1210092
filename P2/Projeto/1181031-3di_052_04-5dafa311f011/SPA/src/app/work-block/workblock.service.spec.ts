import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { WorkblockService } from './workblock.service';

describe('WorkblockService', () => {
  let service: WorkblockService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        WorkblockService
      ]
    });

    //Instantaites HttpClient, HttpTestingController and EmployeeService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(WorkblockService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //Test case 1
  it('should create workblocks and return it', () => {
    var nblock: number, dblock: number, vehicleduty: string, wb: string
    nblock = 12
    dblock = 2
    vehicleduty = "100"
    wb = "workblock1"
    service.addWorkBlock(nblock, dblock, vehicleduty).subscribe(
      data => expect(wb).toEqual(wb, 'should return the line'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(service.rootURL);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual({ number: 12, duration: 2, vehicleduty: '100' });

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: wb });
    req.event(expectedResponse);
  });
});
