import { Input, Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core'
import IWorkBlockDTO from '../dto/IWorkBlockDTO';
import { NodeService } from '../node/node.service';
import { WorkblockService } from './workblock.service';



const MAX_TIME = 14400;

@Component({
  selector: 'app-work-block',
  templateUrl: './work-block.component.html',
  styleUrls: ['./work-block.component.css']
})


export class WorkBlockComponent implements OnInit {

  constructor(private workBlockService: WorkblockService) { }

  resultado: string;
  dbId: string
  @Input() nblocks: number;
  @Input() dblock: number;
  @Input() vehicleduty: string;
  lineDisplay = new Array();
  vehicleDuties = new Array();

  ngOnInit(): void {
    document.title = "WorkBlock"

    this.workBlockService.getAllVehicleDuties().subscribe(vehicleduty => {
      for (let vd of vehicleduty) {
        this.vehicleDuties.push(vd)
        this.lineDisplay.push(vd.description)
      }
      console.log(this.lineDisplay)
    })

  }
  addWorkBlock() {

    console.log(this.vehicleduty)
    let flag = true;
    this.resultado = ''
    if (this.dblock > MAX_TIME) {
      this.resultado = 'The maximum duration is 4h (14400 seconds), please insert a valid one!'
      flag = false;
    }
    if (this.dblock == undefined || this.nblocks == undefined || this.vehicleduty == undefined) {
      this.resultado = 'Please select a valid vehicle duty!'
      flag = false;
    }


    if (flag) {


      for (let vd of this.vehicleDuties) {
        if (vd.description == this.vehicleduty) {
         this.dbId = vd.id
        }
      }
      this.workBlockService.addWorkBlock(this.nblocks, this.dblock, this.dbId).subscribe((response) => {
        console.log(response);
        if (response > 0)
          this.resultado = "Sucess. " + response + " work block/s created!"
        else
          this.resultado = "Not possible to create them, please try again!"
      },
        (error) => {                              //Error callback
          this.resultado = "Not possible to create them, please try again!";
        })
    }
  }
}

