import { Injectable } from '@angular/core';
import IWorkBlockDTO from '../dto/IWorkBlockDTO';
import { HttpClient, HttpParams, HttpRequest, HttpEvent } from '@angular/common/http';
import ILineDTO from '../dto/ILineDTO';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
import IVehicleDutyDTO from '../dto/IVehicleDutyDTO';

@Injectable({
  providedIn: 'root'
})
export class WorkblockService {

  rootURL: string = "/mdv/WorkBlock"

  constructor(private http: HttpClient) { }
  MDV = '/mdv';
  addWorkBlock(nblock: number, dblock: number, vehicleduty: string) {

    return this.http.post(this.rootURL, { number: nblock, duration: dblock, vehicleduty: vehicleduty })
  }

  getAllVehicleDuties(): Observable<IVehicleDutyDTO[]> {
    return this.http.get<IVehicleDutyDTO[]>(this.MDV + '/VehicleDuty').pipe(map(response => { return response }))
  }

  getAllWBs(): Observable<IWorkBlockDTO[]> {
    return this.http.get<IWorkBlockDTO[]>(this.rootURL ).pipe(map(response => { return response }))
}

}


