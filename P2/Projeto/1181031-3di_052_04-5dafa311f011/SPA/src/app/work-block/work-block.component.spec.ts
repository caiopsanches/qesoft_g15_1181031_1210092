import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { WorkBlockComponent } from './work-block.component';
import { WorkblockService } from './workblock.service';

describe('WorkBlockComponent', () => {
  let component: WorkBlockComponent;
  let workblockService : WorkblockService;
  let fixture: ComponentFixture<WorkBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkBlockComponent ],
      providers : [
        {
          provide: WorkblockService, useValue:{
            addWorkBlock: () => of({
              number: 12,
              duration: 3, 
              vehicleduty: "100"
            })
          }
        }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkBlockComponent);
    component = fixture.componentInstance;
    workblockService = TestBed.get(WorkblockService)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
