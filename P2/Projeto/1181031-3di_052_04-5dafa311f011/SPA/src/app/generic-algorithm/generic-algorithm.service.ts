import { HttpClient, HttpParams } from '@angular/common/http';
import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import {catchError, retry, map, tap } from 'rxjs/operators';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import IVehicleDutyDTO from '../dto/IVehicleDutyDTO';
@Injectable({
  providedIn: 'root'
})
export class GenericAlgorithmService {

  constructor(private http: HttpClient) { }
  rootPlan = "/plan"
  MDV ="/mdv"
  getSolution(duty: string, nrGenerations: number , dimension: number , crossing:number ,mutation:number) {
    const params = new HttpParams()
      .set('duty', duty)
      .set('nrGenerations', nrGenerations.toString())
      .set('dimension',dimension.toString())
      .set('crossing',crossing.toString())
      .set('mutation',mutation.toString())
    return this.http.get<string>(this.rootPlan + '/genetic', { params }).pipe(map(response => { return response }))
  }
  getAllVehicleDuties(): Observable<IVehicleDutyDTO[]> {
    return this.http.get<IVehicleDutyDTO[]>(this.MDV + '/VehicleDuty').pipe(map(response => { return response }))
  }
}
