import { Component, Input, OnInit } from '@angular/core';
import { GenericAlgorithmService } from './generic-algorithm.service';

@Component({
  selector: 'app-generic-algorithm',
  templateUrl: './generic-algorithm.component.html',
  styleUrls: ['./generic-algorithm.component.css']
})
export class GenericAlgorithmComponent implements OnInit {

  constructor(private geneticService: GenericAlgorithmService) { }

  @Input() vehicleService: string;
  @Input() nrGenerations: number;
  @Input() dimension: number;
  @Input() crossing: number;
  @Input() mutation: number;

  vehicleServicesDsiplay = new Array();
  lAux = new Array<string>();
  lAux1 = new Array<{ number: string, color: string }>();

  ngOnInit(): void {
    document.title = "GenericAlgorithm"

    this.geneticService.getAllVehicleDuties().subscribe(vehicleduty => {
      for (let vd of vehicleduty) {
        this.vehicleServicesDsiplay.push(vd.description)
      }
      console.log(this.vehicleServicesDsiplay)
    })
  }

  checkSplit(str: string) {
    for (let x of this.lAux) {
      let temp = x.split(":")
      if (temp[0] == str) {
        return temp[1]
      }
    }
    return "null"
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  calculateGenericAlgorithm() {
    this.geneticService.getSolution(this.vehicleServicesDsiplay[0], this.nrGenerations, this.dimension, this.crossing, this.mutation).subscribe(data => {
      this.lAux1 = new Array<{ number: string, color: string }>();
      for (let n of data) {
        let res = this.checkSplit(n.toString())
        if (res == "null") {
          var color = this.getRandomColor()
          console.log(color)
          this.lAux.push(n.toString() + ":" + color)
          this.lAux1.push({ number: n.toString(), color: color })
        } else {
          this.lAux1.push({ number: n.toString(), color: res })
        }
      }
    })
  }
}