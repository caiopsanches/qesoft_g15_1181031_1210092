import ILinePathDTO from './ILinePathDTO';

export default interface ILineDTO {
    lineId: string;
    name: string;
    colour: string;
    paths: Array<ILinePathDTO>;
    allowedVehicles: Array<string>;
    allowedDrivers: Array<string>
}