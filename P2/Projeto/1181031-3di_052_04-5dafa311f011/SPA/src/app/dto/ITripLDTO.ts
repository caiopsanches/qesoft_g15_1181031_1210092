import IPassingTimeDTO from './IPassingTimeDTO';

export default interface ITripLDTO {
    tripName: {name: number};
    lineId: {lineId: string};
    routeId: {routeId: string};
    isEmpty: boolean;
    passingTimes: Array<IPassingTimeDTO>;
    orinetaiton: {orient: string};
}