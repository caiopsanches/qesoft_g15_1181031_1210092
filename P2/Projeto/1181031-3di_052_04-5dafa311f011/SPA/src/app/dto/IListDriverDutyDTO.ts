import IWorkBlockDTO from "./IWorkBlockDTO";

export default interface IListDriverDutyDTO {
    identifier: number,
    workblocks: Array<IWorkBlockDTO>
}