export default interface IPassingTimeDTO {
    time: { time: number };
    nodeId: { nodeId: number };
    reliefP: boolean;
}