export default interface IInputLoginDTO {
    username: string
    password: string
}