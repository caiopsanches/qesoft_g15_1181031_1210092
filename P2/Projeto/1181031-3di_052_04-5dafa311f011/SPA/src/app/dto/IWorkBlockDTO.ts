export default interface IWorkBlockDTO { 
    startTime: string
    finishTime: string
    beginNode: string
    finishNode: string;
    name: string; 
    id:  string
}