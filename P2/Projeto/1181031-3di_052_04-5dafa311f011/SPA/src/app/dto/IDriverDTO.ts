import ICrewmemberDTO from './ICrewmemberDTO';

export default interface IDriverDTO { 
     driverCC: string;
     driverDate: string;
     driverEntry: string;
     driverExit: string;
     driverMecNumber: string;
     driverName: string;
     driverNIF: string;
     driverLicenseExpiry: string;
     driverLicenseNumber: string;
     listDriverType: Array<string>;
}