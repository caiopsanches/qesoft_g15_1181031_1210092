import IUserDTO from "./IUserDTO";

export default interface ILoginDTO {
    token: string;
    user: IUserDTO;
}