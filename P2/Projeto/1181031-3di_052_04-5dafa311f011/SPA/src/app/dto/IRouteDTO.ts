import ISegmentDTO from './ISegmentDTO';

export default interface IRouteDTO {
    routeId: string;
    name: string;
    idNoInicio: string;
    idNoFim: string;
    networkSegments: Array<ISegmentDTO>;
}