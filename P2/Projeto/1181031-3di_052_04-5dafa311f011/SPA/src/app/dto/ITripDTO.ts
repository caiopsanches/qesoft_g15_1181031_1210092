export default interface ITripDTO {
    lineId: string;
    routeId: string;
    startTime: number;
    finishTime: number;
    nrTrips: number;
    frenquency: number;
    nrTripsParallel: number;
}