export default interface ICrewmemberDTO { 
    code: string,
    description: string
}