export default interface IInputDefineVehicleDTO {
    id: {value: string}
    VehicleTypeId: string,
    matricula: {matricula : string},
    vin: string,
    serviceDate: string,
}