export default interface DriverAffectionDTO {
    number: string;
    color: string;
}