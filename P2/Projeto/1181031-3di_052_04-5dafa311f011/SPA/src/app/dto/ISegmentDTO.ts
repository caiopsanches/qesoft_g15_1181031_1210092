export default interface ISegmentDTO {
    segmentId: string,
    idNo1: string;
    idNo2: string;
    distance: number;
    deslocationTime: number;
}