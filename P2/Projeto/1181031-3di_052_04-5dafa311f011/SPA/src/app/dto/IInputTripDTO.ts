export default interface IInputTripDTO { 
    lineId: {lineId: string}
    routeId: {routeId: string}
    passingTimes: [{time:{time:number}}]
    orientation: string
    id: {value: string}
}