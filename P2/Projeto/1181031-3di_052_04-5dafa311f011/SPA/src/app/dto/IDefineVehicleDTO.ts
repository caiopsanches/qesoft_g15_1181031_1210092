export default interface IDefineVehicleDTO {
    VehicleTypeId: string,
    matricula: string,
    vin: string,
    serviceDate: string,
}