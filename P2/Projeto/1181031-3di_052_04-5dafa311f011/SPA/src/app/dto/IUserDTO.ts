

export default interface IUserDTO {
    id: string;
    nome: string;
    username: string;
    password: string;
    email: string;
    nif: Number;
    endereco: string;
    role: string;
} 