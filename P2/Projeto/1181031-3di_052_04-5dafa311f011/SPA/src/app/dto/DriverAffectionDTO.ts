import DriverAffectionAuxDTO from './DriverAffectionAuxDTO';

export default interface DriverAffectionDTO {
    driverDuty: string;
    solution: Array<DriverAffectionAuxDTO>;
}