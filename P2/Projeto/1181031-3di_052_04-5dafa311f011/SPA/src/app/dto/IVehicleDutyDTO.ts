export default interface IVehicleDutyDTO {
    id: string,
    description: string,
    beginDutyDate: number,
    endDutyDate: number,
    beginNode: string,
    endNode: string,
    tripList: string[]
}