import { TestBed } from '@angular/core/testing';
import { RouteService } from './route.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import IRouteDTO from '../dto/IRouteDTO';


describe('test service route', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let routeService: RouteService;

    beforeEach(() => {
        //Configures testing app module
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                RouteService
            ]
        });

        //Instantaites HttpClient, HttpTestingController and EmployeeService
        httpClient = TestBed.inject(HttpClient);
        httpTestingController = TestBed.inject(HttpTestingController);
        routeService = TestBed.inject(RouteService);
    });

    afterEach(() => {
        httpTestingController.verify(); //Verifies that no requests are outstanding.
    });

    it('should be created', () => {
        expect(routeService).toBeTruthy();
    });

    //test case add
    it('should add an route and return it', () => {
        const newRoute: IRouteDTO = {
            routeId: "1", name: "Paredes",
            idNoInicio: "CETE", idNoFim: "VALADARES",
            networkSegments: [
                {
                    segmentId: "111",
                    idNo1: "CETE",
                    idNo2: "RECAREI",
                    distance: 21,
                    deslocationTime: 213,
                },
                {
                    segmentId: "112",
                    idNo1: "RECAREI",
                    idNo2: "PARADA",
                    distance: 212,
                    deslocationTime: 2113,
                }
            ]
        };

        routeService.addRoute(newRoute).subscribe(
            data => expect(data).toEqual(newRoute, 'should return the route'),
            fail
        );


        // addEmploye should have made one request to POST employee
        const req = httpTestingController.expectOne(routeService.rootURL + "/route");
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toEqual(newRoute);

        // Expect server to return the employee after POST
        const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newRoute });
        req.event(expectedResponse);
    });

    it('get all the routes', () => {
        const routes = [
            {
                routeId: "1", name: "Paredes",
                idNoInicio: "CETE", idNoFim: "VALADARES",
                networkSegments: [
                    {
                        segmentId: "111",
                        idNo1: "CETE",
                        idNo2: "RECAREI",
                        distance: 21,
                        deslocationTime: 213,
                    },
                    {
                        segmentId: "112",
                        idNo1: "RECAREI",
                        idNo2: "PARADA",
                        distance: 212,
                        deslocationTime: 2113,
                    }
                ]

            }];

        routeService.getRoute('1').subscribe(g => {
            expect(g.length).toBe(1);
            expect(g).toEqual(routes);
        });

        const req = httpTestingController.expectOne(`${routeService.rootURL}/route?lineId=1`);
        expect(req.request.method).toBe("GET");
        req.flush(routes);
    });
});