import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import IRouteDTO from '../dto/IRouteDTO';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class RouteService {

  constructor(private http: HttpClient) { }
  rootURL = '/api';

  addRoute(data: IRouteDTO) {
    console.log(data.networkSegments);
    return this.http.post(this.rootURL + '/route', data)
  }

  getRoute(lineId: string): Observable<IRouteDTO[]> {
    const params = new HttpParams().set('lineId', lineId)
    return this.http.get<IRouteDTO[]>(this.rootURL + '/route', { params }).pipe(map(response => { return response }))
  }

  getAllRoutes(): Observable<IRouteDTO[]> {
    return this.http.get<IRouteDTO[]>(this.rootURL + '/route').pipe(map(response => { return response }))
  }
}