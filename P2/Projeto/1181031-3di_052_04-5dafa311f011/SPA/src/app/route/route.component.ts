import { Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';
import ISegmentDTO from '../dto/ISegmentDTO'
import IRouteDTO from '../dto/IRouteDTO';
import { RouteService } from './route.service';
import { NodeService } from '../node/node.service';
import { LineService } from '../line/line.service';


@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.css']
})

export class RouteComponent implements OnInit {

  route: IRouteDTO
  constructor(private routeService: RouteService, private lineService: LineService, private nodeService: NodeService) {
  }

  resultado: string;
  resultado1: string;

  @Input() routeId: string;
  @Input() name: string;
  @Input() idNoInicio: string;
  @Input() idNoFim: string;
  @Input() segmentId: string;
  @Input() idNo1: string;
  @Input() idNo2: string;
  @Input() distance: number;
  @Input() deslocationTime: number;
  @Input() lineId: string;

  nodesDisplay = new Array();
  linesDisplay = new Array();
  routes: IRouteDTO[] = [];
  segments: ISegmentDTO[] = [];
  headers = ["Id Route", "Nome", "Nó Início", "Nó Fim", "Segmentos"]
  headers1 = ["Segment Id", "Id Nó 1", "Id Nó 2", "Distância", "Tempo Deslocação"]

  ngOnInit(): void {
    document.title = "Route"

    this.nodeService.getNodes().subscribe(nodes => {
      for (let node of nodes) {
        this.nodesDisplay.push(node.shortName)
      }
    })

    this.lineService.getLines().subscribe(lines => {
      for (let line of lines) {
        this.linesDisplay.push(line.lineId)
      }
    })
  }

  reset() {
    this.segments = [];
  }

  addSegments() {
    this.segments.push({
      segmentId: this.segmentId,
      idNo1: this.idNo1,
      idNo2: this.idNo2,
      distance: this.distance,
      deslocationTime: this.deslocationTime,
    })
    //RESET INPUT
    this.segmentId = "";
    this.idNo1 = '';
    this.idNo2 = '';
    this.distance = 0;
    this.deslocationTime = 0;
  }


  addRoute() {
    this.routeService.addRoute({
      routeId: this.routeId,
      name: this.name,
      idNoInicio: this.idNoInicio,
      idNoFim: this.idNoFim,
      networkSegments: this.segments,
    } as IRouteDTO).subscribe((response) => {
      this.resultado = "Route adicionada!"
    },
      (error) => {
        this.resultado = "Não foi possível adicionar a route, reveja os dados!"
      });
  }

  getRoutesByLineId() {
    this.routes = []
    this.routeService.getRoute(this.lineId).subscribe(routeS => {
      for (let route of routeS) {
        this.routes.push(route)
      }
    },
    (error) => {
      this.resultado1 = "Selecione um id linha válido!"
    });
  }
}