import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { HttpClientModule } from '@angular/common/http';
import { VehicleComponent } from './vehicle/vehicle.component';
import { CrewmemberComponent } from './crewmember/crewmember.component';
import { NodeComponent } from './node/node.component';
import { LineComponent } from './line/line.component';
import { ApiComponent } from './api/api.component';
import { FormsModule } from '@angular/forms';
import { ImportComponent } from './import/import.component';
import { RouteComponent } from './route/route.component';
import { ChangecrewmemberComponent } from './changecrewmember/changecrewmember.component';
import { WorkBlockComponent } from './work-block/work-block.component';
import { RegistoClienteComponent } from './Homepage/registoCliente/registoCliente.component';
import { RGPDComponent } from './Homepage/rgpd/rgpd.component';
import { LoginComponent } from './Homepage/homepage/homepage.component';
import { TripComponent } from './trip/trip.component';
import { DefineVehicleComponent } from './DefineVehicle/defineVehicle.component';
import { DriverComponent } from './driver/driver.component';
import { GenericAlgorithmComponent } from './generic-algorithm/generic-algorithm.component';
import { VehicleDutyComponent } from './vehicleDuty/vehicleDuty.component'
import { DriverDutyComponent } from './driverDuty/driverDuty.component'
import { AdminLayoutComponent } from './DataAdmin/AdminLayoutComponent';
import { ClientLayoutComponent } from './Client/clientLayoutComponent';
import { TriplistComponent } from './triplist/triplist.component';
import { DriveraffectionComponent } from './driveraffection/driveraffection.component';


@NgModule({
  declarations: [
    AdminLayoutComponent,
    ClientLayoutComponent,
    AppComponent,
    LoginComponent,
    MapComponent,
    VehicleComponent,
    CrewmemberComponent,
    NodeComponent,
    LineComponent,
    ApiComponent,
    ImportComponent,
    RouteComponent,
    ChangecrewmemberComponent,
    WorkBlockComponent,
    RegistoClienteComponent,
    RGPDComponent,
    TripComponent,
    DefineVehicleComponent,
    DriverComponent,
    GenericAlgorithmComponent,
    VehicleDutyComponent,
    DriverDutyComponent,
    TriplistComponent,
    DriveraffectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
