import { DefineVehicleService } from './defineVehicle.service';
import defineVehicleDTO from "../dto/IDefineVehicleDTO"
import { VehicleService } from '../vehicle/vehicle.service';
import { Output, Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'app-defineVehicle',
    templateUrl: './defineVehicle.component.html',
    styleUrls: ['./defineVehicle.component.css']
})

export class DefineVehicleComponent implements OnInit {

    defineVehicleDto: defineVehicleDTO
    constructor(private defineVehicleService: DefineVehicleService, private vehicleService: VehicleService) { }

    resultado: string

    @Input() VehicleTypeId: string;
    @Input() matricula: string;
    @Input() vin: string;
    @Input() serviceDate: string;

    vehiclesDisplay = new Array();

    ngOnInit(): void {

        document.title = "DefineVehicle"

        this.vehicleService.getVehicles().subscribe(vehicles => {
            for (let vehicle of vehicles) {
                this.vehiclesDisplay.push(vehicle.code)
            }
            console.log(this.vehiclesDisplay)
        })

    }

    defineVehicle() {
        this.defineVehicleService.defineVehicle({
            VehicleTypeId: this.VehicleTypeId,
            matricula: this.matricula,
            vin: this.vin,
            serviceDate: this.serviceDate,

        } as defineVehicleDTO).subscribe((response) => {
            this.resultado = "Veículo definido com sucesso!"
        },
            (error) => {                              //Error callback
                this.resultado = "Não foi possível definir o veículo, reveja os dados!";
            })
    }

}