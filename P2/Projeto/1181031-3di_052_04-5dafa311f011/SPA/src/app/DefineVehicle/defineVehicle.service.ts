import { Injectable } from '@angular/core';
import IDefineVehicleDTO from '../dto/IDefineVehicleDTO';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
import IInputDefineVehicleDTO from '../dto/IInputDefineVehicleDTO'

@Injectable({
    providedIn: 'root'
})
export class DefineVehicleService {
    constructor(private http: HttpClient) { }
    rootURL: string = "/mdv"


    defineVehicle(data: IDefineVehicleDTO) {
        console.log(data);
        return this.http.post(this.rootURL + '/Vehicle', data)
    }

    getdefineVehicles(): Observable<IInputDefineVehicleDTO[]> {
        return this.http.get<IInputDefineVehicleDTO[]>(this.rootURL + '/Vehicle').pipe(map(response => { return response }))
    }

}