import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import IDefineVehicleDTO from '../dto/IDefineVehicleDTO';

import { DefineVehicleService } from './defineVehicle.service';

describe('DefineVehicleService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let vehicleservice: DefineVehicleService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DefineVehicleService]
        });

        //Instantaites HttpClient, HttpTestingController and EmployeeService
        httpClient = TestBed.inject(HttpClient);
        httpTestingController = TestBed.inject(HttpTestingController);
        vehicleservice = TestBed.inject(DefineVehicleService);
    });

    afterEach(() => {
        httpTestingController.verify(); //Verifies that no requests are outstanding.
    });

    it('should be created', () => {
        expect(vehicleservice).toBeTruthy();
    });

    //Test case 1
    it('should define vehicle and return it', () => {
        const newVehicle: IDefineVehicleDTO = {
            VehicleTypeId: "Autocarro",
            matricula: "12-AS-63",
            vin: "4Y1SL65848Z411439",
            serviceDate: "2021-01-23",
        };

        vehicleservice.defineVehicle(newVehicle).subscribe(
            data => expect(data).toEqual(newVehicle, 'should return the Vehicle'),
            fail
        );

        // addEmploye should have made one request to POST employee
        const req = httpTestingController.expectOne(vehicleservice.rootURL + "/Vehicle");
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toEqual(newVehicle);

        // Expect server to return the employee after POST
        const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newVehicle });
        req.event(expectedResponse);
    });
});
