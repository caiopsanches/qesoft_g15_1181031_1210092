import { Component, Input, OnInit } from '@angular/core';
import ILineDTO from '../dto/ILineDTO';
import ITripLDTO from '../dto/ITripLDTO';
import { LineService } from '../line/line.service';
import { TripService } from '../trip/trip.service';

@Component({
  selector: 'app-triplist',
  templateUrl: './triplist.component.html',
  styleUrls: ['./triplist.component.css']
})
export class TriplistComponent implements OnInit {

  constructor(private tripService: TripService, private lineService: LineService) { }

  @Input() lineIdAux: string;

  headers = ["Nome Viagem", "Id Linha", "Id Percurso", "Em Vazio", "Tempos Passagem", "Orientação"]
  headers1 = ["Tempo", "Id Nó", "Ponto de Troca"]
  trips: ITripLDTO[] = [];
  linesDisplay = new Array();
  lines = new Array<ILineDTO>();
  resultado1: string;

  ngOnInit(): void {
    document.title = "Trip"

    this.lineService.getLines().subscribe(lines => {
      for (let line of lines) {
        this.lines.push(line)
        this.linesDisplay.push(line.lineId)
      }
    })
  }

  getTripsByLineId() {
    this.trips = []
    this.tripService.getTripsByLineId(this.lineIdAux).subscribe(trips => {
      for (let trip of trips) {
        this.trips.push(trip)
      }
      if (!this.lineIdAux) {
        this.resultado1 = "Selecione um id linha válido!"
      }
      if (this.lineIdAux && this.trips.length == 0) {
        this.resultado1 = "Não existem viagens para a linha indicada!"
      }
    });
  }
}
