import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import IInputTripDTO from '../dto/IInputTripDTO';
import TripDTO from '../dto/ITripDTO';
import ITripLDTO from '../dto/ITripLDTO';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(private http: HttpClient) { }
  rootURL = '/mdv';

  addTrip(data: TripDTO) {
    return this.http.post(this.rootURL + '/Trips', data)
  }
  getTrips(): Observable<IInputTripDTO[]> {
    return this.http.get<IInputTripDTO[]>(this.rootURL + '/Trips').pipe(map(response => { return response }))
  }

  getTripsByLineId(lineId:string): Observable<ITripLDTO[]>{
    return this.http.get<ITripLDTO[]>(this.rootURL+ '/Trips/'+ lineId).pipe(map(response => { return response }))
  }
}
