import { Component, Input, OnInit } from '@angular/core';
import ILineDTO from '../dto/ILineDTO';
import IRouteDTO from '../dto/IRouteDTO';
import TripDTO from '../dto/ITripDTO';
import ITripLDTO from '../dto/ITripLDTO';
import { LineService } from '../line/line.service';
import { RouteService } from '../route/route.service';
import { TripService } from './trip.service';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {

  trip: TripDTO;
  constructor(private tripService: TripService, private lineService: LineService, private routeService: RouteService) { }

  @Input() lineId: string;
  @Input() lineIdAux: string;
  @Input() routeId: string;
  @Input() startTime: string;
  @Input() finishTime: string;
  @Input() nrTrips: number;
  @Input() frenquency: number;
  @Input() nrTripsParallel: number;

  headers = ["Nome Viagem", "Id Linha", "Id Percurso", "Em Vazio", "Tempos Passagem", "Orientação"]
  headers1 = ["Tempo", "Id Nó", "Ponto de Troca"]

  resultado: string;
  resultado1: string;
  route: IRouteDTO
  trips: ITripLDTO[] = [];
  linesDisplay = new Array();
  lines = new Array<ILineDTO>();
  routesAux = new Array<IRouteDTO>();
  routesDisplay = new Array();
  time: number

  ngOnInit(): void {
    document.title = "Trip"

    this.lineService.getLines().subscribe(lines => {
      for (let line of lines) {
        this.lines.push(line)
        this.linesDisplay.push(line.lineId)
      }
    })
  }

  getRoutes() {
    var lineAux: ILineDTO;
    for (let line of this.lines) {
      if (line.lineId == this.lineId) {
        lineAux = line;
      }
    }

    this.routesDisplay = new Array();

    this.routeService.getAllRoutes().subscribe(routes => {
      for (let route of routes) {
        this.routesAux.push(route)
        for (var i = 0; i < lineAux.paths.length; i++) {
          if (lineAux.paths[i].routeId == route.routeId) {
            this.routesDisplay.push(route.routeId)
          }
        }
      }
    })
  }

  calculateTime() {
    this.time = 0;
    for (let r of this.routesAux) {
      if (r.routeId == this.routeId) {
        this.route = r
      }
    }

    for (let x of this.route.networkSegments) {
      console.log(x.deslocationTime)
      this.time = +x.deslocationTime + +this.time
    }
  }

  addTrip() {
    var hini = this.convertHHMMToSeconds(this.startTime)
    var hend = this.convertHHMMToSeconds(this.finishTime)
    var freq = this.frenquency * 60
    if (hini < hend) {
      this.tripService.addTrip({
        lineId: this.lineId,
        routeId: this.routeId,
        startTime: hini,
        finishTime: hend,
        nrTrips: this.nrTrips,
        frenquency: freq,
        nrTripsParallel: this.nrTripsParallel
      } as TripDTO).subscribe((response) => {
        this.resultado = "Viagem adicionada com sucesso!"
      },
        (error) => {                              //Error callback
          this.resultado = "Não foi possível adicionar a viagem, reveja os dados!";
        })
    }
  }

  convertHHMMToSeconds(s: string): number {
    var aux = s.split(':'); // split it at the colons

    var seconds = ((+aux[0]) * 60 * 60) + (+aux[1] * 60);
    return seconds
  }

  getTripsByLineId() {
    this.trips = []
    this.tripService.getTripsByLineId(this.lineIdAux).subscribe(trips => {
      for (let trip of trips) {
        this.trips.push(trip)
      }
      if(!this.lineIdAux){
        this.resultado1 = "Selecione um id linha válido!"
      }
      if(this.lineIdAux && this.trips.length==0){
        this.resultado1 = "Não existem viagens para a linha indicada!"
      }
    });
  }
}
