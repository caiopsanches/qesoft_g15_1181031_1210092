import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import IPassingTimeDTO from '../dto/IPassingTimeDTO';
import ITripDTO from '../dto/ITripDTO';
import ITripLDTO from '../dto/ITripLDTO';

import { TripService } from './trip.service';

describe('TripService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let tripService: TripService;

  beforeEach(() => {
    //Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        TripService
      ]
    });

    //Instantaites HttpClient, HttpTestingController and EmployeeService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    tripService = TestBed.inject(TripService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(tripService).toBeTruthy();
  });

  //Test case 1
  it('should add an Trip and return it', () => {
    const newTrip: ITripDTO = {
      lineId: "Line:1",
      routeId: "3",
      startTime: 28800,
      finishTime: 36000,
      nrTrips: 4,
      frenquency: 1800,
      nrTripsParallel: 0
    };

    tripService.addTrip(newTrip).subscribe(
      data => expect(data).toEqual(newTrip, 'should return the Trip'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(tripService.rootURL + "/Trips");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(newTrip);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newTrip });
    req.event(expectedResponse);
  });

    //Test case 2
    it('should get Trips by lineId', () => {
      const passingTime: IPassingTimeDTO = {
        time: { time: 20000 },
        nodeId: { nodeId: 2 },
        reliefP: true,
      }
      let passingTimes = new Array<IPassingTimeDTO>();
      passingTimes.push(passingTime);

      const newTrip: ITripLDTO = {
        tripName: {name: 1},
        lineId: {lineId: "Line:1"},
        routeId: {routeId: "3"},
        isEmpty: false,
        passingTimes: passingTimes,
        orinetaiton: {orient: "Go"},
      };
      let trips = new Array<ITripLDTO>();
      trips.push(newTrip);
  
      tripService.getTripsByLineId("Line:1").subscribe(
        data => expect(data).toEqual(trips, 'should return the Trips with the same lineId'),
        fail
      );
  
      // addEmploye should have made one request to POST employee
      const req = httpTestingController.expectOne(tripService.rootURL + "/Trips/Line:1");
      expect(req.request.method).toEqual('GET');
     // expect(req.request.body).toEqual(newTrip);
  
      // Expect server to return the employee after POST
      const expectedResponse = new HttpResponse({ status: 201, statusText: 'Returned', body: trips });
      req.event(expectedResponse);
    });
});
