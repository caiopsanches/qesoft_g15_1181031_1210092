import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { NodeService } from './node.service'
import  INodeDTO from '../dto/INodeDTO'

describe('NodeService', () => {
  let httpClient: HttpClient
  let httpTestingController: HttpTestingController
  let service: NodeService

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        NodeService
      ]
    })

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(NodeService);
  })

  afterEach(() => {
    httpTestingController.verify()
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a node and return it', () => {
    const newNode: INodeDTO = {
      shortName: "TEST",
      nodeName: "Complete Test",
      maxTimeStop: 50,
      vehicleCapacity: 3,
      latitude: 41.53783,
      longitude: -8.6478538,
      isDepot: false,
      isReliefPoint: true
    };

    service.addNode(newNode).subscribe(
      data => expect(data).toEqual(newNode, 'should return the node'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(service.rootURL+"/node");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(newNode);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newNode });
    req.event(expectedResponse)
  })

  it('get all nodes', () => {
    const nodes = [
      {
        shortName: "TEST",
        nodeName: "Complete Test",
        maxTimeStop: 50,
        vehicleCapacity: 3,
        latitude: 41.53783,
        longitude: -8.6478538,
        isDepot: false,
        isReliefPoint: true
      },
      {
        shortName: "TEST2",
        nodeName: "Complete second Test",
        maxTimeStop: 30,
        vehicleCapacity: 2,
        latitude: 41.970533,
        longitude: -8.0145648,
        isDepot: true,
        isReliefPoint: false
      }
    ];

    service.getNodes().subscribe(g => {
      expect(g.length).toBe(2)
      expect(g).toEqual(nodes)
    })

    const req = httpTestingController.expectOne(`${service.rootURL}/node`)
    expect(req.request.method).toBe("GET")
    req.flush(nodes)
  })
})
