import { Component, OnInit, Input } from '@angular/core';
import { NodeService } from './node.service';
import INodeDTO from '../dto/INodeDTO'

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.css']
})

export class NodeComponent implements OnInit {

  node: Node
  constructor(private nodeService: NodeService) { }

  //@Input() nodes: any[];
  @Input() shortName: string
  @Input() nodeName: string
  @Input() maxTimeStop: number
  @Input() vehicleCapacity: number
  @Input() latitude: number
  @Input() longitude: number
  @Input() isDepot: boolean
  @Input() isReliefPoint: boolean
  result: string;


  headers = ["Abreviatura", "Nome Completo", "Tempo Máximo de Paragem", "Capacidade de Veículos", "Latitude", "Longitude", "Estação de Recolha", "Ponto de Rendição"]
  orders = ["Abreviatura", "Nome Completo", "Tempo Máximo de Paragem", "Capacidade de Veículos", "Latitude", "Longitude"]
  listNodes : INodeDTO[]

  @Input() order: string
  @Input() shortNameFilter: string
  @Input() nodeNameFilter: string


  ngOnInit(): void {
    document.title = "Node"
    this.order = ""
    this.shortNameFilter = ""
    this.nodeNameFilter = ""
  }

  onSubmit() {
    if (this.isDepot === undefined)
      this.isDepot = false

    if (this.isReliefPoint === undefined)
      this.isReliefPoint = false
    
    this.nodeService.addNode({
      shortName: this.shortName,
      nodeName: this.nodeName,
      maxTimeStop: this.maxTimeStop,
      vehicleCapacity: this.vehicleCapacity,
      latitude: this.latitude,
      longitude: this.longitude,
      isDepot: this.isDepot,
      isReliefPoint: this.isReliefPoint
    } as INodeDTO).subscribe(response => {
      this.result = "Nó Criado com Sucesso!"
    }, error => {
      this.result = "Erro em criar o nó!"
    })

  }

  getNodes() {
    this.listNodes = []
    let order
    switch (this.order) {
      case "Abreviatura":
        order = "shortName"
        break
      
      case "Nome Completo":
        order = "nodeName"
        break

      case "Tempo Máximo de Paragem":
        order = "maxTimeStop"
        break
      
      case "Capacidade de Veículos":
        order = "vehicleCapacity"
        break

      case "Latitude":
        order = "latitude"
        break

      case "Longitude":
        order = "longitude"
        break

      default:
        order = ""
        break
    }

    this.nodeService.getNodesFilter(order, this.shortNameFilter, this.nodeNameFilter).subscribe(nodes => this.listNodes = nodes)
    console.log(this.listNodes)
  }
}