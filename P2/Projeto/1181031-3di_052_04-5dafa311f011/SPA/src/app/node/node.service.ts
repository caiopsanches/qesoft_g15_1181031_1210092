import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import NodeDTO from '../dto/INodeDTO'
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class NodeService {

  constructor(private http: HttpClient) { }
  rootURL = '/api'

  addNode(data: NodeDTO) {
    console.log(data)
    return this.http.post(this.rootURL + '/node', data)
  }

  getNodesFilter(order : string, shortName : string, nodeName : string): Observable<NodeDTO[]> {
    console.log(order)
    return this.http.get<NodeDTO[]>(this.rootURL + '/node?order=' + order + "&shortName=" + shortName + "&nodeName=" + nodeName).pipe(map(response => { return response }))
  }

  getNodes(): Observable<NodeDTO[]> {
    return this.http.get<NodeDTO[]>(this.rootURL + '/node').pipe(map(response => { return response }))
  }
}
