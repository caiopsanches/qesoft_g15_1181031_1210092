import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { NodeComponent } from './node.component';
import { NodeService } from './node.service';

describe('NodeComponent', () => {
  let component: NodeComponent
  let nodeService: NodeService
  let fixture: ComponentFixture<NodeComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeComponent ],
      providers: [
        {
          provide: NodeService, useValue: {
            addNode: () => of({
              shortName: "TEST",
              nodeName: "Complete Test",
              maxTimeStop: 50,
              vehicleCapacity: 3,
              latitude: 41.53783,
              longitude: -8.6478538,
              isDepot: false,
              isReliefPoint: true
            })
          }
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeComponent)
    component = fixture.componentInstance
    nodeService = TestBed.get(NodeService)
    fixture.detectChanges()
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
