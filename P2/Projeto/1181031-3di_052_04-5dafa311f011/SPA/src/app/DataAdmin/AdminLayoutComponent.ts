import { FormGroup, FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { from, Subject } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';
import { AppService } from '../app.service';

@Component({
    selector: 'app-root',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminLayoutComponent implements OnDestroy {
    title = 'front';

    constructor(private appService: AppService) { }

    destroy$: Subject<boolean> = new Subject<boolean>();
    onSubmit() {


    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }
}