import { HttpClient, HttpParams } from '@angular/common/http';
import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import {catchError, retry, map, tap } from 'rxjs/operators';
import { Subscriber, Observable, of, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ChangecrewmemberService {

  constructor(private http: HttpClient) { }
  rootPlan = "/plan"

  getSolution(horario: string, no1: string, no2: string, tipo: string) {
    const params = new HttpParams()
      .set('horario', horario)
      .set('no1', no1)
      .set('no2', no2)
      .set('tipo', tipo)
      
    return this.http.get<string>(this.rootPlan + '/solution', { params }).pipe(map(response => { return response }))
  }
}
