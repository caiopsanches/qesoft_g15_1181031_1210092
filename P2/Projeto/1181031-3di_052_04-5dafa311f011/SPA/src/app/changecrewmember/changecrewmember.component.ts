import { Component, Input, OnInit } from '@angular/core';
import { ChangecrewmemberService } from './changecrewmember.service';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { NodeService } from '../node/node.service';
@Component({
  selector: 'app-changecrewmember',
  templateUrl: './changecrewmember.component.html',
  styleUrls: ['./changecrewmember.component.css']
})
export class ChangecrewmemberComponent implements OnInit {

  constructor(private changeCrewService: ChangecrewmemberService, private nodeService: NodeService) { }

  nosDisplay = new Array();

  ngOnInit(): void {
    document.title = "Change Crewmember"

    this.nodeService.getNodes().subscribe(nodes => {
      for (let node of nodes) {
        this.nosDisplay.push(node.shortName)
      }
    })
  }

  caminho = new Array();
  path = new Array()

  @Input() tipo: string;
  @Input() horario: string;
  @Input() no1: string;
  @Input() no2: string;

  getSolution() {
    let i
    if (this.tipo === "gerador") {
      this.changeCrewService.getSolution(this.horario, this.no1, this.no2, this.tipo).subscribe(data => {
        for (i = 0; i < data.length; i++) {
          this.caminho.push(data[i])
        }
      })
    } else if (this.tipo === "astar") {
      this.changeCrewService.getSolution(this.horario, this.no1, this.no2, this.tipo).subscribe(data => {
          this.path.push(data)
      })
    }
  }
}
