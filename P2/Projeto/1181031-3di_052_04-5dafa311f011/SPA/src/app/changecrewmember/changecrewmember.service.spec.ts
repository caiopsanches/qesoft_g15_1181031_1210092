import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { CrewmemberService } from '../crewmember/crewmember.service';
import ICrewmemberDTO from '../dto/ICrewmemberDTO';

import { ChangecrewmemberService } from './changecrewmember.service';

describe('ChangecrewmemberService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ChangecrewmemberService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ChangecrewmemberService
      ]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ChangecrewmemberService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //Test case 1
  it('should get the best path', () => {
    const newCrewmember: ICrewmemberDTO = {
      code: "crew62",
      description: "desc1"
    };

    var horario = "25000"
    var no1 = "SOBRO"
    var no2 = "PARED"
    var tipo = "gerador"

    let res = [["SOBRO", "CRIST", 22], ["CRIST", "LORDL", 26],["LORDL", "PARED", 9]]

    service.getSolution(horario,no1,no2,tipo).subscribe(
      data => expect(data),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne( service.rootPlan + "/solution?horario=25000&no1=SOBRO&no2=PARED&tipo=gerador");
    expect(req.request.method).toEqual('GET');
    expect(req.request.body).toEqual(null);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: 'SOBRO,CRIST,22,CRIST,LORDL,26,LORDL,PARED,9' });
    req.event(expectedResponse);
  });
});
