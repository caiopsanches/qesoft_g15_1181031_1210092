import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import UserDTO from '../../dto/IUserDTO'
import { Observable } from 'rxjs';
import { Subscriber, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
import ILoginDTO from 'src/app/dto/ILoginDTO';

@Injectable({
  providedIn: 'root'
})
export class RegistoClienteService {
  constructor(private http: HttpClient) { }
  rootURL = '/api';

  addCliente(data: UserDTO) {
    return this.http.post(this.rootURL + '/cliente', data)
  }
  getCliente(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(this.rootURL + 'cliente').pipe(map(response => { return response }))
  }

  deleteCliente(username: string) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        'username': username,

      }
    }


    return this.http.delete(this.rootURL + '/cliente', options).subscribe(s => {

    })
  }
}