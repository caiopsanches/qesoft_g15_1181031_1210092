import { Routes } from '@angular/router';
import { RegistoClienteComponent } from './registoCliente.component';
import { ROUTES_RGPD } from '../rgpd/rgpd.routing';


export const ROUTES_REGISTO_CLIENTE: Routes = [
    { path: '', component: RegistoClienteComponent },
    { path: 'rgpd', children: ROUTES_RGPD },
   

];