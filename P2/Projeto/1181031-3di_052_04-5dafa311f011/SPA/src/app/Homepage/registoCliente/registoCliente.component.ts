import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import UserDTO from '../../dto/IUserDTO';
import { RegistoClienteService } from './registoCliente.service';


@Component({
    selector: 'app-cliente',
    templateUrl: './registoCliente.component.html',
    styleUrls: ['./registoCliente.component.css']
})


export class RegistoClienteComponent implements OnInit {

    user: UserDTO
    constructor(private registoClienteService: RegistoClienteService, private router: Router) { }


    //@Input() users: any[];
    @Input() primeironome: string;
    @Input() ultimonome: string;
    @Input() username: string;
    @Input() password: string;
    @Input() email: string;
    @Input() nif: Number;
    @Input() endereco: string;
    @Input() aceitaReg: boolean

    resultado: string;


    ngOnInit(): void {
        document.title = "cliente"
    }

    addCliente() {

        const nomeE = this.primeironome + ' ' + this.ultimonome;

        if (this.aceitaReg === undefined) {
            this.aceitaReg = false;
        }

        if (this.aceitaReg == true) {
            this.registoClienteService.addCliente({
                nome: nomeE,
                username: this.username,
                password: this.password,
                email: this.email,
                nif: this.nif,
                endereco: this.endereco,
                role: 'Cliente',
            } as UserDTO).subscribe(response => {
                this.resultado = "User Criado com Sucesso!"
            }, error => {
                this.resultado = "Erro em criar o user!"
            })
        } else {
            this.resultado = "Erro em criar o user! É necessário que aceite o Regulamento da Proteção de Dados"
        }

    }

    goBack() {
        this.router.navigateByUrl('/login');
    }

}