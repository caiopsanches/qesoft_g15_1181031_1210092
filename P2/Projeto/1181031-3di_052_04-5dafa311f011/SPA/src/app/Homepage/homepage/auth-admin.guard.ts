import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
    providedIn: 'root'
})
export class AuthAdminGuard implements CanActivate {

    constructor(private loginService: LoginService, private router: Router) {
    }

    canActivate(): boolean {
        if (this.loginService.adminIsLoggedIn() && this.loginService.getLogedAdminRole() === 'DataAdmin') {
            return true;
        } else {
            this.router.navigate(['/homepage']);
            return false;
        }
    }

}
