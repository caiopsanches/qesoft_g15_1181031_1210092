import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import LoginDTO from '../../dto/ILoginDTO'
import { Observable } from 'rxjs';
import { Subscriber, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
import IInputLoginDTO from 'src/app/dto/IInputLoginDTO';
import ILoginDTO from '../../dto/ILoginDTO';


@Injectable({
    providedIn: 'root'
})

export class LoginService {
    constructor(private http: HttpClient) { }
    rootURL = '/api';
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        observe: 'response'
    };


    public login(data: IInputLoginDTO): Observable<LoginDTO> {

        return this.http.post<LoginDTO>(this.rootURL + '/login', data).pipe(response => { return response });

    }

    Clientelogout() {
        localStorage.setItem('user', '');
        localStorage.setItem('username', '');
    }

    AdminLogout() {
        localStorage.setItem('admin', '');
    }

    isLoggedIn(): boolean {
        return !!localStorage.getItem('user');
    }

    adminIsLoggedIn(): boolean {
        return !!localStorage.getItem('admin');
    }

    getLogedUser() {
        return localStorage.getItem('user');
    }

    getLogedAdmin() {
        return localStorage.getItem('admin');
    }

    getLogedUserRole(): string {
        let v = localStorage.getItem('user');
        const user: LoginDTO = JSON.parse(v == null ? "" : v);
        return user.user.role;
    }

    getLogedAdminRole(): string {
        const user: LoginDTO = JSON.parse(localStorage.getItem('admin') || '{}');
        return user.user.role;
    }

    getUsernameToDelete(): string {
        let v = localStorage.getItem('username');
        const user: string = JSON.parse(v == null ? "" : v);
        console.log(user)
        return user;
    }
}