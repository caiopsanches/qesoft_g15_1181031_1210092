import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DadosUserService {

    constructor() { }

    getLogedUser() {
        return localStorage.getItem('user');
    }

    getLogedAdmin() {
        return localStorage.getItem('admin');
    }

}
