import { Routes } from '@angular/router';
import { ROUTES_ADMIN, ROUTES_CLIENT } from 'src/app/app-routing.module';
import { ClientLayoutComponent } from 'src/app/Client/clientLayoutComponent';
import { AdminLayoutComponent } from 'src/app/DataAdmin/AdminLayoutComponent';
import { LoginComponent } from '../homepage/homepage.component';
import { ROUTES_REGISTO_CLIENTE } from '../registoCliente/registoCliente.routing';
import { AuthAdminGuard } from './auth-admin.guard';
import { AuthGuard } from './auth.guard';


export const ROUTES_LOGIN: Routes = [
    { path: '', component: LoginComponent },
    { path: 'registoCliente', children: ROUTES_REGISTO_CLIENTE, },
    { path: 'data-admin', component: AdminLayoutComponent, children: ROUTES_ADMIN, },
    { path: 'client', component: ClientLayoutComponent, children: ROUTES_CLIENT, },
];