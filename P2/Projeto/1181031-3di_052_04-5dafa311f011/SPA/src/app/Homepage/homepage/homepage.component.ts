import { Component, Input, OnInit } from "@angular/core";
import LoginDTO from '../../dto/ILoginDTO'
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import IInputLoginDTO from "src/app/dto/IInputLoginDTO";

@Component({
    selector: 'app-login',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})

export class LoginComponent implements OnInit {

    loginDTO: LoginDTO;

    constructor(private loginService: LoginService, private router: Router) { }

    //@Input() users: any[];
    @Input() username: string;
    @Input() password: string;

    resultado: string;

    ngOnInit() {
        if (this.loginService.adminIsLoggedIn()) {
            this.router.navigateByUrl('/data-admin');
        }
        else if (this.loginService.isLoggedIn()) {
            this.router.navigateByUrl('/client');
        }
        //  localStorage.setItem('user', '');
        //  localStorage.setItem('admin', '');
        //  localStorage.setItem('gestor', '');
    }

    onSubmit() {
        this.loginService.login({ username: this.username, password: this.password } as IInputLoginDTO).subscribe(response => {

            if (response.user.role === 'Cliente') {
                this.router.navigateByUrl('/client');
                localStorage.setItem('user', JSON.stringify(response.token));
                localStorage.setItem('username', JSON.stringify(response.user.username));
            } else if (response.user.role === 'DataAdmin') {
                this.router.navigateByUrl('/data-admin');
                localStorage.setItem('admin', JSON.stringify(response.token));
            }
        }, (err => {
            console.log("erro" + err)
            this.resultado = 'Credenciais Inválidas';
            alert(this.resultado);
        }));

    }
}
