import { Routes } from '@angular/router';
import { RGPDComponent } from '../rgpd/rgpd.component';



export const ROUTES_RGPD: Routes = [
    { path: '', component: RGPDComponent },
    { path: 'registoCliente', redirectTo: 'registoCliente' },
    { path: 'homepage', redirectTo: 'homepage' },
];