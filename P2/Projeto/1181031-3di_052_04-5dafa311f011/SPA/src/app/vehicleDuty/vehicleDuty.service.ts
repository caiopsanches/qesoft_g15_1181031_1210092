import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/internal/operators/map'
import IVehicleDutyDTO from '../dto/IVehicleDutyDTO'
import IOutputVehicleDutyDTO from '../dto/IOutputVehicleDutyDTO'

@Injectable({
  providedIn: 'root'
})
export class VehicleDutyService {

  constructor(private http: HttpClient) { }
  rootURL = '/mdv';

  addVehicleDuty(data: IVehicleDutyDTO) {
    console.log(data)
    return this.http.post(this.rootURL + '/VehicleDuty', data)
  }

  getVehicleDuties() {
    return this.http.get<IOutputVehicleDutyDTO[]>(this.rootURL + '/VehicleDuty').pipe(map(response => { return response }))
  }
}
