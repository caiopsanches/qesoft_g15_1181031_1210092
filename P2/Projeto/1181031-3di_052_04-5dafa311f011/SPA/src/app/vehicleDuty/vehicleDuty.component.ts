import { Component, Input, OnInit } from '@angular/core'
import IInputDefineVehicleDTO from '../dto/IInputDefineVehicleDTO'
import INodeDTO from '../dto/INodeDTO'
import IInputTripDTO from '../dto/IInputTripDTO'
import { DefineVehicleService } from '../DefineVehicle/defineVehicle.service'
import { NodeService } from '../node/node.service'
import { TripService } from '../trip/trip.service'
import { VehicleDutyService } from './vehicleDuty.service'
import IVehicleDutyDTO from '../dto/IVehicleDutyDTO'
import IOutputVehicleDutyDTO from '../dto/IOutputVehicleDutyDTO'

@Component({
  selector: 'app-vehicleDuty',
  templateUrl: './vehicleDuty.component.html',
  styleUrls: ['./vehicleDuty.component.css']
})
export class VehicleDutyComponent implements OnInit {

  constructor(private vehicleDutyService: VehicleDutyService, private vehicleService: DefineVehicleService, private tripService: TripService, private nodeService: NodeService) { }

  vehicles: IInputDefineVehicleDTO[]
  trips: IInputTripDTO[]
  nodes: Array<INodeDTO>

  presentationTrips: Array<string>
  resultado: string

  insertedTrips: string[]

  @Input() description: string
  @Input() beginDutyDate: string
  @Input() endDutyDate: string
  @Input() startNode: string
  @Input() endNode: string
  @Input() trip: string


  headers = ["Descrição", "Hora Início", "Hora Fim", "Nó Início", "Nó Fim", "Nome das Viagens"]
  listVehicleDuties: IOutputVehicleDutyDTO[]

  ngOnInit(): void {
    document.title = "VehicleDuty"

    this.insertedTrips = new Array<string>()
    this.presentationTrips = new Array<string>()
    this.nodes = new Array<INodeDTO>()

    this.nodeService.getNodes().subscribe(nodes => {
      console.log(nodes)
      for (let item of nodes) {
        if (item.isDepot)
          this.nodes.push(item)
      }
    })
    this.vehicleService.getdefineVehicles().subscribe(vehicles => this.vehicles = vehicles)
    this.tripService.getTrips().subscribe(trips => {
      this.trips = trips
      let i = 0
      for (let item of trips) {
        this.presentationTrips.push(i + ". " + item.lineId.lineId + "> " + item.routeId.routeId + "> " + item.passingTimes[0].time.time)
        i++
      }
    })

  }

  addTrip() {
    let array = this.trip.split(".")
    this.insertedTrips.push(this.trips[parseInt(array[0])].id.value)
    this.trips.splice(parseInt(array[0]))
    // for (let item of this.trips) {
    //   if (item.lineId.lineId == array[0] && item.routeId.routeId == array[1] && item.passingTimes[0].time.time == Number.parseInt(array[2])) {
    //     this.insertedTrips.push(item.id.value)
    //     this.trip = ''
    //     return
    //   }
    // }
  }

  onSubmit() {

    var startTime = this.convertHHMMToSeconds(this.beginDutyDate)
    var endTime = this.convertHHMMToSeconds(this.endDutyDate)

    this.vehicleDutyService.addVehicleDuty(
      {
        description: this.description,
        beginDutyDate: startTime,
        endDutyDate: endTime,
        beginNode: this.startNode,
        endNode: this.endNode,
        tripList: this.insertedTrips
      } as IVehicleDutyDTO).subscribe((response) => {
        this.resultado = "Serviço de viatura adicionado com sucesso!"
      },
        (error) => {
          this.resultado = "Não foi possível adicionar o serviço de viatura, reveja os dados!";
        })
  }

  convertHHMMToSeconds(s: string): number {
    var aux = s.split(':'); // split it at the colons

    var seconds = ((+aux[0]) * 60 * 60) + (+aux[1] * 60);
    return seconds
  }

  getVehicleDuties() {
    this.vehicleDutyService.getVehicleDuties().subscribe(vehicleDuties => this.listVehicleDuties = vehicleDuties)
  }
}
