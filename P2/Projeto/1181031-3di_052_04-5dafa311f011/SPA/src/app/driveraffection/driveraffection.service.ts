import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import {catchError, retry, map, tap } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
})
export class AffectionService {

    constructor(private http: HttpClient) { }
    rootPlan = "/plan"

    getSolution():Observable<any> {
        return this.http.get<any>(this.rootPlan + '/affect').pipe(map(response => { return response }))
 }
}
