import { Component, OnInit } from '@angular/core';
import DriverAffectionDTO from '../dto/DriverAffectionDTO';
import { AffectionService } from './driveraffection.service'
@Component({
  selector: 'app-driveraffection',
  templateUrl: './driveraffection.component.html',
  styleUrls: ['./driveraffection.component.css']
})
export class DriveraffectionComponent implements OnInit {

  constructor(private affectionService: AffectionService) { }

  lAux = new Array<string>();
  lAux1 = new Array<{ number: string, color: string }>();
  listaResultado = new Array<[number, Array<number>]>()
  listaProcessada = new Array<DriverAffectionDTO>()
  colorAux = "#f4711d"


  ngOnInit(): void {
    document.title = "DriverAffection"

    this.listaResultado.push([8, [11018, 18050, 17673, 276, 11018, 18050, 276, 1461, 11018, 11018, 17630, 11018, 1461, 11018, 1461, 11018]]);
    this.listaResultado.push([10, [18097, 18097, 11692, 1640, 1640, 11692, 11692, 18119, 11692, 1640, 11692, 11692, 18009, 1640]])
    this.listaResultado.push([6, [6616, 889, 889, 889, 6616, 889, 5188, 5188, 889, 6616, 18131, 6616, 889, 5188]])
  }

  checkSplit(str: string) {
    for (let x of this.lAux) {
      let temp = x.split(":")
      if (temp[0] == str) {
        return temp[1]
      }
    }
    return "null"
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  calculateDriverAffection() {
    console.log("this is aux" + this.affectionService.getSolution().subscribe(res => {
      for (let elem of res) {
        console.log(elem)
      }
    }))
    for (let driverduty of this.listaResultado) {
      this.lAux = []
      this.lAux1 = []
      for (let resultado of driverduty[1]) {
        let res = this.checkSplit(resultado.toString())
        if (res == "null") {
          var color = this.getRandomColor()
          this.lAux.push(resultado.toString() + ":" + color)
          this.lAux1.push({ number: resultado.toString(), color: color })
        } else {
          this.lAux1.push({ number: resultado.toString(), color: res })
        }
      }
      this.listaProcessada.push({
        driverDuty: driverduty[0].toString(),
        solution: this.lAux1
      } as DriverAffectionDTO)
    }
    console.log(this.listaProcessada)
  }
}
