import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApiComponent } from './api/api.component';
import { MapComponent } from './map/map.component';
import { NodeComponent } from './node/node.component';
import { VehicleComponent } from './vehicle/vehicle.component'
import { CrewmemberComponent } from './crewmember/crewmember.component'
import { ImportComponent } from './import/import.component';
import { LineComponent } from './line/line.component';
import { RouteComponent } from './route/route.component';
import { ChangecrewmemberComponent } from './changecrewmember/changecrewmember.component';
import { WorkBlockComponent } from './work-block/work-block.component';
import { RegistoClienteComponent } from './Homepage/registoCliente/registoCliente.component';
import { RGPDComponent } from './Homepage/rgpd/rgpd.component';
import { LoginComponent } from './Homepage/homepage/homepage.component';
import { TripComponent } from './trip/trip.component';
import { DefineVehicleComponent } from './DefineVehicle/defineVehicle.component';
import { DriverComponent } from './driver/driver.component';
import { GenericAlgorithmComponent } from './generic-algorithm/generic-algorithm.component';
import { VehicleDutyComponent } from './vehicleDuty/vehicleDuty.component';
import { DriverDutyComponent } from './driverDuty/driverDuty.component';
import { AdminLayoutComponent } from './DataAdmin/AdminLayoutComponent';
import { AuthAdminGuard } from './Homepage/homepage/auth-admin.guard';
import { AuthGuard } from './Homepage/homepage/auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { ClientLayoutComponent } from './Client/clientLayoutComponent';
import { DeleteAccountComponent } from './DeleteAccount/delete.component';
import { TriplistComponent } from './triplist/triplist.component';
import { DriveraffectionComponent } from './driveraffection/driveraffection.component';


export const ROUTES_ADMIN: Routes = [
  { path: 'map', component: MapComponent },
  { path: 'import', component: ImportComponent },
  { path: 'changeCrewmember', component: ChangecrewmemberComponent },
  { path: 'vehicle', component: VehicleComponent },
  { path: 'crewmember', component: CrewmemberComponent },
  { path: 'node', component: NodeComponent },
  { path: 'line', component: LineComponent },
  { path: 'route', component: RouteComponent },
  { path: 'workBlock', component: WorkBlockComponent },
  { path: 'trip', component: TripComponent },
  { path: 'defineVehicle', component: DefineVehicleComponent },
  { path: 'driver', component: DriverComponent },
  { path: 'genericAlgorithm', component: GenericAlgorithmComponent },
  { path: 'driverAffection', component: DriveraffectionComponent },
  { path: 'vehicleDuty', component: VehicleDutyComponent },
  { path: 'crewmemberDuty', component: DriverDutyComponent },
  { path: 'logout', component: LogoutComponent },
]

export const ROUTES_CLIENT: Routes = [
  { path: 'map', component: MapComponent },
  { path: 'triplist', component: TriplistComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'DeleteAccount', component: DeleteAccountComponent },
]

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'client', component: ClientLayoutComponent, children: ROUTES_CLIENT },
  { path: 'data-admin', component: AdminLayoutComponent, children: ROUTES_ADMIN },
  { path: 'import', component: ImportComponent },
  { path: 'changeCrewmember', component: ChangecrewmemberComponent },
  { path: 'vehicle', component: VehicleComponent },
  { path: 'crewmember', component: CrewmemberComponent },
  { path: 'node', component: NodeComponent },
  { path: 'line', component: LineComponent },
  { path: 'route', component: RouteComponent },
  { path: 'workBlock', component: WorkBlockComponent },
  { path: 'cliente', component: RegistoClienteComponent },
  { path: 'rgpd', component: RGPDComponent },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'map', component: MapComponent },
  { path: 'trip', component: TripComponent },
  { path: 'defineVehicle', component: DefineVehicleComponent },
  { path: 'driver', component: DriverComponent },
  { path: 'genericAlgorithm', component: GenericAlgorithmComponent },
  { path: 'vehicleDuty', component: VehicleDutyComponent },
  { path: 'crewmemberDuty', component: DriverDutyComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full' },

]

RouterModule.forRoot(routes, { useHash: true })


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
