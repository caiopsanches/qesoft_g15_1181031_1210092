import { ComponentFixture, TestBed } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { VehicleComponent } from './vehicle.component';
import { VehicleService } from './vehicle.service';
import { of } from 'rxjs';

describe('VehicleComponent', () => {
  let component: VehicleComponent
  let vehicleService: VehicleService;
  let fixture: ComponentFixture<VehicleComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleComponent],

      providers: [
        {
          provide: VehicleService, useValue: {
            addVehicle: () => of({
              code: "Vei1",
              energy: "Diesel", autonomy: 4, kmCost: 5, Consumption: 5, speed: 100
            })
          }
        }],
    }
    )
      .compileComponents();

  });
  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleComponent);
    component = fixture.componentInstance;
    vehicleService = TestBed.get(VehicleService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
