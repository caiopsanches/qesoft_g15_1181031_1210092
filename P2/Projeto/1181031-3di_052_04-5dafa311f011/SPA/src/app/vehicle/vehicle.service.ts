import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import IVehicleDTO from '../dto/IVehicleDTO';
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient) { }
  rootURL = '/api';
  addVehicle(data: IVehicleDTO) {
    return this.http.post(this.rootURL + '/vehicle', data)
  }

  getVehicles(): Observable<IVehicleDTO[]> {
    return this.http.get<IVehicleDTO[]>(this.rootURL + '/vehicle').pipe(map(response => { return response }))
  }
}

