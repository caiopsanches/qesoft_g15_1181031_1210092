import { Output, Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';
import { VehicleService } from './vehicle.service'
import VehicleDTO from '../dto/IVehicleDTO'
import { Location } from '@angular/common';
@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})

export class VehicleComponent implements OnInit {


  constructor(private vehicleService: VehicleService) {
  }
  resultado: string;

  @Input() code: string;
  @Input() energy: string;
  @Input() autonomy: number;
  @Input() kmCost: number;
  @Input() consumption: number;
  @Input() speed: number;

  ngOnInit(): void {
    document.title = "Vehicle"
  }

  addVehicle() {

    this.vehicleService.addVehicle({

      code: this.code,
      energy: this.energy,
      autonomy: this.autonomy,
      kmCost: this.kmCost,
      avgSpeed: this.speed,
      avgConsumption: this.consumption
    } as VehicleDTO).subscribe((response) => {
      this.resultado = "Veiculo adicionado!"
    },
      (error) => {                              //Error callback
        this.resultado = "Não foi possível adicionar o veiculo, reveja os dados!";
      })
  }
}