import { TestBed } from '@angular/core/testing';
import { VehicleService } from './vehicle.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import IVehicleDTO from '../dto/IVehicleDTO';

describe('VehicleService test', () => {
  let service: VehicleService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    //Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        VehicleService
      ]
    });

    //Instantaites HttpClient, HttpTestingController and EmployeeService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(VehicleService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //Test case 1
  it('should add a vehicle and return it', () => {
    const vehicle: IVehicleDTO = {
      id: "Vei1",
      code: "Vei1",
      energy: "Diesel", autonomy: 4, kmCost: 5, avgSpeed: 5, avgConsumption: 100
    }

    service.addVehicle(vehicle).subscribe(
      data => expect(data).toEqual(vehicle, 'should return the line'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(service.rootURL + "/vehicle");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(vehicle);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: vehicle });
    req.event(expectedResponse);
  });

  it('get all vehicles', () => {
    const vehicles = [
      {
        id: "vehicle1",
        code: "vehicle1",
        energy: "Gasolina",
        autonomy: 12,
        kmCost: 24,
        avgConsumption: 30,
        avgSpeed: 35
      },
      {
        id: "vehicle2",
        code: "vehicle2",
        energy: "Gás",
        autonomy: 21,
        kmCost: 22,
        avgConsumption: 35,
        avgSpeed: 30
      }
    ];

    service.getVehicles().subscribe(g => {
      expect(g.length).toBe(2)
      expect(g).toEqual(vehicles)
    })

    const req = httpTestingController.expectOne(`${service.rootURL}/vehicle`)
    expect(req.request.method).toBe("GET")
    req.flush(vehicles)
  })
});