import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../Homepage/homepage/login.service';
import { RegistoClienteService } from '../Homepage/registoCliente/registoCliente.service';


@Component({
    selector: 'app-logout',
    templateUrl: './delete.component.html',
    styleUrls: ['./delete.component.scss']
})
export class DeleteAccountComponent implements OnInit {

    constructor(private loginService: LoginService, private router: Router, private userService: RegistoClienteService) { }

    ngOnInit() {
    }
    resultado = 'Os seus dados foram removidos permanentemente, vamos sentir a sua falta!';
    delete(): void {

        alert(this.resultado);
        var username = this.loginService.getUsernameToDelete();
        this.userService.deleteCliente(username);
        this.loginService.Clientelogout();
        this.router.navigateByUrl('/login');
    }
}