import { DriverDutyService } from './driverDuty.service'
import { Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, NgControlStatus } from '@angular/forms';
import DriverDutyDTO from '../dto/IDriverDutyDTO'
import WorkBlockDTO from '../dto/IWorkBlockDTO'
import { WorkblockService } from '../work-block/workblock.service';
import IListDriverDutyDTO from '../dto/IListDriverDutyDTO';

@Component({
    selector: 'app-driverDuty',
    templateUrl: './driverDuty.component.html',
    styleUrls: ['./driverDuty.component.css']
})

export class DriverDutyComponent implements OnInit {

    driverDuty: DriverDutyDTO
    workblocks: WorkBlockDTO[]
    constructor(private driverDutyService: DriverDutyService, private workBlockService: WorkblockService) {
    }
    resultado: string;
    resultado1: string;
    @Input() code: string;
    @Input() name: string;
    @Input() startTime: string;
    @Input() finishTime: string;
    @Input() beginNode: string;
    @Input() finishNode: string;
    @Input() wb: string;
    @Input() listWBs: string;

    headers = ["Código", "Bloco de Trabalho"]
    headers1 = ["Id", "Tempo Início", "Tempo Final", "Nó Início", "Nó Fim"]

    listWB1: Array<string>

    wbDisplay: Array<WorkBlockDTO>

    servicesT: IListDriverDutyDTO[] = [];
    
    ngOnInit(): void {
        document.title = "CrewmemberDuty"

        this.listWB1 = new Array<string>()
        this.wbDisplay = new Array<WorkBlockDTO>()
        this.workBlockService.getAllWBs().subscribe(workblocks => {

            let sorted = workblocks.sort(x => parseInt(x.name))
            for (let wb of sorted) {
                this.wbDisplay.push(wb)
            }

        })
    }

    addWBs() {
        let idWB = this.listWBs.substr(0, this.listWBs.indexOf(' '));
        for (let item of this.workblocks) {
            if (item.name == idWB.trim())
                this.listWB1.push(item.id)
        }
        this.listWBs = ''
    }

    addDriverDuty() {
        this.driverDutyService.addDriverDuty({
            id: this.code,
            wbList: this.listWB1
        } as DriverDutyDTO).subscribe((response) => {
            this.resultado = "Serviço de Tripulante adicionado!"
            this.listWB1 = new Array<string>()
            this.code = ''
        },
            (error) => {                              //Error callback
                this.resultado = "Não foi possível adicionar o Serviço de Tripulante, reveja os dados!";
            })
    }


    getServicesT() {
        this.servicesT = []
        this.driverDutyService.getDriversDuty1().subscribe(crewsS => {
            for (let crews of crewsS) {
                this.servicesT.push(crews)
            }
            if (this.servicesT.length == 0) {
                this.resultado1 = "Não existem Serviços de Tripulante para listar"
            }
           
        });
        console.log(this.servicesT);
    }

}