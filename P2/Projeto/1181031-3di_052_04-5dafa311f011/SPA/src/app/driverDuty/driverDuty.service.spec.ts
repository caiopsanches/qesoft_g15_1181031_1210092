import { TestBed } from '@angular/core/testing';
import { DriverDutyService } from './driverDuty.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import  IDriverDutyDTO  from '../dto/IDriverDutyDTO';

describe('DriverDutyService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let driverDutyservice: DriverDutyService;

  beforeEach(() => {
    //Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        DriverDutyService
      ]
    });

    //Instantaites HttpClient, HttpTestingController and EmployeeService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    driverDutyservice = TestBed.inject(DriverDutyService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(driverDutyservice).toBeTruthy();
  });

  //Test case 1
  it('should add an driverDuty and return it', () => {
    const newDriverDuty: IDriverDutyDTO = {
      id: "532",
      wbList: ["wb1"]
    };

    driverDutyservice.addDriverDuty(newDriverDuty).subscribe(
      data => expect(data).toEqual(newDriverDuty, 'should return the driverDuty'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(driverDutyservice.rootURL+"/crewmemberDuty");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(newDriverDuty);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newDriverDuty });
    req.event(expectedResponse);
  });

  it('get all driverDuty', () => {
    const driverDutys = [
      {
        id: "543",
        wbList: ["wb1"],
      },
      {
        id: "542",
        wbList: ["wb2"],
      }
    ];

    driverDutyservice.getDriversDuty().subscribe(g => {
      expect(g.length).toBe(2)
      expect(g).toEqual(driverDutys)
    })

    const req = httpTestingController.expectOne(`${driverDutyservice.rootURL}/crewmemberDuty`)
    expect(req.request.method).toBe("GET")
    req.flush(driverDutys)
  })
});