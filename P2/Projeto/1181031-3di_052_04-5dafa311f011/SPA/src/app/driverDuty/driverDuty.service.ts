import { Injectable } from '@angular/core';
import IDriverDutyDTO from '../dto/IDriverDutyDTO';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Subscriber, Observable, of, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
import IListDriverDutyDTO from '../dto/IListDriverDutyDTO';

@Injectable({
    providedIn: 'root'
})
export class DriverDutyService {
    constructor(private http: HttpClient) { }
    rootURL: string = "/mdv"
    p: string

    addDriverDuty(data: IDriverDutyDTO) {
        console.log(data);
        return this.http.post(this.rootURL + '/crewmemberDuty', data)
    }

    getDriversDuty(): Observable<IDriverDutyDTO[]> {
        return this.http.get<IDriverDutyDTO[]>(this.rootURL + '/crewmemberDuty').pipe(map(response => { return response }))
    }

    getDriversDuty1(): Observable<IListDriverDutyDTO[]> {
        return this.http.get<IListDriverDutyDTO[]>(this.rootURL + '/crewmemberDuty').pipe(map(response => { return response }))
    }

}