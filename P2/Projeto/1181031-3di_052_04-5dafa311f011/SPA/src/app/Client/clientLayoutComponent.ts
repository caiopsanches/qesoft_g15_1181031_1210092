import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { AppService } from '../app.service';

@Component({
    selector: 'app-root',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css']
})
export class ClientLayoutComponent implements OnDestroy {
    title = 'front';

    constructor(private appService: AppService) { }

    destroy$: Subject<boolean> = new Subject<boolean>();
    onSubmit() {


    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }
}