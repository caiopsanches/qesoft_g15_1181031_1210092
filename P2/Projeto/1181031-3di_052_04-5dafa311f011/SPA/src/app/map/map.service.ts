import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import IFilesDTO from '../dto/IFilesDTO';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient) { }
  rootURL = '/api'

  getFiles(): Observable<IFilesDTO[]> {
    return this.http.get<IFilesDTO[]>(this.rootURL + '/files').pipe(map(response => { return response }))
  }
}
