import ILineDTO from "../dto/ILineDTO";
import IRouteDTO from "../dto/IRouteDTO";
import ISegmentDTO from "../dto/ISegmentDTO";

export interface SegmentDrawAux {
    color: string
    segment: ISegmentDTO
    route: IRouteDTO
    line: ILineDTO
}