import { Component, OnInit, AfterViewInit, Input } from '@angular/core'
import * as mapboxgl from 'mapbox-gl';
import { MapService } from './map.service'
import { LineService } from '../line/line.service'
import { NodeService } from '../node/node.service'
import { RouteService } from '../route/route.service'
import NodeDTO from '../dto/INodeDTO'
import { SegmentDraw } from './segmentDraw'
import ILineDTO from '../dto/ILineDTO';
import IRouteDTO from '../dto/IRouteDTO';
import { SegmentDrawAux } from './segmentDrawAux';
import * as dat from 'dat.gui';
import * as THREE from 'three';

declare function drawObject(lat: number, long: number, layerId: string, color: any, url: string, spothLight: any): any;
declare function drawObjectDriver(lat: number, long: number, layerId: string, url: string, nodesPosition: Array<[number, number]>): any;
declare function createMap(): mapboxgl.Map;
declare function updateMarkers(map: mapboxgl.Map, elem: any): any;
declare function createPopupNodes(map: mapboxgl.Map, elem: NodeDTO): any;
declare function createPopupLines(map: mapboxgl.Map, id: string, long: number, lat: number, elem: SegmentDrawAux): any;
declare function buildGui(dat: any, name: any): any;
declare function lightCreate(): any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {

  private segments: Map<string, SegmentDraw>
  private lights: Map<string, THREE.SpotLight>
  private nodes: NodeDTO[]
  private nodesPosition: Array<[number, number]>
  private LINESDISTANCE = 0.0005

  private map: mapboxgl.Map;
  private i = 0;

  @Input() glbfile: string;
  @Input() name: string;
  @Input() name1: string;

  check: boolean = false
  gui: dat.GUI

  files: string[] = [];
  chooseNodes: string[] = [];
  nodeAux: NodeDTO

  constructor(private routeService: RouteService,
    private nodeService: NodeService, private mapService: MapService, private lineService: LineService) { }

  async ngOnInit() {
    this.map = createMap();

    document.title = "Map"

    this.map.dragRotate.disable();
    await this.setup()
    this.draw()
    this.centerMap()

    this.mapService.getFiles().subscribe(files => {
      for (let file of files) {
        this.files.push(file.name)
      }
    })
  }

  ngAfterViewInit(): void {
  }

  async getLines(): Promise<ILineDTO[]> {
    let a = await this.lineService.getLines().toPromise()
    return a
  }

  async getRoute(lineId: string): Promise<IRouteDTO[]> {
    let a = await this.routeService.getRoute(lineId).toPromise()
    return a
  }

  async setup(): Promise<number> {
    this.segments = new Map()
    this.nodes = await this.nodeService.getNodes().toPromise()

    for (let node of this.nodes) {
      this.chooseNodes.push(node.nodeName)
    }

    let lineList = await this.getLines()

    for (let line of lineList) {
      let color = line.colour
      let routeList = await this.getRoute(line.lineId)
      for (let route of routeList) {
        for (let segment of route.networkSegments) {
          let str
          let node0 = this.getNode(segment.idNo1)
          let node1 = this.getNode(segment.idNo2)
          if (segment.idNo1 < segment.idNo2)
            str = segment.idNo1 + ":" + segment.idNo2
          else
            str = segment.idNo2 + ":" + segment.idNo1
          let value = this.segments.get(str)
          if (value === undefined) {
            var a = new Set<SegmentDrawAux>()
            var aux = {
              color: color,
              segment: segment,
              route: route,
              line: line
            } as SegmentDrawAux
            a.add(aux)
            this.segments.set(str, {
              latitude0: node0?.latitude, longitude0: node0?.longitude,
              latitude1: node1?.latitude, longitude1: node1?.longitude,
              info: a
            } as SegmentDraw)
          } else {
            var aux = {
              color: color,
              segment: segment,
              route: route,
              line: line
            } as SegmentDrawAux
            value.info.add(aux)
          }
        }
      }
    }
    return Promise.resolve(0)
  }

  centerMap() {
    let somLat = 0
    let somLong = 0
    for (let elem of this.nodes) {
      somLat += elem.latitude
      somLong += elem.longitude
    }

    let lat = somLat / this.nodes.length
    let long = somLong / this.nodes.length
    let lngLat: mapboxgl.LngLatLike = [long, lat];
    this.map.setCenter(lngLat)
  }

  getNode(nodeId: string) {
    for (let elem of this.nodes) {
      if (elem.shortName == nodeId)
        return elem
    }
    return null
  }

  async draw() {
    this.drawSegments()
    this.drawNodes()
  }

  async drawSegments() {
    for (let elem of this.segments) {
      let count = 0
      let r = Math.sqrt(Math.pow(elem[1].latitude0 - elem[1].latitude1, 2) + Math.pow(elem[1].longitude0 - elem[1].longitude1, 2))
      let cosBeta = -(elem[1].latitude1 - elem[1].latitude0) / r
      let sinBeta = (elem[1].longitude1 - elem[1].longitude0) / r
      for (let info of elem[1].info) {
        var pointA = new mapboxgl.LngLat(elem[1].longitude0 + (this.LINESDISTANCE * count) * cosBeta, elem[1].latitude0 + (this.LINESDISTANCE * count) * sinBeta)
        var pointB = new mapboxgl.LngLat(elem[1].longitude1 + (this.LINESDISTANCE * count) * cosBeta, elem[1].latitude1 + (this.LINESDISTANCE * count) * sinBeta)
        this.drawLine(elem[0] + this.i.toString(), info.color, [[pointA.lng, pointA.lat], [pointB.lng, pointB.lat]], info)
        this.i++
        if (count == 0)
          count++
        else if (count > 0)
          count *= -1
        else
          count = Math.abs(count) + 1
      }
    }
  }

  async drawLine(id: string, color: string, list: Array<any>, elem: SegmentDrawAux) {
    this.map.addSource(id, {
      'type': 'geojson',
      'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
          'type': 'LineString',
          'coordinates': list
        }
      }
    });
    this.map.addLayer({
      'id': id,
      'type': 'line',
      'source': id,
      'layout': {
        'line-join': 'round',
        'line-cap': 'round'
      },
      'paint': {
        'line-color': color,
        'line-width': 3
      }
    });

    var pointA = list[0];
    var pointB = list[1];

    var long = ((pointA[0] + pointB[0]) / 2);
    var lat = ((pointA[1] + pointB[1]) / 2);

    createPopupLines(this.map, id, long, lat, elem);
  }

  async drawPoints(id: string, color: string, list: Array<any>) {
    this.map.addSource(id, {
      'type': 'geojson',
      'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
          'type': 'Point',
          'coordinates': list
        }
      }
    });
    this.map.addLayer({
      'id': id,
      'type': 'circle',
      'source': id,
      'paint': {
        // make circles larger as the user zooms from z12 to z22
        'circle-radius': {
          'base': 1.5,
          'stops': [
            [12, 2],
            [22, 180]
          ]
        },
        'circle-color': color
      }
    });
  }

  async drawNodes() {
    let list = [];
    for (let elem of this.nodes) {
      list.push({
        'type': 'Feature',
        'properties': {
          'description': '<p>' + '<b>' + "Nome: " + '</b>' + elem.nodeName + '</p>' +
            '<p>' + '<b>' + "Abreviatura: " + '</b>' + elem.shortName + '</p>' +
            '<p>' + '<b>' + "Estação de Recolha: " + '</b>' + elem.isDepot + '</p>' +
            '<p>' + '<b>' + "Ponto de Rendição: " + '</b>' + elem.isReliefPoint + '</p>' +
            '<p>' + '<b>' + "Latitude: " + '</b>' + elem.latitude + '</p>' +
            '<p>' + '<b>' + "Longitude: " + '</b>' + elem.longitude + '</p>'
        },
        'geometry': {
          'type': 'Point',
          'coordinates': [elem.longitude, elem.latitude]
        }
      })
    }
    updateMarkers(this.map, list);
  }

  public toggleMap3dDefaultBuildingsLayer(): void {
    this.map.dragRotate.enable();
    this.map.dragPan.enable()
    if (this.map.getLayer('places')) {
      this.removeMarkers();
    }
    this.centerMap();
    this.map.setZoom(13)
    this.map.setPitch(60);
    if (this.map.getLayer('condutor')) {
      this.map.removeLayer('condutor')
    }

    //bus object
    this.create3dObject();
    //buildings
    this.create3dDefaultMapboxBuildingsLayer();
  }

  public toggleMap2dDefaultBuildingsLayer(): void {
    this.map.dragPan.enable()
    this.map.dragRotate.disable();
    this.map.setZoom(14);
    this.map.setPitch(0);
    this.centerMap();
    if (this.map.getLayer('condutor')) {
      this.map.removeLayer('condutor')
    }
    if (this.map.getLayer('3d-buildings')) {
      this.map.removeLayer('3d-buildings');
      for (let elem of this.nodes) {
        this.map.removeLayer(elem.nodeName)
        this.map.removeSource(elem.nodeName)
        this.map.removeLayer(elem.shortName)
      }
    }
    //markers
    this.drawNodes();
  }

  public changeNode(): void {
    var color = null
    var url = "https://opt52.herokuapp.com/api/files?name=" + this.glbfile

    for (let node of this.nodes) {
      if (node.nodeName == this.name) {
        this.nodeAux = node
        //draw each point
        if (node.isReliefPoint) {
          color = "red"
        } else {
          color = "blue"
        }
      }
    }

    this.map.removeLayer(this.nodeAux.shortName)
    var spothLight = this.lights.get(this.nodeAux.nodeName)
    var a = drawObject(this.nodeAux.latitude, this.nodeAux.longitude, this.nodeAux.shortName, color, url, spothLight)
    this.map.addLayer(a);
  }

  public async startNavigation() {
    this.nodesPosition = new Array();
    for (let elem of this.nodes) {
      this.nodesPosition.push([elem.longitude, elem.latitude]);
    }
    if (!this.map.getLayer('3d-buildings')) {
      this.toggleMap3dDefaultBuildingsLayer()
    }
    this.map.dragRotate.enable()
    this.map.dragPan.disable()
    this.map.setCenter([-8.3916253, 41.2075057])
    this.map.setZoom(18)
    this.map.setPitch(75)
    var a = drawObjectDriver(41.2075057, -8.3916853, "condutor", "https://opt52.herokuapp.com/api/files?name=car", this.nodesPosition)
    this.map.addLayer(a);
  }

  private create3dObject() {
    var color = null
    this.lights = new Map();
    for (let elem of this.nodes) {
      //draw each point
      if (elem.isReliefPoint) {
        color = "red"
      } else {
        color = "blue"
      }
      this.drawPoints(elem.nodeName, color, [elem.longitude, elem.latitude])
      createPopupNodes(this.map, elem);
      //draw each bus
      var spothLight = lightCreate()
      this.lights.set(elem.nodeName, spothLight)
      var a = drawObject(elem.latitude, elem.longitude, elem.shortName, color, "https://opt52.herokuapp.com/api/files?name=bus", spothLight)
      this.map.addLayer(a);
    }
  }

  public createGUI() {
    if (this.check) {
      this.gui.hide()
    }
    this.gui = new dat.GUI({ autoPlace: false });
    buildGui(this.gui, this.lights.get(this.name1));
    this.check = true;
  }

  private create3dDefaultMapboxBuildingsLayer() {
    this.map.addLayer({
      id: '3d-buildings',
      source: 'composite',
      'source-layer': 'building',
      filter: ['==', 'extrude', 'true'],
      type: 'fill-extrusion',
      minzoom: 5,
      paint: {
        'fill-extrusion-color': '#aaa',

        // use an 'interpolate' expression to add a smooth transition effect to the
        // buildings as the user zooms in
        'fill-extrusion-height': [
          'interpolate', ['linear'], ['zoom'],
          15, 0,
          15.05, ['get', 'height']
        ],
        'fill-extrusion-base': [
          'interpolate', ['linear'], ['zoom'],
          15, 0,
          15.05, ['get', 'min_height']
        ],
        'fill-extrusion-opacity': .6
      }
    });
  }

  private removeMarkers() {
    this.map.removeLayer('places');
    this.map.removeSource('places');
    this.map.removeImage('custom-marker');
  }
}