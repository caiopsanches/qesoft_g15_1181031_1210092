import { SegmentDrawAux } from "./segmentDrawAux";

export interface SegmentDraw {
    latitude0: number
    longitude0: number
    latitude1: number
    longitude1: number
    info: Set<SegmentDrawAux>
}
