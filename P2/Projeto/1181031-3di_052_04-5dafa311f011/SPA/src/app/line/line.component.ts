import { Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';
import { CrewmemberService } from '../crewmember/crewmember.service';
import LineDTO from '../dto/ILineDTO';
import LinePathDTO from '../dto/ILinePathDTO';
import { RouteService } from '../route/route.service';
import { VehicleService } from '../vehicle/vehicle.service';
import { LineService } from './line.service'
@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})

export class LineComponent implements OnInit {

  line: LineDTO
  constructor(private lineService: LineService, private vehicleService: VehicleService,
    private crewService: CrewmemberService, private routeService: RouteService) {
  }

  @Input() lineId: string;
  @Input() name: string;
  @Input() colour: string;
  @Input() routeId: string;
  @Input() orientation: string;
  @Input() codeV: string;
  @Input() codeD: string;
  @Input() order: string;
  @Input() filter: string;

  resultado: string
  routesDisplay = new Array();
  vehiclesDisplay = new Array();
  crewmembersDisplay = new Array();

  paths: LinePathDTO[] = [];
  allowedVehicles: string[] = [];
  allowedDrivers: string[] = [];
  headers = ["Id Linha", "Nome", "Cor", "Percursos", "Veículos Permitidos", "Condutores Permitidos"]
  lines: LineDTO[] = [];

  ngOnInit(): void {
    document.title = "Line"

    this.routeService.getAllRoutes().subscribe(routes => {
      for (let route of routes) {
        this.routesDisplay.push(route.routeId)
      }
    })

    this.crewService.getCrewmembers().subscribe(crews => {
      for (let crew of crews) {
        this.crewmembersDisplay.push(crew.code)
      }
    })

    this.vehicleService.getVehicles().subscribe(vehicles => {
      for (let vehicle of vehicles) {
        this.vehiclesDisplay.push(vehicle.code)
      }
    })
  }

  addPaths() {
    this.paths.push({
      routeId: this.routeId,
      orientation: this.orientation
    })
    //RESET INPUT
    this.routeId = '';
    this.orientation = '';
  }

  addAllowedVehicles() {
    this.allowedVehicles.push(this.codeV);
    //RESET INPUT
    this.codeV = '';
  }

  addAllowedDrivers() {
    this.allowedDrivers.push(this.codeD);
    //RESET INPUT
    this.codeD = '';
  }

  addLine() {
    this.lineService.addLine({
      lineId: this.lineId,
      name: this.name,
      colour: this.colour,
      paths: this.paths,
      allowedVehicles: this.allowedVehicles,
      allowedDrivers: this.allowedDrivers
    } as LineDTO).subscribe((response) => {
      this.resultado = "Linha adicionada com sucesso!"
    },
      (error) => {                              //Error callback
        this.resultado = "Não foi possível adicionar a linha, reveja os dados!";
      })
  }

  getLines() {
    this.lines = []
    this.lineService.getLinesFiltered(this.order, this.filter).subscribe(linesS => {
      for (let line of linesS) {
        this.lines.push(line)
      }
    })
    console.log(this.lines)
  }
}