import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import LineDTO from '../dto/ILineDTO';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LineService {

  constructor(private http: HttpClient) { }
  rootURL = '/api';
  p: string

  addLine(data: LineDTO) {
    return this.http.post(this.rootURL + '/line', data)
  }
  getLines(): Observable<LineDTO[]> {
    return this.http.get<LineDTO[]>(this.rootURL + '/line').pipe(map(response => { return response }))
  }

  getLinesFiltered(order: string, filter: string): Observable<LineDTO[]> {
    if (order && filter) {
      this.p = this.rootURL + "/line?" + order + "=" + filter
    } else if (order) {
      this.p = this.rootURL + "/line?" + order
    } else {
      this.p = this.rootURL + "/line"
    }
    return this.http.get<LineDTO[]>(this.p).pipe(map(response => { return response }))
  }
}