import { TestBed } from '@angular/core/testing';
import { LineService } from './line.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import ILineDTO from '../dto/ILineDTO';

describe('test service line', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let lineService: LineService;

  beforeEach(() => {
    //Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        LineService
      ]
    });

    //Instantaites HttpClient, HttpTestingController and EmployeeService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    lineService = TestBed.inject(LineService);
  });

  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it('should be created', () => {
    expect(lineService).toBeTruthy();
  });

  //Test case 1
  it('should add an line and return it', () => {
    const newLine: ILineDTO = {
      lineId: "1", name: "800Gondomar",
      colour: "RGB(12,12,12)", paths: [
        {
          routeId: "1",
          orientation: "Ida"
        },
        {
          routeId: "2",
          orientation: "Volta"
        }
      ],
      allowedVehicles: ["BMW", "MER"],
      allowedDrivers: ["12", "13"]
    };

    lineService.addLine(newLine).subscribe(
      data => expect(data).toEqual(newLine, 'should return the line'),
      fail
    );

    // addEmploye should have made one request to POST employee
    const req = httpTestingController.expectOne(lineService.rootURL+"/line");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(newLine);

    // Expect server to return the employee after POST
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newLine });
    req.event(expectedResponse);
  });

  it('get all the lines', () => {
    const lines = [
      {
        lineId: "1", name: "800Gondomar",
        colour: "RGB(12,12,12)", paths: [
          {
            routeId: "1",
            orientation: "Go"
          },
          {
            routeId: "2",
            orientation: "Return"
          }
        ],
        allowedVehicles: ["BMW", "MER"],
        allowedDrivers: ["12", "13"]
      },
      {
        lineId: "2", name: "801Gondomar",
        colour: "RGB(1,2,2)", paths: [
          {
            routeId: "3",
            orientation: "Go"
          },
          {
            routeId: "4",
            orientation: "Return"
          }
        ],
        allowedVehicles: ["BMW", "MER"],
        allowedDrivers: ["12", "13"]
      }
    ];

    lineService.getLines().subscribe(g => {
      expect(g.length).toBe(2);
      expect(g).toEqual(lines);
    });

    const req = httpTestingController.expectOne(`${lineService.rootURL}/line`);
    expect(req.request.method).toBe("GET");
    req.flush(lines);
  });

  it('get all the lines filtered', () => {

    var order = "name"
    var filter = "800Gond"
    const line = [
      {
        lineId: "1", name: "800Gondomar",
        colour: "RGB(12,12,12)", paths: [
          {
            routeId: "1",
            orientation: "Go"
          },
          {
            routeId: "2",
            orientation: "Return"
          }
        ],
        allowedVehicles: ["BMW", "MER"],
        allowedDrivers: ["12", "13"]
      }
    ];

    lineService.getLinesFiltered(order, filter).subscribe(g => {
      expect(g.length).toBe(1);
      expect(g).toEqual(line);
    });

    const req = httpTestingController.expectOne(`${lineService.rootURL}/line?`+ order + "=" + filter);
    expect(req.request.method).toBe("GET");
    req.flush(line);
  });
});