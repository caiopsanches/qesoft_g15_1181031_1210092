function createMap() {
    mapboxgl.accessToken = 'pk.eyJ1IjoibWFnbm9saW8iLCJhIjoiY2tqeDhhaTk4MHB0NjJ2bHN4cDNyZ3BhNCJ9.LUmOcGAFEr-cdPN9P1rc7Q';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11', // style URL
        zoom: 11,
        center: [-8.38716802227697, 41.1937898023744]
    });

    return map
}

function updateMarkers(map, list) {
    map.loadImage(
        'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
        // Add an image to use as a custom marker
        function (error, image) {
            if (error) throw error;
            map.addImage('custom-marker', image);

            map.addSource('places', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': list
                }
            });

            // Add a layer showing the places.
            map.addLayer({
                'id': 'places',
                'type': 'symbol',
                'source': 'places',
                'layout': {
                    'icon-image': 'custom-marker',
                    'icon-allow-overlap': true
                }
            });
        }
    );

    // Create a popup, but don't add it to the map yet.
    var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });

    map.on('mouseenter', 'places', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        var coordinates = e.features[0].geometry.coordinates.slice();
        var description = e.features[0].properties.description;

        /*// Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }
        */

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(coordinates).setHTML(description).addTo(map);
    });

    map.on('mouseleave', 'places', function () {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
}

function createPopupNodes(map, elem) {
    var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });

    map.on('mouseenter', elem.nodeName, function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat([elem.longitude, elem.latitude]).setHTML('<p>' + '<b>' + "Nome: " + '</b>' + elem.nodeName + '</p>' +
            '<p>' + '<b>' + "Abreviatura: " + '</b>' + elem.shortName + '</p>' +
            '<p>' + '<b>' + "Estação de Recolha: " + '</b>' + elem.isDepot + '</p>' +
            '<p>' + '<b>' + "Ponto de Rendição: " + '</b>' + elem.isReliefPoint + '</p>' +
            '<p>' + '<b>' + "Latitude: " + '</b>' + elem.latitude + '</p>' +
            '<p>' + '<b>' + "Longitude: " + '</b>' + elem.longitude + '</p>').addTo(map);
    });

    map.on('mouseleave', elem.nodeName, function () {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
}

function createPopupLines(map, id, long, lat, elem) {
    var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });

    map.on('mouseenter', id, function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat([long, lat]).setHTML('<p>' + '<b>' + "Linha: " + '</b>' + elem.line.name + '</p>' +
            '<p>' + '<b>' + "Percurso: " + '</b>' + elem.route.name + '</p>' +
            '<p>' + '<b>' + "Segmento: " + '</b>' + elem.segment.segmentId + '</p>' +
            '<p>' + '<b>' + "Tempo Segmento: " + '</b>' + elem.segment.deslocationTime + '</p>' +
            '<p>' + '<b>' + "Distância Segmento: " + '</b>' + elem.segment.distance + '</p>').addTo(map);
    });

    map.on('mouseleave', id, function () {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
}

function lightCreate() {
    return new THREE.SpotLight(0xffffff, 1);
}

function drawObject(lat, long, layerId, color, url, spotLight) {
    var modelOrigin = [long, lat];
    var modelAltitude = 7;
    var modelRotate = [Math.PI / 2, 0, 0];
    var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
        modelOrigin,
        modelAltitude
    );

    // transformation parameters to position, rotate and scale the 3D model onto the map
    var modelTransform = {
        translateX: modelAsMercatorCoordinate.x,
        translateY: modelAsMercatorCoordinate.y,
        translateZ: modelAsMercatorCoordinate.z,
        rotateX: modelRotate[0],
        rotateY: modelRotate[1],
        rotateZ: modelRotate[2],
        /* Since our 3D model is in real world meters, a scale transform needs to be
        * applied since the CustomLayerInterface expects units in MercatorCoordinates.
        */
        scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
    };

    var THREE = window.THREE;
    var t = 0;
    //var spotLight;

    // configuration of the custom layer for a 3D model per the CustomLayerInterface
    var customLayer = {
        id: layerId,
        type: 'custom',
        renderingMode: '3d',
        onAdd: function (map, gl) {
            this.camera = new THREE.Camera();
            this.scene = new THREE.Scene();

            const ambient = new THREE.AmbientLight(0xffffff, 0.5);
            this.scene.add(ambient);

            //spotLight = new THREE.SpotLight(0xffffff, 1);
            spotLight.position.set(20, 20, 20);
            spotLight.angle = 0.4;
            spotLight.penumbra = 0;
            spotLight.decay = 1;
            //between 1 -- 1.5
            spotLight.distance = 500;

            spotLight.castShadow = true;
            spotLight.shadow.mapSize.width = 2048;
            spotLight.shadow.mapSize.height = 2048;
            spotLight.shadow.camera.near = 0.0000001;
            spotLight.shadow.camera.far = 200;
            spotLight.shadow.focus = 1;

            this.scene.add(spotLight);

            /* HELPER LIGHTS
            lightHelper = new THREE.SpotLightHelper(spotLight);
            this.scene.add(lightHelper);

            shadowCameraHelper = new THREE.CameraHelper(spotLight.shadow.camera);
            this.scene.add(shadowCameraHelper);
            */

            let material = new THREE.MeshPhongMaterial({ color: color });
            let geometry = new THREE.CylinderBufferGeometry(5, 5, 0.3, 70, 1, false);
            let mesh = new THREE.Mesh(geometry, material);
            //mesh.position.set(-0.005, -0.005, -0.005);
            mesh.receiveShadow = true;
            this.scene.add(mesh);

            // use the three.js GLTF loader to add the 3D model to the three.js scene
            var loader = new THREE.GLTFLoader();

            loader.load(url,
                function (gltf) {
                    const mesh = gltf.scene.children[0];
                    mesh.position.y = 0.15;
                    mesh.traverse(child => {
                        if (child.isMesh) {
                            child.castShadow = true;
                        }
                    });
                    this.scene.add(mesh);
                }.bind(this)
            );
            this.map = map;

            // use the Mapbox GL JS map canvas for three.js
            this.renderer = new THREE.WebGLRenderer({
                canvas: map.getCanvas(),
                context: gl,
                antialias: true
            });

            this.flag = true

            this.renderer.autoClear = false;
        },
        render: function (gl, matrix) {

            var rotationX = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(1, 0, 0),
                modelTransform.rotateX
            );

            var rotationY = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(0, 1, 0),
                modelTransform.rotateY
            );

            modelTransform.rotateY += 0.002

            var rotationZ = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(0, 0, 1),
                modelTransform.rotateZ
            );

            /*
            if (modelTransform.translateZ > 0.000003) {
                this.flag = false
            } else if (modelTransform.translateZ < 0.00000005) {
                this.flag = true;
            }

            if (this.flag) {
                modelTransform.translateZ += 0.00000001
            } else {
                modelTransform.translateZ -= 0.00000001
            }
            */
            //ROTATE LIGHT
            t -= 0.007
            // move light in circle around center
            // change light height with sine curve
            var r = 15.0;
            var lx = r * Math.cos(t);
            var lz = r * Math.sin(t);
            var ly = 5.0 + 5.0;
            spotLight.position.set(lx, ly, lz);

            var m = new THREE.Matrix4().fromArray(matrix);
            var l = new THREE.Matrix4()
                .makeTranslation(
                    modelTransform.translateX,
                    modelTransform.translateY,
                    modelTransform.translateZ
                )
                //AUMENTAR O TAMANHO DO OBJETO
                .scale(
                    new THREE.Vector3(
                        modelTransform.scale * 50,
                        -modelTransform.scale * 50,
                        modelTransform.scale * 50
                    )
                )
                .multiply(rotationX)
                .multiply(rotationY)
                .multiply(rotationZ);

            this.camera.projectionMatrix = m.multiply(l);
            this.renderer.state.reset();
            this.renderer.shadowMap.enabled = true;
            this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
            this.renderer.render(this.scene, this.camera);
            this.map.triggerRepaint();
            return true
        }
    };
    return customLayer
}

function drawObjectDriver(lat, long, layerId, url, nodesPosition) {
    var modelOrigin = [long, lat];
    var modelAltitude = 0;
    var modelRotate = [Math.PI / 2, Math.PI / 2, 0];
    var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
        modelOrigin,
        modelAltitude
    );

    // transformation parameters to position, rotate and scale the 3D model onto the map
    var modelTransform = {
        translateX: modelAsMercatorCoordinate.x,
        translateY: modelAsMercatorCoordinate.y,
        translateZ: modelAsMercatorCoordinate.z,
        rotateX: modelRotate[0],
        rotateY: modelRotate[1],
        rotateZ: modelRotate[2],
        /* Since our 3D model is in real world meters, a scale transform needs to be
        * applied since the CustomLayerInterface expects units in MercatorCoordinates.
        */
        scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
    };

    var modelVelocidade = 0;
    var modelDireccao = 90;
    var modelDireccaoRodas = 0;
    var up = false;
    var left = false;
    var right = false;
    var down = false;

    var THREE = window.THREE;

    // configuration of the custom layer for a 3D model per the CustomLayerInterface
    var customLayer = {
        id: layerId,
        type: 'custom',
        renderingMode: '3d',
        onAdd: function (map, gl) {
            this.camera = new THREE.Camera();
            this.scene = new THREE.Scene();

            const ambient = new THREE.AmbientLight(0xffffff, 0.8);
            this.scene.add(ambient);

            document.addEventListener("keydown", onDocumentKeyDown, false);
            document.addEventListener("keyup", onDocumentKeyUp, false);
            function onDocumentKeyDown(event) {
                var keyCode = event.which;
                // up
                if (keyCode == 87) {
                    up = true
                } else if (keyCode == 83) {
                    // down
                    down = true
                } else if (keyCode == 65) {
                    //left
                    left = true
                } else if (keyCode == 68) {
                    // right
                    right = true
                }
            };
            function onDocumentKeyUp(event) {
                var keyCode = event.which;
                // up
                if (keyCode == 87) {
                    up = false
                } else if (keyCode == 83) {
                    // down
                    down = false
                } else if (keyCode == 65) {
                    //left
                    left = false
                } else if (keyCode == 68) {
                    // right
                    right = false
                }
            };

            // use the three.js GLTF loader to add the 3D model to the three.js scene
            var loader = new THREE.GLTFLoader();
            loader.load(url,
                function (gltf) {
                    const mesh = gltf.scene.children[0];
                    mesh.traverse(child => {
                        if (child.isMesh) {
                            child.castShadow = true;
                        }
                    });
                    this.scene.add(mesh);
                }.bind(this)
            );
            this.map = map;

            // use the Mapbox GL JS map canvas for three.js
            this.renderer = new THREE.WebGLRenderer({
                canvas: map.getCanvas(),
                context: gl,
                antialias: true
            });

            this.flag = true
            this.renderer.shadowMap.enabled = true;
            this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
            this.renderer.autoClear = false;
        },
        render: function (gl, matrix) {

            if (up && modelVelocidade < 0.0000003) {
                modelVelocidade += (0.0000003 / 8)
            }
            if (down && modelVelocidade > -0.0000003) {
                modelVelocidade -= (0.0000003 / 8)
            }

            if (!up && !down && modelVelocidade != 0) {
                if (modelVelocidade > 0)
                    modelVelocidade -= (0.0000003 / 50)
                else
                    modelVelocidade += (0.0000003 / 50)
                if (modelVelocidade > (-0.0000003 / 72) && modelVelocidade < (0.0000003 / 72))
                    modelVelocidade = 0;
            }

            if (!right && !left && modelVelocidade != 0 && modelDireccaoRodas != 0) {
                if (modelDireccaoRodas > 0)
                    modelDireccaoRodas -= 3;
                else
                    modelDireccaoRodas += 3;
                if (modelDireccaoRodas > -0.05 && modelDireccaoRodas < 0.05)
                    modelDireccaoRodas = 0;
            }

            if (left && modelDireccaoRodas < 30)
                modelDireccaoRodas += 3;
            if (right && modelDireccaoRodas > -30)
                modelDireccaoRodas -= 3;

            if (modelVelocidade != 0) {
                modelDireccao += modelDireccaoRodas / 10;
            }

            modelTransform.translateX += modelVelocidade * Math.sin(modelDireccao * (Math.PI / 180));
            modelTransform.translateY += modelVelocidade * Math.cos(modelDireccao * (Math.PI / 180));
            modelTransform.rotateY = modelDireccao * (Math.PI / 180)

            lat -= modelVelocidade * Math.cos(modelDireccao * (Math.PI / 180)) * 275
            long += modelVelocidade * Math.sin(modelDireccao * (Math.PI / 180)) * 363

            node = calcNearest([long, lat], nodesPosition);
            modelVelocidade = isInsideCircle(node[0], node[1], long, lat, modelVelocidade)

            this.map.setCenter([long, lat])
            this.map.setBearing(-modelDireccao + 180)

            var rotationX = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(1, 0, 0),
                modelTransform.rotateX
            );

            var rotationY = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(0, 1, 0),
                modelTransform.rotateY
            );

            var rotationZ = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(0, 0, 1),
                modelTransform.rotateZ
            );

            var m = new THREE.Matrix4().fromArray(matrix);
            var l = new THREE.Matrix4()
                .makeTranslation(
                    modelTransform.translateX,
                    modelTransform.translateY,
                    modelTransform.translateZ
                )
                //AUMENTAR O TAMANHO DO OBJETO
                .scale(
                    new THREE.Vector3(
                        modelTransform.scale * 800,
                        -modelTransform.scale * 800,
                        modelTransform.scale * 800
                    )
                )
                .multiply(rotationX)
                .multiply(rotationY)
                .multiply(rotationZ);

            this.camera.projectionMatrix = m.multiply(l);
            this.renderer.state.reset();
            this.renderer.render(this.scene, this.camera);
            this.map.triggerRepaint();
            return true
        }
    };
    return customLayer
}

function isInsideCircle(longC, latC, long, lat, modelVelocidade) {
    var d = (long - longC) * (long - longC) +
        (lat - latC) * (lat - latC)
    if (d < 0.0000075) {
        modelVelocidade = 0;
        console.log("CRASH!")
    }
    return modelVelocidade
}

function calcNearest(coordinatesMap, coordinatesNodes) {
    var minDistancia = 99999999999999;
    let coordinates = [];
    coordinatesNodes.forEach((coords) => {

        var distance = calcCrow(coordinatesMap[0], coordinatesMap[1], coords[0], coords[1]);

        if (distance < minDistancia) {
            coordinates = [coords[0], coords[1]];
            minDistancia = distance;
        }
    });
    return coordinates;
}

function calcCrow(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lon2 - lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

function toRad(Value) {
    return Value * Math.PI / 180;
}

function buildGui(gui, spotLight) {
    //var gui = new dat.GUI({ autoPlace: false });
    gui.domElement.id = 'gui';
    var customContainer = document.getElementById('my-gui-container');
    customContainer.appendChild(gui.domElement);

    const params = {
        'light color': spotLight.color.getHex(),
        intensity: spotLight.intensity,
        distance: spotLight.distance,
        angle: spotLight.angle,
        penumbra: spotLight.penumbra,
        decay: spotLight.decay,
        focus: spotLight.shadow.focus
    };

    gui.addColor(params, 'light color').onChange(function (val) {
        spotLight.color.setHex(val);
    });

    gui.add(params, 'intensity', 0, 2).onChange(function (val) {
        spotLight.intensity = val;
    });

    gui.add(params, 'distance', 10, 200).onChange(function (val) {
        spotLight.distance = val;
    });

    gui.add(params, 'angle', 0, Math.PI / 3).onChange(function (val) {
        spotLight.angle = val;
    });

    gui.add(params, 'penumbra', 0, 1).onChange(function (val) {
        spotLight.penumbra = val;
    });

    gui.add(params, 'decay', 1, 2).onChange(function (val) {
        spotLight.decay = val;
    });

    gui.add(params, 'focus', 0, 1).onChange(function (val) {
        spotLight.shadow.focus = val;
    });

    gui.open();
}