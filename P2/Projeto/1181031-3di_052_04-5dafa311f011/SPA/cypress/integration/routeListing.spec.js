describe('Line Listing', () => {

    it('lists routes of line indicated', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Route').click()
        cy.get('select[name="lineId"]').select('Line:1')
        cy.get('button[name="getRoutesByLineId"]').click()
    })

    it('error cause no line id indicated', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Route').click()
        cy.get('button[name="getRoutesByLineId"]').click()
        cy.get(':nth-child(7) > #result1 > label > b').contains("Selecione um id linha válido!")
    })
})