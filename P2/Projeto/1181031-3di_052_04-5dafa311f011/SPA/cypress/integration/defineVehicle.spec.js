describe('Define Vehicle', () => {

    it('successfully add', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Define Vehicle').click()

        cy.get('select[name="VehicleTypeId"]').select('Autocarro')
        cy.get('input[name="matricula"]').type('AA-53-DA')
        cy.get('input[name="vin"]').type('19UYA31581L000000')
        cy.get('input[name="serviceDate"]').type('2021-01-08')
        cy.contains('Definir veículo').click()
        cy.get('#result1').contains("Veículo definido com sucesso!")
    })

    it('with invalid Matricula -> error!', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Define Vehicle').click()

        cy.get('select[name="VehicleTypeId"]').select('Autocarro')
        cy.get('input[name="matricula"]').type('20-53-12')
        cy.get('input[name="vin"]').type('19UYA31581L110000')
        cy.get('input[name="serviceDate"]').type('2021-01-08')

        cy.contains('Definir veículo').click()
        cy.get('#result1').contains("Não foi possível definir o veículo, reveja os dados!")
    })

    it('with invalid Vin -> error!', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Define Vehicle').click()

        cy.get('select[name="VehicleTypeId"]').select('Autocarro')
        cy.get('input[name="matricula"]').type('20-53-BB')
        cy.get('input[name="vin"]').type('12219UYA31581L110000')
        cy.get('input[name="serviceDate"]').type('2021-01-08')

        cy.contains('Definir veículo').click()
        cy.get('#result1').contains("Não foi possível definir o veículo, reveja os dados!")
    })

})