describe('Trip Adding', () => {

    it('successfully add', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Trip').click()

        cy.get('select[name="lineId"]').select('Line:1')
        cy.get('select[name="route"]').select('3')

        cy.get('input[name="horainicio"]').type('08:00')
        cy.get('input[name="horafim"]').type('10:00')

        cy.get('input[name="nrviagens"]').type('4')
        cy.get('input[name="frequencia"]').type('30')
        cy.get('input[name="nrviagensp"]').click({ force: true })
        cy.get('input[name="nrviagensp"]').type('2')

        cy.get('button[name="addTrip"]').click()
        cy.get('#result1').contains("Viagem adicionada com sucesso!")
    })

    it('with invalid data -> error!', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Trip').click()

        cy.get('select[name="lineId"]').select('Line:1')
        cy.get('select[name="route"]').select('1')

        cy.get('input[name="horainicio"]').type('08:00')
        cy.get('input[name="horafim"]').type('10:00')

        cy.get('input[name="nrviagens"]').type('quatro')
        cy.get('input[name="frequencia"]').type('trinta')

        cy.get('button[name="addTrip"]').click()
        cy.get('#result1').contains("Não foi possível adicionar a viagem, reveja os dados!")
    })
})