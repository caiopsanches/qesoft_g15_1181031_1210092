describe('Crewmember Duty Adding', () => {
    it('successfully add', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Serviço de Tripulante').click()

        cy.get('input[name="code"]').type('444')
        cy.get('select[name="listWBs"]').select('230 - 78720 - 82200 - PARED - PARED')

        cy.get('button[name="addWB"]').click()

        cy.get('button[name="addDriverDuty"]').click()
        cy.get('#result1').contains("Serviço de Tripulante adicionado!")
    })

})

