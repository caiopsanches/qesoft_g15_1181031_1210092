describe('Line Listing', () => {

    it('lists every line', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Line').click()

        cy.get('button[name="getLines"]').click()
    })

    it('lists all lines alphabetically', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Line').click()

        cy.get('input[name="order"]').type('name')

        cy.get('button[name="getLines"]').click()
    })

    it('lists all lines with word Lord in the name', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Line').click()

        cy.get('input[name="order"]').type('name')
        cy.get('input[name="filter"]').type('Lord')

        cy.get('button[name="getLines"]').click()
    })
})