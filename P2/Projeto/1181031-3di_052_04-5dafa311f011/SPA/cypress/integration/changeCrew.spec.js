describe('Best Path with Crew change', () => {

    it('successfully calculated', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Calculate Best Path').click()

        cy.get('select[name="tipo"]').select('Gerador')

        cy.get('input[name="horario"]').type('25000')

        cy.get('select[name="no1"]').select('SOBRO')
        cy.get('select[name="no2"]').select('PARED')

        cy.get('.button > b').click()

        cy.get('[style="font-size: 150%; margin-top: 30px;"] > b').contains("Melhor Caminho:")
        cy.get(':nth-child(2) > b').contains("SOBRO->CRIST pelo Path:22")
        cy.get(':nth-child(3) > b').contains("CRIST->LORDL pelo Path:26")
        cy.get(':nth-child(4) > b').contains("LORDL->PARED pelo Path:9")
    })
})