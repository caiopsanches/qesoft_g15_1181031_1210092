describe('Node Adding', () => {

    it('successfully add', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Node').click()

        cy.get('input[id="shortName"]').type('TEST')
        cy.get('input[id="nodeName"]').type('Complete Test')
        cy.get('input[id="maxTimeStop"]').type('50')

        cy.get('input[id="vehicleCapacity"]').type('3')
        cy.get('input[id="latitude"]').type('41.54654')
        cy.get('input[id="longitude"]').type('-8.483264')
        cy.get('input[id="isReliefPoint"]').check()

        cy.get('button[id="addNode"]').click()
        cy.get('#result').contains("Nó Criado com Sucesso!")
    })

    it('successfully add', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Node').click()

        cy.get('input[id="shortName"]').type('TEST')
        cy.get('input[id="nodeName"]').type('Complete Test')
        cy.get('input[id="maxTimeStop"]').type('50')

        cy.get('input[id="vehicleCapacity"]').type('3')
        cy.get('input[id="latitude"]').type('41.54654')
        cy.get('input[id="longitude"]').type('-8.483264')
        cy.get('input[id="isReliefPoint"]').check()

        cy.get('button[id="addNode"]').click()
        cy.get('#result').contains("Erro em criar o nó!")
        cy.request('DELETE', 'http://localhost:8080/api/node', { shortName: 'TEST' })
    })
    
    it('with invalid data -> error!', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Node').click()

        cy.get('input[id="shortName"]').type('TEST')
        cy.get('input[id="nodeName"]').type('Complete Test')
        cy.get('input[id="maxTimeStop"]').type('50')

        cy.get('input[id="vehicleCapacity"]').type('3')
        cy.get('input[id="latitude"]').type('91.54654')
        cy.get('input[id="longitude"]').type('-8.483264')
        cy.get('input[id="isReliefPoint"]').check()

        cy.get('button[id="addNode"]').click()
        cy.get('#result').contains("Erro em criar o nó!")
    })
          
})