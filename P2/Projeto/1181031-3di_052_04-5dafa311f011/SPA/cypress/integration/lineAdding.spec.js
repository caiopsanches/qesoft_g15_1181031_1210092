describe('Line Adding', () => {

    it('successfully add', () => {
      cy.visit('http://localhost:4200/data-admin')
      cy.contains('Line').click()
      
      cy.get('input[name="lineId"]').type('Line56')
      cy.get('input[name="name"]').type('Valdevez')
      cy.get('input[name="colour"]').type('RGB(12,65,23)')
      
      cy.get('select[name="route"]').select('3')
      cy.get('select[name="orientation"]').select('Go')
      cy.get('button[name="addP"]').click()

      cy.get('select[name="codeV"]').select('Autocarro')
      cy.get('button[name="addV"]').click()
      
      cy.get('select[name="codeD"]').select('13')
      cy.get('button[name="addD"]').click()

      cy.get('button[name="addLine"]').click()
      cy.get('#result1').contains("Linha adicionada com sucesso!")
      cy.request('DELETE', 'http://localhost:8080/api/line', { lineId: 'Line56' })
    })

    it('with invalid data -> error!', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Line').click()
        
        cy.get('input[name="lineId"]').type('Line56')
        cy.get('input[name="name"]').type('Valdevez')
        cy.get('input[name="colour"]').type('RGB(1265,23)')
        
        cy.get('select[name="route"]').select('3')
        cy.get('select[name="orientation"]').select('Go')
        cy.get('button[name="addP"]').click()
  
        cy.get('select[name="codeV"]').select('Autocarro')
        cy.get('button[name="addV"]').click()
        
        cy.get('select[name="codeD"]').select('13')
        cy.get('button[name="addD"]').click()
  
        cy.get('button[name="addLine"]').click()
        cy.get('#result1').contains("Não foi possível adicionar a linha, reveja os dados!")
      })
  })