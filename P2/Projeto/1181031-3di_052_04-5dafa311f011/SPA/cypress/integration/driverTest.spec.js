describe('Driver Adding', () => {

    it('successfully add', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Driver').click()

        cy.get('input[name="driverCC"]').type('92345678')
        cy.get('input[name="driverDate"]').type('1990-11-20')
        cy.get('input[name="driverEntry"]').type('2018-11-12')
        cy.get('input[name="driverExit"]').type('2020-05-10')
        cy.get('input[name="driverMecNumber"]').type('pbcde1234')
        cy.get('input[name="driverName"]').type('Vigario')
        cy.get('input[name="driverNIF"]').type('923456789')
        cy.get('input[name="driverLicenseExpiry"]').type('2030-11-20')
        cy.get('input[name="driverLicenseNumber"]').type('P-1234567 7')


        cy.get('select[name="code"]').select('13')
        cy.get('button[name="addD"]').click()

        cy.get('button[name="addDriver"]').click()
        cy.get('#result1').contains("Tripulante definido com sucesso!")
    })

    it('with invalid driverCC -> error!', () => {
        cy.visit('http://localhost:4200/data-admin')
        cy.contains('Driver').click()

        cy.get('input[name="driverCC"]').type('1')
        cy.get('input[name="driverDate"]').type('1990-11-20')
        cy.get('input[name="driverEntry"]').type('2018-11-12')
        cy.get('input[name="driverExit"]').type('2020-05-10')
        cy.get('input[name="driverMecNumber"]').type('abcde1234')
        cy.get('input[name="driverName"]').type('Vigario')
        cy.get('input[name="driverNIF"]').type('123456789')
        cy.get('input[name="driverLicenseExpiry"]').type('2030-11-20')
        cy.get('input[name="driverLicenseNumber"]').type('P-1234567 8')


        cy.get('select[name="code"]').select('13')
        cy.get('button[name="addD"]').click()

        cy.get('button[name="addDriver"]').click()
        cy.get('#result1').contains("Não foi possível definir o tripulante, reveja os dados!")
    })
})