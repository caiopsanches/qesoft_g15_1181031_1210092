describe('Vehicle Adding', () => {

  it('successfully add', () => {
    cy.visit('http://localhost:4200/data-admin')
    cy.contains('Vehicle').click()
    cy.get('input[name="code"]').type('Taxi')
    cy.get('input[name="energy"]').type('Diesel')
    cy.get('input[name="autonomy"]').type(420)
    cy.get('input[name="kmCost"]').type(0.4)
    cy.get('input[name="consumption"]').type(8)
    cy.get('input[name="speed"]').type(120)
    cy.contains('Add Vehicle').click()
    cy.get('#result1').contains("Veiculo adicionado!")
    cy.request('DELETE', 'http://localhost:8080/api/vehicle', { code: 'Taxi' })
  })
})

it('cannot add duplicates', () => {
  cy.visit('http://localhost:4200/data-admin')
  cy.contains('Vehicle').click()
  cy.get('input[name="code"]').type('Taxi')
  cy.get('input[name="energy"]').type('Diesel')
  cy.get('input[name="autonomy"]').type(420)
  cy.get('input[name="kmCost"]').type(0.4)
  cy.get('input[name="consumption"]').type(8)
  cy.get('input[name="speed"]').type(120)
  cy.contains('Add Vehicle').click()
  cy.contains('Vehicle').click()
  
  cy.get('input[name="code"]').type('Taxi')
  cy.get('input[name="energy"]').type('Gás')
  cy.get('input[name="autonomy"]').type(321)
  cy.get('input[name="kmCost"]').type(0.5)
  cy.get('input[name="consumption"]').type(4)
  cy.get('input[name="speed"]').type(200)
  cy.contains('Add Vehicle').click()
  
  cy.get('#result1').contains("Não foi possível adicionar o veiculo, reveja os dados!")

  cy.request('DELETE', 'http://localhost:8080/api/vehicle', { code: 'Taxi' })
})
