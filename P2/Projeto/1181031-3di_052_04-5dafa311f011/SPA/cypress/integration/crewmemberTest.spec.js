describe('Crewmember Adding', () => {

    it('successfully add', () => {
      cy.visit('http://localhost:4200/data-admin')
      cy.contains('Crewmember').click()
      cy.get('input[name="code"]').type('Crew062')
      cy.get('input[name="description"]').type('Expert driver')
      cy.contains('Add Crewmember').click()
      cy.get('#result1').contains("Crewmember added!")
      cy.request('DELETE', 'http://localhost:8080/api/crewmember', { code: 'Crew062' })
    })
  })
  
  it('cannot add duplicates', () => {
    cy.visit('http://localhost:4200/data-admin')
    cy.contains('Crewmember').click()
    cy.get('input[name="code"]').type('Crew062')
    cy.get('input[name="description"]').type('Expert driver')
    cy.contains('Add Crewmember').click()
    cy.contains('Crewmember').click()
    
    cy.get('input[name="code"]').type('Crew062')
    cy.get('input[name="description"]').type('Expert driver')
    cy.contains('Add Crewmember').click()
    
    cy.get('#result1').contains("Unable to add the crewmember, please review the data!")
  
    cy.request('DELETE', 'http://localhost:8080/api/crewmember', { code: 'Crew062' })
  })