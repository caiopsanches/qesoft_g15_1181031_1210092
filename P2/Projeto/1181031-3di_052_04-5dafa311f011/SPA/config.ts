export default {

  options: {

    observe: 'body' || 'events' || 'response',

    responseType: 'arraybuffer' || 'blob' || 'json' || 'text',

  }
}