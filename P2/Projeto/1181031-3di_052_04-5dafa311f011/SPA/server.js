//Install express server
const express = require('express');
const path = require('path');
const { createProxyMiddleware } = require('http-proxy-middleware');

const app = express();

var backend = 'https://opt52.herokuapp.com'
var backend1 = 'https://mdv52.herokuapp.com'
var planning = 'https://opt-planning.herokuapp.com'
// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist'));


app.use('/api', createProxyMiddleware({ target: backend, changeOrigin: true }));
app.use('/mdv', createProxyMiddleware({ target: backend1, changeOrigin: true }));
app.use('/plan', createProxyMiddleware({ target: planning, changeOrigin: true }));

app.get('/*', function(req,res) {
    
res.sendFile(path.join(__dirname+'/dist/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 4200);