# SPA -  Como data administrator, quero listar os percursos de uma linha. #
=======================================

# 1. Objetivo
- Para este user storie (como pode ser identificada no projeto presente no bitbucket da equipa), o administrador pretende que seja possível listar os percursos existentes para uma determinada linha no contexto da rede de transportes, nomeadamente apartir da interface presente no SPA(single page application).

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito de toda a informação relativa a uma rede de transportes é necessário a listagem dos percursos de uma linha pretendida.
- Uma linha neste contexto corresponde a um conjunto de percursos onde compreenderá um percurso de ida e outro de volta, podendo também ter percursos em vazio
(sem passageiros) para se deslocar de e para estações de recolha, sendo identificada por um id linha, representada por uma cor e têm associada uma lista para os tipos de condutores permitidos a conduzir nessa linha, bem como, uma lista para os veículos autorizados a realizar viagens na mesma.
* Um percurso é um trajeto que compreende uma sequência de nós tendo associado o tempo de deslocação, bem como a distância.
- Aquando da listagem dos percursos da linha selecionada será apresentada uma tabela com toda a informação relativa a estes, isto é, id percurso, nome, nó início, nó fim e os respetivos segmentos que compreendem o id segmento, os nós do segmento( nó 1 e nó 2), a distância e o tempo de deslocação entre ambos.

#### 2.2 Regras de Negócio
* Id linha: único, alfanumérico

#### 2.3 Pré-condições
* Necessita de linhas previamente criadas com percursos associados.
#### 2.4 Pós-condições
- Listagem de acordo com o pretendido.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests 

**Teste 1:** Verificar que é listado todos os percursos da linha selecionada.
**Teste 2:** Verificar que se não for selecionado uma linha é pedido que seja selecionado um id linha válido

# 4. Integração 
Esta user story será indiretamente integrada com uma outra também relativa às linhas, visto que, para que esta seja possível de ocorrer é necessário que antes a outra tenha acontecido, nomeadamente a criação de linhas com percursos associados.