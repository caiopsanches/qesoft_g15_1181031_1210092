# SPA -  Como data administrator, quero listar linhas. #
=======================================

# 1. Objetivo
- Para este user storie (como pode ser identificada no projeto presente no bitbucket da equipa), o administrador pretende que seja possível listar as linhas existentes no contexto da rede de transportes, nomeadamente apartir da interface presente no SPA(single page application).

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito de toda a informação relativa a uma rede de transportes é necessário a listagem das linhas existentes.
- Uma linha neste contexto corresponde a um conjunto de percursos onde compreenderá um percurso de ida e outro de volta, podendo também ter percursos em vazio
(sem passageiros) para se deslocar de e para estações de recolha, sendo identificada por um id linha, representada por uma cor e têm associada uma lista para os tipos de condutores permitidos a conduzir nessa linha, bem como, uma lista para os veículos autorizados a realizar viagens na mesma.
- Aquando da listagem das linhas será apresentada uma tabela com toda a informação relativa às linhas, isto é, id linha, nome, cor, os seus percursos, os veículos permitidos e condutores permitidos podendo estes dois últimos estarem a vazio. É de notar ainda que será possível ordenar o resultado por id linha e nome e/ou filtrar esses mesmos resultados por id linha e nome também. (ex., todos as linhas cujo nome começa por “Par”).

#### 2.2 Regras de Negócio
* Id linha: único, alfanumérico
- Filtro: referente ao nome ou ao Id linha

#### 2.3 Pré-condições
* Necessita de linhas previamente criadas.
#### 2.4 Pós-condições
- Listagem de acordo com o pretendido.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests 

**Teste 1:** Verificar que sem filtro é listado todas as linhas.
**Teste 2:** Verificar que com filtro por nome é listado todas as linhas com o nome indicado. 
**Teste 3:** Verificar que com filtro de ordem por nome é listado todas as linhas por ordem alfabética do nome. 

# 4. Integração 
Esta user story será indiretamente integrada com uma outra também relativa às linhas, visto que, para que esta seja possível de ocorrer é necessário que antes a outra tenha acontecido, nomeadamente a criação de linhas.