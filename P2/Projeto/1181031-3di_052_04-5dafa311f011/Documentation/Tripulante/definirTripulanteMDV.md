# C12 - MDV -  Como data administrator, quero definir tripulante. #
=======================================

# 1. Objetivo
- Para este caso de uso C12,o data administrator pretende que seja possível definir um tripulante na API MDV(master data viagem).

**C12** Como data administrator, quero definir tripulante.

# 2. Análise
#### 2.1. Contexto de Aplicação

Os tripulantes são os condutores das viaturas que irão efetuar as viagens das linhas da rede de transportes. Um tripulante caracteriza-se pelo seu número mecanográfico, nome, data de nascimento, numero de cartão de cidadão, NIF, tipo de tripulante, data de entrada na empresa, data de saída da empresa, número de carta de condução e data de validade de licença de condução. 

Aos tripulantes será atribuído um serviço que corresponderá ao seu período diário de trabalho. Esse serviço é definido como uma sequência de blocos de trabalho obedecendo a um conjunto de regras. Por exemplo, um motorista não pode trabalhar mais que 4 horas seguidas; um motorista pode efetuar dois blocos de trabalho de 4 horas desde que com uma pausa de 1 hora entre eles e um motorista deve efetuar o mínimo de trocas de viatura por serviço.

#### 2.2 Regras de Negócio
* Número mecanográfico: único, obrigatório e alfanumérico de 9 caracteres.
- Nome.
- Data de nascimento.
- Número de cartão de cidadão.
- NIF.
- Tipo de tripulante.
- Data de entrada na empresa.
- Data de saída da empresa.
- Número de carta de condução.
- Data de validade de licença de condução.
#### 2.3 Pré-condições
* Para ser atribuído um ou mais tipos de tripulante, é necessário que estes já estejam criados.
#### 2.4 Pós-condições
- O tripulante será adicionado e poderá efetuar serviços de tripulante.

# 3. Design

## 3.1. Diagrama de Classes

Corresponde ao PostDiagram presente na pata de diagramas.

## 3.2. Padrões

Foram utilizados os padrões DTO e Data Mappers, bem como o pradrão repository e strategy.

## 3.3. Tests  

**Teste 1:** Verificar a criação de um tripulante com todos os seus atributos (válidos).

**Teste 2:** Verificar que não é possível criar tripulante com a falta de algum parâmetro ou algum parâmetro inválido.

# 4. Integração 

Esta user storie será integrada no SPA.

