# C24 - MVD -  Como data administrator, quero definir viatura. #
=======================================

# 1. Objetivo
- Para este user storie C24 o administrador pretende que seja possível definir uma viatura no contexto da rede de transportes, nomeadamente na SPA(Single Page Appliactions), como pode ser identificada no projeto presente no bitbucket da equipa.

**C24** Como data administrator, quero definir viatura.

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito da criação de toda a informação relativa a uma rede de transportes é necessário definir viatura.
* Uma viatura é caracterizado pela matricula, VIN, pelo seu tipo e data de entrada ao serviço.
- Tanto a matricula como o VIN têm de ser únicos.
- A data do serviço tem de ser posterior à data atual
- O VIN terá de seguir certo Padrões


Caso Exemplo:

Na definição de viatura, o administrador indica o tipo de viatura autocarro, pela matricula QR-08-PQ, pelo VIN 9BWHE21JX24060960* e pela data de entrada ao serviço 23-07-21.

#### 2.2. Modelo de Negócio
![viaturaDomain](domainmodel.PNG)

#### 2.3 Regras de Negócio
* Matricula: único, alfanumérico
- tipo de viatura referente à viatura
* VIN: único, alfanumérico
- Data de entrada: no futuro.

#### 2.4 Pré-condições
* Necessita de tipos de viatura criados.
#### 2.5 Pós-condições
- Não podem haver viaturas com o mesmo VIN e matricula. Uma viatura é única.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos. Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade. É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests


**Teste 1:** Verificar que o tipo de viatura referentes à Viatura realmente existem.

**Teste 2:** Verificar a criação de uma viatura com todos os seus atributos (válidos).

**Teste 3:** Verificar se falha ao definir uma viatura com uma matricula em formato inválido.

**Teste 4:** Verificar se falha ao definir uma viatura com um VIN em formato inválido.

# 4. Integração

Esta user storie será integrada com uma outra também relativa à criação de viaturas, de forma a que no final seja possível a criação de viaturas  numa base de dados, neste caso através da aplicação MDV(master data viagens)
