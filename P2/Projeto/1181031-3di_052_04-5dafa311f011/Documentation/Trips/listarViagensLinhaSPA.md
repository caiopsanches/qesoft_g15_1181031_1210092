# SPA -  Como data administrador ou cliente quero listar as viagens de uma linha. #
=======================================

# 1. Objetivo
- Para este user storie (identificada no projeto presente no bitbucket da equipa), o administrador ou cliente pretende que seja possível obter uma lista com as viagens no contexto da rede de transportes, nomeadamente no SPA(single page application).

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito da criação de toda a informação relativa a uma rede de transportes é necessário a listagem de viagens.
- Uma viagem neste contexto corresponde ao conjunto de horas de passagem em cada um dos nós do percurso de uma linha, ou por outras palavras, é a definição de horário do percurso.
* Uma viagem é caracterizado por um código viagem, um nome, um id linha, o respetivo id percurso pertencente à linha e um lista com as horas de passagem em cada nó do percurso indicado.
- O código da viagem identifica uma viagem e, por isso, o código tem de ser único.
* O nome da viagem representa o "id" da viagem mas para efeitos de representação na UI.
- Um id linha identifica uma linha existente.
* Um id percurso será o identificador de um dos percursos da linha indicada.
- A lista com as horas de passagem mostra as horas de passagem em cada nó do percurso indicado.
* Aquando da listagem de viagens de uma linha é selecionado o id linha de entre os existentes e ao confirmar no butão de listagem é apresentada a informação de todas as viagens respetivas ao id linha indicado numa tabela.

Caso Exemplo:
Na listagens de viagens, o utilizador seleciona um id linha dos existentes apresentados, e ao confirmar no botão de listagem é apresentada numa tabela toda informação relativa as viagens pertencentes a essa linha.

#### 2.2 Regras de Negócio
* Id Linha: único, alfanumérico

#### 2.3 Pré-condições
* Necessita de linhas criadas e com viagens associadas as mesmas.
#### 2.4 Pós-condições
- Apresentar numa tabela apenas as viagens do id linha pretendido.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests 

**Teste 1:** Verificar que a listagem é obtida com sucesso para o id linha indicado. 

**Teste 2:** Verificar que a listagem é vazia quando para o id linha indicado não existem viagens. 

**Teste 3:** Verificar que a listagem não é obtida com sucesso quando não é indicado um id linha.

# 4. Integração 

Esta user storie será integrada com uma outra também relativa à criação de viagens, de forma a que no final seja possível obter uma listagem com as respetivas viagens.