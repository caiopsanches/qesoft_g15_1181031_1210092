# C26 - SPA -  Como data administrator, quero criar viagem. #
=======================================

# 1. Objetivo
- Para este user storie C26 (como pode ser identificada no projeto presente no bitbucket da equipa), o administrador pretende que seja possível criar uma viagem no contexto da rede de transportes, nomeadamente apartir do SPA(single page application)
**C26** Como data administrator, quero criar viagem.

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito da criação de toda a informação relativa a uma rede de transportes é necessário a criação de viagens.
- Uma viagem neste contexto corresponde ao conjunto de horas de passagem em cada um dos nós do percurso de uma linha, ou por outras palavras, é a definição de horário do percurso.
* Uma viagem é caracterizado por um código viagem, uma linha, o respetivo percurso pertencente à linha e um lista com as horas de passagem em cada nó do percurso indicado.
- O código da viagem identifica uma viagem e, por isso, o código tem de ser único.
* Uma linha corresponde a um conjunto de percursos.
- Um percurso é um trajeto correspondendo a uma sequência de nós.
* A lista com as horas de passagem mostra as horas de passagem em cada nó do percurso indicado.
- Aquando da criação de viagens é necessário escolher um id linhas dos ids disponíveis e com base nesse id escolhido, escolher os id de percursos que se apresentaram na combobox dos percursos, isto é os ids apresentados na secção do percurso depende do id que for escolhido no da linha.
Após a escolha do percurso será guardado o tempo total de viagem para o percurso indicado que será usado para comparação. Depois é pedido a hora início e hora fim das viagens, o número de viagens que podem ser criadas nesses intervalo de tempo e ainda a frenquência com que pode ser criada uma nova viagem. Neste momento, será necessário verificar se a frenquência introduzida apresenta menor tempo que o tempo total calculado, visto que se tal acontecer, é necessário perguntar ao utilizador o número de viagens que podem acontecer em paralelo.
* De notar que a frequência é a frequência de partida e nada tem a ver com a duração da viagem.
- Após toda a informação introduzida será enviado um pedido de criação à aplicação MDV responsável pela sua criação.

#### 2.2 Regras de Negócio
* Código Viagem: único, alfanumérico
- Código id referente a uma Linha
* Código id referente a um Percurso
- Hora início em segundos
* Hora fim em segundos
- Número de Viagens: inteiro positivo
* Frequência: tempo expresso em minutos
- Número de Viagens Paralelo: inteiro positivo
* Lista Horários: conjunto de horários calculado com base na frequência e hora inicial indicada.

#### 2.3 Pré-condições
* Necessita de linhas e percursos criados.
#### 2.4 Pós-condições
- Não podem haver viagens com o mesmo código. Uma viagem é única.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests 

**Teste 1:** Verificar que os tempos em segundos fazem parte do intervalo [1,86400]. 

**Teste 2:** Verificar que os Ids referentes à Linha e Percurso indicados realmente existem.

**Teste 3:** Verificar a criação de uma viagem com todos os seus atributos (válidos).

# 4. Integração 
Esta user storie será integrada com uma outra também relativa à criação de viagens, de forma a que no final seja possível a criação de viagens numa base de dados, neste caso através da aplicação MDV(master data viagens)