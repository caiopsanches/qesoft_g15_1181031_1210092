# - Criar Blocos de Trabalho "Como data administrator, quero criar bloco de trabalho." #
=======================================

# 1. Objetivo
- Esta user story tem como finalidade ser dada a possibilidade ao Data Administrator de criar blocos de trabalho na API MDV (master data viagem).

# 2. Análise
#### 2.1. Contexto e aplicabilidade

De modo a ser possível a elaboração desta US é necessário ter em conta definições e conceitos apresentados pelo cliente para certas lógicas e conceitos de negócio. De notar que ao que o cliente se refere "bloco de trabalho" a equipa, por vezes, refere-se como "work block" dado a lógica de negócio em código estar em Inglês.

- “Um bloco de trabalho é um conjunto de viagens feitas, sem interrupções, pela mesma viatura, e com ou sem interrupções, pelo mesmo tripulante.”

- “Um motorista não pode trabalhar mais que 4 horas seguidas.”

- “Uma viatura deve fazer no máximo 16 horas de serviço diário.”

- "Um motorista pode efetuar dois blocos de trabalho de 4 horas desde que com uma pausa de 1 hora
entre eles."

- "... Quero criar os blocos de trabalho de um serviço de viatura com base na duração
de cada bloco e número de blocos máximos e consecutivos"

- "... é necessário dividir cada serviço de viatura em blocos de trabalho para os tripulantes"

Estes blocos de trabalho aparecem na necessidade de a empresa planear e gerir os seus recursos da melhor forma, com isso em conta, será possível maximizar e controlar de uma forma exaustiva as suas viaturas e tripulantes presentes na rede. 

Esmiuçando estes conceitos, podemos retirar que um bloco de trabalho faz um apanhado dos conceitos referente às viagens, relacionando-o com serviços de viatura e de tripulante. Deste modo, será tido em conta os tempos máximos que os motoristas e as viaturas podem estar em serviço continuo.


O que se propõe é que tendo em conta as variáveis a cima referidas, tanto de motoristas como de viaturas, seja de modo automatico criado uma série de blocos de trabalho com o objetivo de atribuir a uma dada viagem os serviços necessários para que essa possa ser realizada.

Será dada a possibilidade ao utilizador de definir o número de blocos de trabalho e a duração em que quer subdividir os serviços de viaturas. Se um bloco de trabalho é referente apenas a um serviço de tripulante que no maxímo pode estar 4h a exercer a sua função, consequentemente, a duração máxima que o utilizador pode escolher para cada bloco de trabalho será essas 4 horas. Nesse caso, cada serviço de viatura poderia realizar 4 blocos de trabalho.

Caso Exemplo:

Inicio de viagem na linha L1 às 8h, escolhida a viatura V1 e o tripulante T1. (T2 e V2 disponiveis).
L1 funciona até as 20h.
Cada veiculo apenas pode estar em serviço 6h.
Cada motorista apenas pode estar em serviço 4h.

Uma possível solução para criar blocos de trabalho neste exemplo seria:

Bloco de trabalho:
BT1 - V1 T1 das 8h até as 12h, V1 T2 das 12h até às 14h.
BT2 - V2 T2 das 14h até as 18h.
BT3 - V2 T1 das 18h até as 20h.


#### 2.2. Business Model
![workBlockDomain](domainmodel.PNG)

#### 2.3 Regras de negocio

"um bloco de trabalho caracteriza-se pelo instante de inicio e instante de fim, bem como a quais as viagens a que se refere. para realizar uma viagem podem ser necessários vários blocos de trabalho e um bloco de trabalho pode cumprir apenas parcialmente uma viagem."

- Id próprio (GUI)
- Tempo Inicio em segundos
- Tempo fim em segundos
- Nó inicio
- Nó fim

#### 2.4 Pre-conditions
* Ter viagens e nós na base de conhecimento.
#### 2.5 Pro-conditions
- Será possível usar os blocos de trabalho de modo a controlar a rede. 

# 3. Design

## 3.2. Padrões

Para este caso de uso foi necessário utilizar o padrão DTO e Data Mappers para ser possível a transformação da lógica de negócio em tipos primitivos e vice-versa.
Também foi usado o padrão repository e o strategy, para que de uma forma mais benéfica ser facilitada a persistência e o desacoplamento de responsabilidades.

## 3.3. Tests 

**Teste 1:** Verificar que os tempos em segundos fazem parte do intrevalo [1,86400]. 

**Teste 2:** Verificar que os Ids referentes às viagem e nós estão na sintaxe correta.

**Teste 3:** Verificar que os Ids referentes às viagem e nós existem.

**Teste 4:** Obrigar a criar um work block com todos os seus atributos (válidos).


# 4. Integração 

Esta US será integrada com a sua análoga no SPA, isto é, apôs a conclusão de ambas, será possível adicionar "work blocks" através da interface gráfica do data administrator.



