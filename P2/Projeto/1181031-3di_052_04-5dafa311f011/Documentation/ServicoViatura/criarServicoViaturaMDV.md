# C14 - MVD -  Como data administrator quero criar um serviço de viatura ad hoc. #
=======================================

# 1. Objetivo
- Para este user storie C14 (como pode ser identificada no projeto presente no bitbucket da equipa), o administrador pretende que seja possível criar um serviço de viatura no contexto da rede de transportes através da API MDV.

**C14** Como data administrator quero criar um serviço de viatura ad hoc.

# 2. Análise
#### 2.1. Contexto de Aplicação

- No âmbito da criação de toda a informação relativa a uma rede de transportes é necessário a criação de serviços de viatura.
- O Serviço de Viatura corresponde a um intervalo tempo que uma determinada viatura pode ser utilizada para o serviço da empresa.
- O Serviço de Viatura tem uma hora de início e uma hora de fim.
- Tem que começar e terminar numa estação de recolha.
- Uma viatura tem um limite máximo de 16 horas de serviço diário.
- Uma viatura não deve estar parada numa estação de de recolha mais do que 48 horas.

Caso Exemplo:

Na criação de serviço de viaturas, são indicadas horas de início e término do serviço, 8h00 e 20h00, respetivamente, de um dado dia, é indicado o veículo que vai realizar esse serviço e a estação onde deve começar o serviço e a estação que deve terminar.


#### 2.2 Regras de Negócio
- Código de Serviço de Viatura: único e alfanumérico
- Código de do Veiculo
- Data e hora de Ínicio, no futuro
- Data e hora de Fim, no futuro
- Estas duas horas não podem distanciar-se mais de 16 horas
- Código da estação de recolha de ínicio
- Código da estação de recolha de ínicio
- Lista de Viagens não podendo ser repetidas.

#### 2.3 Pré-condições
* Necessita de veículos e nós criados.
#### 2.4 Pós-condições
- Não podem existir serviços de viatura com o mesmo código.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests 

**Teste 1:** Verificar que as horas em segundos fazem parte do intrevalo [1,86400]. 

**Teste 2:** Verificar que os Ids referentes aos Veículos e Nós indicados realmente existem.

**Teste 3:** Verificar que os Nós de termino são estações de recolha.

**Teste 4:** Verificar que as horas de começo e de fim não têm uma diferença superior a 16 horas.

**Teste 3:** Verificar a criação do serviço de transporte com todos os seus atributos (válidos).

# 4. Integração 

Esta user storie será integrada com uma outra também relativa à criação de serciço de viatura ad hoc, de forma a que no final seja possível a criação de serciços de viatura através de uma interface gráfica do data administrator.