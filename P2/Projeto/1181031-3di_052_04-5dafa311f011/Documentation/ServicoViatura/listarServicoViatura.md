# SPA -  Como data administrator, quero listar serviço de viatura. #

# 1. Objetivo
- Para este user storie o administrador pretende que seja possível listar o serviço de viatura existentes no contexto da rede de transportes, através da interface da SPA (Single Page Application).

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito de toda a informação relativa a uma rede de transportes é necessário a listagem dos serviços de viatura.
- Um serviço de viatura corresponde ao período diário de trabalho de uma viatura.
- Um serviço é definido como uma sequência de blocos de trabalho obedecendo a um conjunto de regras.

#### 2.2 Regras de Negócio
* Código alfanumérico de 10 caracteres (descrição)
- um serviço de viatura nunca pode ter uma duração superior a 16 horas diárias.

#### 2.3 Pré-condições
* Necessita de serviço de viatura previamente criados.
#### 2.4 Pós-condições
- Listagem de acordo com o pretendido.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsabilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests

**Teste 1:** Verificar que sem filtro é listado todas os serviços.


# 4. Integração
Esta user story será indiretamente integrada com uma outra também relativa aos serviços de tripulante, visto que, para que esta seja possível de ocorrer é necessário que antes a outra tenha acontecido, nomeadamente a criação de serviço de viatura.
