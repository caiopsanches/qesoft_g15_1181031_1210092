# SPA - Como data administrator, quero criar um serviço de tripulante ad hoc. #
=======================================

# 1. Objetivo
- Esta user story tem como finalidade ser dada ao data administrator criar um serviço de tripulante ad hoc no SPA(Single Page Application).

# 2. Análise
#### 2.1. Contexto de Aplicação

Um serviço de tripulante corresponde ao período diário de trabalho de um tripulante.
Um serviço é definido como uma sequência de blocos de trabalho obedecendo a um conjunto de regras.


#### 2.2 Regras de negócio
- código alfanumérico de 10 caracteres
- com um conjunto de blocos de trabalho. 
- um serviço de tripulante nunca pode ter uma duração superior a 8 horas (parametrizável no sistema) devendo existir uma pausa ao fim de 4 horas. 
- a duração do serviço corresponde à duração dos blocos associados (que devem ser contíguos em cada período).


#### 2.3 Pré-condições
* Existirem blocos de trabalho e,por sua vez, serviços de viatura.
#### 2.4 Pós-condições
- Será possível afetar motoristas a serviços de viatura.

# 3. Design


## 3.1. Tests 

**Teste 1:** Verificar que o Id é um alfanumérico de 10 caracteres. 

**Teste 2:** Verificar que existe pelo um bloco de trabalho afeto.

**Teste 3:** Verificar que o serviço não pode ter uma duração superior a 8h.

**Teste 4:** Verificar que existe uma pausa ao fim de 4h.


# 4. Integração 

Esta US será integrada com a sua análoga no MDV, isto é, apôs a conclusão de ambas, será possível criar serviços de viatura através da interface gráfica do data administrator.



