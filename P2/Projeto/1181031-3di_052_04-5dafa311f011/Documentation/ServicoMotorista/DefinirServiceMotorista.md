# - Criar Blocos de Trabalho "Como data administrator, quero criar bloco de trabalho." #
=======================================

# 1. Objetivo
- Esta user story tem como finalidade ser dada ao data administrator criar um serviço de tripulante ad hoc

# 2. Análise
#### 2.1. Contexto e aplicabilidade

Um serviço de tripulante corresponde ao período diário de trabalho de um tripulante.
Um serviço é definido como uma sequência de blocos de trabalho obedecendo a um conjunto de regras.

#### 2.2. Business Model
![workBlockDomain](domainmodel.PNG)


#### 2.3 Regras de negocio
- código alfanumérico de 10 caracteres
está relacionado:
- com um conjunto de blocos de trabalho. 
- um serviço de tripulante nunca pode ter uma duração superior a 8 horas (parametrizavel no sistema) devendo existir uma pausa ao fim de 4 horas. 
- a duração do serviço corresponde à duração dos blocos associados (que devem ser contiguos em cada periodo).




#### 2.4 Pre-conditions
* Existirem blocos de trabalho e,por sua vez, serviços de viatura.
#### 2.5 Pro-conditions
- Será possível afetar motoristas a serviços de viatura.

# 3. Design


## 3.3. Tests 

**Teste 1:** Verificar que o Id é um alfanumérico de 10 caracteres. 

**Teste 2:** Verificar que existe pelo um bloco de trabalho afeto.

**Teste 3:** Verificar que o serviço não pode ter uma duração superior a 8h.

**Teste 4:** Verificar que existe uma pausa ao fim de 4h.


# 4. Integração 

Esta US será integrada com a sua análoga no SPA, isto é, apôs a conclusão de ambas, será possível criar serviços de viatura através da interface gráfica do data administrator.



