# SPA -  Implementar a opção que permita o exercício do direito ao esquecimento (RGPD) #

# 1. Objetivo
- Para este user storie o cliente pretende que seja possível remover a sua conta da mesma maneira que a adicionou, nomeadamente através da interface presente no SPA(single page application).

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito de toda a informação relativa a uma rede de transportes é necessário a opção que permita o exercício do direito ao esquecimento (RGPD).

#### 2.2 Pré-condições
* Necessita que o cliente esteja criado e guardado na base de dados.
* È necessário que o cliente esteja logado na aplicação para assim conseguir iniciar a remoção da sua conta.

#### 2.3 Pós-condições
- Iria ser redirecionado para a página inicial da aplicação.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsabilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

# 4. Integração
Esta user story será indiretamente integrada com uma outra também relativa á criação de usuários nomeadamente clientes, visto que, para que esta seja possível de ocorrer é necessário que antes a outra tenha acontecido, nomeadamente a criação da conta do cliente com os respetivos dados.
