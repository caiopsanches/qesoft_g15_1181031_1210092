# SPA -  Como data administrator, quero listar nós. #
=======================================

# 1. Objetivo
- Para este user story (como pode ser identificada no projeto presente no bitbucket da equipa), o administrador pretende que seja possível listar os nós existentes no contexto da rede de transportes, a partir da interface da SPA (Single Page Application).

# 2. Análise
#### 2.1. Contexto de Aplicação
* No âmbito de toda a informação relativa a uma rede de transportes é necessário a listagem dos nós existentes.
- Um nó neste contexto corresponde a um ponto no mapa que pode ser normal, ponto de rendição ou estação de recolha. Os pontos normais são simples paragens onde os veículos podem parar, os pontos de rendição são paragens onde os motoristas podem mudar de veículo e/ou linha e as estações de recolha são locais onde os veículos ficam guardados.
- Aquando da listagem das linhas será apresentada uma tabela com toda a informação relativa aos nós, isto é, abreviatura, nome, tempo máximo de paragem, Capacidade de veículos, coordenadas geográficas, e se é ou não ponto de rendição. É de notar ainda que será possível ordenar o resultado por qualquer um deste parametros e/ou filtrar esses mesmos resultados por nome ou abreviatura.

#### 2.2 Regras de Negócio
* Abreviatura: único, alfanumérico
- Filtro: referente ao nome ou à abreviatura

#### 2.3 Pré-condições
* Necessita de pelo menos um nó previamente criado.
#### 2.4 Pós-condições
- Listagem de acordo com o pretendido.

# 3. Design

## 3.1. Padrões
Foi utilizado o padrão DTO que nos permite isolar nosso modelo de domínio da camada de apresentação e como resultado podemos obter código de baixo acoplamento, transferência de dados otimizada e maior flexibilidade no design de toda a aplicação quando houver alterações de requisitos.
Também foi utilizado o padrão repository que fornece um interface para instanciar todos os repositórios necessários e o strategy de forma a delegar as responsibilidades adquiridas por cada entidade.
É claro que também existem alguns princípios SOLID e GRASP subjacentes. Sendo parte do SOLID foi utilizado o princípio de Responsabilidade Única onde cada classe é responsável por uma única parte da funcionalidade fornecida pelo software e essa responsabilidade deve ser totalmente encapsulada pela classe. Outro princípio SOLID usado é o princípio de Inversão de Dependência que divide a dependência entre os módulos de alto e baixo nível, introduzindo uma abstração entre eles permitindo o desacoplamento.

## 3.2. Tests 

**Teste 1:** Verificar que sem filtro é listado todos os nós.
**Teste 2:** Verificar que com filtro por nome é listado todos os nós com o nome indicado. 
**Teste 3:** Verificar que com filtro de ordem por nome é listado todos os nós por ordem alfabética do nome. 

# 4. Integração 
Esta user story será indiretamente integrada com uma outra também relativa ao nós, visto que, para que esta seja possível de ocorrer é necessário que antes a outra tenha acontecido, nomeadamente a criação de nós.