import * as sinon from 'sinon';
import { Response, Request, NextFunction } from 'express';
import { Container } from 'typedi';
import config from "../config";
import { Result } from '../core/logic/Result';
import ILineService from "../services/IServices/ILineService";
import LineController from "../controllers/lineController";
import ILineDTO from '../dto/ILineDTO';
import ILineController from '../controllers/IController/ILineController';

describe('Line controller', function () {

    let req: Partial<Request> = {};
    let req1: Partial<Request> = {};
    let req2: Partial<Request> = {};
    let req3: Partial<Request> = {};

    let next: Partial<NextFunction> = () => { };
    let next1: Partial<NextFunction> = () => { };
    let next2: Partial<NextFunction> = () => { };
    let next3: Partial<NextFunction> = () => { };

    const body = {
        "id": 'line12', "lineId": '1', "name": '800Gondomar',
        "colour": 'RGB(12,12,12)', "paths": [
            {
                "routeId": "1",
                "orientation": "Go"
            },
            {
                "routeId": "2",
                "orientation": "Return"
            }
        ],
        "allowedVehicles": ["BMW", "MER"],
        "allowedDrivers": ["maedohugo", "pai"]
    };
    req.body = body;
    req1.query = {}
    req2.body = { lineId: 'line12' }
    req3.body = { name: '800Gondomar' }

    let lineServiceClass = require(config.services.line.path).default;
    let lineServiceInstance = Container.get(lineServiceClass)
    Container.set(config.services.line.name, lineServiceInstance);
    lineServiceInstance = Container.get(config.services.line.name);
    let lineControllerClass = require(config.controller.line.path).default;
    let lineControllerInstance: ILineController = Container.get(lineControllerClass)
    Container.set(config.controller.line.name, lineControllerInstance)
    lineControllerInstance = Container.get(config.controller.line.name)

    beforeEach(function () {
        sinon.stub(lineServiceInstance, "createLine").returns(Result.ok<ILineDTO>({
            "id": req.body.id, "lineId": req.body.lineId, "name": req.body.name,
            "colour": req.body.colour, "paths": req.body.paths,
            "allowedVehicles": req.body.allowedVehicles,
            "allowedDrivers": req.body.allowedDrivers
        }));
    });

    afterEach(function () {
        sinon.verifyAndRestore()
    });

    it('CreateLine: shoould create', async function () {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')

        const ctrl = new LineController(lineServiceInstance as ILineService);

        await ctrl.createLine(<Request>req, <Response>res, <NextFunction>next);

        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": req.body.id, "lineId": req.body.lineId, "name": req.body.name,
            "colour": req.body.colour, "paths": req.body.paths,
            "allowedVehicles": req.body.allowedVehicles,
            "allowedDrivers": req.body.allowedDrivers
        });
    });

    it('GetLine1: should return', async () => {

        const var1 = ["BMW", "MER"]
        const var2 = ["maedohugo", "pai"]
        const var3 = [
            {
                "routeId": "1",
                "orientation": "Go"
            },
            {
                "routeId": "2",
                "orientation": "Return"
            }
        ]

        const jsonStub = sinon.stub(lineServiceInstance, "getAllLines").returns(Result.ok<Array<ILineDTO>>(
            [{
                "id": 'line12', "lineId": '1', "name": '800Gondomar',
                "colour": 'RGB(12,12,12)', "paths": var3,
                "allowedVehicles": var1,
                "allowedDrivers": var2
            }]
        ))

       // const stat = 201;

        //const jsonStub = sinon.stub()
        const res = { json: jsonStub => ({ status: status, send: err => err }) }
        const statusSpy = sinon.spy(res, 'json')

        const ctrl = new LineController(lineServiceInstance as ILineService);

        await ctrl.getLines(<Request>req1, <Response><unknown>res, <NextFunction>next1);

        sinon.assert.calledWith(res.json, [{
            "id": req.body.id, "lineId": req.body.lineId, "name": req.body.name,
            "colour": req.body.colour, "paths": req.body.paths,
            "allowedVehicles": req.body.allowedVehicles,
            "allowedDrivers": req.body.allowedDrivers
        }]);
    });
});