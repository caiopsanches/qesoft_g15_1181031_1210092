const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import IVehicleService from '../services/IServices/IVehicleService'
import IVehicleController from '../controllers/IController/IvehicleController';
import IVehicleDTO from "../dto/IVehicleDTO";
import { Result } from '../core/logic/Result';
import IVehicleRepo from '../repos/IRepos/IVehicleRepo';
import { Vehicle } from "../domain/vehicle/vehicle"
import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { VehicleEnergy } from "../domain/vehicle/vehicleEnergy"
import { VehicleAutonomy } from "../domain/vehicle/vehicleAutonomy"
import { VehicleAvgSpeed } from "../domain/vehicle/vehicleAvgSpeed"
import { VehicleKmCost } from "../domain/vehicle/vehicleKmCost"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { VehicleAvgConsumption } from "../domain/vehicle/vehicleAvgConsumption"
describe('vehicle service create', () => {


    const codeE = VehicleCode.create('mini-bus')
    const energyE = VehicleEnergy.create("Diesel")
    const autonomyE = VehicleAutonomy.create(200)
    const kmCostE = VehicleKmCost.create(2)
    const avgConsumptionE = VehicleAvgConsumption.create(4)
    const avgSpeedE = VehicleAvgSpeed.create(79)



    let vehicle: Result<Vehicle> = Vehicle.create({
        code: codeE.getValue(),
        energy: energyE.getValue(),
        autonomy: autonomyE.getValue(),
        kmCost: kmCostE.getValue(),
        avgConsumption: avgConsumptionE.getValue(),
        avgSpeed: avgSpeedE.getValue(),


    }, new UniqueEntityID("mini-bus"))


    let res = Result.ok<IVehicleDTO>({
        id: "mini-bus",
        code: "mini-bus",
        energy: "Diesel",
        autonomy: 200,
        kmCost: 2,
        avgConsumption: 4,
        avgSpeed: 79
    })


    const dto: IVehicleDTO = {
        "id": "mini-bus",
        "code": "mini-bus",
        "energy": "Diesel",
        "autonomy": 200,
        "kmCost": 2,
        "avgConsumption": 4,
        "avgSpeed": 79
    }

    var allVehicles = new Array<Result<IVehicleDTO>>()
    allVehicles.push(res)

    let vehicleServiceClass = require(config.services.vehicle.path).default;
    let vehicleServiceInstance: IVehicleService = Container.get(vehicleServiceClass)
    Container.set(config.services.vehicle.name, vehicleServiceInstance);
    vehicleServiceInstance = Container.get(config.services.vehicle.name);


    let vehicleRepoClass = require(config.repos.vehicle.path).default
    let vehicleRepoInstance: IVehicleRepo = Container.get(vehicleRepoClass)
    Container.set(config.repos.vehicle.name, vehicleRepoInstance)
    vehicleRepoInstance = Container.get(config.repos.vehicle.name);
    beforeEach(() => {
    });
    afterEach(function () {
        sinon.restore();
    });

    it('should create ', async () => {
        sinon.stub(vehicleRepoInstance, "save").returns(vehicle.getValue())
        sinon.assert.match((await vehicleServiceInstance.createVehicle(dto)), res);
    })

    it('get all vehicles', function(done){
        sinon.stub(vehicleRepoInstance, "findAll").returns(vehicle.getValue())
        sinon.assert.match((vehicleServiceInstance.getAllVehicles('')), Promise.resolve(allVehicles));
        done();
    }
    )})