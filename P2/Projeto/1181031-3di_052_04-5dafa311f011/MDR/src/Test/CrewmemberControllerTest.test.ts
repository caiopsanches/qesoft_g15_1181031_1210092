const  sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import ICrewmemberService from '../services/IServices/ICrewmemberService'
import ICrewmemberController from '../controllers/IController/IcrewmemberController';
import ICrewmemberDTO from "../dto/ICrewmemberDTO";
import { Result } from '../core/logic/Result';
import CrewmemberController from '../controllers/crewmemberController'

describe('crewmember controller create', () => {

    let req: Partial<Request> = {};
    let req1: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };
    let next1: Partial<NextFunction> = () => { };

    const body: ICrewmemberDTO = {
        "id": "123",
        "code": "crew1",
        "description": "desc1"
    }

    req.body = body
    req1.body = {}

    let crewmemberServiceClass = require(config.services.crewmember.path).default;
    let crewmemberServiceInstance: ICrewmemberService = Container.get(crewmemberServiceClass)
    Container.set(config.services.crewmember.name, crewmemberServiceInstance);
    crewmemberServiceInstance = Container.get(config.services.crewmember.name);
    let crewmemberControllerClass = require(config.controller.crewmember.path).default;
    let crewmemberControllerInstance: ICrewmemberController = Container.get(crewmemberControllerClass)
    Container.set(config.controller.crewmember.name, crewmemberControllerInstance);
    crewmemberControllerInstance = Container.get(config.controller.crewmember.name);

    beforeEach(() => {


    });
    afterEach(function () {
        sinon.restore();
    });
    it('should create ', async () => {

        sinon.stub(crewmemberServiceInstance, "createCrewmember").returns(Result.ok<ICrewmemberDTO>({
            "id": "123", 
            "code": req.body.code,
            "description": req.body.description
        }));

        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
       

        await crewmemberControllerInstance.createCrewmember(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": "123", 
            "code": req.body.code,
            "description": req.body.description
        });
    });


    it('should not  create ', async () => {


        sinon.stub(crewmemberServiceInstance, "createCrewmember").returns(Result.ok<ICrewmemberDTO>({
            "id": "1234",
            "code": req.body.code,
            "description": "desc"
        }));

        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')



        await crewmemberControllerInstance.createCrewmember(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.failException

    });

    it('should get ', async () => {

        const jsonStub = sinon.stub(crewmemberServiceInstance,"getAllCrewmembers").returns(Result.ok<Array<ICrewmemberDTO>>([{
            "id": "123",
            "code": "crew1",
            "description": "desc1"
        }]))
        
        const res = { json: jsonStub => ({ status: status, send: err => err }) }
        const statusSpy = sinon.spy(res, 'json')
       

        const ctrl = new CrewmemberController(crewmemberServiceInstance as ICrewmemberService);

        await ctrl.getCrewmembers(<Request>req1, <Response><unknown>res, <NextFunction>next1);
        sinon.assert.calledWith(res.json, [ {
            "id": "123", 
            "code": req.body.code,
            "description": req.body.description
        }])
    });
})