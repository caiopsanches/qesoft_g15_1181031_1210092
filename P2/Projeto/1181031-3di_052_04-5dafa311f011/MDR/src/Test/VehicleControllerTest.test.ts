const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import IVehicleService from '../services/IServices/IVehicleService'
import IVehicleController from '../controllers/IController/IvehicleController';
import IVehicleDTO from "../dto/IVehicleDTO";
import { Result } from '../core/logic/Result';
import VehicleController from '../controllers/vehicleController'

describe('vehicle controller create', () => {

    let req: Partial<Request> = {};
    let req1: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };
    let next1: Partial<NextFunction> = () => { };

    const body: IVehicleDTO = {
        "id": "123",
        "code": "mini-bus",
        "energy": "Gasolina",
        "autonomy": 4,
        "kmCost": 1,
        "avgConsumption": 1,
        "avgSpeed": 5
    }

    req.body = body
    req1.body = {}

    let vehicleServiceClass = require(config.services.vehicle.path).default;
    let vehicleServiceInstance: IVehicleService = Container.get(vehicleServiceClass)
    Container.set(config.services.vehicle.name, vehicleServiceInstance);
    vehicleServiceInstance = Container.get(config.services.vehicle.name);
    let vehicleControllerClass = require(config.controller.vehicle.path).default;
    let vehicleControllerInstance: IVehicleController = Container.get(vehicleControllerClass)
    Container.set(config.controller.vehicle.name, vehicleControllerInstance);
    vehicleControllerInstance = Container.get(config.controller.vehicle.name);

    beforeEach(() => {


    });
    afterEach(function () {
        sinon.restore();
    });
    it('should create ', async () => {

        sinon.stub(vehicleServiceInstance, "createVehicle").returns(Result.ok<IVehicleDTO>({
            id: "123",
            "code": req.body.code,
            "energy": req.body.energy,
            "autonomy": req.body.autonomy,
            "kmCost": req.body.kmCost,
            "avgConsumption": req.body.avgConsumption,
            "avgSpeed": req.body.avgSpeed
        }));

        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')


        await vehicleControllerInstance.createVehicle(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": "123", "code": req.body.code,
            "energy": req.body.energy,
            "autonomy": req.body.autonomy,
            "kmCost": req.body.kmCost,
            "avgConsumption": req.body.avgConsumption,
            "avgSpeed": req.body.avgSpeed
        });
    });

    it('should not  create ', async () => {


        sinon.stub(vehicleServiceInstance, "createVehicle").returns(Result.ok<IVehicleDTO>({
            id: "123",
            "code": req.body.code,
            "energy": req.body.energy,
            "autonomy": 1,
            "kmCost": req.body.kmCost,
            "avgConsumption": req.body.avgConsumption,
            "avgSpeed": req.body.avgSpeed
        }));

        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')



        await vehicleControllerInstance.createVehicle(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.failException
    

    });

    it('should get ', async () => {

        const jsonStub = sinon.stub(vehicleServiceInstance,"getAllVehicles").returns(Result.ok<Array<IVehicleDTO>>([{
            "id": "123",
            "code": "mini-bus",
            "energy": "Gasolina",
            "autonomy": 4,
            "kmCost": 1,
            "avgConsumption": 1,
            "avgSpeed": 5
        }]))
        
        const res = { json: jsonStub => ({ status: status, send: err => err }) }
        const statusSpy = sinon.spy(res, 'json')
       

        const ctrl = new VehicleController(vehicleServiceInstance as IVehicleService);

        await ctrl.getVehicles(<Request>req1, <Response><unknown>res, <NextFunction>next1);
        sinon.assert.calledWith(res.json, [ {
            "id": "123", "code": req.body.code,
            "energy": req.body.energy,
            "autonomy": req.body.autonomy,
            "kmCost": req.body.kmCost,
            "avgConsumption": req.body.avgConsumption,
            "avgSpeed": req.body.avgSpeed
        }])
    });
})