import * as sinon from 'sinon';
import { Result } from "../core/logic/Result";
import { Line } from "../domain/line/Line";
import { LineId } from "../domain/line/LineId";
import { LineName } from "../domain/line/LineName";
import { LineColour } from "../domain/line/LineColour";
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { VehicleCode } from '../domain/vehicle/vehicleCode';
import { CrewmemberCode } from '../domain/crewmember/crewmemberCode';
import { RouteId } from '../domain/Route/RouteId';
import { LinePath } from '../domain/line/LinePath';
import { RouteDirection } from '../domain/Route/RouteDirection';


describe('Line Test', () => {
  it('Can create Line with all values', () => {
    let expected = Result.ok<Line>();
    const lineIdE = LineId.create("Linha1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
      routeId: rt1.getValue(),
      orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
      routeId: rt2.getValue(),
      orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)

    let result = Line.create({
      lineId: lineIdE.getValue(),
      name: nameE.getValue(),
      colour: colourE.getValue(),
      paths: pathsAux,
      allowedVehicles: allowedVehiclesAux,
      allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("Linha1"))

    sinon.assert.match(result.isSuccess, true)
  });

  it('Cannot create without Paths', () => {
    let expected = Result.ok<Line>();
    const lineIdE = LineId.create("Linha1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    let result = Line.create({
      lineId: lineIdE.getValue(),
      name: nameE.getValue(),
      colour: colourE.getValue(),
      paths: null,
      allowedVehicles: allowedVehiclesAux,
      allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("Linha1"))

    sinon.assert.match(result.isFailure, true)
  });

  it('Cannot create without Paths', () => {
    let expected = Result.ok<Line>();
    const lineIdE = LineId.create("Linha1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    let result = Line.create({
      lineId: lineIdE.getValue(),
      name: nameE.getValue(),
      colour: colourE.getValue(),
      paths: null,
      allowedVehicles: allowedVehiclesAux,
      allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("Linha1"))

    sinon.assert.match(result.isFailure, true)
  });

  it('Can create Line with empty allowedVehicles', () => {
    let expected = Result.ok<Line>();
    const lineIdE = LineId.create("Linha1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    var allowedVehiclesAux = new Array<VehicleCode>()

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
      routeId: rt1.getValue(),
      orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
      routeId: rt2.getValue(),
      orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)
    let result = Line.create({
      lineId: lineIdE.getValue(),
      name: nameE.getValue(),
      colour: colourE.getValue(),
      paths: pathsAux,
      allowedVehicles: allowedVehiclesAux,
      allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("Linha1"))

    sinon.assert.match(result.isSuccess, true)
  });

  it('Can create Line with empty allowedDrivers', () => {
    let expected = Result.ok<Line>();
    const lineIdE = LineId.create("Linha1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    var allowedDriversAux = new Array<CrewmemberCode>()

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
      routeId: rt1.getValue(),
      orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
      routeId: rt2.getValue(),
      orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)

    let result = Line.create({
      lineId: lineIdE.getValue(),
      name: nameE.getValue(),
      colour: colourE.getValue(),
      paths: pathsAux,
      allowedVehicles: allowedVehiclesAux,
      allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("Linha1"))

    sinon.assert.match(result.isSuccess, true)
  });

  it('Can create Line with empty allowedVehicles and allowedDrivers', () => {
    let expected = Result.ok<Line>();
    const lineIdE = LineId.create("Linha1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    var allowedVehiclesAux = new Array<VehicleCode>()
    var allowedDriversAux = new Array<CrewmemberCode>()

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
      routeId: rt1.getValue(),
      orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
      routeId: rt2.getValue(),
      orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)

    let result = Line.create({
      lineId: lineIdE.getValue(),
      name: nameE.getValue(),
      colour: colourE.getValue(),
      paths: pathsAux,
      allowedVehicles: allowedVehiclesAux,
      allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("Linha1"))

    sinon.assert.match(result.isSuccess, true)
  });

  it('Can create LineId', () => {
    const lineIdE = "Linha1";
    let result = LineId.create(lineIdE)
    sinon.assert.match(result.isSuccess, true)
  });

  it('Cannot create LineId with wrong lineId', () => {
    const lineIdE = "qweqweqeqeqqeqeqeddddddwddwdqqweygqoeygoqyegoqyge";
    let result = LineId.create(lineIdE)
    sinon.assert.match(result.isFailure, true)
  });

  it('Can create name', () => {
    const lineName = "800Gondomar";
    let result = LineName.create(lineName)
    sinon.assert.match(result.isSuccess, true)
  });

  it('Cannot create LineName with wrong lineName', () => {
    const lineName = "qweqweqeqeqqeqeqeddddddwddwdqqweygqoeygoqyegoqyge";
    let result = LineName.create(lineName)
    sinon.assert.match(result.isFailure, true)
  });

  it('Cannot create LineName with wrong lineName', () => {
    const lineName = "qweqweqeqeqqeqeqeddddddwddwdqqweygqoeygoqyegoqyge";
    let result = LineName.create(lineName)
    sinon.assert.match(result.isFailure, true)
  });

  it('Can create colour', () => {
    const colour = "RGB(12,12,12)";
    let result = LineColour.create(colour)

    sinon.assert.match(result.isSuccess, true)
  });

  it('Cannot create LineColour with wrong colour', () => {
    const colour = "amarelo";
    let result = LineColour.create(colour)
    sinon.assert.match(result.isFailure, true)
  });
})