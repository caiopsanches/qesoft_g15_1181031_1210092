const sinon = require('sinon')
import Container from 'typedi'
import config from '../config'
import { Response, Request, NextFunction } from 'express'
import { Result } from '../core/logic/Result'
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { RouteId } from '../domain/Route/RouteId'
import { Name } from '../domain/Route/Name'
import { ShortName } from '../domain/network/valueObjects/shortName'
import { SegmentId } from '../domain/Route/SegmentId'
import { Distance } from '../domain/Route/Distance'
import { DeslocationTime } from '../domain/Route/DeslocationTime'
import { NetworkSegment } from '../domain/Route/NetworkSegment'
import { Route } from '../domain/Route/Route'
import { RouteDirection } from '../domain/Route/RouteDirection'
import IRouteDTO from '../dto/IRouteDTO'
import ISegementDTO from '../dto/ISegementDTO'
import IRouteRepo from '../repos/IRepos/IRouteRepo'
import IRouteService from '../services/IServices/IRouteService'
import IRouteController from '../controllers/IController/IRouteController'
import { Segments } from 'celebrate'


describe('Route integration test', () => {

    const routeIdE = RouteId.create('12')
    const nameE = Name.create('Paredes')
    const idNoInicioE = ShortName.create('PPP')
    const idNoFimE = ShortName.create('AAA')

    const segmentIdE = SegmentId.create('13')
    const idNo1E = ShortName.create('CCC')
    const idNo2E = ShortName.create('MPN')
    const distanceE = Distance.create('123')
    const deslocationTimeE = DeslocationTime.create(1233)

    // var segmentNetworkAux = new Array<NetworkSegment>()

    const seg1 = NetworkSegment.create({
        segmentId: segmentIdE.getValue(),
        idNo1: idNo1E.getValue(),
        idNo2: idNo2E.getValue(),
        distance: distanceE.getValue(),
        deslocationTime: deslocationTimeE.getValue()
    }, new UniqueEntityID("1")).getValue()

    var segmentsAux = new Array<NetworkSegment>()
    segmentsAux.push(seg1)

    let r: Result<Route> = Route.create({
        routeId: routeIdE.getValue(),
        name: nameE.getValue(),
        idNoInicio: idNoInicioE.getValue(),
        idNoFim: idNoFimE.getValue(),
        networkSegments: segmentsAux,
    }, new UniqueEntityID("12"))


    let req: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };


    const body = {
        "id": '12',
        "routeId": '12',
        "name": 'Paredes',
        "idNoInicio": 'PPP',
        "idNoFim": 'AAA',
        "networkSegments": {
            "segmentId": '13',
            "idNo1": 'CCC',
            "idNo2": 'MPN',
            "distance": '123',
            "deslocationTime": 1233
        },
    }

    req.body = body

    let routeRepoClass = require(config.repos.route.path).default
    let routeRepoInstance: IRouteRepo = Container.get(routeRepoClass)
    Container.set(config.repos.route.name, routeRepoInstance)
    routeRepoInstance = Container.get(config.repos.route.name)


    let routeServiceClass = require(config.services.route.path).default;
    let routeServiceInstance: IRouteService = Container.get(routeServiceClass)
    Container.set(config.services.route.name, routeServiceInstance);
    routeServiceInstance = Container.get(config.services.route.name);


    let routeControllerClass = require(config.controller.route.path).default;
    let routeControllerInstance: IRouteController = Container.get(routeControllerClass)
    Container.set(config.controller.route.name, routeControllerInstance);
    routeControllerInstance = Container.get(config.controller.route.name);

    beforeEach(() => {
        sinon.stub(routeServiceInstance, "createRoute").returns(Result.ok<IRouteDTO>({
            "id": req.body.id,
            "routeId": req.body.routeId,
            "name": req.body.name,
            "idNoInicio": req.body.idNoInicio,
            "idNoFim": req.body.idNoFim,
            "networkSegments": req.body.networkSegments
        }));
    });
    afterEach(function () {
        sinon.verifyAndRestore();
    });

    it('should create ', async () => {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
        sinon.stub(routeRepoInstance, "save").returns(r.getValue())

        await routeControllerInstance.createRoute(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": req.body.id,
            "routeId": req.body.routeId,
            "name": req.body.name,
            "idNoInicio": req.body.idNoInicio,
            "idNoFim": req.body.idNoFim,
            "networkSegments": req.body.networkSegments
        });
    });
})
