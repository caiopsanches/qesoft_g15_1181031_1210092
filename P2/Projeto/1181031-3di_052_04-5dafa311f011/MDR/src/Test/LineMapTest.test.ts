const sinon = require('sinon');
import { Result } from '../core/logic/Result';
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Line } from '../domain/line/Line';
import { LineId } from '../domain/line/LineId'
import { LineName } from '../domain/line/LineName'
import { LineColour } from '../domain/line/LineColour'
import { RouteId } from '../domain/Route/RouteId';
import { CrewmemberCode } from '../domain/crewmember/crewmemberCode';
import { VehicleCode } from '../domain/vehicle/vehicleCode';
import ILineDTO from '../dto/ILineDTO';
import { LineMap } from '../mappers/LineMap';
import { LinePath } from '../domain/line/LinePath';
import { RouteDirection } from '../domain/Route/RouteDirection';


describe('line map create', () => {

    const dto: ILineDTO = {
        "id": "1", "lineId": "1", "name": "800Gondomar",
        "colour": "RGB(12,12,12)", "paths": [
            {
                "routeId": "1",
                "orientation": "Go"
            },
            {
                "routeId": "2",
                "orientation": "Return"
            }
        ],
        "allowedVehicles": ["BMW", "MER"],
        "allowedDrivers": ["12", "13"]
    }

    const lineIdE = LineId.create("1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
      routeId: rt1.getValue(),
      orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
      routeId: rt2.getValue(),
      orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)

    let line: Result<Line> = Line.create({
        lineId: lineIdE.getValue(),
        name: nameE.getValue(),
        colour: colourE.getValue(),
        paths: pathsAux,
        allowedVehicles: allowedVehiclesAux,
        allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("1"))

    beforeEach(() => {
    });
    
    afterEach(function () {
        sinon.restore();
    });

    it('to DTO', async () => {
        sinon.assert.match(LineMap.toDTO(line.getValue()), dto)
    });

    it('to Domain', async () => {
        sinon.assert.match(await LineMap.toDomain(dto), line.getValue())
    });
})