import { expect } from 'chai';
import * as sinon from 'sinon';
import Container from 'typedi';
import config from '../config'
import { assert } from 'console';
import { RouteId } from "../domain/Route/RouteId";
import { UniqueEntityID } from '../core/domain/UniqueEntityID';
import { Result } from "../core/logic/Result";
import { Route } from "../domain/Route/Route";
import { DeslocationTime } from "../domain/Route/DeslocationTime";
import { Distance } from "../domain/Route/Distance";
import { Name } from "../domain/Route/Name";
import { NetworkSegment } from "../domain/Route/NetworkSegment";
import { RouteDirection } from "../domain/Route/RouteDirection";
import { SegmentId } from "../domain/Route/SegmentId";
import { ShortName } from "../domain/network/valueObjects/shortName";




describe('Route Test', () => {
    it('Can create Line with all values', () => {
        const routeIdE = RouteId.create('12')
        const nameE = Name.create('Paredes')
        const idNoInicioE = ShortName.create('PPP')
        const idNoFimE = ShortName.create('AAA')

        const segmentIdE = SegmentId.create('13')
        const idNo1E = ShortName.create('CCC')
        const idNo2E = ShortName.create('MPN')
        const distanceE = Distance.create('123')
        const deslocationTimeE = DeslocationTime.create(1233)


        const seg1 = NetworkSegment.create({
            segmentId: segmentIdE.getValue(),
            idNo1: idNo1E.getValue(),
            idNo2: idNo2E.getValue(),
            distance: distanceE.getValue(),
            deslocationTime: deslocationTimeE.getValue()
        }, new UniqueEntityID("1")).getValue()

        var segmentsAux = new Array<NetworkSegment>()
        segmentsAux.push(seg1)

        let result: Result<Route> = Route.create({
            routeId: routeIdE.getValue(),
            name: nameE.getValue(),
            idNoInicio: idNoInicioE.getValue(),
            idNoFim: idNoFimE.getValue(),
            networkSegments: segmentsAux,
        }, new UniqueEntityID("12"))

        sinon.assert.match(result.isSuccess, true)
    });

    it('Name cant have > 20 chars', () => {
        const nameE = Name.create('012345678901234567890123456789')
        sinon.assert.match(nameE.isFailure, false)
    })

    it('Route Nodes cant have > 20 chars', () => {
        const idNoInicioE = ShortName.create('012345678901234567890123456789')
        sinon.assert.match(idNoInicioE.isFailure, true)
    })

    it('Deslocation Time must be greater than 0 ', () => {
        const deslocationTimeE = DeslocationTime.create(0)
        sinon.assert.match(deslocationTimeE.isFailure, true)
    })

    it('Cannot create without Segments', () => {
        let expected = Result.ok<Route>();


        const routeIdE = RouteId.create('12')
        const nameE = Name.create('Paredes')
        const idNoInicioE = ShortName.create('PPP')
        const idNoFimE = ShortName.create('AAA')

        const segmentIdE = SegmentId.create('13')
        const idNo1E = ShortName.create('CCC')
        const idNo2E = ShortName.create('MPN')
        const distanceE = Distance.create('123')
        const deslocationTimeE = DeslocationTime.create(1233)


        const seg1 = NetworkSegment.create({
            segmentId: segmentIdE.getValue(),
            idNo1: idNo1E.getValue(),
            idNo2: idNo2E.getValue(),
            distance: distanceE.getValue(),
            deslocationTime: deslocationTimeE.getValue()
        }, new UniqueEntityID("1")).getValue()

        var segmentsAux = new Array<NetworkSegment>()
        segmentsAux.push(seg1)

        let result: Result<Route> = Route.create({
            routeId: routeIdE.getValue(),
            name: nameE.getValue(),
            idNoInicio: idNoInicioE.getValue(),
            idNoFim: idNoFimE.getValue(),
            networkSegments: null,
        }, new UniqueEntityID("12"))
        sinon.assert.match(result.isFailure, true)
    });

    it('Can create RouteId', () => {
        const routeIdE = "Route1";
        let result = RouteId.create(routeIdE)
        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create name', () => {
        const routeName = "800Gondomar";
        let result = ShortName.create(routeName)
        sinon.assert.match(result.isSuccess, true)
    });

    it('Cannot create RouteName with wrong routeName', () => {
        const routeName = "qweqweqeqeqqeqeqeddddddwddwdqqweygqoeygoqyegoqyge";
        let result = ShortName.create(routeName)
        sinon.assert.match(result.isFailure, true)
    });

    it('Can create distance', () => {
        const distance = "122";
        let result = Distance.create(distance)

        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create deslocation time', () => {
        const deslocationTime = 122;
        let result = DeslocationTime.create(deslocationTime)

        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create id nó inicio', () => {
        const shortName = '122';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create id nó FIM', () => {
        const shortName = '122';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create id nó 1', () => {
        const shortName = '122';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create id nó 2', () => {
        const shortName = '122';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, true)
    });

    it('Can create With invalid id nó inicio', () => {
        const shortName = '';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, false)
    });

    it('Can create With invalid id nó fim', () => {
        const shortName = '';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, false)
    });

    it('Can create With invalid id nó 1', () => {
        const shortName = '';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, false)
    });

    it('Can create With invalid id nó 2', () => {
        const shortName = '';
        let result = ShortName.create(shortName)

        sinon.assert.match(result.isSuccess, false)
    });

    it('Can create With invalid distance', () => {
        const distance = null;
        let result = Distance.create(distance)

        sinon.assert.match(result.isSuccess, false)
    });

    it('Can create With invalid deslocation time', () => {
        const deslocationTime = 0;
        let result = DeslocationTime.create(deslocationTime)

        sinon.assert.match(result.isSuccess, false)
    });



});
