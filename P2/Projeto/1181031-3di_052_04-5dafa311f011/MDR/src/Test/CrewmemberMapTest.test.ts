const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Result } from '../core/logic/Result';
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Mapper } from "../core/infra/Mapper";
import { CrewmemberCode } from "../domain/crewmember/crewmemberCode"
import { CrewmemberDescription } from "../domain/crewmember/crewmemberDescription"
import { Crewmember } from '../domain/crewmember/crewmember'
import ICrewmemberDTO from "../dto/ICrewmemberDTO";
import { CrewmemberMap } from '../mappers/CrewmemberMap';

describe('crewmember map create', () => {

    const dto: ICrewmemberDTO = {
        id: "crew62",
        code: "crew62",
        description: "desc1"
    }

    const codeE = CrewmemberCode.create('crew62')
    const descriptionE = CrewmemberDescription.create("desc1")

    let crewmember: Result<Crewmember> = Crewmember.create({
        code: codeE.getValue(),
        description: descriptionE.getValue()
    }, new UniqueEntityID("crew62"))

    beforeEach(() => {
    });
    
    afterEach(function () {
        sinon.restore();
    });

    it('to DTO', async () => {
        sinon.assert.match(CrewmemberMap.toDTO(crewmember.getValue()), dto)
    });

    it('to Domain', async () => {
        sinon.assert.match(await CrewmemberMap.toDomain(dto), crewmember.getValue())
    });

    it('to persistence', async () => {
        sinon.assert.match(CrewmemberMap.toPersistence(crewmember.getValue()), dto)
    });
})