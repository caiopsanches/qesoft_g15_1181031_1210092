const sinon = require('sinon')
import Container from 'typedi'
import config from '../config'
import { Response, Request, NextFunction } from 'express'
import INodeService from '../services/IServices/INodeService'
import INodeController from '../controllers/IController/INodeController'
import { Result } from '../core/logic/Result'
import INodeRepo from '../repos/IRepos/INodeRepo'
import { Node } from "../domain/network/node"
import { ShortName } from "../domain/network/valueObjects/shortName"
import { NodeName } from "../domain/network/valueObjects/nodeName"
import { MaxTimeStop } from "../domain/network/valueObjects/maxTimeStop"
import { VehicleCapacity } from "../domain/network/valueObjects/vehicleCapacity"
import { NodeLocation } from "../domain/network/valueObjects/nodeLocations"
import { Latitude } from "../domain/network/valueObjects/latitude"
import { Longitude } from "../domain/network/valueObjects/longitude"
import { IsDepot } from "../domain/network/valueObjects/isDepot"
import { IsReliefPoint } from "../domain/network/valueObjects/isReliefPoint"
import INodeDTO from "../dto/INodeDTO"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"

describe('Node integration test', () => {
    const shortName = ShortName.create('CAMPAN')
    const nodeName = NodeName.create('Campanha')
    const maxTimeStop = MaxTimeStop.create(20)
    const vehicleCapacity = VehicleCapacity.create(30)
    const nodeLatitude = Latitude.create(41.543536)
    const nodeLongitude = Longitude.create(-8.543654)
    const nodeLocation = NodeLocation.create({latitude: nodeLatitude.getValue(), longitude: nodeLongitude.getValue()})
    const isDepot = IsDepot.create(true)
    const isReliefPoint = IsReliefPoint.create(false)

    let node: Result<Node> = Node.create({
        shortName: shortName.getValue(),
        nodeName: nodeName.getValue(),
        maxTimeStop: maxTimeStop.getValue(),
        vehicleCapacity: vehicleCapacity.getValue(),
        nodeLocation: nodeLocation.getValue(),
        isDepot: isDepot.getValue(),
        isReliefPoint: isReliefPoint.getValue()
    }, new UniqueEntityID('CAMPAN'))

    let req: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };

    const body: INodeDTO = {
        shortName: "CAMPAN",
        nodeName: "Campanha",
        maxTimeStop: 20,
        vehicleCapacity: 30,
        latitude: 41.543536,
        longitude: -8.543654,
        isDepot: true,
        isReliefPoint: false
    }

    req.body = body


    let nodeRepoClass = require(config.repos.node.path).default
    let nodeRepoInstance: INodeRepo = Container.get(nodeRepoClass)
    Container.set(config.repos.node.name, nodeRepoInstance)
    nodeRepoInstance = Container.get(config.repos.node.name)

    let nodeServiceClass = require(config.services.node.path).default;
    let nodeServiceInstance: INodeService = Container.get(nodeServiceClass)
    Container.set(config.services.node.name, nodeServiceInstance);
    nodeServiceInstance = Container.get(config.services.node.name);
    let nodeControllerClass = require(config.controller.node.path).default;
    let nodeControllerInstance: INodeController = Container.get(nodeControllerClass)
    Container.set(config.controller.node.name, nodeControllerInstance);
    nodeControllerInstance = Container.get(config.controller.node.name);

    beforeEach(() => {


    });
    afterEach(function () {
        sinon.restore();
    });
    it('should create ', async () => {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
        sinon.stub(nodeRepoInstance, "save").returns(node.getValue())
        await nodeControllerInstance.createNode(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "shortName": req.body.shortName,
            "nodeName": req.body.nodeName,
            "maxTimeStop": req.body.maxTimeStop,
            "vehicleCapacity": req.body.vehicleCapacity,
            "latitude": req.body.latitude,
            "longitude": req.body.longitude,
            "isDepot": req.body.isDepot,
            "isReliefPoint": req.body.isReliefPoint
        });

    });

})