const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import ILineService from '../services/IServices/ILineService'
import ILineController from '../controllers/IController/ILineController';
import ILineDTO from "../dto/ILineDTO";
import { Result } from '../core/logic/Result';
import ILineRepo from '../repos/IRepos/ILineRepo';
import { Line } from "../domain/line/Line"
import { LineId } from "../domain/line/LineId"
import { LineName } from "../domain/line/LineName"
import { LineColour } from "../domain/line/LineColour"
import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { CrewmemberCode } from "../domain/crewmember/crewmemberCode"
import { RouteId } from "../domain/Route/RouteId"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { LinePath } from '../domain/line/LinePath';
import { RouteDirection } from '../domain/Route/RouteDirection';

describe('line integration test', () => {
    const lineIdE = LineId.create("1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
        routeId: rt1.getValue(),
        orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
        routeId: rt2.getValue(),
        orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)

    let line: Result<Line> = Line.create({
        lineId: lineIdE.getValue(),
        name: nameE.getValue(),
        colour: colourE.getValue(),
        paths: pathsAux,
        allowedVehicles: allowedVehiclesAux,
        allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("1"))

    let req: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };
    const body = {
        "id": 'line12', "lineId": '1', "name": '800Gondomar',
        "colour": 'RGB(12,12,12)', "paths": [
            {
                "routeId": "1",
                "orientation": "Go"
            },
            {
                "routeId": "2",
                "orientation": "Return"
            }
        ],
        "allowedVehicles": ["BMW", "MER"],
        "allowedDrivers": ["maedohugo", "pai"]
    };
    req.body = body;


    let lineRepoClass = require(config.repos.line.path).default
    let lineRepoInstance: ILineRepo = Container.get(lineRepoClass)
    Container.set(config.repos.line.name, lineRepoInstance)
    lineRepoInstance = Container.get(config.repos.line.name)

    let lineServiceClass = require(config.services.line.path).default;
    let lineServiceInstance: ILineService = Container.get(lineServiceClass)
    Container.set(config.services.line.name, lineServiceInstance);
    lineServiceInstance = Container.get(config.services.line.name);

    let lineControllerClass = require(config.controller.line.path).default;
    let lineControllerInstance: ILineController = Container.get(lineControllerClass)
    Container.set(config.controller.line.name, lineControllerInstance);
    lineControllerInstance = Container.get(config.controller.line.name);

    beforeEach(() => {
        sinon.stub(lineServiceInstance, "createLine").returns(Result.ok<ILineDTO>({
            "id": req.body.id, "lineId": req.body.lineId, "name": req.body.name,
            "colour": req.body.colour, "paths": req.body.paths,
            "allowedVehicles": req.body.allowedVehicles,
            "allowedDrivers": req.body.allowedDrivers
        }));
    });
    afterEach(function () {
        sinon.verifyAndRestore();
    });
    it('should create ', async () => {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
        sinon.stub(lineRepoInstance, "save").returns(line.getValue())

        await lineControllerInstance.createLine(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": req.body.id, "lineId": req.body.lineId, "name": req.body.name,
            "colour": req.body.colour, "paths": req.body.paths,
            "allowedVehicles": req.body.allowedVehicles,
            "allowedDrivers": req.body.allowedDrivers
        });
    });
})