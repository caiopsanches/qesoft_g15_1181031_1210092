const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import IVehicleService from '../services/IServices/IVehicleService'
import IVehicleController from '../controllers/IController/IvehicleController';
import IVehicleDTO from "../dto/IVehicleDTO";
import { Result } from '../core/logic/Result';
import IVehicleRepo from '../repos/IRepos/IVehicleRepo';
import { Vehicle } from "../domain/vehicle/vehicle"
import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { VehicleEnergy } from "../domain/vehicle/vehicleEnergy"
import { VehicleAutonomy } from "../domain/vehicle/vehicleAutonomy"
import { VehicleAvgSpeed } from "../domain/vehicle/vehicleAvgSpeed"
import { VehicleKmCost } from "../domain/vehicle/vehicleKmCost"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { VehicleAvgConsumption } from "../domain/vehicle/vehicleAvgConsumption"

describe('vehicle integration test', () => {
    const codeE = VehicleCode.create('mini-bus')
    const energyE = VehicleEnergy.create("Diesel")
    const autonomyE = VehicleAutonomy.create(4)
    const kmCostE = VehicleKmCost.create(1)
    const avgConsumptionE = VehicleAvgConsumption.create(1)
    const avgSpeedE = VehicleAvgSpeed.create(5)

    let vehicle: Result<Vehicle> = Vehicle.create({
        code: codeE.getValue(),
        energy: energyE.getValue(),
        autonomy: autonomyE.getValue(),
        kmCost: kmCostE.getValue(),
        avgConsumption: avgConsumptionE.getValue(),
        avgSpeed: avgSpeedE.getValue(),
    }, new UniqueEntityID("mini-bus"))

    let req: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };

    const body: IVehicleDTO = {
        "id": "mini-bus",
        "code": "mini-bus",
        "energy": "Diesel",
        "autonomy": 4,
        "kmCost": 1,
        "avgConsumption": 1,
        "avgSpeed": 5
    }

    req.body = body


    let vehicleRepoClass = require(config.repos.vehicle.path).default
    let vehicleRepoInstance: IVehicleRepo = Container.get(vehicleRepoClass)
    Container.set(config.repos.vehicle.name, vehicleRepoInstance)
    vehicleRepoInstance = Container.get(config.repos.vehicle.name)

    let vehicleServiceClass = require(config.services.vehicle.path).default;
    let vehicleServiceInstance: IVehicleService = Container.get(vehicleServiceClass)
    Container.set(config.services.vehicle.name, vehicleServiceInstance);
    vehicleServiceInstance = Container.get(config.services.vehicle.name);
    let vehicleControllerClass = require(config.controller.vehicle.path).default;
    let vehicleControllerInstance: IVehicleController = Container.get(vehicleControllerClass)
    Container.set(config.controller.vehicle.name, vehicleControllerInstance);
    vehicleControllerInstance = Container.get(config.controller.vehicle.name);

    beforeEach(() => {

    });
    afterEach(function () {
        sinon.restore();
    });
    it('should create ', async () => {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
        sinon.stub(vehicleRepoInstance, "save").returns(vehicle.getValue())
        await vehicleControllerInstance.createVehicle(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": "mini-bus",
            "code": req.body.code,
            "energy": req.body.energy,
            "autonomy": req.body.autonomy,
            "kmCost": req.body.kmCost,
            "avgConsumption": req.body.avgConsumption,
            "avgSpeed": req.body.avgSpeed
        });

    });

})