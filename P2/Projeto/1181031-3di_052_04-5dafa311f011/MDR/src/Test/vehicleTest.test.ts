import * as sinon from 'sinon';
import Container from 'typedi';
import config from '../config'
import { Vehicle } from '../domain/vehicle/vehicle';
import { Result } from "../core/logic/Result"
import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { VehicleEnergy } from "../domain/vehicle/vehicleEnergy"
import { VehicleAutonomy } from "../domain/vehicle/vehicleAutonomy"
import { VehicleAvgSpeed } from "../domain/vehicle/vehicleAvgSpeed"
import { VehicleKmCost } from "../domain/vehicle/vehicleKmCost"
import { VehicleAvgConsumption } from "../domain/vehicle/vehicleAvgConsumption"
import { expect } from 'chai';
import { assert } from 'console';
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
describe('Vehicle Test', () => {

    it('Can create Vehicle with all props', () => {
        const codeE = VehicleCode.create('mini-bus')
        const energyE = VehicleEnergy.create("Diesel")
        const autonomyE = VehicleAutonomy.create(200)
        const kmCostE = VehicleKmCost.create(2)
        const avgConsumptionE = VehicleAvgConsumption.create(4)
        const avgSpeedE = VehicleAvgSpeed.create(79)



        let v: Result<Vehicle> = Vehicle.create({
            code: codeE.getValue(),
            energy: energyE.getValue(),
            autonomy: autonomyE.getValue(),
            kmCost: kmCostE.getValue(),
            avgConsumption: avgConsumptionE.getValue(),
            avgSpeed: avgSpeedE.getValue(),

        }, new UniqueEntityID("mini-bus"))
        sinon.assert.match(v.isSuccess, true)
    }),


        it('Vehicle Code cant have > 20 chars', () => {
            const codeE = VehicleCode.create('mini-bus a diesel com autonomia de 200')
            sinon.assert.match(codeE.isFailure, true)
        })
    it('Vehicle Autonomy must be greater than 0 ', () => {
        const aE = VehicleAutonomy.create(0)
        sinon.assert.match(aE.isFailure, true)
    })
    it('Vehicle AVG consumption cant be less or equals than 0 ', () => {
        const aE = VehicleAvgConsumption.create(-2)
        sinon.assert.match(aE.isFailure, true)
    })
    it('Vehicle AVG speed  cant be less or equals than 0 ', () => {
        const aE = VehicleAvgConsumption.create(-10)
        sinon.assert.match(aE.isFailure, true)
    })

    it('Vehicle energy cant be something not following the existing ones', () => {
        const aE = VehicleEnergy.create("Water")
        sinon.assert.match(aE.isFailure, true)
    })


    it('Vehicle energy cant be something not following the existing ones', () => {
        const aE = VehicleEnergy.create("Water")
        sinon.assert.match(aE.isFailure, true)
    })
}

);