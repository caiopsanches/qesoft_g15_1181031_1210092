const sinon = require('sinon')
import { Result } from '../core/logic/Result'
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { Node } from "../domain/network/node"
import { ShortName } from "../domain/network/valueObjects/shortName"
import { NodeName } from "../domain/network/valueObjects/nodeName"
import { MaxTimeStop } from "../domain/network/valueObjects/maxTimeStop"
import { VehicleCapacity } from "../domain/network/valueObjects/vehicleCapacity"
import { NodeLocation } from "../domain/network/valueObjects/nodeLocations"
import { Latitude } from "../domain/network/valueObjects/latitude"
import { Longitude } from "../domain/network/valueObjects/longitude"
import { IsDepot } from "../domain/network/valueObjects/isDepot"
import { IsReliefPoint } from "../domain/network/valueObjects/isReliefPoint"
import INodeDTO from "../dto/INodeDTO"
import { NodeMap } from '../mappers/NodeMap'

describe('Node map create', () => {

    const dto: INodeDTO = {
        shortName: "CAMPAN",
        nodeName: "Campanha",
        maxTimeStop: 20,
        vehicleCapacity: 30,
        latitude: 41.543536,
        longitude: -8.543654,
        isDepot: true,
        isReliefPoint: false
    }

    const shortName = ShortName.create('CAMPAN')
    const nodeName = NodeName.create('Campanha')
    const maxTimeStop = MaxTimeStop.create(20)
    const vehicleCapacity = VehicleCapacity.create(30)
    const nodeLatitude = Latitude.create(41.543536)
    const nodeLongitude = Longitude.create(-8.543654)
    const nodeLocation = NodeLocation.create({latitude: nodeLatitude.getValue(), longitude: nodeLongitude.getValue()})
    const isDepot = IsDepot.create(true)
    const isReliefPoint = IsReliefPoint.create(false)

    let node: Result<Node> = Node.create({
        shortName: shortName.getValue(),
        nodeName: nodeName.getValue(),
        maxTimeStop: maxTimeStop.getValue(),
        vehicleCapacity: vehicleCapacity.getValue(),
        nodeLocation: nodeLocation.getValue(),
        isDepot: isDepot.getValue(),
        isReliefPoint: isReliefPoint.getValue()
    }, new UniqueEntityID('CAMPAN'))

    beforeEach(() => {
    });
    
    afterEach(function () {
        sinon.restore();
    });

    it('to DTO', async () => {
        sinon.assert.match(NodeMap.toDTO(node.getValue()), dto)
    });

    it('to Domain', async () => {
        sinon.assert.match(await NodeMap.toDomain(dto), node.getValue())
    });

    it('to persistence', async () => {
        sinon.assert.match(NodeMap.toPersistence(node.getValue()), dto)
    });
})