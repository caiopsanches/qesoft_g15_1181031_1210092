const sinon = require('sinon')
import Container from 'typedi'
import config from '../config'
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { Result } from '../core/logic/Result'
import { ShortName } from "../domain/network/valueObjects/shortName"
import { DeslocationTime } from '../domain/Route/DeslocationTime'
import { Distance } from '../domain/Route/Distance'
import { Name } from '../domain/Route/Name'
import { NetworkSegment } from '../domain/Route/NetworkSegment'
import { Route } from '../domain/Route/Route'
import { RouteDirection } from '../domain/Route/RouteDirection'
import { RouteId } from '../domain/Route/RouteId'
import { SegmentId } from '../domain/Route/SegmentId'
import IRouteDTO from '../dto/IRouteDTO'
import ISegementDTO from '../dto/ISegementDTO'
import IRouteRepo from '../repos/IRepos/IRouteRepo'
import IRouteService from '../services/IServices/IRouteService'

describe('Route service create', () => {

     const routeIdE = RouteId.create('12')
     const nameE = Name.create('Paredes')
     const idNoInicioE = ShortName.create('PPP')
     const idNoFimE = ShortName.create('AAA')

     const segmentIdE = SegmentId.create('13')
     const idNo1E = ShortName.create('CCC')
     const idNo2E = ShortName.create('MPN')
     const distanceE = Distance.create('123')
     const deslocationTimeE = DeslocationTime.create(1233)


     const seg1 = NetworkSegment.create({
         segmentId: segmentIdE.getValue(),
         idNo1: idNo1E.getValue(),
         idNo2: idNo2E.getValue(),
         distance: distanceE.getValue(),
         deslocationTime: deslocationTimeE.getValue()
     }, new UniqueEntityID("13")).getValue()

     var segmentsAux = new Array<NetworkSegment>()
     segmentsAux.push(seg1)

     let route: Result<Route> = Route.create({
         routeId: routeIdE.getValue(),
         name: nameE.getValue(),
         idNoInicio: idNoInicioE.getValue(),
         idNoFim: idNoFimE.getValue(),
         networkSegments: segmentsAux,
     }, new UniqueEntityID("12"))

     let res = Result.ok<IRouteDTO>({
         id: '12',
         routeId: '12',
         name: 'Paredes',
         idNoInicio: 'PPP',
         idNoFim: 'AAA',
         networkSegments: [{
             segmentId: '13',
             idNo1: 'CCC',
             idNo2: 'MPN',
             distance: '123',
             deslocationTime: 1233
         }],
     })

    const dto: IRouteDTO = {
         "id": '12',
         "routeId": '12',
         "name": 'Paredes',
         "idNoInicio": 'PPP',
         "idNoFim": 'AAA',
         "networkSegments": [{
             "segmentId": '13',
             "idNo1": 'CCC',
             "idNo2": 'MPN',
             "distance": '123',
             "deslocationTime": 1233,
         }],
     }

     var allRoutes = new Array<Result<IRouteDTO>>()
     allRoutes.push(res)


     let routeServiceClass = require(config.services.route.path).default;
     let routeServiceInstance: IRouteService = Container.get(routeServiceClass)
     Container.set(config.services.route.name, routeServiceInstance);
     routeServiceInstance = Container.get(config.services.route.name);

     let routeRepoClass = require(config.repos.route.path).default
     let routeRepoInstance: IRouteRepo = Container.get(routeRepoClass)
     Container.set(config.repos.route.name, routeRepoInstance)
     routeRepoInstance = Container.get(config.repos.route.name);

     beforeEach(() => {
     });

     afterEach(function () {
         sinon.restore();
     });

     it('create route: should create ', function (done) {
         sinon.stub(routeRepoInstance, "save").returns(route.getValue())
         sinon.assert.match((routeServiceInstance.createRoute(dto)), Promise.resolve(res));
         done();
     })

    
})