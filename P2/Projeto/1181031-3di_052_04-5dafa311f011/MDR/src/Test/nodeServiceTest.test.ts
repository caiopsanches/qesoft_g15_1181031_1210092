const sinon = require('sinon')
import Container from 'typedi'
import config from '../config'
import INodeService from '../services/IServices/INodeService'
import INodeDTO from "../dto/INodeDTO"
import { Result } from '../core/logic/Result'
import INodeRepo from '../repos/IRepos/INodeRepo'
import { Node } from "../domain/network/node"
import { ShortName } from "../domain/network/valueObjects/shortName"
import { NodeName } from "../domain/network/valueObjects/nodeName"
import { MaxTimeStop } from "../domain/network/valueObjects/maxTimeStop"
import { VehicleCapacity } from "../domain/network/valueObjects/vehicleCapacity"
import { NodeLocation } from "../domain/network/valueObjects/nodeLocations"
import { Latitude } from "../domain/network/valueObjects/latitude"
import { Longitude } from "../domain/network/valueObjects/longitude"
import { IsDepot } from "../domain/network/valueObjects/isDepot"
import { IsReliefPoint } from "../domain/network/valueObjects/isReliefPoint"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"

describe('Node service', () => {

    const shortName = ShortName.create('CAMPAN')
    const nodeName = NodeName.create('Campanha')
    const maxTimeStop = MaxTimeStop.create(20)
    const vehicleCapacity = VehicleCapacity.create(30)
    const nodeLatitude = Latitude.create(41.543536)
    const nodeLongitude = Longitude.create(-8.543654)
    const nodeLocation = NodeLocation.create({latitude: nodeLatitude.getValue(), longitude: nodeLongitude.getValue()})
    const isDepot = IsDepot.create(true)
    const isReliefPoint = IsReliefPoint.create(false)

    let node: Result<Node> = Node.create({
        shortName: shortName.getValue(),
        nodeName: nodeName.getValue(),
        maxTimeStop: maxTimeStop.getValue(),
        vehicleCapacity: vehicleCapacity.getValue(),
        nodeLocation: nodeLocation.getValue(),
        isDepot: isDepot.getValue(),
        isReliefPoint: isReliefPoint.getValue()
    }, new UniqueEntityID('CAMPAN'))

    let res = Result.ok<INodeDTO>({
        shortName: "MATRECO",
        nodeName: "Campanha",
        maxTimeStop: 40,
        vehicleCapacity: 60,
        latitude: 41.534534,
        longitude: -8.584374,
        isDepot: true,
        isReliefPoint: false
    })

    var array = new Array<Result<INodeDTO>>()
    array.push(res)

    const dto: INodeDTO = {
        shortName: "MATRECO",
        nodeName: "Campanha",
        maxTimeStop: 40,
        vehicleCapacity: 60,
        latitude: 41.534534,
        longitude: -8.584374,
        isDepot: true,
        isReliefPoint: false
    }

    let nodeServiceClass = require(config.services.node.path).default;
    let nodeServiceInstance: INodeService = Container.get(nodeServiceClass)
    Container.set(config.services.node.name, nodeServiceInstance);
    nodeServiceInstance = Container.get(config.services.node.name);


    let nodeRepoClass = require(config.repos.node.path).default
    let nodeRepoInstance: INodeRepo = Container.get(nodeRepoClass)
    Container.set(config.repos.node.name, nodeRepoInstance)
    nodeRepoInstance = Container.get(config.repos.node.name)

    beforeEach(() => {
    });
    
    afterEach(function () {
        sinon.restore()
    });

    it('should create ', async () => {
        sinon.stub(nodeRepoInstance, "save").returns(node.getValue())
        sinon.assert.match((await nodeServiceInstance.createNode(dto)), res)
    })

    it('should get all ', function(done) {
        sinon.stub(nodeRepoInstance, "findAll").returns(node.getValue())
        sinon.assert.match((nodeServiceInstance.getAllNodes("")), Promise.resolve(array))
        done()
    })

    it('should get by short name ', function(done) {
        sinon.stub(nodeRepoInstance, "findByShortName").returns(node.getValue())
        sinon.assert.match((nodeServiceInstance.getNodeByShortName("", "")), Promise.resolve(array))
        done()
    })

    it('should get by name ', function(done) {
        sinon.stub(nodeRepoInstance, "findByName").returns(node.getValue())
        sinon.assert.match((nodeServiceInstance.getNodeByNodeName("", "")), Promise.resolve(array))
        done()
    })
})