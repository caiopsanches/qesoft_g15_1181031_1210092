const  sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import INodeService from '../services/IServices/INodeService'
import INodeController from '../controllers/IController/INodeController';
import INodeDTO from "../dto/INodeDTO";
import { Result } from '../core/logic/Result';
import { AlternativeServiceOptions } from 'http2';
import NodeController from '../controllers/nodeController'

describe('Node controller create', () => {

    let req: Partial<Request> = {};
    let req1: Partial<Request> = {};
    let req2: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };
    let next1: Partial<NextFunction> = () => { };

    const body: INodeDTO = {
        "shortName": "CAMPAN",
        "nodeName": "Campanha",
        "maxTimeStop": 40,
        "vehicleCapacity": 60,
        "latitude": 41.534534,
        "longitude": -8.584374,
        "isDepot": true,
        "isReliefPoint": false
    }

    req.body = body
    req1.body = {}
    req2.query = {}

    let nodeServiceClass = require(config.services.node.path).default;
    let nodeServiceInstance: INodeService = Container.get(nodeServiceClass)
    Container.set(config.services.node.name, nodeServiceInstance);
    nodeServiceInstance = Container.get(config.services.node.name);
    let nodeControllerClass = require(config.controller.node.path).default;
    let nodeControllerInstance: INodeController = Container.get(nodeControllerClass)
    Container.set(config.controller.node.name, nodeControllerInstance);
    nodeControllerInstance = Container.get(config.controller.node.name);

    beforeEach(() => {

      
        sinon.stub(nodeServiceInstance, "createNode").returns(Result.ok<INodeDTO>({
            "shortName": req.body.shortName,
            "nodeName": req.body.nodeName,
            "maxTimeStop": req.body.maxTimeStop,
            "vehicleCapacity": req.body.vehicleCapacity,
            "latitude": req.body.latitude,
            "longitude": req.body.longitude,
            "isDepot": req.body.isDepot,
            "isReliefPoint": req.body.isReliefPoint
        }));
    });
    afterEach(function () {
        sinon.verifyAndRestore();
    });
    it('should create ', async () => {

        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
       

        await nodeControllerInstance.createNode(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "shortName": req.body.shortName,
            "nodeName": req.body.nodeName,
            "maxTimeStop": req.body.maxTimeStop,
            "vehicleCapacity": req.body.vehicleCapacity,
            "latitude": req.body.latitude,
            "longitude": req.body.longitude,
            "isDepot": req.body.isDepot,
            "isReliefPoint": req.body.isReliefPoint
        });
    });

    it('should get ', async () => {

        const jsonStub = sinon.stub(nodeServiceInstance,"getAllNodes").returns(Result.ok<Array<INodeDTO>>([{
            "shortName": "CAMPAN",
            "nodeName": "Campanha",
            "maxTimeStop": 40,
            "vehicleCapacity": 60,
            "latitude": 41.534534,
            "longitude": -8.584374,
            "isDepot": true,
            "isReliefPoint": false
        }]))
        
        const res = { json: jsonStub => ({ status: status, send: err => err }) }
        const statusSpy = sinon.spy(res, 'json')
       

        const ctrl = new NodeController(nodeServiceInstance as INodeService);

        await ctrl.getNodes(<Request>req2, <Response><unknown>res, <NextFunction>next1);
        sinon.assert.calledWith(res.json, [ {
            "shortName": req.body.shortName,
            "nodeName": req.body.nodeName,
            "maxTimeStop": req.body.maxTimeStop,
            "vehicleCapacity": req.body.vehicleCapacity,
            "latitude": req.body.latitude,
            "longitude": req.body.longitude,
            "isDepot": req.body.isDepot,
            "isReliefPoint": req.body.isReliefPoint
        }])
    });

})