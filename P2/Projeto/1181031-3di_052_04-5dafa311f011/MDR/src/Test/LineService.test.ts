const sinon = require('sinon');
import { Container } from 'typedi';
import config from "../config";
import { Result } from '../core/logic/Result';
import ILineRepo from "../repos/IRepos/ILineRepo";
import ILineDTO from '../dto/ILineDTO';
import ILineService from '../services/IServices/ILineService'
import { UniqueEntityID } from '../core/domain/UniqueEntityID';
import { Line } from '../domain/line/Line';
import { LineId } from '../domain/line/LineId'
import { LineName } from '../domain/line/LineName'
import { LineColour } from '../domain/line/LineColour'
import { RouteId } from '../domain/Route/RouteId';
import { CrewmemberCode } from '../domain/crewmember/crewmemberCode';
import { VehicleCode } from '../domain/vehicle/vehicleCode';
import IVehicleRepo from '../repos/IRepos/IVehicleRepo';
import { RouteDirection } from '../domain/Route/RouteDirection';
import { LinePath } from '../domain/line/LinePath';

describe('Line Service',  () => {

    const lineIdE = LineId.create("1");
    const nameE = LineName.create("800Gondomar");
    const colourE = LineColour.create("RGB(12,12,12)");

    const cod1 = VehicleCode.create("BMW");
    const cod2 = VehicleCode.create("MER");

    var allowedVehiclesAux = new Array<VehicleCode>()
    allowedVehiclesAux.push(cod1.getValue())
    allowedVehiclesAux.push(cod2.getValue())

    const drv1 = CrewmemberCode.create("12")
    const drv2 = CrewmemberCode.create("13")

    var allowedDriversAux = new Array<CrewmemberCode>()
    allowedDriversAux.push(drv1.getValue())
    allowedDriversAux.push(drv2.getValue())

    const rt1 = RouteId.create("1")
    const rt2 = RouteId.create("2")

    const ort1 = RouteDirection.create("Go")
    const ort2 = RouteDirection.create("Return")

    const pat1 = LinePath.create({
      routeId: rt1.getValue(),
      orientation: ort1.getValue(),
    }, new UniqueEntityID("1")).getValue()

    const pat2 = LinePath.create({
      routeId: rt2.getValue(),
      orientation: ort2.getValue(),
    }, new UniqueEntityID("2")).getValue()

    var pathsAux = new Array<LinePath>()
    pathsAux.push(pat1)
    pathsAux.push(pat2)

    let line: Result<Line> = Line.create({
        lineId: lineIdE.getValue(),
        name: nameE.getValue(),
        colour: colourE.getValue(),
        paths: pathsAux,
        allowedVehicles: allowedVehiclesAux,
        allowedDrivers: allowedDriversAux
    }, new UniqueEntityID("1"))

    let res = Result.ok<ILineDTO>({
        id: "1", lineId: "1", name: "800Gondomar",
        colour: "RGB(21,12,12)", paths: [
            {
                routeId: "1",
                orientation: "Go"
            },
            {
                routeId: "2",
                orientation: "Return"
            }
        ],
        allowedVehicles: ["BMW", "MER"],
        allowedDrivers: ["maedohugo", "pai"]
    });
    
    const dto: ILineDTO = {
        "id": "1", "lineId": "1", "name": "800Gondomar",
        "colour": "RGB(21,12,12)", "paths": [
            {
                "routeId": "1",
                "orientation": "Go"
            },
            {
                "routeId": "2",
                "orientation": "Return"
            }
        ],
        "allowedVehicles": ["BMW", "MER"],
        "allowedDrivers": ["maedohugo", "pai"]
    }

    var allLines = new Array<Result<ILineDTO>>()
    allLines.push(res)

    let lineServiceClass = require(config.services.line.path).default;
    let lineServiceInstance: ILineService = Container.get(lineServiceClass)
    Container.set(config.services.line.name, lineServiceInstance)
    lineServiceInstance = Container.get(config.services.line.name)

    let lineRepoClass = require(config.repos.line.path).default;
    let lineRepoInstance : ILineRepo = Container.get(lineRepoClass)
    Container.set(config.repos.line.name, lineRepoInstance)
    lineRepoInstance = Container.get(config.repos.line.name)

    let vehicleRepoClass = require(config.repos.vehicle.path).default
    let vehicleRepoInstance: IVehicleRepo = Container.get(vehicleRepoClass)
    Container.set(config.repos.vehicle.name, vehicleRepoInstance)
    vehicleRepoInstance = Container.get(config.repos.vehicle.name);

    beforeEach(() => {
    });

    afterEach(function () {
        sinon.restore()
    });

    it('createLine: should create', function(done) {
        sinon.stub(lineRepoInstance, "save").returns(line.getValue())
        sinon.assert.match((lineServiceInstance.createLine(dto)), Promise.resolve(res));
        done();
    })

    it('getAllLines: should return all', function(done) {
        sinon.stub(lineRepoInstance, "findAll").returns(line.getValue())
        sinon.assert.match((lineServiceInstance.getAllLines('')), Promise.resolve(allLines));
        done();
    })

    it('getLineById: should return by Id', function(done) {
        sinon.stub(lineRepoInstance, "findByLineId").returns(line.getValue())
        sinon.assert.match((lineServiceInstance.getLineByLineId('1','')), Promise.resolve(allLines));
        done();
    })

    it('getLineById: should return by Name', function(done) {
        sinon.stub(lineRepoInstance, "findByLineName").returns(line.getValue())
        sinon.assert.match((lineServiceInstance.getLineByLineName('800Gondomar','')), Promise.resolve(allLines));
        done();
    })
});