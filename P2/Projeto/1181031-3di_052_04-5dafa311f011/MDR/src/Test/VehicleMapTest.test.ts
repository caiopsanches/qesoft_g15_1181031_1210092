const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import IVehicleDTO from "../dto/IVehicleDTO";
import { Result } from '../core/logic/Result';
import { VehicleMap } from '../mappers/VehicleMap'
import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { VehicleEnergy } from "../domain/vehicle/vehicleEnergy"
import { VehicleAutonomy } from "../domain/vehicle/vehicleAutonomy"
import { VehicleAvgSpeed } from "../domain/vehicle/vehicleAvgSpeed"
import { VehicleKmCost } from "../domain/vehicle/vehicleKmCost"
import { VehicleAvgConsumption } from "../domain/vehicle/vehicleAvgConsumption"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { Vehicle } from '../domain/vehicle/vehicle';
import * as sinon from 'sinon';
describe('vehicle mapper test', () => {

    const codeE = VehicleCode.create('mini-bus')
    const energyE = VehicleEnergy.create("Diesel")
    const autonomyE = VehicleAutonomy.create(4)
    const kmCostE = VehicleKmCost.create(1)
    const avgConsumptionE = VehicleAvgConsumption.create(1)
    const avgSpeedE = VehicleAvgSpeed.create(5)



    let v: Result<Vehicle> = Vehicle.create({
        code: codeE.getValue(),
        energy: energyE.getValue(),
        autonomy: autonomyE.getValue(),
        kmCost: kmCostE.getValue(),
        avgConsumption: avgConsumptionE.getValue(),
        avgSpeed: avgSpeedE.getValue(),

    }, new UniqueEntityID("mini-bus"))

    const dto: IVehicleDTO = {
        "id": "mini-bus",
        "code": "mini-bus",
        "energy": "Diesel",
        "autonomy": 4,
        "kmCost": 1,
        "avgConsumption": 1,
        "avgSpeed": 5
    }

    const dto1 = {
        "code": "mini-bus a disel e com 4 de autonomia",
        "energy": "Diesel",
        "autonomy": 4,
        "kmCost": 1,
        "avgConsumption": 1,
        "avgSpeed": 5
    }

    beforeEach(() => {


    });
    afterEach(function () {
        sinon.restore();
    });
    it('from domain to DTO ', async () => {
        sinon.assert.match(VehicleMap.toDTO(v.getValue()), dto)
    });
    it('from raw to domain ', async () => {

        sinon.assert.match(await VehicleMap.toDomain(dto), v.getValue())
    });

    it('from domain to persistence ', async () => {
        sinon.assert.match(VehicleMap.toPersistence(v.getValue()), dto)
    });
})