const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import ICrewmemberService from '../services/IServices/ICrewmemberService'
import ICrewmemberController from '../controllers/IController/IcrewmemberController';
import ICrewmemberDTO from "../dto/ICrewmemberDTO";
import { Result } from '../core/logic/Result';
import ICrewmemberRepo from '../repos/IRepos/ICrewmemberRepo';
import { Crewmember } from "../domain/crewmember/crewmember"
import { CrewmemberCode } from "../domain/crewmember/crewmemberCode"
import { CrewmemberDescription } from "../domain/crewmember/crewmemberDescription"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"

describe('crewmember integration test', () => {
    const codeE = CrewmemberCode.create('crew62')
    const descE = CrewmemberDescription.create("desc1")

    let c: Result<Crewmember> = Crewmember.create({
        code: codeE.getValue(),
        description: descE.getValue()
    }, new UniqueEntityID("crew62"))

    let req: Partial<Request> = {};
    let next: Partial<NextFunction> = () => { };

    const body: ICrewmemberDTO = {
        "id": "crew62",
        "code": "crew62",
        "description": "desc1"
    }

    req.body = body


    let crewmemberRepoClass = require(config.repos.crewmember.path).default
    let crewmemberRepoInstance: ICrewmemberRepo = Container.get(crewmemberRepoClass)
    Container.set(config.repos.crewmember.name, crewmemberRepoInstance)
    crewmemberRepoInstance = Container.get(config.repos.crewmember.name)

    let crewmemberServiceClass = require(config.services.crewmember.path).default;
    let crewmemberServiceInstance: ICrewmemberService = Container.get(crewmemberServiceClass)
    Container.set(config.services.crewmember.name, crewmemberServiceInstance);
    crewmemberServiceInstance = Container.get(config.services.crewmember.name);
    let crewmemberControllerClass = require(config.controller.crewmember.path).default;
    let crewmemberControllerInstance: ICrewmemberController = Container.get(crewmemberControllerClass)
    Container.set(config.controller.crewmember.name, crewmemberControllerInstance);
    crewmemberControllerInstance = Container.get(config.controller.crewmember.name);

    beforeEach(() => {


    });
    afterEach(function () {
        sinon.restore();
    });
    it('should create ', async () => {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')
        sinon.stub(crewmemberRepoInstance, "save").returns(c.getValue())
        await crewmemberControllerInstance.createCrewmember(<Request>req, <Response>res, <NextFunction>next);
        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": "crew62",
            "code": req.body.code,
            "description": req.body.description
        });

    });

})