import * as sinon from 'sinon'
import { UniqueEntityID } from '../core/domain/UniqueEntityID'
import { Result } from "../core/logic/Result"
import { Node } from "../domain/network/node";
import { ShortName } from "../domain/network/valueObjects/shortName"
import { NodeName } from "../domain/network/valueObjects/nodeName"
import { MaxTimeStop } from "../domain/network/valueObjects/maxTimeStop"
import { VehicleCapacity } from "../domain/network/valueObjects/vehicleCapacity"
import { NodeLocation } from "../domain/network/valueObjects/nodeLocations"
import { IsDepot } from "../domain/network/valueObjects/isDepot"
import { IsReliefPoint } from "../domain/network/valueObjects/isReliefPoint"
import { Longitude } from '../domain/network/valueObjects/longitude'
import { Latitude } from '../domain/network/valueObjects/latitude'

describe('Node Test', () => {

    it('Can create Node with all atributes.', () => {
      const shortName = ShortName.create('CAMPAN')
      const nodeName = NodeName.create('Campanha')
      const maxTimeStop = MaxTimeStop.create(20)
      const vehicleCapacity = VehicleCapacity.create(30)
      const nodeLatitude = Latitude.create(41.543536)
      const nodeLongitude = Longitude.create(-8.543654)
      const nodeLocation = NodeLocation.create({latitude: nodeLatitude.getValue(), longitude: nodeLongitude.getValue()})
      const isDepot = IsDepot.create(true)
      const isReliefPoint = IsReliefPoint.create(false)
      
      let c: Result<Node> = Node.create({
        shortName: shortName.getValue(),
        nodeName: nodeName.getValue(),
        maxTimeStop: maxTimeStop.getValue(),
        vehicleCapacity: vehicleCapacity.getValue(),
        nodeLocation: nodeLocation.getValue(),
        isDepot: isDepot.getValue(),
        isReliefPoint: isReliefPoint.getValue()
      }, new UniqueEntityID('CAMPAN'))
      sinon.assert.match(c.isSuccess, true)
    }),

  it('Cant create Node with invalid short name (Size).', () => {
    const shortname = ShortName.create('012345678901234567890123456789')
    sinon.assert.match(shortname.isFailure, true)
  }),

  it('Cant create Node with empty short name (size).', () => {
    const shortname = ShortName.create('')
    sinon.assert.match(shortname.isFailure, true)
  }),

  it('Cant create Node with invalid short name (null).', () => {
    const shortname = ShortName.create(null)
    sinon.assert.match(shortname.isFailure, true)
  }),

  it('Cant create Node with invalid name (Size).', () => {
    const nodeName = NodeName.create('KDQBJjulkt5pkn8jNzKw1y1hcW4OpdFIZ6iVmfzzH1HruQYfp6ZS4GccOVjje6NIPvJDhhYGPg4kjNnveVfkHp6ygkUbzpaR3Ke9ul1oZ4rAnxOoyYIut7kCcHd2WHT5tN3Y8dCLpOTcWV5naQc2xe8hv9jhDH8rrJmAIbHmRhrGMflqOnO3UGqY6zxJRG8VcW0bS3NtmsM72WFZTlB61bRWlrKMy59ovV65QUlu1oXs5nUwLFq88B4qWiQbquXkCKgo')
    sinon.assert.match(nodeName.isFailure, true)
  }),

  it('Cant create Node with empty name (Size).', () => {
    const nodeName = NodeName.create('')
    sinon.assert.match(nodeName.isFailure, true)
  }),

  it('Cant create Node with invalid name (null).', () => {
    const nodeName = NodeName.create(null)
    sinon.assert.match(nodeName.isFailure, true)
  })

  it('Cant create Node with invalid max stop time (null).', () => {
    const maxTimeStop = MaxTimeStop.create(null)
    sinon.assert.match(maxTimeStop.isFailure, true)
  })

  it('Cant create Node with invalid vehicle capacity (null).', () => {
    const vehicleCapacity = VehicleCapacity.create(null)
    sinon.assert.match(vehicleCapacity.isFailure, true)
  })

  it('Cant create Node with invalid latitude (range).', () => {
    const nodeLatitude = Latitude.create(100)
    sinon.assert.match(nodeLatitude.isFailure, true)
  })

  it('Cant create Node with invalid longitude (range).', () => {
    const nodeLongitude = Longitude.create(-200)
    sinon.assert.match(nodeLongitude.isFailure, true)
  })

  it('Cant create Node with invalid latitude (null).', () => {
    const nodeLatitude = Latitude.create(null)
    sinon.assert.match(nodeLatitude.isFailure, true)
  })

  it('Cant create Node with invalid longitude (null).', () => {
    const nodeLongitude = Longitude.create(null)
    sinon.assert.match(nodeLongitude.isFailure, true)
  })

  it('Cant create Node with invalid IsDepot (null).', () => {
    const isDepot = IsDepot.create(null)
    sinon.assert.match(isDepot.isFailure, true)
  })

  it('Cant create Node with invalid IsReliefPoint (null).', () => {
    const isReliefPoint = IsReliefPoint.create(null)
    sinon.assert.match(isReliefPoint.isFailure, true)
  })
});