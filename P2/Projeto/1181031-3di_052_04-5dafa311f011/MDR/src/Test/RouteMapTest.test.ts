const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import { Result } from '../core/logic/Result';
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import * as sinon from 'sinon';
import { RouteId } from '../domain/Route/RouteId';
import { Name } from '../domain/Route/Name';
import { ShortName } from '../domain/network/valueObjects/shortName';
import { SegmentId } from '../domain/Route/SegmentId';
import { Distance } from '../domain/Route/Distance';
import { DeslocationTime } from '../domain/Route/DeslocationTime';
import { RouteDirection } from '../domain/Route/RouteDirection';
import { Route } from '../domain/Route/Route';
import { NetworkSegment } from '../domain/Route/NetworkSegment';
import IRouteDTO from "../dto/IRouteDTO";
import ISegementDTO from '../dto/ISegementDTO';
import { RouteMap } from '../mappers/RouteMap';


describe('route mapper test', () => {

    const dto: IRouteDTO = {
        "id": '12',
        "routeId": '12',
        "name": 'Paredes',
        "idNoInicio": 'PPP',
        "idNoFim": 'AAA',
        "networkSegments": [{
            "segmentId": '13',
            "idNo1": 'CCC',
            "idNo2": 'MPN',
            "distance": '123',
            "deslocationTime": 1233,
        }]

    }

    const routeIdE = RouteId.create('12')
    const nameE = Name.create('Paredes')
    const idNoInicioE = ShortName.create('PPP')
    const idNoFimE = ShortName.create('AAA')

    const segmentIdE = SegmentId.create('13')
    const idNo1E = ShortName.create('CCC')
    const idNo2E = ShortName.create('MPN')
    const distanceE = Distance.create('123')
    const deslocationTimeE = DeslocationTime.create(1233)

    const seg1 = NetworkSegment.create({
        segmentId: segmentIdE.getValue(),
        idNo1: idNo1E.getValue(),
        idNo2: idNo2E.getValue(),
        distance: distanceE.getValue(),
        deslocationTime: deslocationTimeE.getValue()
    }, new UniqueEntityID("1")).getValue()

    var segmentsAux = new Array<NetworkSegment>()
    segmentsAux.push(seg1)

    let r: Result<Route> = Route.create({
        routeId: routeIdE.getValue(),
        name: nameE.getValue(),
        idNoInicio: idNoInicioE.getValue(),
        idNoFim: idNoFimE.getValue(),
        networkSegments: segmentsAux,
    }, new UniqueEntityID("12"))

    beforeEach(() => {
    });

    afterEach(function () {
        sinon.restore();
    });

    it('from domain to DTO ', async () => {
        sinon.assert.match(RouteMap.toDTO(r.getValue()), dto)
    });

    // it('to Domain', async () => {
    //     sinon.assert.match(await RouteMap.toDomain(dto), r.getValue())
    // });

})