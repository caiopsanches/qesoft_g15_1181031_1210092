const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import ICrewmemberService from '../services/IServices/ICrewmemberService'
import ICrewmemberController from '../controllers/IController/IcrewmemberController';
import ICrewmemberDTO from "../dto/ICrewmemberDTO";
import { Result } from '../core/logic/Result';
import ICrewmemberRepo from '../repos/IRepos/ICrewmemberRepo';
import { Crewmember } from "../domain/crewmember/crewmember"
import { CrewmemberCode } from "../domain/crewmember/crewmemberCode"
import { CrewmemberDescription } from '../domain/crewmember/crewmemberDescription';
import { UniqueEntityID } from "../core/domain/UniqueEntityID"

describe('Crewmember service create', () => {

    const codeE = CrewmemberCode.create('crew62')
    const descriptionE = CrewmemberDescription.create("desc1")

    let crewmember: Result<Crewmember> = Crewmember.create({
        code: codeE.getValue(),
        description: descriptionE.getValue()
    }, new UniqueEntityID("crew62"))

    let res = Result.ok<ICrewmemberDTO>({
        id: "crew62",
        code: "crew62",
        description: "desc1"
    })

    const dto: ICrewmemberDTO = {
        id: "crew62",
        code: "crew62",
        description: "desc1"
    }

    var allCrewmembers = new Array<Result<ICrewmemberDTO>>()
    allCrewmembers.push(res)

    let crewmemberServiceClass = require(config.services.crewmember.path).default;
    let crewmemberServiceInstance: ICrewmemberService = Container.get(crewmemberServiceClass)
    Container.set(config.services.crewmember.name, crewmemberServiceInstance);
    crewmemberServiceInstance = Container.get(config.services.crewmember.name);


    let crewmemberRepoClass = require(config.repos.crewmember.path).default
    let crewmemberRepoInstance: ICrewmemberRepo = Container.get(crewmemberRepoClass)
    Container.set(config.repos.crewmember.name, crewmemberRepoInstance)
    crewmemberRepoInstance = Container.get(config.repos.crewmember.name);

    beforeEach(() => {
    });
    
    afterEach(function () {
        sinon.restore();
    });

    it('should create ', function(done) {
        sinon.stub(crewmemberRepoInstance, "save").returns(crewmember.getValue())
        sinon.assert.match((crewmemberServiceInstance.createCrewmember(dto)),Promise.resolve(res));
        done();
    })

    it('get all crewmembers', function(done){
        sinon.stub(crewmemberRepoInstance, "findAll").returns(crewmember.getValue())
        sinon.assert.match((crewmemberServiceInstance.getAllCrewmembers('')), Promise.resolve(allCrewmembers));
        done();
    }
)})