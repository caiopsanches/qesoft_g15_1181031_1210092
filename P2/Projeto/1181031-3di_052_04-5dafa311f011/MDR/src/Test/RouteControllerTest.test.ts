const sinon = require('sinon');
import Container from 'typedi';
import config from '../config'
import { Response, Request, NextFunction } from 'express';
import IRouteService from '../services/IServices/IRouteService'
import IRouteController from '../controllers/IController/IRouteController'
import IRouteDTO from "../dto/IRouteDTO"
import ISegementDTO from "../dto/ISegementDTO"
import { Result } from '../core/logic/Result';
import RouteController from '../controllers/RouteController';

describe('route controller create', () => {

    let req: Partial<Request> = {};
    let req1: Partial<Request> = {};
    let req2: Partial<Request> = {};
    let req3: Partial<Request> = {};

    let next: Partial<NextFunction> = () => { };
    let next1: Partial<NextFunction> = () => { };
    let next2: Partial<NextFunction> = () => { };
    let next3: Partial<NextFunction> = () => { };

    const body = {
        "id": "12",
        "routeId": "21",
        "name": "Paredes",
        "idNoInicio": "MMM",
        "idNoFim": "PAA",
        "networkSegments": [
            {
                "segmentId": "16",
                "idNo1": "PPP",
                "idNo2": "JPPJ",
                "distance": "asdad",
                "deslocationTime": 5432
            }
        ],
    };

    req.body = body;
    req1.query = {}
    req2.body = { routeId: '21' }
    req3.body = { name: 'Paredes' }



    let routeServiceClass = require(config.services.route.path).default;
    let routeServiceInstance = Container.get(routeServiceClass)
    Container.set(config.services.route.name, routeServiceInstance);
    routeServiceInstance = Container.get(config.services.route.name);
    let routeControllerClass = require(config.controller.route.path).default;
    let routeControllerInstance: IRouteController = Container.get(routeControllerClass)
    Container.set(config.controller.route.name, routeControllerInstance);
    routeControllerInstance = Container.get(config.controller.route.name);

    const seg = {
        "segmentId": req.body.segmentId,
        "idNo1": req.body.idNo1,
        "idNo2": req.body.idNo2,
        "distance": req.body.distance,
        "deslocationTime": req.body.deslocationTime
    }
    beforeEach(function () {
        sinon.stub(routeServiceInstance, "createRoute").returns(Result.ok<IRouteDTO>({
            "id": req.body.id,
            "routeId": req.body.routeId,
            "name": req.body.name,
            "idNoInicio": req.body.idNoInicio,
            "idNoFim": req.body.idNoFim,
            "networkSegments": req.body.networkSegments,
        }));
    });

    afterEach(function () {
        sinon.verifyAndRestore();
    });


    it('CreateRoute: should create ', async function () {
        const jsonStub = sinon.stub()
        const res = { status: status => ({ json: jsonStub, send: err => err }) }
        const statusSpy = sinon.spy(res, 'status')

        const ctrl = new RouteController(routeServiceInstance as IRouteService);

        await ctrl.createRoute(<Request>req, <Response>res, <NextFunction>next);

        sinon.assert.calledWith(res.status, 201);
        sinon.assert.calledWith(jsonStub, {
            "id": req.body.id, "routeId": req.body.routeId,
            "name": req.body.name,
            "idNoInicio": req.body.idNoInicio,
            "idNoFim": req.body.idNoFim,
            "networkSegments": req.body.networkSegments,
        });

    });

    it('GetRoutes: should return', async function () {

        const jsonStub = sinon.stub(routeServiceInstance, "getAllRoutes").returns(Result.ok<Array<IRouteDTO>>(
            [{ 
                    "id": "12",
                    "routeId": "21",
                    "name": "Paredes",
                    "idNoInicio": "MMM",
                    "idNoFim": "PAA",
                    "networkSegments": [
                        {
                            "segmentId": "16",
                            "idNo1": "PPP",
                            "idNo2": "JPPJ",
                            "distance": "asdad",
                            "deslocationTime": 5432
                        }
                    ],}]))
                

        const stat=201;

        //const jsonStub = sinon.stub()
        const res = { json: jsonStub => ({ status: status, send: err => err }) }
        const statusSpy = sinon.spy(res, 'json')

        const ctrl = new RouteController(routeServiceInstance as IRouteService);

        await ctrl.getRoutes(<Request>req1, <Response><unknown>res, <NextFunction>next1);

        sinon.assert.calledWith(res.json, [{
            "id": req.body.id,
            "routeId": req.body.routeId,
            "name": req.body.name,
            "idNoInicio": req.body.idNoInicio,
            "idNoFim": req.body.idNoFim,
            "networkSegments": req.body.networkSegments,
        }]);
    });


});
