import { expect } from 'chai';
import * as sinon from 'sinon';
import Container from 'typedi';
import config from '../config'
import { assert } from 'console';
import { UniqueEntityID } from '../core/domain/UniqueEntityID';
import { Result } from "../core/logic/Result";
import { Crewmember } from "../domain/crewmember/crewmember";
import { CrewmemberCode } from "../domain/crewmember/crewmemberCode";
import { CrewmemberDescription } from "../domain/crewmember/crewmemberDescription";

describe('Crewmember Test', () => {

    it('Can create Crewmember with code and description.', () => {
      const codeE = CrewmemberCode.create('crew62');
      const descriptionE = CrewmemberDescription.create('desc1');
      
      let c: Result<Crewmember> = Crewmember.create({
          code: codeE.getValue(),
          description: descriptionE.getValue()
      }, new UniqueEntityID('crew62'))
      sinon.assert.match(c.isSuccess, true)
    }),

  it('Cant create Crewmember with invalid code (Size).', () => {
    const codeE = CrewmemberCode.create('crewmember com o codigo 760 da galp');
    sinon.assert.match(codeE.isFailure, true)
  }),

  it('Cant create Crewmember with invalid code (null).', () => {
    const codeE = CrewmemberCode.create('');
    sinon.assert.match(codeE.isFailure, true)
  }),

  it('Cant create Crewmember with invalid description (Size).', () => {
    const descriptionE = CrewmemberDescription.create('KDQBJjulkt5pkn8jNzKw1y1hcW4OpdFIZ6iVmfzzH1HruQYfp6ZS4GccOVjje6NIPvJDhhYGPg4kjNnveVfkHp6ygkUbzpaR3Ke9ul1oZ4rAnxOoyYIut7kCcHd2WHT5tN3Y8dCLpOTcWV5naQc2xe8hv9jhDH8rrJmAIbHmRhrGMflqOnO3UGqY6zxJRG8VcW0bS3NtmsM72WFZTlB61bRWlrKMy59ovV65QUlu1oXs5nUwLFq88B4qWiQbquXkCKgo');
    sinon.assert.match(descriptionE.isFailure, true)
  }),

  it('Cant create Crewmember with invalid description (null).', () => {
    const descriptionE = CrewmemberDescription.create('');
    sinon.assert.match(descriptionE.isFailure, true)
  })

});