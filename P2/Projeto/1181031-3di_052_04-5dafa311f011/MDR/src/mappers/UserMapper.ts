import { Container } from 'typedi';
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Email } from '../domain/user/Email';
import { Endereco } from '../domain/user/Endereco';
import { Nif } from '../domain/user/Nif';
import { Nome } from '../domain/user/Nome';
import { Password } from '../domain/user/Password';
import { User } from '../domain/user/User';
import { Username } from '../domain/user/Username';
import IUserDTO from '../dto/IUserDTO';
import bcrypt from 'bcryptjs';
import { Role } from '../domain/user/Role';



export class UserMapper {
    static saltRounds: 10;

    public static toDTO(user: User): IUserDTO {
        return {
            id: user.id.toString(),
            nome: user.nome.value,
            username: user.username.value,
            password: user.password.value,
            email: user.email.value,
            nif: user.nif.value,
            endereco: user.endereco.value,
            role: user.role.value

        } as IUserDTO
    }


    public static async toDomain(raw: any): Promise<User> {

        const nomeE = Nome.create(raw.nome);
        const usernameE = Username.create(raw.username);
        const passwordE = Password.create(raw.password);
        const emailE = Email.create(raw.email);
        const nifE = Nif.create(raw.nif);
        const enderecoE = Endereco.create(raw.endereco);


        const userOrError = User.create({
            nome: nomeE.getValue(),
            username: usernameE.getValue(),
            password: passwordE.getValue(),
            email: emailE.getValue(),
            nif: nifE.getValue(),
            endereco: enderecoE.getValue(),
            role: Role.create(raw.role).getValue(),
        }, new UniqueEntityID(raw.username))
        if (userOrError.isSuccess)
            return userOrError.getValue()
        else {
            console.log(userOrError.error)
            return null
        }
    }


    public static toPersistence(user: User): any {
        const a = {
            id: user.id.toString(),
            nome: user.nome.value,
            username: user.username.value,
            password: bcrypt.hashSync(user.password.value, this.saltRounds),
            email: user.email.value,
            nif: user.nif.value,
            endereco: user.endereco.value,
            role: user.role.value,
        }
        return a;
    }


    public static async toDomain1(raw: any): Promise<User> {

        const crewOrError = User.create({
            nome: Nome.create(raw.nome).getValue(),
            username: Username.create(raw.username).getValue(),
            password: Password.create(raw.password).getValue(),
            email: Email.create(raw.email).getValue(),
            nif: Nif.create(raw.nif).getValue(),
            endereco: Endereco.create(raw.endereco).getValue(),
            role: Role.create(raw.role).getValue(),
        }, new UniqueEntityID(raw.username))

        crewOrError.isFailure ? console.log(crewOrError.error) : '';
        return crewOrError.isSuccess ? crewOrError.getValue() : null;
    }

}