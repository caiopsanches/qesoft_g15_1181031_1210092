import { Container } from 'typedi';

import { Mapper } from "../core/infra/Mapper";

import IVehicleDTO from "../dto/IVehicleDTO";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { VehicleEnergy } from "../domain/vehicle/vehicleEnergy"
import { VehicleAutonomy } from "../domain/vehicle/vehicleAutonomy"
import { VehicleAvgSpeed } from "../domain/vehicle/vehicleAvgSpeed"
import { VehicleKmCost } from "../domain/vehicle/vehicleKmCost"
import { VehicleAvgConsumption } from "../domain/vehicle/vehicleAvgConsumption"
import { Vehicle } from '../domain/vehicle/vehicle'

export class VehicleMap extends Mapper<Vehicle> {

    public static toDTO(vehicle: Vehicle): IVehicleDTO {
        return {
            id: vehicle.id.toString(),
            code: vehicle.code.value,
            energy: vehicle.energy.value,
            autonomy: vehicle.autonomy.value,
            kmCost: vehicle.kmCost.value,
            avgConsumption: vehicle.avgConsumption.value,
            avgSpeed: vehicle.avgSpeed.value
        } as IVehicleDTO
    }

    public static async toDomain(raw: any): Promise<Vehicle> {

        const codeE = VehicleCode.create(raw.code);
        const energyE = VehicleEnergy.create(raw.energy);
        const autonomyE = VehicleAutonomy.create(raw.autonomy);
        const kmCostE = VehicleKmCost.create(raw.kmCost);
        const avgConsumptionE = VehicleAvgConsumption.create(raw.avgConsumption);
        const avgSpeedE = VehicleAvgSpeed.create(raw.avgSpeed);

        const vehicleOrError = Vehicle.create({
            code: codeE.getValue(),
            energy: energyE.getValue(),
            autonomy: autonomyE.getValue(),
            kmCost: kmCostE.getValue(),
            avgConsumption: avgConsumptionE.getValue(),
            avgSpeed: avgSpeedE.getValue()
        }, new UniqueEntityID(raw.code))

        vehicleOrError.isFailure ? console.log(vehicleOrError.error) : '';

        return vehicleOrError.isSuccess ? vehicleOrError.getValue() : null;
    }

    public static toPersistence(vehicle: Vehicle): any {
        const a = {
            id: vehicle.id.toString(),
            code: vehicle.code.value,
            energy: vehicle.energy.value,
            autonomy: vehicle.autonomy.value,
            kmCost: vehicle.kmCost.value,
            avgConsumption: vehicle.avgConsumption.value,
            avgSpeed: vehicle.avgSpeed.value
        }
        return a;
    }

    public static async toDomain1(raw: any): Promise<Vehicle> {

        const crewOrError = Vehicle.create({
            code: VehicleCode.create(raw.code).getValue(),
            energy: VehicleEnergy.create(raw.energy).getValue(),
            autonomy: VehicleAutonomy.create(raw.autonomy).getValue(),
            kmCost: VehicleKmCost.create(raw.kmCost).getValue(),
            avgConsumption: VehicleAvgConsumption.create(raw.avgConsumption).getValue(),
            avgSpeed: VehicleAvgSpeed.create(raw.avgSpeed).getValue(),
        }, new UniqueEntityID(raw.code))

        crewOrError.isFailure ? console.log(crewOrError.error) : '';
        return crewOrError.isSuccess ? crewOrError.getValue() : null;
    }
}