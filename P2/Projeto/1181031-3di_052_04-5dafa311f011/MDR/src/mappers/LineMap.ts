import { Mapper } from "../core/infra/Mapper";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import ILineDTO from "../dto/ILineDTO";
import { LineId } from "../domain/line/LineId"
import { LineName } from "../domain/line/LineName"
import { LineColour } from "../domain/line/LineColour"
import { Line } from '../domain/line/Line'
import { VehicleCode } from '../domain/vehicle/vehicleCode';
import { CrewmemberCode } from '../domain/crewmember/crewmemberCode';
import { RouteId } from '../domain/Route/RouteId';
import ILinePathDTO from "../dto/ILinePathDTO";
import { LinePath } from "../domain/line/LinePath";
import { RouteDirection } from "../domain/Route/RouteDirection";
import { ILinePathPersistence } from "../dataschema/ILinePathPersistence";
import { ILinePersistence } from "../dataschema/ILinePersistence";

export class LineMap extends Mapper<Line> {

    public static toDTO(line: Line): ILineDTO {

        var pathsAux = new Array<ILinePathDTO>()
        var allowedVehiclesAux = new Array<string>()
        var allowedDriversAux = new Array<string>()

        for (let x = 0; x < line.paths.length; x++) {
            pathsAux.push((
                {
                    routeId: line.paths[x].routeId.value,
                    orientation: line.paths[x].orientation.value
                })
            )
        }

        if (line.allowedVehicles.length > 0) {
            allowedVehiclesAux = line.allowedVehicles.map((g) => g.value)
        }

        if (line.allowedDrivers.length > 0) {
            allowedDriversAux = line.allowedDrivers.map((g) => g.value)
        }

        return {
            id: line.id.toString(),
            lineId: line.lineId.value,
            name: line.name.value,
            colour: line.colour.value,
            paths: pathsAux,
            allowedVehicles: allowedVehiclesAux,
            allowedDrivers: allowedDriversAux
        } as ILineDTO
    }

    public static async toDomain(raw: any): Promise<Line> {
        const lineIdE = LineId.create(raw.lineId);
        const nameE = LineName.create(raw.name);
        const colourE = LineColour.create(raw.colour);

        var pathsAux = new Array<LinePath>()
        var allowedVehiclesAux = new Array<VehicleCode>()
        var allowedDriversAux = new Array<CrewmemberCode>()

        for (let elem of raw.paths) {
            try {
                pathsAux.push(LinePath.create(
                    {
                        routeId: RouteId.create(elem.routeId).getValue(),
                        orientation: RouteDirection.create(elem.orientation).getValue()
                    }, new UniqueEntityID(elem.routeId)).getValue())
            } catch {
                console.log("callback")
            }
        }

        if (raw.allowedVehicles.length > 0) {
            allowedVehiclesAux = raw.allowedVehicles.map((g) => VehicleCode.create(g).getValue())
        }

        if (raw.allowedDrivers.length > 0) {
            allowedDriversAux = raw.allowedDrivers.map((g) => CrewmemberCode.create(g).getValue())
        }
        const lineOrError = Line.create({
            lineId: lineIdE.getValue(),
            name: nameE.getValue(),
            colour: colourE.getValue(),
            paths: pathsAux,
            allowedVehicles: allowedVehiclesAux,
            allowedDrivers: allowedDriversAux
        }, new UniqueEntityID(raw.lineId))

        lineOrError.isFailure ? console.log(lineOrError.error) : '';

        return lineOrError.isSuccess ? lineOrError.getValue() : null;
    }

    public static toPersistence(line: Line): any {
        console.log("Persisting Line!")

        var allowedVehiclesAux = new Array<string>()
        var allowedDriversAux = new Array<string>()

        if (line.allowedVehicles.length > 0) {
            allowedVehiclesAux = line.allowedVehicles.map((g) => g.value)
        }

        if (line.allowedDrivers.length > 0) {
            allowedDriversAux = line.allowedDrivers.map((g) => g.value)
        }

        const a = {
            id: line.id.toString(),
            lineId: line.lineId.value,
            name: line.name.value,
            colour: line.colour.value,
            paths: line.paths,
            allowedDrivers: allowedDriversAux,
            allowedVehicles: allowedVehiclesAux
        }
        return a;
    }

    public static async toDomain1(raw: any): Promise<Line> {

        var pathsAux = new Array<LinePath>()
        var allowedVehiclesAux = new Array<VehicleCode>()
        var allowedDriversAux = new Array<CrewmemberCode>()

        for (let elem of raw.paths) {
            try {
                pathsAux.push(LinePath.create(
                    {
                        routeId: RouteId.create(elem.props.routeId.props.value).getValue(),
                        orientation: RouteDirection.create(elem.props.orientation.props.value).getValue()
                    }, new UniqueEntityID(elem.props.routeId.props.value)).getValue())
            } catch (e) {
                console.log(e)
            }
        }

        if (raw.allowedVehicles.length > 0) {
            allowedVehiclesAux = raw.allowedVehicles.map((g) => VehicleCode.create(g).getValue())
        }

        if (raw.allowedDrivers.length > 0) {
            allowedDriversAux = raw.allowedDrivers.map((g) => CrewmemberCode.create(g).getValue())
        }

        const lineOrError = Line.create({
            lineId: LineId.create(raw.lineId).getValue(),
            name: LineName.create(raw.name).getValue(),
            colour: LineColour.create(raw.colour).getValue(),
            paths: pathsAux,
            allowedVehicles: allowedVehiclesAux,
            allowedDrivers: allowedDriversAux
        }, new UniqueEntityID(raw.lineId))

        lineOrError.isFailure ? console.log(lineOrError.error) : '';
        return lineOrError.isSuccess ? lineOrError.getValue() : null;
    }
}