import { Container } from 'typedi';

import { Mapper } from "../core/infra/Mapper";

import ICrewmemberDTO from "../dto/ICrewmemberDTO";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { CrewmemberCode } from "../domain/crewmember/crewmemberCode"
import { CrewmemberDescription } from "../domain/crewmember/crewmemberDescription"
import { Crewmember } from '../domain/crewmember/crewmember'

export class CrewmemberMap extends Mapper<Crewmember> {

    public static toDTO(crewmember: Crewmember): ICrewmemberDTO {
        return {
            id: crewmember.id.toString(),
            code: crewmember.code.value,
            description: crewmember.description.value
        } as ICrewmemberDTO
    }

    public static async toDomain(raw: any): Promise<Crewmember> {

        const codeE = CrewmemberCode.create(raw.code);
        const descriptionE = CrewmemberDescription.create(raw.description);

        const crewmemberOrError = Crewmember.create({
            code: codeE.getValue(),
            description: descriptionE.getValue()

        }, new UniqueEntityID(raw.code))

        crewmemberOrError.isFailure ? console.log(crewmemberOrError.error) : '';

        return crewmemberOrError.isSuccess ? crewmemberOrError.getValue() : null;
    }

    public static toPersistence(crewmember: Crewmember): any {
        const a = {
            id: crewmember.id.toString(),
            code: crewmember.code.value,
            description: crewmember.description.value
        }
        return a;
    }

    public static async toDomain1(raw: any): Promise<Crewmember> {

        const crewOrError = Crewmember.create({
            code: CrewmemberCode.create(raw.code).getValue(),
            description: CrewmemberDescription.create(raw.description).getValue(),
        }, new UniqueEntityID(raw.code))

        crewOrError.isFailure ? console.log(crewOrError.error) : '';
        return crewOrError.isSuccess ? crewOrError.getValue() : null;
    }
}