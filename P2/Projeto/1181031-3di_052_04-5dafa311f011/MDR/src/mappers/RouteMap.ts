import { Container } from 'typedi';
import { Mapper } from "../core/infra/Mapper";
import IRouteDTO from "../dto/IRouteDTO";
import { Result } from "../core/logic/Result";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { SegmentId } from '../domain/Route/SegmentId';
import { RouteId } from "../domain/Route/RouteId"
import { RouteDirection } from "../domain/Route/RouteDirection"
import { Route } from "../domain/Route/Route"
import { Name } from '../domain/Route/Name';
import { NetworkSegment } from '../domain/Route/NetworkSegment';
import { ShortName } from '../domain/network/valueObjects/shortName'
import { Distance } from '../domain/Route/Distance'
import { DeslocationTime } from '../domain/Route/DeslocationTime'
import { Console } from 'console';
import ISegementDTO from '../dto/ISegementDTO';
import { resolve } from 'path';
import { raw } from 'body-parser';


export class RouteMap extends Mapper<Route> {


    public static toDTO(route: Route): IRouteDTO {
        var networkSegmentAux = new Array<ISegementDTO>()

        for (let x = 0; x < route.networkSegments.length; x++) {
            networkSegmentAux.push((
                {
                    segmentId: route.networkSegments[x].segmentId.value,
                    idNo1: route.networkSegments[x].idNo1.value,
                    idNo2: route.networkSegments[x].idNo2.value,
                    distance: route.networkSegments[x].distance.value,
                    deslocationTime: route.networkSegments[x].deslocationTime.value
                })
            )
        }
        return {
            id: route.id.toString(),
            routeId: route.routeId.value,
            name: route.name.value,
            idNoInicio: route.idNoInicio.value,
            idNoFim: route.idNoFim.value,
            networkSegments: networkSegmentAux,
        } as IRouteDTO

    }

    public static async toDomain(raw: any): Promise<Route> {

        var networkSegmentAux = new Array<NetworkSegment>()

        for (let elem of raw.networkSegments) {
            try {
                networkSegmentAux.push(NetworkSegment.create(
                    {
                        segmentId: SegmentId.create(elem.segmentId).getValue(),
                        idNo1: ShortName.create(elem.idNo1).getValue(),
                        idNo2: ShortName.create(elem.idNo2).getValue(),
                        distance: Distance.create(elem.distance).getValue(),
                        deslocationTime: DeslocationTime.create(elem.deslocationTime).getValue(),
                    }, new UniqueEntityID(elem.segmentId)).getValue())
            } catch {
                console.log("callback")
            }
        }

        const routeOrError = Route.create({
            routeId: RouteId.create(raw.routeId).getValue(),
            name: Name.create(raw.name).getValue(),
            idNoInicio: ShortName.create(raw.idNoInicio).getValue(),
            idNoFim: ShortName.create(raw.idNoFim).getValue(),
            networkSegments: networkSegmentAux,
        }, new UniqueEntityID(raw.routeId))

        routeOrError.isFailure ? console.log(routeOrError.error) : '';
        return routeOrError.isSuccess ? routeOrError.getValue() : null;
    }

    public static toPersistence(route: Route): any {
        const a = {
            id: route.id.toString(),
            routeId: route.routeId.value,
            name: route.name.value,
            idNoInicio: route.idNoInicio.value,
            idNoFim: route.idNoFim.value,
            networkSegments: route.networkSegments,
        }

        return a;

    }

    public static async toDomain1(raw: any): Promise<Route> {

        var networkSegmentAux = new Array<NetworkSegment>()

        for (let elem of raw.networkSegments) {
            try {
                networkSegmentAux.push(NetworkSegment.create(
                    {
                        segmentId: SegmentId.create(elem.props.segmentId.props.value).getValue(),
                        idNo1: ShortName.create(elem.props.idNo1.props.value).getValue(),
                        idNo2: ShortName.create(elem.props.idNo2.props.value).getValue(),
                        distance: Distance.create(elem.props.distance.props.value).getValue(),
                        deslocationTime: DeslocationTime.create(elem.props.deslocationTime.props.value).getValue()
                    }, new UniqueEntityID(elem.props.segmentId.props.value)).getValue())
            } catch (e) {
                console.log(e)
            }
        }

        const routeOrError = Route.create({
            routeId: RouteId.create(raw.routeId).getValue(),
            name: Name.create(raw.name).getValue(),
            idNoInicio: ShortName.create(raw.idNoInicio).getValue(),
            idNoFim: ShortName.create(raw.idNoFim).getValue(),
            networkSegments: networkSegmentAux,
        }, new UniqueEntityID(raw.routeId))

        routeOrError.isFailure ? console.log(routeOrError.error) : '';
        return routeOrError.isSuccess ? routeOrError.getValue() : null;
    }

}
