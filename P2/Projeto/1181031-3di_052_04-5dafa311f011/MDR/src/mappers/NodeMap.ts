import { Mapper } from "../core/infra/Mapper"
import { UniqueEntityID } from "../core/domain/UniqueEntityID"
import { Node } from "../domain/network/node"
import { ShortName } from "../domain/network/valueObjects/shortName"
import { NodeName } from "../domain/network/valueObjects/nodeName"
import { MaxTimeStop } from "../domain/network/valueObjects/maxTimeStop"
import { VehicleCapacity } from "../domain/network/valueObjects/vehicleCapacity"
import { NodeLocation } from "../domain/network/valueObjects/nodeLocations"
import { IsDepot } from "../domain/network/valueObjects/isDepot"
import { IsReliefPoint } from "../domain/network/valueObjects/isReliefPoint"
import INodeDTO from "../dto/INodeDTO"
import { Latitude } from "../domain/network/valueObjects/latitude"
import { Longitude } from "../domain/network/valueObjects/longitude"

export class NodeMap extends Mapper<Node> {

    public static toDTO(node: Node): INodeDTO {
        return {
            shortName: node.shortName.value,
            nodeName: node.nodeName.value,
            maxTimeStop: node.maxTimeStop.value,
            vehicleCapacity: node.vehicleCapacity.value,
            latitude: node.nodeLocation.latitude.value,
            longitude: node.nodeLocation.longitude.value,
            isDepot: node.isDepot.value,
            isReliefPoint: node.isReliefPoint.value
        } as INodeDTO
    }

    public static async toDomain(raw: any): Promise<Node> {
        const shortName = ShortName.create(raw.shortName)
        const nodeName = NodeName.create(raw.nodeName)
        const maxTimeStop = MaxTimeStop.create(raw.maxTimeStop)
        const vehicleCapacity = VehicleCapacity.create(raw.vehicleCapacity)
        const nodeLatitude = Latitude.create(raw.latitude)
        const nodeLongitude = Longitude.create(raw.longitude)
        const nodeLocation = NodeLocation.create({ latitude: nodeLatitude.getValue(), longitude: nodeLongitude.getValue() })
        const isDepot = IsDepot.create(raw.isDepot)
        const isReliefPoint = IsReliefPoint.create(raw.isReliefPoint)
        const nodeOrError = Node.create({
            shortName: shortName.getValue(),
            nodeName: nodeName.getValue(),
            maxTimeStop: maxTimeStop.getValue(),
            vehicleCapacity: vehicleCapacity.getValue(),
            nodeLocation: nodeLocation.getValue(),
            isDepot: isDepot.getValue(),
            isReliefPoint: isReliefPoint.getValue()
        }, new UniqueEntityID(raw.shortName))

        if (nodeOrError.isSuccess)
            return nodeOrError.getValue()
        else {
            console.log(nodeOrError.error)
            return null
        }
    }

    public static toPersistence(node: Node): any {
        return {
            shortName: node.shortName.value,
            nodeName: node.nodeName.value,
            maxTimeStop: node.maxTimeStop.value,
            vehicleCapacity: node.vehicleCapacity.value,
            latitude: node.nodeLocation.latitude.value,
            longitude: node.nodeLocation.longitude.value,
            isDepot: node.isDepot.value,
            isReliefPoint: node.isReliefPoint.value
        }
    }
}
