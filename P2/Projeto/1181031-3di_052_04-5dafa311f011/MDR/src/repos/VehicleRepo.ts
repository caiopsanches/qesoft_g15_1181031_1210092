import { Service, Inject } from 'typedi';
import IVehicleRepo from "./IRepos/IVehicleRepo";
import { VehicleCode } from "../domain/vehicle/vehicleCode"
import { Vehicle } from "../domain/vehicle/vehicle"
import { VehicleMap } from "../mappers/VehicleMap"
import vehicleSchema from "../persistence/schemas/vehicleSchema"
import { Result } from '../core/logic/Result';

@Service()
export default class VehicleRepo implements IVehicleRepo {


    constructor(
    ) { }
    public async delete(vehicleId: VehicleCode): Promise<Boolean> {
        const codeX = vehicleId instanceof VehicleCode ? (<VehicleCode>vehicleId).value : vehicleId;
        const query = { code: codeX };
        const userDocument = await vehicleSchema.deleteOne(query);
        return !!userDocument === true;
    }



    public async exists(vehicleCode: VehicleCode): Promise<boolean> {

        const codeX = vehicleCode instanceof VehicleCode ? (<VehicleCode>vehicleCode).value : vehicleCode;
        const query = { code: codeX };
        const userDocument = await vehicleSchema.findOne(query);
        return !!userDocument === true;
    }


    public async save(vehicle: Vehicle): Promise<Vehicle> {

        const query = { code: vehicle.code.value };
        const vehicleDocument = await vehicleSchema.findOne(query);
        try {
            if (vehicleDocument === null) {
                const rawVehicle: any = VehicleMap.toPersistence(vehicle);
                const vehicleCreated = await vehicleSchema.create(rawVehicle);

                return VehicleMap.toDomain(vehicleCreated);
            } else {
                vehicleDocument._id = vehicle.code.value;
                await vehicleDocument.save();
                return vehicle;
            }
        } catch (err) {
            throw err;
        }
    }

    public async findAll(order: string): Promise<Array<Vehicle>> {
        var ret = new Array<Vehicle>()

        const listResult = await vehicleSchema.find().sort(order)
        await listResult.forEach(async element => await VehicleMap.toDomain1(element).then(vehicle => ret.push(vehicle)))

        return Promise.resolve(ret)
    }
}
