import { Service, Inject } from 'typedi';
import ILineRepo from "../repos/IRepos/ILineRepo";
import { LineId } from "../domain/line/LineId"
import { Line } from "../domain/line/Line"
import { LineMap } from "../mappers/LineMap"
import lineSchema from "../persistence/schemas/LineSchema"

@Service()
export default class LineRepo implements ILineRepo {
    private models: any;

    constructor(
        //   @Inject('lineSchema') private lineSchema1 Model<ILinePersistence & Document>,
        @Inject('logger') private logger
    ) { }


    public async exists(lineId: LineId | string): Promise<boolean> {

        const lid = lineId instanceof LineId ? (<LineId>lineId).value : lineId;

        const query = { lineId: lid };
        const userDocument = await lineSchema.findOne(query);

        return !!userDocument === true;
    }

    public async save(line: Line): Promise<Line> {
        const query = { id: line.lineId.value };
        const lineDocument = await lineSchema.findOne(query);
        try {
            if (lineDocument === null) {
                const rawLine: any = LineMap.toPersistence(line);
                const lineCreated = await lineSchema.create(rawLine);
                console.log("Line Created!")
                return LineMap.toDomain(lineCreated);
            } else {
                lineDocument.id = line.lineId.value;
                await lineDocument.save();
                return line;
            }
        } catch (err) {
            throw err;
        }
    }

    public async findAll(order: string): Promise<Array<Line>> {
        var ret = new Array<Line>()

        const listResult = await lineSchema.find().sort(order)
        await listResult.forEach(async element => await LineMap.toDomain1(element).then(line => ret.push(line)))

        return Promise.resolve(ret)
    }

    public async findByLineId(filter: string, order: string): Promise<Array<Line>> {
        var ret = new Array<Line>()

        const query = { lineId: { $regex: filter, $options: "i" } }
        const listResult = await lineSchema.find(query).sort(order)
        await listResult.forEach(async element => await LineMap.toDomain1(element).then(line => ret.push(line)))

        return Promise.resolve(ret)
    }

    public async findOneByLineId(filter: string): Promise<Line> {

        const query = { lineId: filter }
        const Result = await lineSchema.findOne(query)
        let line = await LineMap.toDomain1(Result)

        return Promise.resolve(line)
    }

    public async findByLineName(filter: string, order: string): Promise<Array<Line>> {
        var ret = new Array<Line>()

        const query = { name: { $regex: filter, $options: "i" } }
        const listResult = await lineSchema.find(query).sort(order)
        await listResult.forEach(async element => await LineMap.toDomain1(element).then(line => ret.push(line)))

        return Promise.resolve(ret)
    }

    public async delete(lineId: LineId): Promise<Boolean> {
        const lineIdX = lineId instanceof LineId ? (<LineId>lineId).value : lineId;
        const query = { lineId: lineIdX };
        const userDocument = await lineSchema.deleteOne(query);
        return !!userDocument === true;
    }
}