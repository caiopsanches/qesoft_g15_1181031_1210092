import { Service, Inject } from 'typedi';
import { Document, Model } from 'mongoose';
import {ICrewmemberPersistence} from '../dataschema/ICrewmemberPersistence';
import ICrewmemberRepo from "../repos/IRepos/ICrewmemberRepo";
import { CrewmemberCode } from "../domain/crewmember/crewmemberCode"
import { CrewmemberDescription } from "../domain/crewmember/crewmemberDescription"
import ICrewmemberDTO from "../dto/ICrewmemberDTO";
import { Crewmember } from "../domain/crewmember/crewmember"
import { CrewmemberMap } from "../mappers/CrewmemberMap"
import crewmemberSchema from "../persistence/schemas/crewmemberSchema"

@Service()
export default class CrewmemberRepo implements ICrewmemberRepo {
    private models: any;

    constructor(
       // @Inject('crewmemberSchema') private crewmemberSchema1 Model<ICrewmemberPersistence & Document>
       @Inject('logger') private logger
    ) { }

   
    public async exists(crewmemberCode: CrewmemberCode): Promise<boolean> {

        const codeX = crewmemberCode instanceof CrewmemberCode ? (<CrewmemberCode>crewmemberCode).value : crewmemberCode;

        const query = { code: codeX };
        const userDocument = await crewmemberSchema.findOne(query);

        return !!userDocument === true;
    }

    public async save(crewmember: Crewmember): Promise<Crewmember> {
        const query = { code: crewmember.code.value };
        const crewmemberDocument = await crewmemberSchema.findOne( query );
       try {
            if (crewmemberDocument === null) {
                const rawCrewmember: any = CrewmemberMap.toPersistence(crewmember);

                const crewmemberCreated = await crewmemberSchema.create(rawCrewmember);

                return CrewmemberMap.toDomain(crewmemberCreated);
            } else {
                crewmemberDocument._id = crewmember.code.value;

                await crewmemberDocument.save();

                return crewmember;
            }
        } catch (err) {
            throw err;
        }
    }

    public async delete(crewmemberId: CrewmemberCode): Promise<Boolean> {
        const codeX = crewmemberId instanceof CrewmemberCode ? (<CrewmemberCode>crewmemberId).value : crewmemberId;
        const query = { code: codeX };
        const userDocument = await crewmemberSchema.deleteOne(query);
        return !!userDocument === true;
    }

    public async findAll(order: string): Promise<Array<Crewmember>> {
        var ret = new Array<Crewmember>()

        const listResult = await crewmemberSchema.find().sort(order)
        await listResult.forEach(async element => await CrewmemberMap.toDomain1(element).then(crew => ret.push(crew)))

        return Promise.resolve(ret)
    }
}