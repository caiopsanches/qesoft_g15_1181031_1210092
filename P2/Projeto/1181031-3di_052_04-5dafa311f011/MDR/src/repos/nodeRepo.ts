import { Service, Inject } from 'typedi'
import NodeSchema from "../persistence/schemas/nodeSchema"
import INodeRepo from "./IRepos/INodeRepo"
import { NodeMap } from '../mappers/NodeMap'
import { Node } from "../domain/network/node"
import { ShortName } from '../domain/network/valueObjects/shortName'

@Service()
export default class NodeRepo implements INodeRepo {
    private models: any;

    constructor(
        //   @Inject('nodeSchema') private nodeSchema1 Model<INodePersistence & Document>,
        @Inject('logger') private logger
    ) { }


    public async exists(nodeId: ShortName | string): Promise<boolean> {

        const lid = nodeId instanceof ShortName ? (<ShortName>nodeId).value : nodeId

        const query = { shortName: lid }
        const userDocument = await NodeSchema.findOne(query)

        return !!userDocument === true;
    }

    public async save(node: Node): Promise<Node> {
        const query = { id: node.shortName.value }
        const nodeDocument = await NodeSchema.findOne(query)
        try {
            if (nodeDocument === null) {
                const rawNode: any = NodeMap.toPersistence(node)
                const nodeCreated = await NodeSchema.create(rawNode)
                console.log("Node Created!")
                return NodeMap.toDomain(nodeCreated)
            } else {
                nodeDocument.id = node.shortName.value
                await nodeDocument.save()
                return node
            }
        } catch (err) {
            throw err
        }
    }

    public async findAll(order: string): Promise<Array<Node>> {
        var ret = new Array<Node>()

        const listResult = await NodeSchema.find().sort(order)
        await listResult.forEach(async element => await NodeMap.toDomain(element).then(node => ret.push(node)))

        return Promise.resolve(ret)
    }

    public async findByShortName(filter: string, order: string): Promise<Array<Node>> {
        var ret = new Array<Node>()

        const query = { shortName: { $regex: filter, $options: "i" } }
        const listResult = await NodeSchema.find(query).sort(order)
        await listResult.forEach(async element => await NodeMap.toDomain(element).then(node => ret.push(node)))

        return Promise.resolve(ret)
    }

    public async findByName(filter: string, order: string): Promise<Array<Node>> {
        var ret = new Array<Node>()

        const query = { nodeName: { $regex: filter, $options: "i" } }
        const listResult = await NodeSchema.find(query).sort(order)
        await listResult.forEach(async element => await NodeMap.toDomain(element).then(node => ret.push(node)))

        return Promise.resolve(ret)
    }

    public async deleteNode(shortName: ShortName): Promise<Boolean> {
        const shortNameX = shortName instanceof ShortName ? (<ShortName>shortName).value : shortName;
        const query = { shortName: shortNameX };
        const userDocument = await NodeSchema.deleteOne(query);
        return !!userDocument === true;
    }
}