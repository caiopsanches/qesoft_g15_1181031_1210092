import { Service, Inject } from 'typedi';
import { Route } from "../domain/Route/Route";
import { RouteId } from "../domain/Route/RouteId";
import IRouteRepo from "../repos/IRepos/IRouteRepo";
import { RouteMap } from "../mappers/RouteMap";
import routeSchema from "../persistence/schemas/RouteSchema";
import { IRoutePersistence } from "../dataschema/IRoutePersistence";


@Service()
export default class RouteRepo implements IRouteRepo {
    private models: any;

    constructor(
        //   @Inject('vehicleSchema') private vehicleSchema1 Model<IVehiclePersistence & Document>,
        @Inject('logger') private logger
    ) { }

    public async delete(routeId: RouteId): Promise<Boolean> {
        const codeX = routeId instanceof RouteId ? (<RouteId>routeId).value : routeId;
        const query = { code: codeX };
        const userDocument = await routeSchema.deleteOne(query);
        return !!userDocument === true;
    }

    public async exists(routeId: RouteId | string): Promise<boolean> {
        const codeX = routeId instanceof RouteId ? (<RouteId>routeId).value : routeId;

        const query = { code: codeX };
        const userDocument = await routeSchema.findOne(query);

        return !!userDocument === true;
    }

    public async save(route: Route): Promise<Route> {
        const query = { id: route.routeId.value };
        const routeDocument = await routeSchema.findOne(query);
        try {
            if (routeDocument === null) {
                const rawRoute: any = RouteMap.toPersistence(route);
                const routeCreated = await routeSchema.create(rawRoute);
                return await RouteMap.toDomain(routeCreated);
            } else {
                routeDocument._id = route.routeId.value;
                await routeDocument.save();
                return route;
            }
        } catch (err) {
            throw err;
        }
    }

    public async findByRouteId(filter: string): Promise<Route> {
        const query = { routeId: filter }
        const listResult = await routeSchema.findOne(query)
        return Promise.resolve(RouteMap.toDomain1(listResult))
    }

    public async findAll(): Promise<Array<Route>> {
        var ret = new Array<Route>()
        const listResult = await routeSchema.find()
        await listResult.forEach(async element => await RouteMap.toDomain1(element).then(route => ret.push(route)))

        return Promise.resolve(ret)
    }
}
