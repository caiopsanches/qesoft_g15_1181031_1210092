import { Service, Inject } from 'typedi';
import { Document, Model } from 'mongoose';
import IUserRepo from './IRepos/IUserRepo';
import { User } from '../domain/user/User';
import { Username } from '../domain/user/Username';
import { UserMapper } from '../mappers/UserMapper';
import userSchema from '../persistence/schemas/userSchema';
import { IUserPersistence } from '../dataschema/IUserPersistence';



@Service()
export default class UserRepo implements IUserRepo {
    private models: any;

    constructor(
        // @Inject('userSchema') private userSchema1 Model<IUserPersistence & Document>
        @Inject('logger') private logger
    ) { }


    public async findByUsername1(username: string): Promise<User> {
        const query = { username: username }
        const listResult = await userSchema.findOne(query)
        let user = await UserMapper.toDomain1(listResult)
        return Promise.resolve(user)
    }

    public async findByUsername(username: string, order: string): Promise<Array<User>> {
        var ret = new Array<User>()

        const query = { username: { $regex: username, $options: "i" } }
        const listResult = await userSchema.find(query).sort(order)
        await listResult.forEach(async element => await UserMapper.toDomain(element).then(user => ret.push(user)))
        return Promise.resolve(ret)
    }

    public async exists(clienteUsername: Username): Promise<boolean> {
        const codeX = clienteUsername instanceof Username ? (<Username>clienteUsername).value : clienteUsername;

        const query = { code: codeX };
        const userDocument = await userSchema.findOne(query);
        return !!userDocument === true;
    }


    public async save(user: User): Promise<User> {
        const query = { id: user.username.value };
        const userDocument = await userSchema.findOne(query);
        try {
            if (userDocument === null) {
                const rawUser: any = UserMapper.toPersistence(user);
                const userCreated = await userSchema.create(rawUser);
                console.log("Client Created!")
                return UserMapper.toDomain(userCreated);
            } else {
                userDocument.id = user.username.value;

                await userDocument.save();

                return user;
            }
        } catch (err) {
            throw err;
        }
    }
    public async deleteCliente(username: Username): Promise<Boolean> {
        const codeX = username instanceof Username ? (<Username>username).value : username;
        const query = { username: codeX };
        const userDocument = await userSchema.deleteOne(query);
        return !!userDocument === true;
    }

    public async findAll(): Promise<Array<User>> {
        var ret = new Array<User>()

        const listResult = await userSchema.find()
        await listResult.forEach(async element => await UserMapper.toDomain1(element).then(user => ret.push(user)))

        return Promise.resolve(ret)
    }

}