import { Node } from "../../domain/network/node"
import { ShortName } from "../../domain/network/valueObjects/shortName"

export default interface INodeRepo {
    exists(node: ShortName | string): Promise<boolean>
    save(node: Node): Promise<Node>
    findAll(order: string): Promise<Array<Node>>
    findByShortName(filter: string, order: string): Promise<Array<Node>>
    findByName(filter: string, order: string): Promise<Array<Node>>
    deleteNode(shortName: ShortName): Promise<Boolean>
}