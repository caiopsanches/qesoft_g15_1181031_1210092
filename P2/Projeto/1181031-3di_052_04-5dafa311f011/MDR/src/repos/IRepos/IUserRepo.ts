import { Result } from "../../core/logic/Result";
import { Repo } from "../../core/infra/Repo";
import { User } from "../../domain/user/User";
import { Username } from "../../domain/user/Username";

export default interface IUserRepo {
    save(user: User): Promise<User>;
    exists(clienteUsername: Username): Promise<boolean>;
    deleteCliente(username: Username): Promise<Boolean>
    findAll(): Promise<Array<User>>
    findByUsername(username: string, order: string): Promise<Array<User>>;
    findByUsername1(username: string): Promise<User>;
}