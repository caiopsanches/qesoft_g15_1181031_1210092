import { Result } from "../../core/logic/Result";
import ILineDTO from "../../dto/ILineDTO";
import { Line } from "../../domain/line/Line"
import { LineId } from "../../domain/line/LineId";

export default interface ILineRepo {
    exists(line: LineId): Promise<boolean>
    save(line: Line): Promise<Line>
    delete(lineId: LineId): Promise<Boolean>
    findAll(order: string): Promise<Array<Line>>
    findByLineId(filter: string, order: string): Promise<Array<Line>>
    findByLineName(filter: string, order: string): Promise<Array<Line>>
    findOneByLineId(filter: string): Promise<Line>
}