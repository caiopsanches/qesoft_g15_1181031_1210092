import { Result } from "../../core/logic/Result";
import IVehicleDTO from "../../dto/IVehicleDTO";
import { VehicleCode } from "../../domain/vehicle/vehicleCode"
import { Vehicle } from "../../domain/vehicle/vehicle"
import { Repo } from "../../core/infra/Repo";
export default interface IVehicleRepo {
  delete(vehicleId: VehicleCode): Promise<Boolean>
  save(vehicle: Vehicle): Promise<Vehicle>;
  exists(vehicle: VehicleCode): Promise<boolean>;
  findAll(order: string): Promise<Array<Vehicle>>
  //updateRole(roleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>>;
}
