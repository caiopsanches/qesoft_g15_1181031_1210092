import { Result } from "../../core/logic/Result";
import ICrewmemberDTO from "../../dto/ICrewmemberDTO";
import { Crewmember } from "../../domain/crewmember/crewmember"
import { CrewmemberCode } from "../../domain/crewmember/crewmemberCode"

export default interface ICrewmemberRepo  {
  save(crewmember: Crewmember): Promise<Crewmember>;
  exists(crewmember: CrewmemberCode): Promise<boolean>;
  delete(crewmemberId: CrewmemberCode): Promise<Boolean>
  findAll(order: string): Promise<Array<Crewmember>>
  //updateRole(roleDTO: ICrewmemberDTO): Promise<Result<ICrewmemberDTO>>;
}