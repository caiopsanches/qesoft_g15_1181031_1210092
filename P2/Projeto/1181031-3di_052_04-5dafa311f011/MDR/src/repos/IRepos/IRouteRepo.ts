import { Result } from "../../core/logic/Result";
import IRouteDTO from "../../dto/IRouteDTO";
import { Route } from "../../domain/Route/Route"
import { RouteId } from "../../domain/Route/RouteId";

export default interface IRouteRepo {
    save(route: Route): Promise<Route>;
    delete(routeId: RouteId): Promise<Boolean>
    exists(route: RouteId): Promise<boolean>;
    findByRouteId(filter: string): Promise<Route>;
    findAll(): Promise<Array<Route>>
    //updateRole(roleDTO: IRouteDTO): Promise<Result<IRouteDTO>>;
}