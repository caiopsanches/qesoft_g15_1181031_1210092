import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard"
import { LineId } from "./LineId";
import { LineName } from "./LineName";
import { LineColour } from "./LineColour";
import { CrewmemberCode } from "../crewmember/crewmemberCode";
import { VehicleCode } from "../vehicle/vehicleCode";
import { RouteId } from "../Route/RouteId";
import { LinePath } from "./LinePath";

interface LineProps {
  lineId: LineId;
  name: LineName;
  colour: LineColour;
  paths: Array<LinePath>
  allowedVehicles: Array<VehicleCode>
  allowedDrivers: Array<CrewmemberCode>
}

export class Line extends AggregateRoot<LineProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get lineId(): LineId {
    return this.props.lineId;
  }

  get name(): LineName {
    return this.props.name;
  }

  get colour(): LineColour {
    return this.props.colour;
  }

  get paths(): Array<LinePath> {
    return this.props.paths
  }

  get allowedVehicles(): Array<VehicleCode> {
    return this.props.allowedVehicles
  }

  get allowedDrivers(): Array<CrewmemberCode> {
    return this.props.allowedDrivers
  }

  set lineId(value: LineId) {
    this.props.lineId = value;
  }

  set name(value: LineName) {
    this.props.name = value;
  }

  set colour(value: LineColour) {
    this.props.colour = value;
  }

  set paths(value: Array<LinePath>) {
    this.props.paths = value;
  }

  set allowedVehicles(value: Array<VehicleCode>) {
    this.props.allowedVehicles = value;
  }

  set allowedDrivers(value: Array<CrewmemberCode>) {
    this.props.allowedDrivers = value;
  }

  private constructor(props: LineProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: LineProps, id?: UniqueEntityID): Result<Line> {
    const guardedProps = [
      { argument: props.lineId, argumentName: 'lineId' },
      { argument: props.name, argumentName: 'name' },
      { argument: props.colour, argumentName: 'colour' },
      { argument: props.paths, argumentName: 'paths' },
      { argument: props.allowedVehicles, argumentName: 'allowedVehicles' },
      { argument: props.allowedDrivers, argumentName: 'allowedDrivers' },
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    if (!guardResult.succeeded) {
      return Result.fail<Line>(guardResult.message)
    }
    else {
      const line = new Line({
        ...props,
        paths: Array.isArray(props.paths) ? props.paths : [],
        allowedVehicles: Array.isArray(props.allowedVehicles) ? props.allowedVehicles : [],
        allowedDrivers: Array.isArray(props.allowedDrivers) ? props.allowedDrivers : []
      }, id);

      return Result.ok<Line>(line);
    }
  }
}
