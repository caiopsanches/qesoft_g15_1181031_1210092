import { AggregateRoot } from "../../core/domain/AggregateRoot"
import { UniqueEntityID } from "../../core/domain/UniqueEntityID"
import { Result } from "../../core/logic/Result"
import { Guard } from "../../core/logic/Guard"
import { RouteId } from "../Route/RouteId"
import { RouteDirection } from "../Route/RouteDirection"

interface LinePathProps {
    routeId: RouteId;
    orientation: RouteDirection;
}


export class LinePath extends AggregateRoot<LinePathProps> {

    get id(): UniqueEntityID {
        return this._id;
    }

    get routeId(): RouteId {
        return this.props.routeId;
    }

    get orientation(): RouteDirection {
        return this.props.orientation;
    }

    set routeId(value: RouteId) {
        this.props.routeId = value;
    }

    set direction(value: RouteDirection) {
        this.props.orientation = value;
    }

    private constructor(props: LinePathProps, id?: UniqueEntityID) {
        super(props, id);
    }


    public static create(props: LinePathProps, id?: UniqueEntityID): Result<LinePath> {

        const guardedProps = [
            { argument: props.routeId, argumentName: 'routeId' },
            { argument: props.orientation, argumentName: 'orientation' }
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<LinePath>(guardResult.message)
        }
        else {
            const linePath = new LinePath({
                ...props,
            }, id);

            return Result.ok<LinePath>(linePath);
        }
    }
}