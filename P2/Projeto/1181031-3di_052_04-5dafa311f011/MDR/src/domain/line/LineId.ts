import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface LineIdProps {
  value: string;
}
export class LineId extends ValueObject<LineIdProps> {
  public static MAX_LENGTH = 30;
  public static MIN_LENGTH = 0;
  get value(): string {
    return this.props.value;
  }

  private constructor(props: LineIdProps) {
    super(props)
  }

  public static create(props: string): Result<LineId> {
    const propsResult = Guard.againstNullOrUndefined(props, 'id');
    const propsResult1 = Guard.inRange(props.length, 1, 30, 'id');
    
    if (!propsResult.succeeded) {
      return Result.fail<LineId>(propsResult.message);
    }

    if(!propsResult1.succeeded){
      return Result.fail<LineId>(propsResult1.message);
    }

    return Result.ok<LineId>(new LineId({ value: props }))
  }
}