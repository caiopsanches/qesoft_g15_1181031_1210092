import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";
import { match } from "assert";

interface LineColourProps {
    value: string;
}

export class LineColour extends ValueObject<LineColourProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: LineColourProps) {
        super(props);
    }

    public static create(props: string): Result<LineColour> {
        var regObj = new RegExp(/RGB[(][0-9]+,[0-9]+,[0-9]+[)]/g)
        var res = props.match(regObj);

        if(res === null){
            return Result.fail<LineColour>("Sintaxe Errada");
        }

        const propsResult = Guard.againstNullOrUndefined(props, 'colour');
        if (!propsResult.succeeded) {
            return Result.fail<LineColour>(propsResult.message);
        }

        return Result.ok<LineColour>(new LineColour({ value: props }))
    }
}