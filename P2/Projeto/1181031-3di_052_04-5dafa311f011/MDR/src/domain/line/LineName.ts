import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface LineNameProps {
  value: string;
}

export class LineName extends ValueObject<LineNameProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: LineNameProps) {
    super(props);
  }

  public static create(props: string): Result<LineName> {

    const propsResult = Guard.againstNullOrUndefined(props, 'name');
    if (!propsResult.succeeded) {
      return Result.fail<LineName>(propsResult.message);
    }

    const propsResult1 = Guard.inRange(props.length, 1, 30, 'name');
    if (!propsResult1.succeeded) {
      return Result.fail<LineName>('Name is not in the right range 1-30');
    }

    return Result.ok<LineName>(new LineName({ value: props }))
  }
}