import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";



interface EnderecoProps {
    value: string;
}



export class Endereco extends ValueObject<EnderecoProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: EnderecoProps) {
        super(props);
    }

    public static create(props: string): Result<Endereco> {
        const propsResult = Guard.againstNullOrUndefined(props, 'endereço');

        if (!propsResult.succeeded) {
            return Result.fail<Endereco>(propsResult.message);
        }
        return Result.ok<Endereco>(new Endereco({ value: props }))
    }



    public getEndereco(): string {
        return this.value;
    }
}