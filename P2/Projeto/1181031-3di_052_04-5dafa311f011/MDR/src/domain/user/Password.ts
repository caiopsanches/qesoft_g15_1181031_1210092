import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface PasswordProps {
    value: string;
}


export class Password extends ValueObject<PasswordProps> {
    get value(): string {
        return this.props.value;
    }

    public constructor(props: PasswordProps) {
        super(props);
    }

    public static create(props: string): Result<Password> {
        const propsResult = Guard.againstNullOrUndefined(props, 'password');

        if (!propsResult.succeeded) {
            return Result.fail<Password>(propsResult.message);
        } else {
            if (props.length < 8 || props === null) {
                throw new Error("Password inválido");
            }
        }
        return Result.ok<Password>(new Password({ value: props }))
    }
}