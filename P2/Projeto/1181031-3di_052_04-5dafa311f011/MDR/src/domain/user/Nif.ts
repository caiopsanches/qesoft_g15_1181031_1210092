import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface NifProps {
    value: number;
}


export class Nif extends ValueObject<NifProps> {
    get value(): number {
        return this.props.value;
    }
    private constructor(props: NifProps) {
        super(props);
    }

    public static create(props: number): Result<Nif> {
        const propsResult = Guard.againstNullOrUndefined(props, 'nif');

        if (!propsResult.succeeded) {
            return Result.fail<Nif>(propsResult.message);
        } else {
            if (props < 100000000 || props > 999999999) {
                throw new Error("NIF inválido");
            }
        }
        return Result.ok<Nif>(new Nif({ value: props }))
    }
}
