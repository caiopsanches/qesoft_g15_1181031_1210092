import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";


const regex = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@[*[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+]*/;


interface EmailProps {
    value: string;
}

export class Email extends ValueObject<EmailProps> {

    get value(): string {
        return this.props.value;
    }

    private constructor(props: EmailProps) {
        super(props);
    }

    public static create(props: string): Result<Email> {
        const propsResult = Guard.againstNullOrUndefined(props, 'email');

        if (!propsResult.succeeded) {
            return Result.fail<Email>(propsResult.message);
        } else {
            if (!regex.test(props)) {
                return Result.fail<Email>('Email inválido!');
            }
        }
        return Result.ok<Email>(new Email({ value: props }))
    }
}
