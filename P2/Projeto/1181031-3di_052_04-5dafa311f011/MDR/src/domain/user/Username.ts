import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";


interface UsernameProps {
    value: string;
}


export class Username extends ValueObject<UsernameProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: UsernameProps) {
        super(props);
    }

    public static create(props: string): Result<Username> {
        const propsResult = Guard.againstNullOrUndefined(props, 'username');

        if (!propsResult.succeeded) {
            return Result.fail<Username>(propsResult.message);
        }
        return Result.ok<Username>(new Username({ value: props }))
    }
}