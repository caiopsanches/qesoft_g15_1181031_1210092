import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface NomeProps {
    value: string;
}


export class Nome extends ValueObject<NomeProps> {
    get value(): string {
        return this.props.value;
    }

    public constructor(props: NomeProps) {
        super(props);
    }

    public static create(props: string): Result<Nome> {
        const propsResult = Guard.againstNullOrUndefined(props, 'nome');

        if (!propsResult.succeeded) {
            return Result.fail<Nome>(propsResult.message);
        } else {
            const propsResult1 = Guard.inRange(props.length, 1, 30, 'nome');
            if (!propsResult1) {
                return Result.fail<Nome>('Nome is not in the right range [1-30]');
            }
        }
        return Result.ok<Nome>(new Nome({ value: props }))
    }
}