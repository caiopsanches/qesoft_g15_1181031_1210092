import { AggregateRoot } from "../../core/domain/AggregateRoot"
import { UniqueEntityID } from "../../core/domain/UniqueEntityID"
import { Result } from "../../core/logic/Result"
import { Guard } from "../../core/logic/Guard"
import { Nome } from "./Nome";
import { Username } from "./Username";
import { Password } from "./Password";
import { Email } from "./Email";
import { Nif } from "./Nif";
import { Endereco } from "./Endereco";
import { Role } from "./Role";
import bcrypt from 'bcryptjs';

interface UserProps {
    nome: Nome;
    username: Username;
    password: Password;
    email: Email;
    nif: Nif;
    endereco: Endereco;
    role: Role;
}




export class User extends AggregateRoot<UserProps> {

    /**
        * Método que retorna o ID do utilizador
        */
    get id(): UniqueEntityID {
        return this._id;
    }

    /**
     * Método que retorna o nome do utilizador
     */
    get nome(): Nome {
        return this.props.nome;
    }

    /**
     * Método que retorna o username do utilizador
     */
    get username(): Username {
        return this.props.username;
    }

    /**
     * Método que retorna a password do utilizador
     */
    get password(): Password {
        return this.props.password;
    }

    /**
     * Salt rounds para encriptar a password
     */
    private saltRounds = 10;

    /**
     * Método que retorna o email do utilizador
     */
    get email(): Email {
        return this.props.email;
    }

    /**
     * Método que retorna o endereço do utilizador
     */
    get endereco(): Endereco {
        return this.props.endereco;
    }

    /**
     * Método que retorna o NIF do utilizador
     */
    get nif(): Nif {
        return this.props.nif;
    }

    /**
     * Método que retorna o role do utilizador
     */
    get role(): Role {
        return this.props.role;
    }



    set nome(value: Nome) {
        this.props.nome = value;
    }

    set username(value: Username) {
        this.props.username = value;
    }

    set password(value: Password) {
        this.props.password = value;
    }

    set email(value: Email) {
        this.props.email = value;
    }

    set endereco(value: Endereco) {
        this.props.endereco = value;
    }

    set nif(value: Nif) {
        this.props.nif = value;
    }

    private constructor(props: UserProps, id?: UniqueEntityID) {
        super(props, id);
    }


    public static create(props: UserProps, id?: UniqueEntityID): Result<User> {
        const guardedProps = [
            { argument: props.nome, argumentName: 'nome' },
            { argument: props.username, argumentName: 'username' },
            { argument: props.password, argumentName: 'password' },
            { argument: props.email, argumentName: 'email' },
            { argument: props.nif, argumentName: 'nif' },
            { argument: props.endereco, argumentName: 'endereco' },
            { argument: props.role, argumentName: 'role' },

        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<User>(guardResult.message)
        }
        else {
            const route = new User({
                ...props,
            }, id);

            return Result.ok<User>(route);
        }
    }


}