import config from "../../config";
import { ValueObject } from "../../core/domain/ValueObject";
import { Guard } from "../../core/logic/Guard";
import { Result } from "../../core/logic/Result";

interface RoleEnumProps {
    value: string;
}

export class Role extends ValueObject<RoleEnumProps>  {
    private static roleEnum = ["DataAdmin", "Cliente", "Gestor"];

    get value(): string {
        return this.props.value;
    }

    public getRole(): string {
        return this.props.value;
    }

    public static create(type: string): Result<Role> {
        const guardResult = Guard.isOneOf(type, Role.roleEnum, 'role');
        if (!guardResult.succeeded) {
            return Result.fail<Role>(guardResult.message);
        } else {
            return Result.ok<Role>(new Role({ value: type }))
        }
    }
}

