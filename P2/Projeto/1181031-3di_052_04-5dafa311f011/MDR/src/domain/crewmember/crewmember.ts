import { AggregateRoot } from "../../core/domain/AggregateRoot"
import { UniqueEntityID } from "../../core/domain/UniqueEntityID"
import { Result } from "../../core/logic/Result"
import { Guard } from "../../core/logic/Guard"
import { CrewmemberCode } from "./crewmemberCode"
import { CrewmemberDescription } from "./crewmemberDescription"
import ICrewmemberDTO from "../../dto/ICrewmemberDTO";

interface CrewmemberProps {
  code: CrewmemberCode
  description: CrewmemberDescription
}

export class Crewmember extends AggregateRoot<CrewmemberProps> {
  get id(): UniqueEntityID {
    return this._id;
  }
  get code(): CrewmemberCode {
    return this.props.code;
  }
  get description(): CrewmemberDescription {
    return this.props.description;
  }
  private constructor(props: CrewmemberProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: CrewmemberProps, id?: UniqueEntityID): Result<Crewmember> {

    const guardedProps = [
      { argument: props.code, argumentName: 'code' },
      { argument: props.description, argumentName: 'description' },
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    if (!guardResult.succeeded) {
      return Result.fail<Crewmember>(guardResult.message)
    }
    else {
      const crewmember = new Crewmember({
        ...props
      }, id);

      return Result.ok<Crewmember>(crewmember);
    }
  }
}
