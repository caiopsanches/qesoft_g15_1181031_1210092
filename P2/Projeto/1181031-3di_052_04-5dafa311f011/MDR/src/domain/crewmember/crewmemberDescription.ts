import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface CrewmemberDescriptionProps {
  value: string;
}

export class CrewmemberDescription extends ValueObject<CrewmemberDescriptionProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: CrewmemberDescriptionProps) {
    super(props)
  }

  public static create(props: string): Result<CrewmemberDescription> {
    const propsResult = Guard.againstNullOrUndefined(props, 'code');

    if (!propsResult.succeeded) {
      return Result.fail<CrewmemberDescription>(propsResult.message);
    } else {
      const propsResult1 = Guard.inRange(props.length, 1, 250, 'code');
      if (!propsResult1.succeeded) {
        return Result.fail<CrewmemberDescription>('Description is not in the right range [1-250]');
      }
    }
    return Result.ok<CrewmemberDescription>(new CrewmemberDescription({ value: props }))
  }
}