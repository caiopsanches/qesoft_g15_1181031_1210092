import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface CrewmemberCodeProps {
  value: string;
}

export class CrewmemberCode extends ValueObject<CrewmemberCodeProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: CrewmemberCodeProps) {
    super(props)
  }

  public static create(props: string): Result<CrewmemberCode> {
    const propsResult = Guard.againstNullOrUndefined(props, 'code');

    if (!propsResult.succeeded) {
      return Result.fail<CrewmemberCode>(propsResult.message);
    } else {
      const propsResult1 = Guard.inRange(props.length, 1, 20, 'code');
      if (!propsResult1.succeeded) {
        return Result.fail<CrewmemberCode>('Code is not in the right range [1-20]');
      }
    }
    return Result.ok<CrewmemberCode>(new CrewmemberCode({ value: props }))
  }
}