import { ShortName } from "./valueObjects/shortName"
import { NodeName } from "./valueObjects/nodeName"
import { MaxTimeStop } from "./valueObjects/maxTimeStop"
import { VehicleCapacity } from "./valueObjects/vehicleCapacity"
import { NodeLocation } from "./valueObjects/nodeLocations"
import { IsDepot } from "./valueObjects/isDepot"
import { IsReliefPoint } from "./valueObjects/isReliefPoint"
import { AggregateRoot } from "../../core/domain/AggregateRoot"
import { UniqueEntityID } from "../../core/domain/UniqueEntityID"
import { Result } from "../../core/logic/Result"
import { Guard } from "../../core/logic/Guard"

interface NodeProps {
  shortName: ShortName
  nodeName: NodeName
  maxTimeStop: MaxTimeStop
  vehicleCapacity: VehicleCapacity
  nodeLocation: NodeLocation
  isDepot: IsDepot
  isReliefPoint: IsReliefPoint
}

export class Node extends AggregateRoot<NodeProps> {
  get id(): UniqueEntityID { return this._id; }

  public get shortName(): ShortName { return this.props.shortName }

  public get nodeName(): NodeName { return this.props.nodeName }

  public get maxTimeStop(): MaxTimeStop { return this.props.maxTimeStop }

  public get vehicleCapacity(): VehicleCapacity { return this.props.vehicleCapacity }

  public get nodeLocation(): NodeLocation { return this.props.nodeLocation }

  public get isDepot(): IsDepot { return this.props.isDepot }

  public get isReliefPoint(): IsReliefPoint { return this.props.isReliefPoint }

  private constructor(props: NodeProps, id?: UniqueEntityID) {
    super(props, id)
  }

  public static create(props: NodeProps, id?: UniqueEntityID): Result<Node> {
    const guardedProps = [
      { argument: props.shortName, argumentName: 'shortName' },
      { argument: props.nodeName, argumentName: 'nodeName'},
      { argument: props.maxTimeStop, argumentName: 'maxTimeStop'},
      { argument: props.vehicleCapacity, argumentName: 'vehicleCapacity'},
      { argument: props.nodeLocation, argumentName: 'nodeLocation'},
      { argument: props.isDepot, argumentName: 'isDepot'},
      { argument: props.isReliefPoint, argumentName: 'isReliefPoint'}
    ]

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps)

    if (guardResult.succeeded)
      return Result.ok<Node>(new Node({... props}, id))
    else
      return Result.fail<Node>(guardResult.message)
  }
}