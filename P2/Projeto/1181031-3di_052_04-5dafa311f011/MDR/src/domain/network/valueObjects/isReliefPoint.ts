import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface IsReliefPointProps {
    value: boolean
}

export class IsReliefPoint extends ValueObject<IsReliefPointProps> {

    public get value(): boolean {
        return this.props.value
    }

    private constructor(props: IsReliefPointProps) {
        super(props)
    }

    public static create(props: boolean): Result<IsReliefPoint> {
        const propsResult = Guard.againstNullOrUndefined(props, 'IsReliefPoint')

        if (propsResult.succeeded) {
            return Result.ok<IsReliefPoint>(new IsReliefPoint({ value: props }))
        } else
            return Result.fail<IsReliefPoint>(propsResult.message)
    }
}
