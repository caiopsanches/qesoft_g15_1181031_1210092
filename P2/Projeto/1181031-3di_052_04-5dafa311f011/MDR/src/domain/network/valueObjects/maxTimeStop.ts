import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface MaxTimeStopProps {
    value: number
}

export class MaxTimeStop extends ValueObject<MaxTimeStopProps> {
    private static MIN_VALUE = 1
    private static MAX_VALUE = Number.MAX_SAFE_INTEGER

    public get value(): number {
        return this.props.value
    }

    private constructor(props: MaxTimeStopProps) {
        super(props)
    }

    public static create(props: number): Result<MaxTimeStop> {
        const propsResult = Guard.againstNullOrUndefined(props, 'MaxTimeStop')

        if (propsResult.succeeded) {
            const propsResult1 = Guard.inRange(props, this.MIN_VALUE, this.MAX_VALUE, 'MaxTimeStop')
            if (!propsResult1.succeeded)
                return Result.fail<MaxTimeStop>('Maximum Stop Time cannot by a non positive value.')
            else
                return Result.ok<MaxTimeStop>(new MaxTimeStop({ value: props }))
        } else
            return Result.fail<MaxTimeStop>(propsResult.message)
    }
}