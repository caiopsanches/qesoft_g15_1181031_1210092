import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"
import { Latitude } from "./latitude"
import { Longitude } from "./longitude"

interface NodeLocationProps {
    latitude: Latitude
    longitude: Longitude
}

export class NodeLocation extends ValueObject<NodeLocationProps> {

    public get latitude(): Latitude {
        return this.props.latitude
    }

    public get longitude(): Longitude {
        return this.props.longitude
    }

    private constructor(props: NodeLocationProps) {
        super(props)
    }

    public static create(props: NodeLocationProps): Result<NodeLocation> {
        const guardedProps = [
            { argument: props.latitude, argumentName: 'latitude' },
            { argument: props.longitude, argumentName: 'longitude' },
          ]

        const propsResult = Guard.againstNullOrUndefined(props, 'NodeLocation')

        if (propsResult.succeeded) {
            return Result.ok<NodeLocation>(new NodeLocation({ ... props }))
        } else
            return Result.fail<NodeLocation>(propsResult.message)
    }
}