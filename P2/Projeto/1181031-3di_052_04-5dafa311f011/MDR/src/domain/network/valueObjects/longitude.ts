import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface LongitudeProps {
    value: number
}

export class Longitude extends ValueObject<LongitudeProps> {
    private static MIN_VALUE = -180
    private static MAX_VALUE = 180

    public get value(): number {
        return this.props.value
    }

    private constructor(props: LongitudeProps) {
        super(props)
    }

    public static create(props: number): Result<Longitude> {
        const propsResult = Guard.againstNullOrUndefined(props, 'Longitude')

        if (propsResult.succeeded) {
            const propsResult1 = Guard.inRange(props, this.MIN_VALUE, this.MAX_VALUE, 'Longitude')
            if (!propsResult1.succeeded)
                return Result.fail<Longitude>('Invalid Value to Node Longitude.')
            else
                return Result.ok<Longitude>(new Longitude({ value: props }))
        } else
            return Result.fail<Longitude>(propsResult.message)
    }
}