import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface IsDepotProps {
    value: boolean
}

export class IsDepot extends ValueObject<IsDepotProps> {

    public get value(): boolean {
        return this.props.value
    }

    private constructor(props: IsDepotProps) {
        super(props)
    }

    public static create(props: boolean): Result<IsDepot> {
        const propsResult = Guard.againstNullOrUndefined(props, 'IsDepot')

        if (propsResult.succeeded) {
            return Result.ok<IsDepot>(new IsDepot({ value: props }))
        } else
            return Result.fail<IsDepot>(propsResult.message)
    }
}
