import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface VehicleCapacityProps {
    value: number
}

export class VehicleCapacity extends ValueObject<VehicleCapacityProps> {
    private static MIN_VALUE = 1
    private static MAX_VALUE = Number.MAX_SAFE_INTEGER

    public get value(): number {
        return this.props.value
    }

    private constructor(props: VehicleCapacityProps) {
        super(props)
    }

    public static create(props: number): Result<VehicleCapacity> {
        const propsResult = Guard.againstNullOrUndefined(props, 'VehicleCapacity')

        if (propsResult.succeeded) {
            const propsResult1 = Guard.inRange(props, this.MIN_VALUE, this.MAX_VALUE, 'VehicleCapacity')
            if (!propsResult1.succeeded)
                return Result.fail<VehicleCapacity>('Capacity cannot by a non positive value.')
            else
                return Result.ok<VehicleCapacity>(new VehicleCapacity({ value: props }))
        } else
            return Result.fail<VehicleCapacity>(propsResult.message)
    }
}