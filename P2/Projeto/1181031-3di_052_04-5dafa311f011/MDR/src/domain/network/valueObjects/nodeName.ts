import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result";
import { Guard } from "../../../core/logic/Guard";

interface NodeNameProps {
    value: string
}

export class NodeName extends ValueObject<NodeNameProps> {
    private static MIN_LENGTH = 1
    private static MAX_LENGTH = 200

    public get value(): string {
        return this.props.value
    }

    private constructor(props: NodeNameProps) {
        super(props)
    }

    public static create(props: string): Result<NodeName> {
        const propsResult = Guard.againstNullOrUndefined(props, 'NodeName')

        if (propsResult.succeeded) {
            const propsResult1 = Guard.inRange(props.length, this.MIN_LENGTH, this.MAX_LENGTH, 'NodeName')
            if (!propsResult1.succeeded)
                return Result.fail<NodeName>('Node\'s name has not a acceptable length.')
            else
                return Result.ok<NodeName>(new NodeName({ value: props }))
        } else
            return Result.fail<NodeName>(propsResult.message)
    }
}