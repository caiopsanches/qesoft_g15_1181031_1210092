import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface ShortNameProps {
    value: string
}

export class ShortName extends ValueObject<ShortNameProps> {
    
    public get value(): string {
        return this.props.value
    }

    private constructor(props: ShortNameProps) {
        super(props)
    }

    public static create(props: string): Result<ShortName> {
        const propsResult = Guard.againstNullOrUndefined(props, 'ShortName')

        if (propsResult.succeeded) {
            const propsResult1 = Guard.inRange(props.length, 1, 20, 'ShortName')
            if (!propsResult1.succeeded)
                return Result.fail<ShortName>('Node\'s short name has not a acceptable length.')
            else {
                return Result.ok<ShortName>(new ShortName({ value: props })) }
        } else
            return Result.fail<ShortName>(propsResult.message)
    }
}