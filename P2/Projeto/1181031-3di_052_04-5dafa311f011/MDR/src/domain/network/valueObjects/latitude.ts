import { ValueObject } from "../../../core/domain/ValueObject"
import { Result } from "../../../core/logic/Result"
import { Guard } from "../../../core/logic/Guard"

interface LatitudeProps {
    value: number
}

export class Latitude extends ValueObject<LatitudeProps> {
    private static MIN_VALUE = -90
    private static MAX_VALUE = 90

    public get value(): number {
        return this.props.value
    }

    private constructor(props: LatitudeProps) {
        super(props)
    }

    public static create(props: number): Result<Latitude> {
        const propsResult = Guard.againstNullOrUndefined(props, 'Latitude')

        if (propsResult.succeeded) {
            const propsResult1 = Guard.inRange(props, this.MIN_VALUE, this.MAX_VALUE, 'Latitude')
            if (!propsResult1.succeeded)
                return Result.fail<Latitude>('Invalid Value to Node Latitude.')
            else
                return Result.ok<Latitude>(new Latitude({ value: props }))
        } else
            return Result.fail<Latitude>(propsResult.message)
    }
}