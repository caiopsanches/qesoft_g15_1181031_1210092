import { AggregateRoot } from "../../core/domain/AggregateRoot"
import { UniqueEntityID } from "../../core/domain/UniqueEntityID"
import { Result } from "../../core/logic/Result"
import { Guard } from "../../core/logic/Guard"
import { VehicleCode } from "./vehicleCode"
import { VehicleEnergy } from "./vehicleEnergy"
import { VehicleAutonomy } from "./vehicleAutonomy"
import { VehicleAvgSpeed } from "./vehicleAvgSpeed"
import { VehicleKmCost } from "./vehicleKmCost"
import { VehicleAvgConsumption } from "./vehicleAvgConsumption"
import IVehicleDTO from "../../dto/IVehicleDTO";

interface VehicleProps {
  code: VehicleCode
  energy: VehicleEnergy
  autonomy: VehicleAutonomy
  kmCost: VehicleKmCost
  avgConsumption: VehicleAvgConsumption;
  avgSpeed: VehicleAvgSpeed;
}

export class Vehicle extends AggregateRoot<VehicleProps> {
  get id(): UniqueEntityID {
    return this._id;
  }
  get code(): VehicleCode {
    return this.props.code;
  }
  get energy(): VehicleEnergy {
    return this.props.energy;
  }
  get autonomy(): VehicleAutonomy {
    return this.props.autonomy;
  }
  get kmCost(): VehicleKmCost {
    return this.props.kmCost;
  }
  get avgConsumption(): VehicleAvgConsumption {
    return this.props.avgConsumption;
  }
  get avgSpeed(): VehicleAvgSpeed {
    return this.props.avgSpeed;
  }
  private constructor(props: VehicleProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: VehicleProps, id?: UniqueEntityID): Result<Vehicle> {

    const guardedProps = [
      { argument: props.code, argumentName: 'code' },
      { argument: props.energy, argumentName: 'energy' },
      { argument: props.autonomy, argumentName: 'autonomy' },
      { argument: props.kmCost, argumentName: 'kmCost' },
      { argument: props.avgConsumption, argumentName: 'avgConsumption' },
      { argument: props.avgSpeed, argumentName: 'avgSpeed' },
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    if (!guardResult.succeeded) {
      return Result.fail<Vehicle>(guardResult.message)
    }
    else {
      const vehicle = new Vehicle({
        ...props
      }, id);

      return Result.ok<Vehicle>(vehicle);
    }
  }
}
