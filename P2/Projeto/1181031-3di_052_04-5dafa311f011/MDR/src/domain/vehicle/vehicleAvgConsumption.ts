import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface vehicleAvgConsumptionProps {
    value: number;
}

export class VehicleAvgConsumption extends ValueObject<vehicleAvgConsumptionProps> {
    public static MIN_VALUE = 0
    get value(): number {
        return this.props.value
    }
    public static create(auto: number): Result<VehicleAvgConsumption> {
        const propsResult = Guard.againstNullOrUndefined(auto, 'cost')

        if (!propsResult.succeeded) {
            return Result.fail<VehicleAvgConsumption>(propsResult.message)
        } else {
            if (!VehicleAvgConsumption.isPositive(auto)) {
                auto.toFixed(3);
                return Result.fail<VehicleAvgConsumption>('The cost must be a positive value')
            } else {
                return Result.ok<VehicleAvgConsumption>(new VehicleAvgConsumption({ value: auto }))
            }
        }
    }
    public static isPositive(value: number): boolean {
        return value > 0;
    }
}