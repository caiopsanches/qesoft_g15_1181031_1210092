import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface vehicleAutonomyProps {
    value: number;
}

export class VehicleAutonomy extends ValueObject<vehicleAutonomyProps> {
    public static MIN_VALUE = 0
    get value(): number {
        return this.props.value
    }
    public static create(auto: number): Result<VehicleAutonomy> {
        const propsResult = Guard.againstNullOrUndefined(auto, 'autonomy')

        if (!propsResult.succeeded) {
            return Result.fail<VehicleAutonomy>(propsResult.message)
        } else {
            if (!VehicleAutonomy.isPositive(auto)) {
                return Result.fail<VehicleAutonomy>('The Autonomy must be a positive value')
            } else {
                return Result.ok<VehicleAutonomy>(new VehicleAutonomy({ value: auto }))
            }
        }
    }
    public static isPositive(value: number): boolean {
        return value > 0;
    }
}