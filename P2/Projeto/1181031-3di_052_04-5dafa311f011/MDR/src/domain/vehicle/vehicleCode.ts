
import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface VehicleCodeProps {
  value: string;
}
export class VehicleCode extends ValueObject<VehicleCodeProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: VehicleCodeProps) {
    super(props)
  }
  
  public static create(props: string): Result<VehicleCode> {
    const propsResult = Guard.againstNullOrUndefined(props, 'code');

    if (!propsResult.succeeded) {
      return Result.fail<VehicleCode>(propsResult.message);
    } else {
      const propsResult1 = Guard.inRange(props.length, 1, 20, 'code');
      if (!propsResult1.succeeded) {
        return Result.fail<VehicleCode>('Code is not in the right range [1-20]');
      }
    }
    return Result.ok<VehicleCode>(new VehicleCode({ value: props }))
  }
}