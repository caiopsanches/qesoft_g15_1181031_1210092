import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface VehicleAvgSppedProps {
    value: number;
}

export class VehicleAvgSpeed extends ValueObject<VehicleAvgSppedProps> {
    public static MIN_VALUE = 0;
    get value(): number {
        return this.props.value;
    }
    public static create(auto: number): Result<VehicleAvgSpeed> {
        const propsResult = Guard.againstNullOrUndefined(auto, 'code');

        if (!propsResult.succeeded) {
            return Result.fail<VehicleAvgSpeed>(propsResult.message)
        } else {
            if (!VehicleAvgSpeed.isPositive(auto)) {
             return   Result.fail<VehicleAvgSpeed>('The Autonomy must be a positive value')
            } else {
             return   Result.ok<VehicleAvgSpeed>(new VehicleAvgSpeed({ value: auto }))
            }
        }
    }
    public static isPositive(value: number): boolean {
        return value > 0
    }
}