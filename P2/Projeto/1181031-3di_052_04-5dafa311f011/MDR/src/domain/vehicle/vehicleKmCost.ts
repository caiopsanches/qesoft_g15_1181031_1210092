import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface vehicleKmCostProps {
    value: number;
}

export class VehicleKmCost extends ValueObject<vehicleKmCostProps> {
    public static MIN_VALUE = 0
    get value(): number {
        return this.props.value
    }
    public static create(auto: number): Result<VehicleKmCost> {
        const propsResult = Guard.againstNullOrUndefined(auto, 'cost')

        if (!propsResult.succeeded) {
            return Result.fail<VehicleKmCost>(propsResult.message)
        } else {
            if (!VehicleKmCost.isPositive(auto)) {
                return Result.fail<VehicleKmCost>('The cost must be a positive value')
            } else {
                return Result.ok<VehicleKmCost>(new VehicleKmCost({ value: auto }))
            }
        }
    }
    public static isPositive(value: number): boolean {
        return value > 0;
    }
}