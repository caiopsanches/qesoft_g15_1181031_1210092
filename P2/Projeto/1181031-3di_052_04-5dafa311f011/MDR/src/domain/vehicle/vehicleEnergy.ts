
import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";
import config from '../../config'
interface VehicleEnergyProps {
    value: string;
}

export class VehicleEnergy extends ValueObject<VehicleEnergyProps> {
    private static existingType = config.existingTypes;
    get value(): string {
        return this.props.value;
    }

    public static create(type: string): Result<VehicleEnergy> {
        const guardResult = Guard.isOneOf(type, VehicleEnergy.existingType, 'energy type');
        if (!guardResult.succeeded) {
            return Result.fail<VehicleEnergy>(guardResult.message);
        } else {
            return Result.ok<VehicleEnergy>(new VehicleEnergy({ value: type }))
        }
    }

}

