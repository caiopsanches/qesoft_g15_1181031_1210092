import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";


interface DeslocationTimeProps {
    value: number;
}

export class DeslocationTime extends ValueObject<DeslocationTimeProps> {
    public static MIN_VALUE = 0
    get value(): number {
        return this.props.value
    }

    private constructor(props: DeslocationTimeProps) {
        super(props);
    }


    public static create(props: number): Result<DeslocationTime> {
        const propsResult = Guard.againstNullOrUndefined(props, 'deslocation time')

        if (!propsResult.succeeded) {
            return Result.fail<DeslocationTime>(propsResult.message)
        } else {
            if (!DeslocationTime.isPositive(props)) {
                return Result.fail<DeslocationTime>('The deslocation time must be a positive value')
            } else {
                return Result.ok<DeslocationTime>(new DeslocationTime({ value: props }))
            }
        }
    }
    public static isPositive(value: number): boolean {
        return value > 0;
    }
}