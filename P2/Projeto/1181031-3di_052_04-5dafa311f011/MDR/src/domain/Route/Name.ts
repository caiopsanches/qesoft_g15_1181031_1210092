import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface NameProps {
  value: string;
}


export class Name extends ValueObject<NameProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: NameProps) {
    super(props);
  }

  public static create(props: string): Result<Name> {
    const propsResult = Guard.againstNullOrUndefined(props, 'name');

    if (!propsResult.succeeded) {
      return Result.fail<Name>(propsResult.message);
    } else {
      const propsResult1 = Guard.inRange(props.length, 1, 20, 'name');
      if (!propsResult1) {
        return Result.fail<Name>('Name is not in the right range [1-20]');
      }
    }
    return Result.ok<Name>(new Name({ value: props }))
  }
}