import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";



interface DistanceProps {
    value: string;
}


export class Distance extends ValueObject<DistanceProps> {

    get value(): string {
        return this.props.value;
    }

    private constructor(props: DistanceProps) {
        super(props);
    }

    public static create(props: string): Result<Distance> {
        const propsResult = Guard.againstNullOrUndefined(props, 'distance');
        if (!propsResult.succeeded) {
            return Result.fail<Distance>(propsResult.message);
        }

        return Result.ok<Distance>(new Distance({ value: props }))
    }


}