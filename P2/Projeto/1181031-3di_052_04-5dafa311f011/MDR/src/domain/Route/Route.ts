import { AggregateRoot } from "../../core/domain/AggregateRoot"
import { UniqueEntityID } from "../../core/domain/UniqueEntityID"
import { Result } from "../../core/logic/Result"
import { Guard } from "../../core/logic/Guard"
import { RouteId } from "./RouteId"
import { RouteDirection } from "./RouteDirection"
import { Name } from "./Name";
import { NetworkSegment } from "./NetworkSegment"
import { ShortName } from "../network/valueObjects/shortName"

interface RouteProps {
    routeId: RouteId;
    name: Name;
    idNoInicio: ShortName;
    idNoFim: ShortName;
    networkSegments: Array<NetworkSegment>;
}


export class Route extends AggregateRoot<RouteProps> {

    get id(): UniqueEntityID {
        return this._id;
    }

    get routeId(): RouteId {
        return this.props.routeId;
    }

    get name(): Name {
        return this.props.name;
    }

    get idNoInicio(): ShortName {
        return this.props.idNoInicio;
    }

    get idNoFim(): ShortName {
        return this.props.idNoFim;
    }

    get networkSegments(): Array<NetworkSegment> {
        return this.props.networkSegments;
    }


    set routeId(value: RouteId) {
        this.props.routeId = value;
    }

    set name(value: Name) {
        this.props.name = value;
    }

    set idNoInicio(value: ShortName) {
        this.props.idNoInicio;
    }

    set idNoFim(value: ShortName) {
        this.props.idNoFim;
    }

    set networkSegments(value: Array<NetworkSegment>) {
        this.props.networkSegments;
    }

    private constructor(props: RouteProps, id?: UniqueEntityID) {
        super(props, id);
    }


    public static create(props: RouteProps, id?: UniqueEntityID): Result<Route> {

        const guardedProps = [
            { argument: props.routeId, argumentName: 'routeId' },
            { argument: props.name, argumentName: 'name' },
            { argument: props.idNoInicio, argumentName: 'idNoInicio' },
            { argument: props.idNoFim, argumentName: 'idNoFim' },
            { argument: props.networkSegments, argumentName: 'networkSegments' },

        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<Route>(guardResult.message)
        }
        else {
            const route = new Route({
                ...props,
            }, id);

            return Result.ok<Route>(route);
        }
    }
}