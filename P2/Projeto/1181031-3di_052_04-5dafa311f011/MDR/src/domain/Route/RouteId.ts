import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";


interface RouteCodeProps {
    value: string;
}


export class RouteId extends ValueObject<RouteCodeProps>{

    get value(): string {
        return this.props.value;
    }

    private constructor(props: RouteCodeProps) {
        super(props)
    }


    public static create(props: string): Result<RouteId> {
        const propsResult = Guard.againstNullOrUndefined(props, 'routeId');

        if (!propsResult.succeeded) {
            return Result.fail<RouteId>(propsResult.message);
        }

        return Result.ok<RouteId>(new RouteId({ value: props }))
    }
}