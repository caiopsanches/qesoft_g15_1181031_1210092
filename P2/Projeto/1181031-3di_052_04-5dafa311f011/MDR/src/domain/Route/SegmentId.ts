import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";


interface SegmentIdProps {
    value: string;
}


export class SegmentId extends ValueObject<SegmentIdProps>{

    get value(): string {
        return this.props.value;
    }

    private constructor(props: SegmentIdProps) {
        super(props)
    }


    public static create(props: string): Result<SegmentId> {
        const propsResult = Guard.againstNullOrUndefined(props, 'SegmentId');

        if (!propsResult.succeeded) {
            return Result.fail<SegmentId>(propsResult.message);
        }

        return Result.ok<SegmentId>(new SegmentId({ value: props }))
    }
}