import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface DirectionRouteProps {
    value: string;
}

export class RouteDirection extends ValueObject<DirectionRouteProps> {
    public static directionType = ["Go", "Return"];

    get value(): string {
        return this.props.value;
    }


    public static create(type: string): Result<RouteDirection> {
        const guardResult = Guard.isOneOf(type, RouteDirection.directionType, 'direction');
        if (!guardResult.succeeded) {
            return Result.fail<RouteDirection>(guardResult.message);
        } else {
            return Result.ok<RouteDirection>(new RouteDirection({ value: type }))
        }
    }
}