import { Entity } from "../../core/domain/Entity";
import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";
import { Distance } from "./Distance";
import { DeslocationTime } from "./DeslocationTime";
import { ShortName } from "../network/valueObjects/shortName"
import { SegmentId } from "./SegmentId"


interface NetworkProps {
    segmentId: SegmentId;
    idNo1: ShortName;
    idNo2: ShortName;
    distance: Distance;
    deslocationTime: DeslocationTime;
}

export class NetworkSegment extends Entity<NetworkProps> {

    get id(): UniqueEntityID {
        return this._id;
    }

    get segmentId(): SegmentId {
        return this.props.segmentId;
    }

    get idNo1(): ShortName {
        return this.props.idNo1;
    }

    get idNo2(): ShortName {
        return this.props.idNo2;
    }

    get distance(): Distance {
        return this.props.distance;
    }
    get deslocationTime(): DeslocationTime {
        return this.props.deslocationTime;
    }

    set segmentId(value: SegmentId) {
        this.props.segmentId = value;
    }

    set idNo1(value: ShortName) {
        this.props.idNo1 = value;
    }

    set idNo2(value: ShortName) {
        this.props.idNo2 = value;
    }

    set distance(value: Distance) {
        this.props.distance = value;
    }

    set deslocationTime(value: DeslocationTime) {
        this.props.deslocationTime = value;
    }

    public constructor(props: NetworkProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: NetworkProps, id?: UniqueEntityID): Result<NetworkSegment> {
        const guardedProps = [
            { argument: props.segmentId, argumentName: 'segmentId' },
            { argument: props.idNo1, argumentName: 'idNo1' },
            { argument: props.idNo2, argumentName: 'idNo2' },
            { argument: props.distance, argumentName: 'distance' },
            { argument: props.deslocationTime, argumentName: 'deslocationTime' },
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<NetworkSegment>(guardResult.message)
        }
        else {
            const networkSegment = new NetworkSegment({
                ...props,
            }, id);
            return Result.ok<NetworkSegment>(networkSegment);
        }
    }

    public toString(): string {
        return this.idNo1.value.concat(',', this.idNo2.value, ',', this.distance.value, ',', this.deslocationTime.value.toString());
    }
}