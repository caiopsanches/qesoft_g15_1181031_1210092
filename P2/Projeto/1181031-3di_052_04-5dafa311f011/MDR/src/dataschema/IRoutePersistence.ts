import { ISegmentPersistence } from "./ISegmentPersistence";

export interface IRoutePersistence {
    routeId: string;
    name: string;
    idNoInicio: string;
    idNoFim: string;
    networkSegments: Array<ISegmentPersistence>
}