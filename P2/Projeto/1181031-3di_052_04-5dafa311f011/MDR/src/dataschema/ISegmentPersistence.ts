export interface ISegmentPersistence {
    segmentId: string;
    idNo1: string;
    idNo2: string;
    distance: Number;
    deslocationTime: Number;
}