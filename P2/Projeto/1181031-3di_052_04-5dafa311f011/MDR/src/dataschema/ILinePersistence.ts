import { ILinePathPersistence } from "./ILinePathPersistence";

export  interface ILinePersistence { 
    id: string,   
    name: string,
    colour: string,
    paths: Array<ILinePathPersistence>
    allowedVehicles: Array<string>
    allowedDrivers: Array<string>
}