export interface IUserPersistence {
    nome: string
    username: string
    password: string
    email: string
    nif: number
    endereco: string
}