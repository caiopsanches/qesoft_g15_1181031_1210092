export interface ILinePathPersistence {
    routeId: string;
    orientation: string;
}