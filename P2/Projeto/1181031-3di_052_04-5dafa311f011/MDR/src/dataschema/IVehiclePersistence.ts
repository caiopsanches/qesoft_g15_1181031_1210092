export  interface IVehiclePersistence { 
    code: string,   
    energy: string,
    autonomy: number,
    kmCost: number,
    avgConsumption: number;
    avgSpeed: number;
}