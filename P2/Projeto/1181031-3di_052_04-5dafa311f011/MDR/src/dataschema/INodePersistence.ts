export  interface INodePersistence { 
    shortName: string
    nodeName: string
    maxTimeStop: number
    vehicleCapacity: number
    latitude: number
    longitude: number
    isDepot: boolean
    isReliefPoint: boolean
}
