import jwt from '../../node_modules/jsonwebtoken';
export class TextUtil {

  public static isUUID(text: string): boolean {
    return new RegExp('\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b').test(text)
  }


  public static validateToken = function (token: string) {
    const secret = "addjsonwebtokensecretherelikeQuiscustodietipsoscustodes";
    const options = {
      expiresIn: "2d",
    };
    try {
      // verify makes sure that the token hasn't expired and has been issued by us
      var result = jwt.verify(token, secret, options);
      // Let's pass back the decoded token to the request object
      // We call next to pass execution to the subsequent middleware
      //res.json("Funcionou I guess");
      return true;
    } catch (err) {
      // Throw an error just in case anything goes wrong with verification
      throw new Error(err);
    }
  }
}