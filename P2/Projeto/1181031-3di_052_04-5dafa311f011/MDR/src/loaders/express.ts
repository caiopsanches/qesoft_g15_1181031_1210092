import express from 'express';
import bodyParser from 'body-parser';
import config from '../config';
import router from '../routes';
import cors from 'cors';

export default ({ app }: { app: express.Application }) => {

    var options = {
        inflate: true,
        limit: '700kb',
        type: "*/*"
    };

    /* app.get('/', (req,res) => {
         res.sendFile(process.cwd()+"../../front/dist/front/index.html")
       });
 */
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(bodyParser.raw(options));
    app.use(cors())
    //app.use(express.static(process.cwd() + "../../front/dist/front"));
    app.use(config.api.prefix, router());
};  