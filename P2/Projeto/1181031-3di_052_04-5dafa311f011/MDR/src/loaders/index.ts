import expressLoader from './express';
import mongooseLoader from './mongoose';
import dependencyInjectorLoader from './dependencyInjector';
//import Logger from './logger';

import config from '../config';
import LoginController from '../controllers/loginController';
import loginController from '../controllers/loginController';

export default async ({ expressApp }) => {
  const mongoConnection = mongooseLoader();

  console.log("Connected to the DB!");

  const vehicleController = {
    name: config.controller.vehicle.name,
    path: config.controller.vehicle.path
  }
  const vehicleService = {
    name: config.services.vehicle.name,
    path: config.services.vehicle.path
  }

  const vehicleRepo = {
    name: config.repos.vehicle.name,
    path: config.repos.vehicle.path
  }

  const vehicleSchema = {
    name: 'VehicleSchema',
    schema: '../persistence/schemas/vehicleSchema'
  };

  const crewmemberController = {
    name: config.controller.crewmember.name,
    path: config.controller.crewmember.path
  }
  const crewmemberService = {
    name: config.services.crewmember.name,
    path: config.services.crewmember.path
  }

  const crewmemberRepo = {
    name: config.repos.crewmember.name,
    path: config.repos.crewmember.path
  }

  const crewmemberSchema = {
    name: 'CrewmemberSchema',
    schema: '../persistence/schemas/crewmemberSchema'
  };

  const lineController = {
    name: config.controller.line.name,
    path: config.controller.line.path
  }
  const lineService = {
    name: config.services.line.name,
    path: config.services.line.path
  }

  const lineRepo = {
    name: config.repos.line.name,
    path: config.repos.line.path
  }

  const lineSchema = {
    name: 'LineSchema',
    schema: '../persistence/schemas/LineSchema'
  };

  const nodeController = {
    name: config.controller.node.name,
    path: config.controller.node.path
  }
  const nodeService = {
    name: config.services.node.name,
    path: config.services.node.path
  }

  const nodeRepo = {
    name: config.repos.node.name,
    path: config.repos.node.path
  }

  const nodeSchema = {
    name: 'NodeSchema',
    schema: '../persistence/schemas/nodeSchema'
  };

  const routeController = {
    name: config.controller.route.name,
    path: config.controller.route.path
  }

  const routeService = {
    name: config.services.route.name,
    path: config.services.route.path
  }

  const routeRepo = {
    name: config.repos.route.name,
    path: config.repos.route.path
  }

  const importController = {
    name: config.controller.import.name,
    path: config.controller.import.path
  }

  const importService = {
    name: config.services.import.name,
    path: config.services.import.path
  }

  const routeSchema = {
    name: 'RouteSchema',
    schema: '../persistence/schemas/RouteSchema'
  }

  const userController = {
    name: config.controller.user.name,
    path: config.controller.user.path
  }
  const userService = {
    name: config.services.user.name,
    path: config.services.user.path
  }

  const userRepo = {
    name: config.repos.user.name,
    path: config.repos.user.path
  }

  const userSchema = {
    name: 'UserSchema',
    schema: '../persistence/schemas/userSchema'
  };

  const loginController = {
    name: config.controller.login.name,
    path: config.controller.login.path
  }

  const files3dController = {
    name: config.controller.files.name,
    path: config.controller.files.path
  }

  await dependencyInjectorLoader({
    mongoConnection,
    schemas: [
      vehicleSchema,
      crewmemberSchema,
      lineSchema,
      routeSchema,
      nodeSchema,
      userSchema,
    ],
    controllers: [
      vehicleController,
      crewmemberController,
      lineController,
      routeController,
      nodeController,
      importController,
      userController,
      loginController,
      files3dController

    ],
    repos: [
      vehicleRepo,
      crewmemberRepo,
      lineRepo,
      routeRepo,
      nodeRepo,
      userRepo
    ],
    services: [
      vehicleService,
      crewmemberService,
      lineService,
      routeService,
      nodeService,
      importService,
      userService
    ],
  });

  await expressLoader({ app: expressApp });

};
