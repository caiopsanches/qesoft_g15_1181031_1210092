import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../config";
import IVehicleController from './IController/IvehicleController';
import IVehicleService from '../services/IServices/IVehicleService';
import IVehicleDTO from "../dto/IVehicleDTO";
import { Result } from "../core/logic/Result";
import { Container } from 'typedi';

export default class VehicleController implements IVehicleController {
    constructor(
        @Inject(config.services.vehicle.name) private vehicleService: IVehicleService
    ) {
    }
    public async createVehicle(req: Request, res: Response, next: NextFunction) {
        try {
            const IVehicleServ = Container.get(config.services.vehicle.name) as
                IVehicleService;
            const vehicleOrError = await IVehicleServ.createVehicle(req.body as IVehicleDTO) as Result<IVehicleDTO>;
            if (vehicleOrError.isFailure) {
                return res.status(402).send();
            }

            const vehicleDTO = vehicleOrError.getValue();
            return res.status(201).json(vehicleDTO);
        }
        catch (e) {
            return next(e);
        }
    };

    public async deleteVehicle(req: Request, res: Response, next: NextFunction) {

        try {
            const IVehicleServ = Container.get(config.services.vehicle.name) as
                IVehicleService;
            const vehicleOrError = await IVehicleServ.deleteVehicle(req.body.code as string) as Result<Boolean>;
            if (vehicleOrError.isFailure) {
                return res.status(402).send()
            }
            return res.status(201).send()
        }
        catch (e) {
            return next(e);
        }
    };

    public async getVehicles(req: Request, res: Response, next: NextFunction) {
        try {
            const IVehicleServ = Container.get(config.services.vehicle.name) as IVehicleService

            var vehicleOrError
            if (!("filter" in req.body)) {
                req.body.filter = ""
            }
            vehicleOrError = await IVehicleServ.getAllVehicles(req.body.filter) as Result<Array<IVehicleDTO>>
            
            if (vehicleOrError.isFailure) {
                return res.status(402).send()
            }

            const crewmemberDTO = vehicleOrError.getValue()
            return res.json(crewmemberDTO).status(201)
        } catch (e) {
            return next(e)
        }
    };
}
