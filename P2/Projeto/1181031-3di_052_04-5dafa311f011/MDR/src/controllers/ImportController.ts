import { Response, Request, NextFunction } from 'express';
import config from "../config";
import IImportVehicle from './IController/IImportController';
import IVehicleService from '../services/IServices/IVehicleService';
import { Container } from 'typedi';
import IImportService from '../services/IServices/IImportService';
const { DOMParser } = require('xmldom')
import ICrewmemberService from '../services/IServices/ICrewmemberService';


export default class ImportController implements IImportVehicle {

    public async processData(req: Request, res: Response, next: NextFunction) {
        const IImportServ = Container.get(config.services.import.name) as IImportService
        try {
           
            var bytes = req.body;
          
            let xml = new TextDecoder().decode(bytes).replace((/  |\r\n|\n|\r/gm), "");
            var parser = new DOMParser();
            var doc = parser.parseFromString(xml, 'text/xml');

            const endRes = await IImportServ.importFromXML(doc)
            if (endRes.isFailure) {
                return res.status(402).send("Errors in the import");
            }
            else {
                return res.status(201).json("Sucessfully imported");
            }
        }
        catch (e) {
            return next(e);
        }
    }
}
