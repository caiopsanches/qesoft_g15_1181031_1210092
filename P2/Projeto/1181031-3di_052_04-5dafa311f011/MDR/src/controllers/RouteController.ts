import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../config";
import { Container } from 'typedi';
import IRouteController from './IController/IRouteController';
import IRouteService from '../services/IServices/IRouteService';
import IRouteDTO from '../dto/IRouteDTO';

import { Result } from "../core/logic/Result";


export default class RouteController implements IRouteController {
    constructor(
        @Inject(config.services.route.name) private routeServiceInstance: IRouteService) { }

    public async createRoute(req: Request, res: Response, next: NextFunction) {
        try {

            const IRouteService = Container.get(config.services.route.name) as IRouteService;
            const routeOrError = await IRouteService.createRoute(req.body as IRouteDTO) as Result<IRouteDTO>;

            if (routeOrError.isFailure) {
                return res.status(402).send();
            }
            const routeDTO = routeOrError.getValue();
            return res.status(201).json(routeDTO);
        }
        catch (e) {
            return next(e);
        }
    };

    public async deleteRoute(req: Request, res: Response, next: NextFunction) {

        try {
            const IRouteServ = Container.get(config.services.route.name) as
                IRouteService;
            const routeOrError = await IRouteServ.deleteRoute(req.body.code as string) as Result<Boolean>;
            if (routeOrError.isFailure) {
                return res.status(402).send()
            }
            return res.status(201).send()
        }
        catch (e) {
            return next(e);
        }
    };


    public async getRoutes(req: Request, res: Response, next: NextFunction) {
        try {
            const IRouteServ = Container.get(config.services.route.name) as IRouteService
            if(req.query.lineId){
            let lineId = req.query.lineId.toString()
            console.log(lineId)
            var routeOrError
            routeOrError = await IRouteServ.getAllRoutesbyLineId(lineId) as Result<Array<IRouteDTO>>
            } else {
                routeOrError = await IRouteServ.getAllRoutes() as Result<Array<IRouteDTO>>
            }
            if (routeOrError.isFailure) {
                return res.send().status(402)
            }

            const routeDTO = routeOrError.getValue()
            return res.json(routeDTO).status(201)

        } catch (e) {
            return next(e)
        }
    }
}