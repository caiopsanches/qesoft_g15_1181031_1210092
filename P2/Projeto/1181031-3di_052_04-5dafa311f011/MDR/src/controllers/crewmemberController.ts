import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../config";
import ICrewmemberController from './IController/IcrewmemberController';
import ICrewmemberService from '../services/IServices/ICrewmemberService';
import ICrewmemberDTO from "../dto/ICrewmemberDTO";
import { Result } from "../core/logic/Result";
import { Container } from 'typedi';

export default class CrewmemberController implements ICrewmemberController {
    constructor(
        @Inject(config.services.crewmember.name) private crewmemberServiceInstance: ICrewmemberService
    ) { }

    public async createCrewmember(req: Request, res: Response, next: NextFunction) {
        try {
            const ICrewmemberServ = Container.get(config.services.crewmember.name) as
                ICrewmemberService;
            const crewmemberOrError = await ICrewmemberServ.createCrewmember(req.body as ICrewmemberDTO) as Result<ICrewmemberDTO>;
            if (crewmemberOrError.isFailure) {
                return res.status(402).send();
            }

            const crewmemberDTO = crewmemberOrError.getValue();
            return res.status(201).json(crewmemberDTO);
        }
        catch (e) {
            return next(e);
        }
    };


    public async deleteCrewmember(req: Request, res: Response, next: NextFunction) {

        try {
            const ICrewmemberServ = Container.get(config.services.crewmember.name) as
            ICrewmemberService;
            const crewmemberOrError = await ICrewmemberServ.deleteCrewmember(req.body.code as string) as Result<Boolean>;
            if (crewmemberOrError.isFailure) {
                return res.status(402).send()
            }
            return res.status(201).send()
        }
        catch (e) {
            return next(e);
        }
    };

    public async getCrewmembers(req: Request, res: Response, next: NextFunction) {
        try {
            const ICrewmemberServ = Container.get(config.services.crewmember.name) as ICrewmemberService

            var crewOrError
            if (!("filter" in req.body)) {
                req.body.filter = ""
            }
            crewOrError = await ICrewmemberServ.getAllCrewmembers(req.body.filter) as Result<Array<ICrewmemberDTO>>
            
            if (crewOrError.isFailure) {
                return res.status(402).send()
            }

            const crewmemberDTO = crewOrError.getValue()
            return res.json(crewmemberDTO).status(201)
        } catch (e) {
            return next(e)
        }
    }

}