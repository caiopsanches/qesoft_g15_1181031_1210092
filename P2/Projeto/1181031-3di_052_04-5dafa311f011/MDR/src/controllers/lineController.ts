import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import { Container } from 'typedi';
import { Result } from "../core/logic/Result";
import config from "../config";
import ILineController from './IController/ILineController'
import ILineService from '../services/IServices/ILineService';
import ILineDTO from '../dto/ILineDTO';

export default class LineController implements ILineController {
    constructor(
        @Inject(config.services.line.name) private lineServiceInstance: ILineService
    ) { }
    public async createLine(req: Request, res: Response, next: NextFunction) {
        try {
            const ILineServ = Container.get(config.services.line.name) as
                ILineService;
            console.log("Creating Line...");
            console.log(req.body);
            const lineOrError = await ILineServ.createLine(req.body as ILineDTO) as Result<ILineDTO>;
            if (lineOrError.isFailure) {
                return res.status(402).send();
            }

            const lineDTO = lineOrError.getValue();
            return res.status(201).json(lineDTO);
        }
        catch (e) {
            return next(e);
        }
    };

    public async getLines(req: Request, res: Response, next: NextFunction) {
        try {
            const ILineServ = Container.get(config.services.line.name) as ILineService
            var lineOrError
            if (!("filter" in req.query)) {
                req.query.filter = ""
            }

            if ("lineId" in req.query) {
                lineOrError = await ILineServ.getLineByLineId(req.query.lineId.toString(), req.query.filter.toString()) as Result<Array<ILineDTO>>
            } else if ("name" in req.query) {
                lineOrError = await ILineServ.getLineByLineName(req.query.name.toString(), req.query.filter.toString()) as Result<Array<ILineDTO>>
            } else {
                lineOrError = await ILineServ.getAllLines(req.query.filter.toString()) as Result<Array<ILineDTO>>
            }

            if (lineOrError.isFailure) {
                return res.status(402).send()
            }

            const lineDTO = lineOrError.getValue()
            return res.json(lineDTO).status(201)
        } catch (e) {
            return next(e)
        }
    }

    public async deleteLine(req: Request, res: Response, next: NextFunction) {
        try {
            const ILineServ = Container.get(config.services.line.name) as
                ILineService;
            const lineOrError = await ILineServ.deleteLine(req.body.lineId as string) as Result<Boolean>;
            if (lineOrError.isFailure) {
                return res.status(402).send()
            }
            return res.status(201).send()
        }
        catch (e) {
            return next(e);
        }
    };
}