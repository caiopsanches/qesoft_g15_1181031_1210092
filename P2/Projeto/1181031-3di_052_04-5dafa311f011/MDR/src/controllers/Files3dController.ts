import { Response, Request, NextFunction } from 'express'
import IFiles3dController from './IController/IFiles3dController'
import * as fs from 'fs'

export default class Files3dController implements IFiles3dController {

    public async downloadStop(req: Request, res: Response, next: NextFunction) {

        const directoryPath = "./assets/"
        if (req.query.name != null) {
            let fileName = req.query.name + ".glb"

            res.download(directoryPath + fileName, fileName, (err) => {
                if (err) {
                    res.status(500).send({
                        message: "Could not download the file. " + err,
                    });
                }
            });
        } else {
            fs.readdir(directoryPath, function (err, files) {
                if (err) {
                    res.status(500).send({
                        message: "Unable to scan files!"
                    });
                }
                let fileInfos = [];

                files.forEach((file) => {
                    
                    let t = file.split(".")
                    fileInfos.push({
                        name: t[0]
                    });
                });
                res.status(200).send(fileInfos);
            });
        }
    }
}
