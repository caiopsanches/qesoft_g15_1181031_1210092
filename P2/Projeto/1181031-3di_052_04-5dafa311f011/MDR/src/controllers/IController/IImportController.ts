import { Request, Response, NextFunction } from 'express';

export default interface IVehicleController {
    processData(req: Request, res: Response, next: NextFunction);
}