import { Request, Response, NextFunction } from 'express';

export default interface IVehicleController {
    createVehicle(req: Request, res: Response, next: NextFunction);
    deleteVehicle(req: Request, res: Response, next: NextFunction);
    getVehicles(req: Request, res: Response, next: NextFunction);
    //updateVehicle(req: Request, res: Response, next: NextFunction);

}