import { NextFunction, Request, Response } from 'express';

export default interface IUserController {
    criarCliente(req: Request, res: Response, next: NextFunction)
    getAllClientes(req: Request, res: Response)
    deleteCliente(req: Request, res: Response, next: NextFunction)
}