import { Request, Response, NextFunction } from 'express';

export default interface ICrewmemberController {
    createCrewmember(req: Request, res: Response, next: NextFunction);
    deleteCrewmember(req: Request, res: Response, next: NextFunction);
    //updateCrewmember(req: Request, res: Response, next: NextFunction);
    getCrewmembers(req: Request, res: Response, next: NextFunction);

}