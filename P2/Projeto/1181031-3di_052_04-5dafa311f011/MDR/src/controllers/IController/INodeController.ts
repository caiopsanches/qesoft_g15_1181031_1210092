import { Request, Response, NextFunction } from 'express'

export default interface INodeController {
    createNode(req: Request, res: Response, next: NextFunction)
    getNodes(req: Request, res: Response, next: NextFunction)
    deleteNode(req: Request, res: Response, next: NextFunction)
}