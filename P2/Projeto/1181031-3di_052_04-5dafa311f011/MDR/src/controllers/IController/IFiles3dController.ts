import { Request, Response, NextFunction } from 'express';

export default interface IFiles3dController {
    downloadStop(req: Request, res: Response, next: NextFunction)
}