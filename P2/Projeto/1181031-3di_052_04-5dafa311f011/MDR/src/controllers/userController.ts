import IUserController from "./IController/IUserController";
import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import { Container } from 'typedi';
import { Result } from "../core/logic/Result";
import config from "../config";
import IUserService from "../services/IServices/IUserService";
import IUserDTO from "../dto/IUserDTO";
import { TextUtil } from "../utils/TextUtil";
import { Role } from "../domain/user/Role";

export default class UserController implements IUserController {

    constructor(
        @Inject(config.services.user.name) private userServiceInstance: IUserService
    ) { }


    public async criarCliente(req: Request, res: Response, next: NextFunction) {
        try {
            const IUserServ = Container.get(config.services.user.name) as
                IUserService
            const clienteOrError = await IUserServ.criarCliente(req.body as IUserDTO) as Result<IUserDTO>
            if (clienteOrError.isFailure) {
                return res.status(402).send()
            }

            const userDTO = clienteOrError.getValue()
            return res.status(201).json(userDTO)
        }
        catch (e) {
            return next(e)
        }
    }

    async getAllClientes(req: Request, res: Response) {
        var token;
        if (req.headers.token) {
            token = req.headers.token;
        } else {
            res.status(400).type("text").send("Token inexistente.");
        }

        if (TextUtil.validateToken(token)) {
            await this.userServiceInstance.getAllClientes(req.query.order.toString()).then(
                clientesRet => { return res.status(200).json(clientesRet); }
            ).catch(err => {
                res.status(400).type("text").send(err.message);
            });
        } else {
            res.status(400).type("text").send("Token inválido.");
        }
    }


    public async deleteCliente(req: Request, res: Response, next: NextFunction) {
        try {
            const IUserServ = Container.get(config.services.user.name) as
                IUserService;
            const clienteOrError = await IUserServ.deleteCliente(req.body.username as string) as Result<Boolean>;
            if (clienteOrError.isFailure) {
                return res.status(402).send()
            }
            return res.status(201).send()
        }
        catch (e) {
            return next(e);
        }
    };





}