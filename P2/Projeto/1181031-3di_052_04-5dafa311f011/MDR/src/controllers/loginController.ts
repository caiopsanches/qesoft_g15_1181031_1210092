import ILoginController from "./IController/ILoginController";
import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import { Container } from 'typedi';
import config from "../config";
import IUserService from "../services/IServices/IUserService";


export default class LoginController implements ILoginController {

    constructor(
        @Inject(config.services.user.name) private userServiceInstance: IUserService
    ) { }


    public async login(req: Request, res: Response, next: NextFunction) {
        try {
            const IUserServ = Container.get(config.services.user.name) as IUserService
            IUserServ.comparePasswords(req.body.password, req.body.username).then(logDTO => {
                res.status(200).json(logDTO);
            }).catch(erro => {
                res.status(400).json({ "message": erro.message });
            });
        }
        catch (e) {
            return next(e)

        }

    }
}
