import { Response, Request, NextFunction } from 'express';
import { Inject } from 'typedi';
import { Container } from 'typedi';
import { Result } from "../core/logic/Result";
import config from "../config";
import INodeController from './IController/INodeController'
import INodeService from '../services/IServices/INodeService';
import INodeDTO from '../dto/INodeDTO';

export default class NodeController implements INodeController {
    constructor(
        @Inject(config.services.node.name) private nodeServiceInstance: INodeService
    ) { }
    public async createNode(req: Request, res: Response, next: NextFunction) {
        try {
            const INodeServ = Container.get(config.services.node.name) as
                INodeService
            const nodeOrError = await INodeServ.createNode(req.body as INodeDTO) as Result<INodeDTO>
            if (nodeOrError.isFailure) {
                return res.status(402).send()
            }

            const nodeDTO = nodeOrError.getValue()
            return res.status(201).json(nodeDTO)
        }
        catch (e) {
            return next(e)
        }
    }

    public async getNodes(req: Request, res: Response, next: NextFunction) {

        try {
            const INodeServ = Container.get(config.services.node.name) as INodeService

            var nodeOrError
            if (!("order" in req.query)) {
                req.query.order = ""
            }

            if ("shortName" in req.query) {
                nodeOrError = await INodeServ.getNodeByShortName(req.query.shortName.toString(), req.query.order.toString()) as Result<Array<INodeDTO>>
            } else if ("nodeName" in req.query) {
                nodeOrError = await INodeServ.getNodeByNodeName(req.query.nodeName.toString(), req.query.order.toString()) as Result<Array<INodeDTO>>
            } else {
                nodeOrError = await INodeServ.getAllNodes(req.query.order.toString()) as Result<Array<INodeDTO>>
            }

            if (nodeOrError.isFailure) {
                return res.status(402).send()
            }

            const nodeDTO = nodeOrError.getValue()
            return res.json(nodeDTO).status(201)

        } catch (e) {
            return next(e)
        }
    }

    public async deleteNode(req: Request, res: Response, next: NextFunction) {
        try {
            const INodeServ = Container.get(config.services.node.name) as
                INodeService;
            const lineOrError = await INodeServ.deleteNode(req.body.shortName as string) as Result<Boolean>
            if (lineOrError.isFailure) {
                return res.status(402).send()
            }
            return res.status(201).send()
        }
        catch (e) {
            return next(e);
        }
    };
}