import { Router, Request, Response } from 'express';
import config from "../config";
import { Container } from 'typedi';
import { celebrate, Joi } from 'celebrate';
import IVehicleController from '../controllers/IController/IvehicleController';


const route = Router()

export default (app: Router) => {

  const ctrl = Container.get(config.controller.vehicle.name) as
    IVehicleController;

  app.use('/vehicle', route)

  route.post('/', celebrate({

    body: Joi.object({
      code: Joi.string(),
      energy: Joi.string(),
      autonomy: Joi.number(),
      kmCost: Joi.number(),
      avgConsumption: Joi.number(),
      avgSpeed: Joi.number()
    }),
  }),
    (req, res, next) => ctrl.createVehicle(req, res, next))

  route.delete('/', celebrate({

    body: Joi.object({
      code: Joi.string()
    }),
  }),
    (req, res, next) => ctrl.deleteVehicle(req, res, next))

  route.get('/', celebrate({
    body: Joi.object({
      code: Joi.string(),
      energy: Joi.string(),
      autonomy: Joi.number(),
      kmCost: Joi.number(),
      avgConsumption: Joi.number(),
      avgSpeed: Joi.number()
    })
  }),
    (req, res, next) => ctrl.getVehicles(req, res, next))
}
