import { Router, Request, Response } from 'express';
import config from "../config";
import { Container } from 'typedi';
import { celebrate, Joi } from 'celebrate';
import ICrewmemberController from '../controllers/IController/IcrewmemberController';


const route = Router()

export default (app: Router) => {

  const ctrl = Container.get(config.controller.crewmember.name) as
    ICrewmemberController;

  app.use('/crewmember', route)


  route.post('/', celebrate({
    body: Joi.object({
      code: Joi.string(),
      description: Joi.string()
    }),
  }),
    (req, res, next) => ctrl.createCrewmember(req, res, next))

  route.get('/', celebrate({
    body: Joi.object({
      code: Joi.string(),
      description: Joi.string()
    })
  }),
    (req, res, next) => ctrl.getCrewmembers(req, res, next))

    route.delete('/', celebrate({

      body: Joi.object({
        code: Joi.string()
      }),
    }),
      (req, res, next) => ctrl.deleteCrewmember(req, res, next))
}