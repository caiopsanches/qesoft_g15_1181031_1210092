import { Router, Request, Response, NextFunction } from 'express';
import config from "../config";
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import ILineController from '../controllers/IController/ILineController';
import { LineId } from '../domain/line/LineId';
import { link } from 'fs';

const route = Router();

export default (app: Router) => {
    const ctrl = Container.get(config.controller.line.name) as ILineController;

    app.use('/line', route);

    route.post('/', celebrate({
        body: Joi.object({
            lineId: Joi.string().required(),
            name: Joi.string().required(),
            colour: Joi.string().required(),
            paths: Joi.array().items({
                routeId: Joi.string(),
                orientation: Joi.string(),
            }),
            allowedVehicles: Joi.array(),
            allowedDrivers: Joi.array(),
        })
    }),
        (req, res, next) => ctrl.createLine(req, res, next));

    route.get('/', celebrate({
        body: Joi.object({
            lineId: Joi.string().required(),
            name: Joi.string().required(),
            colour: Joi.string().required(),
            paths: Joi.array().items({
                routeId: Joi.string(),
                orientation: Joi.string(),
            }),
            allowedVehicles: Joi.array(),
            allowedDrivers: Joi.array(),
        })
    }),
        (req, res, next) => ctrl.getLines(req, res, next))

    route.delete('/', celebrate({
        body: Joi.object({
            lineId: Joi.string()
        }),
    }),
        (req, res, next) => ctrl.deleteLine(req, res, next))
};