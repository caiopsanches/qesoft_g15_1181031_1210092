import { Router, Request, Response, NextFunction } from 'express';
import config from "../config";
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import IUserController from '../controllers/IController/IUserController';
import ILoginController from '../controllers/IController/ILoginController';

const route = Router();

export default (app: Router) => {
    const ctrl = Container.get(config.controller.user.name) as IUserController;
    const ctrl1 = Container.get(config.controller.user.name) as ILoginController;

    app.use('/cliente', route);
    route.post('/', celebrate({
        body: Joi.object({
            nome: Joi.string().required(),
            username: Joi.string().required(),
            password: Joi.string().required(),
            email: Joi.string().required(),
            nif: Joi.number().required(),
            endereco: Joi.string().required(),
            role: Joi.string().required(),
        })

    }),


        (req, res, next) => ctrl.criarCliente(req, res, next))

    route.get('/', celebrate({
        body: Joi.object({
            nome: Joi.string().required(),
            username: Joi.string().required(),
            password: Joi.string().required(),
            email: Joi.string().required(),
            nif: Joi.number().required(),
            endereco: Joi.string().required(),
            role: Joi.string().required(),
        })
    }),
        (req, res) => ctrl.getAllClientes(req, res))

    route.delete('/', celebrate({
        body: Joi.object({
            username: Joi.string()
        }),
    }),
        (req, res, next) => ctrl.deleteCliente(req, res, next))
};