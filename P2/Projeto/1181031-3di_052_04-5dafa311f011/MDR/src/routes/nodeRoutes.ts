import { Router, Request, Response, NextFunction } from 'express';
import config from "../config";
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import INodeController from '../controllers/IController/INodeController';

const route = Router();

export default (app: Router) => {
    const ctrl = Container.get(config.controller.node.name) as INodeController;

    app.use('/node', route);
    route.post('/', celebrate({
        body: Joi.object({
            shortName: Joi.string().required(),
            nodeName: Joi.string().required(),
            maxTimeStop: Joi.number().required(),
            vehicleCapacity: Joi.number().required(),
            latitude: Joi.number().required(),
            longitude: Joi.number().required(),
            isDepot: Joi.boolean().required(),
            isReliefPoint: Joi.boolean().required()
        })
    }),
        (req, res, next) => ctrl.createNode(req, res, next))

    route.get('/', celebrate({
        body: Joi.object({
            shortName: Joi.string().required(),
            nodeName: Joi.string().required(),
            maxTimeStop: Joi.number().required(),
            vehicleCapacity: Joi.number().required(),
            latitude: Joi.number().required(),
            longitude: Joi.number().required(),
            isDepot: Joi.boolean().required(),
            isReliefPoint: Joi.boolean().required()
        })
    }),
        (req, res, next) => ctrl.getNodes(req, res, next))

    route.delete('/', celebrate({
        body: Joi.object({
            shortName: Joi.string()
        }),
    }),
        (req, res, next) => ctrl.deleteNode(req, res, next))
};