import { Router, Request, Response } from 'express';
import config from "../config";
import { Container } from 'typedi';
import { celebrate, Joi } from 'celebrate';
import IFiles3dController from '../controllers/IController/IFiles3dController';


const route = Router()

export default (app: Router) => {

  const ctrl = Container.get(config.controller.files.name) as
    IFiles3dController;

  app.use('/files', route)

  route.get("/", ctrl.downloadStop)
}
