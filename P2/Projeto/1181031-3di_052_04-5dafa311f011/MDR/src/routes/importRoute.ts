import { Router, Request, Response } from 'express';
import config from "../config";
import { Container } from 'typedi';
import { celebrate, Joi } from 'celebrate';
import IImportController from '../controllers/IController/IImportController';
var parser = require('xml2json-light');
const route = Router()

export default (app: Router) => {

    const ctrl = Container.get(config.controller.import.name) as IImportController
    app.use('/import', route);

    route.post('/', function (req, res, next) {

        ctrl.processData(req, res, next);
    })
}

