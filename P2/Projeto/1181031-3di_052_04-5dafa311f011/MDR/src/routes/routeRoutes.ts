import { Router, Request, Response, NextFunction } from 'express';
import config from "../config";
import { Container } from 'typedi';
import { celebrate, Joi } from 'celebrate';
import IRouteController from '../controllers/IController/IRouteController';

const route = Router()

export default (app: Router) => {

    const ctrl = Container.get(config.controller.route.name) as IRouteController;

    app.use('/route', route)


    route.post('/', celebrate({
        body: Joi.object({
            routeId: Joi.string().required(),
            name: Joi.string(),
            idNoInicio: Joi.string(),
            idNoFim: Joi.string(),
            networkSegments: Joi.array().items({
                segmentId: Joi.string(),
                idNo1: Joi.string(),
                idNo2: Joi.string(),
                distance: Joi.number(),
                deslocationTime: Joi.number(),
            }),
        }),
    }),

        (req, res, next) => ctrl.createRoute(req, res, next))

    route.delete('/', celebrate({

        body: Joi.object({
            code: Joi.string()
        }),
    }),
        (req, res, next) => ctrl.deleteRoute(req, res, next))

    route.get('/', celebrate({
        body: Joi.object({
            routeId: Joi.string().required(),
            name: Joi.string(),
            idNoInicio: Joi.string(),
            idNoFim: Joi.string(),
            networkSegments: Joi.array().items({
                segmentId: Joi.string(),
                idNo1: Joi.string(),
                idNo2: Joi.string(),
                distance: Joi.number(),
                deslocationTime: Joi.number(),
            }),
        }),
    }),
        (req, res, next) => ctrl.getRoutes(req, res, next))
}
