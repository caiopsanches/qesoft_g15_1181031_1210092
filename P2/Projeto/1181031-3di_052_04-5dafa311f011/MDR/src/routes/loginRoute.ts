import { Router, Request, Response, NextFunction } from 'express';
import config from "../config";
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import ILoginController from '../controllers/IController/ILoginController';

const route = Router();

export default (app: Router) => {

    const ctrl = Container.get(config.controller.login.name) as ILoginController;


    app.use('/login', route);
    route.post('/', celebrate({
        body: Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
        })

    }),

        (req: Request, res: Response, next: NextFunction) => ctrl.login(req, res, next))

};