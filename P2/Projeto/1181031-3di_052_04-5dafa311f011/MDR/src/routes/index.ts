
import { Router } from 'express';
//import auth from './routes/authRoute';
import vehicle from './vehiclesRoutes';
import crewmember from './crewmembersRoutes';
import line from './lineRoute';
import importRoute from './importRoute'
import route from './routeRoutes';
import node from './nodeRoutes';
import api from './apiRoute';
import clientes from './userRoute';
import files from './files3d'
import loginRoute from './loginRoute';

export default () => {
	const app = Router();
	//auth(app);
	vehicle(app);
	crewmember(app);
	line(app);
	route(app);
	node(app);
	api(app);
	importRoute(app);
	clientes(app)
	files(app)
	loginRoute(app)
	return app
}