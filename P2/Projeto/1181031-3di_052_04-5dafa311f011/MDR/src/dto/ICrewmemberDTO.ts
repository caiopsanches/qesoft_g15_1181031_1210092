export default interface ICrewmemberDTO { 
    id: string,
    code: string,
    description: string,
}