import ISegementDTO from "./ISegementDTO"

export default interface IRouteDTO {
    id: string,
    routeId: string;
    name: string;
    idNoInicio: string;
    idNoFim: string;
    networkSegments: Array<ISegementDTO>;
}