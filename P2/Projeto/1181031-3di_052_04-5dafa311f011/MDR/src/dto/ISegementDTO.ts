export default interface ISegementDTO {
    segmentId: string,
    idNo1: string,
    idNo2: string;
    distance: string;
    deslocationTime: number;
}