export default interface ILinePathDTO {
    routeId: string,
    orientation: string
}