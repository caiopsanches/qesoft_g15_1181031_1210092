import ILinePathDTO from "./ILinePathDTO"

export default interface ILineDTO {
  id: string;
  lineId: string;
  name: string
  colour: string
  paths: Array<ILinePathDTO>
  allowedVehicles: Array<string>
  allowedDrivers: Array<string>
}