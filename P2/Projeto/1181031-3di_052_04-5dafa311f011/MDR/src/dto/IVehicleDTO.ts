export default interface IVehicleDTO { 
    id: string,
    code: string,
    energy: string,
    autonomy: number,
    kmCost: number,
    avgConsumption: number;
    avgSpeed: number;
}