import ILoginDTO from "./ILoginDTO";
import IUserDTO from "./IUserDTO";

export class LoginDTO implements ILoginDTO {
    public token: string;
    public user: IUserDTO;

    constructor(token: string, user: IUserDTO) {
        this.token = token;
        this.user = user;
    }
}