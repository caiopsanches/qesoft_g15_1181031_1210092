export default {
    port: process.env.PORT || 8080,
    api: { prefix: "/api" },

    jwtSecret: process.env.JWT_SECRET,

    import: {
        key: "key",
        nameC: "Config",
        configName: "GlDocumentConfig",
        parameter: "Parameter",
        name: "Name",
        title: "Title",
        vehicle: {
            name: "VehicleType",
            atr: {
                code: "Name",
                energy: "EnergySource",
                autonomy: "Autonomy",
                cost: "Cost",
                avgSpeed: "AverageSpeed",
                avgConsumption: "Consumption"
            }
        },

        path: { name: "Paths" },
        line: {
            name: "Line",
            atr: {
                id: "LineId",
                key: "key",
                name: "Name",
                color: "Color",

                allowedVehicles: {
                    name: "AllowedVehicles",
                    vehicleCode: {
                        name: "VehicleCode",
                        atr: {
                            code: "Code"
                        }
                    }
                },
                deniedVehicles: {
                    name: "DeniedVehicles",
                    vehicleCode: {
                        name: "VehicleCode",
                        atr: {
                            code: "Code"
                        }
                    }
                },

                allowedDrivers: {
                    name: "AllowedDrivers",
                    crewMemberCode: {
                        name: "CrewMemberCode",
                        atr: {
                            code: "Code"
                        }
                    }
                },
                deniedDrivers: {
                    name: "DeniedDrivers",
                    crewMemberCode: {
                        name: "CrewMemberCode",
                        atr: {
                            code: "Code"
                        }
                    }
                },
                paths: {
                    name: "Paths",
                    pathId: {
                        name: "LinePath",
                        id: "Path",
                        orientation: "Orientation"
                    }
                }
            }
        },
        routes: {
            name: "Routes",
            route: {
                name: "Path",
                atr: {
                    routeId: "RouteId",
                    name: "Name",
                    begin: "BeginNode",
                    end: "EndNode",
                    direction: "Direction",
                    isEmpty: "IsEmpty",
                    pathNodes: "PathNodes",
                    pathNode: "PathNode"
                }
            },
            segments: {
                name: "Segments"
            },
            segment: {
                name: "Segment"
            },
            atr: {
                segmentId: "SegmentId",
                node: "Node",
                distance: "Distance",
                duration: "Duration"
            }
        },
        node: {
            name: "Node",
            atr: {
                key: "Key",
                shortName: "ShortName",
                name: "Name",
                capacity: "1",
                time: 60,
                latitude: "Latitude",
                longitude: "Longitude",
                isDepot: "IsDepot",
                isReliefPoint: "IsReliefPoint"
            }
        },

        crewMember: {
            name: "CrewMember",
            atr: {
                code: "Code",
                description: "Description"
            }
        }
    },
    controller: {
        vehicle: {
            name: "VehicleController",
            path: "../controllers/vehicleController"
        },

        crewmember: {
            name: "CrewmemberController",
            path: "../controllers/crewmemberController"
        },
        line: {
            name: "LineController",
            path: "../controllers/lineController"

        },
        route: {
            name: "RouteController",
            path: "../controllers/RouteController"
        },
        import: {
            name: "ImportController",
            path: "../controllers/ImportController"
        },
        node: {
            name: "NodeController",
            path: "../controllers/nodeController"
        },
        user: {
            name: "UserController",
            path: "../controllers/userController"
        },
        login: {
            name: "LoginController",
            path: "../controllers/loginController"
        },
        files: {
            name: "Files3dController",
            path: "../controllers/Files3dController"
        }
    },
    repos: {
        vehicle: {
            name: "VehicleRepo",
            path: "../repos/VehicleRepo"

        },

        crewmember: {
            name: "CrewmemberRepo",
            path: "../repos/crewmemberRepo"
        },
        line: {
            name: "LineRepo",
            path: "../repos/LineRepo"

        },
        route: {
            name: "RouteRepo",
            path: "../repos/RouteRepo"
        },
        node: {
            name: "NodeRepo",
            path: "../repos/nodeRepo"
        },
        user: {
            name: "UserRepo",
            path: "../repos/UserRepo"
        }
    },
    services: {
        import: {
            name: "ImportService",
            path: "../services/ImportService"
        },
        vehicle: {
            name: "VehicleService",
            path: "../services/VehicleService"
        },

        crewmember: {
            name: "CrewmemberService",
            path: "../services/CrewmemberService"
        },

        line: {
            name: "LineService",
            path: "../services/LineService"
        },
        route: {
            name: "RouteService",
            path: "../services/RouteService"
        },
        node: {
            name: "NodeService",
            path: "../services/NodeService"
        },
        user: {
            name: "UserService",
            path: "../services/UserService"
        }

    }
    , databaseURL: 'mongodb+srv://grupo52:Lusiadas2020@cluster-3di-g52-04.fhbmr.mongodb.net/optDB?retryWrites=true&w=majority',

    existingTypes: ["Diesel", "Gasolina", "Elétrico", "GPL", "Gás"],
    RoleEnum: ["DataAdmin", "Cliente", "Gestor"]


};