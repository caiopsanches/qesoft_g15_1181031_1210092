import express from 'express';

async function startServer() {
  const app = express();
  await require('./loaders').default({ expressApp: app });
  var port = process.env.PORT || 8080;

  console.log('Server listening on port: ' + port);

/*  app.get('/', (req, res) => {
    res.sendFile(process.cwd() + "../front/dist/front/index.html")
  })

  app.use(express.static(process.cwd() + "../front/dist/front"));
*/
  app.listen(port);
}

startServer();