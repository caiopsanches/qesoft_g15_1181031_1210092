import { ILinePersistence } from '../../dataschema/ILinePersistence';
import mongoose from 'mongoose';

const LineSchema = new mongoose.Schema(
    {
        lineId: { type: String, unique: true },
        name: { type: String, unique: true },
        colour: { type: String, unique: true },
        paths: { type: Array },
        allowedVehicles: { type: [String] },
        allowedDrivers: { type: [String] },
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ILinePersistence & mongoose.Document>('Line', LineSchema);
