import { ICrewmemberPersistence } from '../../dataschema/ICrewmemberPersistence';
import mongoose from 'mongoose';

const CrewmemberSchema = new mongoose.Schema(
    {
        code: { type: String, unique: true, require: true },
        description: { type: String, unique: false, require: true },
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ICrewmemberPersistence & mongoose.Document>('Crewmember', CrewmemberSchema);