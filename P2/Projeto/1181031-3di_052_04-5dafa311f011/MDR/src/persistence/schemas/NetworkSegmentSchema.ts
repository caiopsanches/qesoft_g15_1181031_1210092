import { ISegmentPersistence } from '../../dataschema/ISegmentPersistence';
import mongoose from 'mongoose';

const NetworkSegmentSchema = new mongoose.Schema(
    {
        segmentId: { type: String },
        idNo1: { type: String },
        idNo2: { type: String },
        distance: { type: String },
        deslocationTime: { type: Number },
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ISegmentPersistence & mongoose.Document>('NetworkSegment', NetworkSegmentSchema);