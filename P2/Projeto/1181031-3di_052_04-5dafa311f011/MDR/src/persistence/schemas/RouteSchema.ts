import { IRoutePersistence } from '../../dataschema/IRoutePersistence';
import mongoose from 'mongoose';
import NetworkSegmentSchema from '../../persistence/schemas/NetworkSegmentSchema';
const RouteSchema = new mongoose.Schema(
    {
        routeId: { type: String, unique: true },
        name: { type: String, unique: true },
        idNoInicio: { type: String, unique: false },
        idNoFim: { type: String, unique: false },
        networkSegments: { type: Array },
    },
    {
        timestamps: true
    }
);
export default mongoose.model<IRoutePersistence & mongoose.Document>('Route', RouteSchema);