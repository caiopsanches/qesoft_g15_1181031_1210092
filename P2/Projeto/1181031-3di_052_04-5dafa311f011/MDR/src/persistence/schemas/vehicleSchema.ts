import { IVehiclePersistence } from '../../dataschema/IVehiclePersistence';
import mongoose from 'mongoose';

const VehicleSchema = new mongoose.Schema(
    {
        code: { type: String, unique: true, require: true },
        energy: { type: String, unique: false, require: true },
        autonomy: { type: Number, unique: false, require: true },
        kmCost: { type: Number, unique: false, require: true },
        avgConsumption: { type: Number, unique: false, require: true },
        avgSpeed: { type: Number, unique: false, require: true }
    },
    {
        timestamps: true
    }
);
export default mongoose.model<IVehiclePersistence & mongoose.Document>('vehicle', VehicleSchema);   