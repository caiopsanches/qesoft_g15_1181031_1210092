import { INodePersistence } from '../../dataschema/INodePersistence';
import mongoose from 'mongoose';

const NodeSchema = new mongoose.Schema(
    {
        shortName: {type: String, unique: true},
        nodeName: {type: String, unique: true},
        maxTimeStop: {type: Number},
        vehicleCapacity: {type: Number},
        latitude: {type: Number},
        longitude: {type: Number},
        isDepot: {type: Boolean},
        isReliefPoint: {type: Boolean}
    },
    {
        timestamps: true
    }
);

export default mongoose.model<INodePersistence & mongoose.Document>('Node', NodeSchema);
