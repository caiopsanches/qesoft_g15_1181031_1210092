import mongoose from 'mongoose';
import { ILinePathPersistence } from '../../dataschema/ILinePathPersistence';

const LinePathSchema = new mongoose.Schema(
    {
        routeId: { type: String, unique: true },
        orientation: { type: String },
    },
    {
        timestamps: true
    }
);
export default mongoose.model<ILinePathPersistence & mongoose.Document>('LinePath', LinePathSchema);