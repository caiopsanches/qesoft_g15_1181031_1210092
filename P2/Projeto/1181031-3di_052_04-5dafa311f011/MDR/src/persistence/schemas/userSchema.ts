import { IUserPersistence } from '../../dataschema/IUserPersistence';
import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema(
    {
        nome: { type: String, unique: false },
        username: { type: String, unique: true },
        password: { type: String, unique: false },
        email: { type: String, unique: true },
        nif: { type: Number, unique: true },
        endereco: { type: String, unique: false },
        role: { type: String, unique: false },
    },
    {
        timestamps: true
    }
);

export default mongoose.model<IUserPersistence & mongoose.Document>('Cliente', UserSchema);

