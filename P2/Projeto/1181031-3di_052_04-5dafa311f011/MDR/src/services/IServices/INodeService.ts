import { Result } from "../../core/logic/Result"
import INodeDTO from "../../dto/INodeDTO"

export default interface INodeService {
  createNode(nodeDTO: INodeDTO): Promise<Result<INodeDTO>>
  getAllNodes(order: string): Promise<Result<Array<INodeDTO>>>
  getNodeByShortName(filter: string, order: string): Promise<Result<Array<INodeDTO>>>
  getNodeByNodeName(filter: string, order: string): Promise<Result<Array<INodeDTO>>>
  deleteNode(shortName: string): Promise<Result<Boolean>>
}