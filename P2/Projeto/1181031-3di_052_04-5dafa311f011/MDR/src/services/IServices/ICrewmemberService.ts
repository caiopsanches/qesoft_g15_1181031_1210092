import { Result } from "../../core/logic/Result";
import ICrewmemberDTO from "../../dto/ICrewmemberDTO"

export default interface IVehicleService  {
  createCrewmember(crewmemberDTO: ICrewmemberDTO): Promise<Result<ICrewmemberDTO>>;
  deleteCrewmember(crewmemberId: string): Promise<Result<Boolean>>;
  //updateCrewmember(crewmemberDTO: ICrewmemberDTO): Promise<Result<ICrewmemberDTO>>;
  getAllCrewmembers(order: string): Promise<Result<Array<ICrewmemberDTO>>>
}