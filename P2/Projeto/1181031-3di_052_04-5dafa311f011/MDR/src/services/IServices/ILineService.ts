import { Result } from "../../core/logic/Result";
import ILineDTO from "../../dto/ILineDTO";

export default interface ILineService  {
  createLine(lineDTO: ILineDTO): Promise<Result<ILineDTO>>;
  getAllLines(order: string): Promise<Result<Array<ILineDTO>>>
  getLineByLineId(filter: string, order: string): Promise<Result<Array<ILineDTO>>>
  getLineByLineName(filter: string, order: string): Promise<Result<Array<ILineDTO>>>
  deleteLine(lineId: string): Promise<Result<Boolean>>;
}