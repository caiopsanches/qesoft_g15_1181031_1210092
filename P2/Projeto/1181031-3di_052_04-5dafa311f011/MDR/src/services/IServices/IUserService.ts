import { Result } from "../../core/logic/Result";
import IUserDTO from "../../dto/IUserDTO";
import { LoginDTO } from "../../dto/LoginDTO";


export default interface IUserService {
    criarCliente(userDTO: IUserDTO): Promise<Result<IUserDTO>>;
    getAllClientes(order: string): Promise<Result<Array<IUserDTO>>>
    deleteCliente(username: string): Promise<Result<Boolean>>
    comparePasswords(passIntroduzida: String, username: string): Promise<LoginDTO>
}