import { Result } from "../../core/logic/Result";
import IVehicleDTO from "../../dto/IVehicleDTO"


export default interface IVehicleService {
  deleteVehicle(vehicleId: string): Promise<Result<Boolean>>;
  createVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>>;
  getAllVehicles(order: string): Promise<Result<Array<IVehicleDTO>>>
  //updateVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>>;
}
