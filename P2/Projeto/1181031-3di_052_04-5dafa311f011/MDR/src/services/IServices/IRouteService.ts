import { Result } from "../../core/logic/Result";
import IRouteDTO from '../../dto/IRouteDTO';

export default interface IRouteService {
    createRoute(roleDTO: IRouteDTO): Promise<Result<IRouteDTO>>;
    deleteRoute(routeId: string): Promise<Result<Boolean>>;
    getAllRoutesbyLineId(filter: string): Promise<Result<Array<IRouteDTO>>>;
    getAllRoutes(): Promise<Result<Array<IRouteDTO>>>
    //updateRoute(roleDTO: IRouteDTO): Promise<Result<IRouteDTO>>;
}