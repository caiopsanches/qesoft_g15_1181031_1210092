import { Result } from "../../core/logic/Result";

export default interface IImportService {
    importFromXML(doc: Document): Promise<Result<Boolean>>

}
