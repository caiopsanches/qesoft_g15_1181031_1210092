import { Service, Inject } from 'typedi';
import config from "../config";
import ILineDTO from '../dto/ILineDTO';
import ILineService from './IServices/ILineService';
import { LineMap } from "../mappers/LineMap";
import ILineRepo from '../repos/IRepos/ILineRepo';
import IVehicleRepo from '../repos/IRepos/IVehicleRepo'
import { Result } from "../core/logic/Result";
import { Container } from 'typedi';
import { Line } from '../domain/line/Line';
import ICrewmemberRepo from '../repos/IRepos/ICrewmemberRepo';
import IRouteRepo from '../repos/IRepos/IRouteRepo';
import { VehicleCode } from '../domain/vehicle/vehicleCode';
import { CrewmemberCode } from '../domain/crewmember/crewmemberCode';
import { RouteId } from '../domain/Route/RouteId';
import { LineId } from '../domain/line/LineId';

@Service()
export default class LineService implements ILineService {
    constructor(
        @Inject(config.repos.vehicle.name) private vehicleRepo: ILineRepo
    ) { }

    public async createLine(lineDTO: ILineDTO): Promise<Result<ILineDTO>> {
        try {
            const ILineRepo = Container.get(config.repos.line.name) as
                ILineRepo;

            const lineResult = await LineMap.toDomain(lineDTO);

            const lineAux = await this.verifyLists(lineResult)

            await ILineRepo.save(lineAux);
            const ILineDTOResult = LineMap.toDTO(lineAux) as ILineDTO;
            return Result.ok<ILineDTO>(ILineDTOResult)

        } catch (e) {
            throw e;
        }
    }

    private async verifyLists(line: Line): Promise<Line> {
        const IVehicleRepo = Container.get(config.repos.vehicle.name) as
            IVehicleRepo;
        const ICrewmemberRepo = Container.get(config.repos.crewmember.name) as
            ICrewmemberRepo;
        const IRouteRepo = Container.get(config.repos.route.name) as
            IRouteRepo;
        
        var allowedVehiclesAux = new Array<VehicleCode>()
        for (let index = 0; index < line.allowedVehicles.length; index++) {

            if (await IVehicleRepo.exists(line.allowedVehicles[index])) {
                allowedVehiclesAux.push(line.allowedVehicles[index])
            }
        }
        line.allowedVehicles = allowedVehiclesAux

        var allowedDriversAux = new Array<CrewmemberCode>()
        for (let index = 0; index < line.allowedDrivers.length; index++) {

            if (await ICrewmemberRepo.exists(line.allowedDrivers[index])) {
                allowedDriversAux.push(line.allowedDrivers[index])
            }
        }
        line.allowedDrivers = allowedDriversAux

        //var pathsAux = new Array<RouteId>()
        //for (let index = 0; index < line.paths.length; index++) {
        //    if (await IRouteRepo.exists(line.paths[index])) {
        //        pathsAux.push(line.paths[index])
        //    }
        //}
        //line.paths = pathsAux

        return Promise.resolve(line)
    }

    public async getAllLines(order: string): Promise<Result<Array<ILineDTO>>> {
        const ILineRepo = Container.get(config.repos.line.name) as ILineRepo
        const result = await ILineRepo.findAll(order);

        var listLineDTO = new Array<ILineDTO>()
        await result.forEach(elem => listLineDTO.push(LineMap.toDTO(elem)))
        return Result.ok<Array<ILineDTO>>(listLineDTO)
    }

    public async getLineByLineId(filter: string, order: string): Promise<Result<Array<ILineDTO>>> {
        const ILineRepo = Container.get(config.repos.line.name) as ILineRepo
        const result = await ILineRepo.findByLineId(filter, order);

        var ret = new Array<ILineDTO>()
        await result.forEach(elem => ret.push(LineMap.toDTO(elem)))
        return Result.ok<Array<ILineDTO>>(ret)
    }

    public async getLineByLineName(filter: string, order: string): Promise<Result<Array<ILineDTO>>> {
        const ILineRepo = Container.get(config.repos.line.name) as ILineRepo
        const result = await ILineRepo.findByLineName(filter, order);

        var ret = new Array<ILineDTO>()
        await result.forEach(elem => ret.push(LineMap.toDTO(elem)))
        return Result.ok<Array<ILineDTO>>(ret)
    }

    public async deleteLine(lineId: string): Promise<Result<Boolean>> {
        try {
            const ILineRepo = Container.get(config.repos.line.name) as
            ILineRepo;
            let flag = await ILineRepo.delete(LineId.create(lineId).getValue());
            return Result.ok<Boolean>(flag)
        } catch (e) {
            throw e;
        }
    }
}
