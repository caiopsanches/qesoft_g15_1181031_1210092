import { Service, Inject } from 'typedi';
import config from "../config";
import IImportService from './IServices/IImportService';
import { Container } from 'typedi';
import IVehicleService from '../services/IServices/IVehicleService';
import ICrewmemberService from '../services/IServices/ICrewmemberService'
import INodeService from '../services/IServices/INodeService'
import ILineService from '../services/IServices/ILineService'
import IRouteService from './IServices/IRouteService';
import ISegmentDTO from '../dto/ISegementDTO'
import { Result } from '../core/logic/Result';
import { Console } from 'console';
import ILinePathDTO from '../dto/ILinePathDTO';
@Service()
export default class ImportService implements IImportService {

    public async importFromXML(doc: Document): Promise<Result<Boolean>> {

        try {

            let docConfig = doc.getElementsByTagName(config.import.parameter)

            var vehicles = doc.getElementsByTagName(config.import.vehicle.name);
            await this.processVehicle(vehicles, docConfig)

            var crewMembers = doc.getElementsByTagName(config.import.crewMember.name)
            await this.processCrewMember(crewMembers, docConfig)

            var nodes = doc.getElementsByTagName(config.import.node.name)
            await this.processNodes(nodes, docConfig)

            var lines = doc.getElementsByTagName(config.import.line.name)
            await this.processLines(lines, docConfig)

            var routes = doc.getElementsByTagName(config.import.routes.route.name)
            await this.processRoutes(routes, nodes)

        } catch {
            return Result.fail<Boolean>(false)
        }
        return Result.ok<Boolean>(true)
    }

    private async processRoutes(routes: any, nodes: any) {
        const IRouteServ = Container.get(config.services.route.name) as IRouteService

        for (const elem of routes) {
            var id = elem.getAttribute(config.import.key)
            let id1 = id.replace("Path:", '')
            var isEmpty = elem.getAttribute(config.import.routes.route.atr.isEmpty)
            var pathNodes = elem.getElementsByTagName(config.import.routes.route.atr.pathNode)
            let aDTO = new Array<ISegmentDTO>()

            let len = pathNodes.length
            let noInicio = pathNodes[0].getAttribute(config.import.routes.atr.node)
            let noFim = pathNodes[len - 1].getAttribute(config.import.routes.atr.node)

            for (var i = 0; i < pathNodes.length - 1; i++) {
                let no = pathNodes[i].getAttribute(config.import.routes.atr.node)
                let no1 = pathNodes[i + 1].getAttribute(config.import.routes.atr.node)
                for (const elem1 of nodes) {
                    if (elem1.getAttribute(config.import.key) === no) {
                        no = elem1.getAttribute(config.import.node.atr.shortName)
                    }
                    if (elem1.getAttribute(config.import.key) === no1) {
                        no1 = elem1.getAttribute(config.import.node.atr.shortName)
                    }
                }
                let distance = pathNodes[i + 1].getAttribute(config.import.routes.atr.distance)
                let duration = pathNodes[i + 1].getAttribute(config.import.routes.atr.duration)
                let auxSegmentId1 = pathNodes[i].getAttribute(config.import.key)
                let auxSegmentId2 = pathNodes[i + 1].getAttribute(config.import.key)
                auxSegmentId1 = auxSegmentId1.replace("PathNode:", '')
                auxSegmentId2 = auxSegmentId2.replace("PathNode:", '')
                let finalId = auxSegmentId1 + ":" + auxSegmentId2
                aDTO.push({
                    "segmentId": finalId,
                    "idNo1": no,
                    "idNo2": no1,
                    "distance": distance,
                    "deslocationTime": duration
                })
            }

            await IRouteServ.createRoute({
                "id": id1,
                "routeId": id1,
                "name": id1,
                "idNoInicio": noInicio,
                "idNoFim": noFim,
                "networkSegments": aDTO,
            })

            console.log("routes")
        }

    }
    private async processLines(lines: any, docConfig: any) {
        const ILineServ = Container.get(config.services.line.name) as ILineService
        for (const elem of lines) {
            var name = elem.getAttribute(config.import.line.atr.name)
            var id = elem.getAttribute(config.import.line.atr.key)
            var color = elem.getAttribute(config.import.line.atr.color)

            const aV = new Array<string>()
            const aC = new Array<string>()
            const aP = new Array<ILinePathDTO>()

            var allowedVehicles = elem.getElementsByTagName(config.import.line.atr.allowedVehicles.name)
            for (const elem1 of allowedVehicles) {
                if (elem1.hasAttribute("Code")) {
                    aV.push(elem1.getAttribute(config.import.line.atr.allowedVehicles.vehicleCode.atr.code))
                }
            }

            var allowedCrew = elem.getElementsByTagName(config.import.line.atr.allowedDrivers.name)

            for (const elem1 of allowedCrew) {
                if (elem1.hasAttribute("Code")) {
                    aC.push(elem1.getAttribute(config.import.line.atr.allowedDrivers.crewMemberCode.atr.code))
                }
            }
            var paths = elem.getElementsByTagName(config.import.line.atr.paths.pathId.name)

            for (const elem1 of paths) {
                let pathId = elem1.getAttribute(config.import.line.atr.paths.pathId.id)
                let id1 = pathId.replace("Path:", '')
                let orientation = elem1.getAttribute(config.import.line.atr.paths.pathId.orientation)
                aP.push({ routeId: id1, orientation: orientation })
            }
            await ILineServ.createLine({
                "id": id,
                "lineId": id,
                "name": name,
                "colour": color,
                "paths": aP,
                "allowedVehicles": aV,
                "allowedDrivers": aC
            })

        }
        console.log("lines")
        return true
    }
    private async processNodes(nodes: any, docConfig: any) {


        const INodeServ = Container.get(config.services.node.name) as INodeService
        for (const elem of nodes) {
            var shortName = elem.getAttribute(config.import.node.atr.shortName)
            var name = elem.getAttribute(config.import.node.atr.name)
            var lat = elem.getAttribute(config.import.node.atr.latitude)
            var lon = elem.getAttribute(config.import.node.atr.longitude)
            var dep = elem.getAttribute(config.import.node.atr.isDepot)
            var rel = elem.getAttribute(config.import.node.atr.isReliefPoint)
            if (dep === 'False') {
                dep = false
            } else if (dep === 'True') {
                dep = true
            }
            if (rel === 'False') {
                rel = false
            } else if (rel === 'True') {
                rel = true
            }

            await INodeServ.createNode({
                "shortName": shortName,
                "nodeName": name,
                "maxTimeStop": config.import.node.atr.time,
                "vehicleCapacity": parseInt(config.import.node.atr.capacity),
                "latitude": lat,
                "longitude": lon,
                "isDepot": dep,
                "isReliefPoint": rel
            })
        }

        console.log("nodes")

    }

    private async processVehicle(vehicles: any, docConfig: any) {
        const IVehicleServ = Container.get(config.services.vehicle.name) as
            IVehicleService;
        try {
            for (const elem of vehicles) {
                var code = elem.getAttribute(config.import.vehicle.atr.code)
                var autonomy = elem.getAttribute(config.import.vehicle.atr.autonomy)
                var par = elem.getAttribute(config.import.vehicle.atr.energy)
                for (const elem1 of docConfig) {
                    if (elem1.getAttribute(config.import.key) == config.import.parameter.concat(":", par)) {
                        var energy = elem1.getAttribute(config.import.title)
                    }
                }
                var avgConsumption = elem.getAttribute(config.import.vehicle.atr.avgConsumption)
                var avgSpeed = elem.getAttribute(config.import.vehicle.atr.avgSpeed)
                var cost = elem.getAttribute(config.import.vehicle.atr.cost)


                await IVehicleServ.createVehicle({
                    "id": code,
                    "code": code,
                    "energy": energy,
                    "autonomy": autonomy,
                    "kmCost": cost,
                    "avgConsumption": avgConsumption,
                    "avgSpeed": avgSpeed
                })
            }
        } catch {
            console.log("error")
            return false
        }
        console.log("veiculos")
        return true

    }

    private async processCrewMember(crewMembers: any, docConfig: any) {
        const ICrewmemberServ = Container.get(config.services.crewmember.name) as
            ICrewmemberService
        try {
            for (const elem of crewMembers) {
                var code = elem.getAttribute(config.import.crewMember.atr.code)
                var desc = elem.getAttribute(config.import.crewMember.atr.description)
                await ICrewmemberServ.createCrewmember({
                    "id": code,
                    "code": code,
                    "description": desc
                })
            }

        } catch {
            console.log("error")
            return false
        }
        console.log("membros")
        return true
    }
}



