import { Service, Inject } from 'typedi';
import config from "../config";
import ICrewmemberDTO from '../dto/ICrewmemberDTO';
import ICrewmemberRepo from '../repos/IRepos/ICrewmemberRepo';
import ICrewmemberService from './IServices/ICrewmemberService';
import { Result } from "../core/logic/Result";
import { CrewmemberMap } from "../mappers/CrewmemberMap";
import { Container } from 'typedi';
import { CrewmemberCode } from '../domain/crewmember/crewmemberCode';

@Service()
export default class CrewmemberService implements ICrewmemberService {
    constructor(
        @Inject(config.repos.crewmember.name) private crewmemberRepo: ICrewmemberRepo
    ) { }

    public async createCrewmember(crewmemberDTO: ICrewmemberDTO): Promise<Result<ICrewmemberDTO>> {
        try {
            const ICrewmemberRepo = Container.get(config.repos.crewmember.name) as
            ICrewmemberRepo;
            const crewmemberResult = await CrewmemberMap.toDomain(crewmemberDTO);
            await ICrewmemberRepo.save(crewmemberResult);

            const ICrewmemberDTOResult = CrewmemberMap.toDTO(crewmemberResult) as ICrewmemberDTO;
            return Result.ok<ICrewmemberDTO>(ICrewmemberDTOResult)
        } catch (e) {
            throw e;
        }
    }

    public async deleteCrewmember(crewmemberId: string): Promise<Result<Boolean>> {
        try {
            const ICrewmemberRepo = Container.get(config.repos.crewmember.name) as
            ICrewmemberRepo;
            let flag = await ICrewmemberRepo.delete(CrewmemberCode.create(crewmemberId).getValue());
            return Result.ok<Boolean>(flag)
        } catch (e) {
            throw e;
        }
    }

    public async getAllCrewmembers(order: string): Promise<Result<Array<ICrewmemberDTO>>> {
        const ICrewRepo = Container.get(config.repos.crewmember.name) as ICrewmemberRepo
        const result = await ICrewRepo.findAll(order);

        var listCrewDTO = new Array<ICrewmemberDTO>()
        await result.forEach(elem => listCrewDTO.push(CrewmemberMap.toDTO(elem)))
        return Result.ok<Array<ICrewmemberDTO>>(listCrewDTO)
    }
}