import { Service, Inject } from 'typedi';
import config from "../config";
import IVehicleDTO from '../dto/IVehicleDTO';
import IVehicleRepo from '../repos/IRepos/IVehicleRepo';
import IVehicleService from './IServices/IVehicleService';
import { Result } from "../core/logic/Result";
import { VehicleMap } from "../mappers/VehicleMap";
import { Container } from 'typedi';
import { VehicleCode } from '../domain/vehicle/vehicleCode';

@Service()
export default class VehicleService implements IVehicleService {
    constructor(
        @Inject(config.repos.vehicle) private vehicleRepo: IVehicleRepo
    ) { }



    public async createVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>> {
        try {
            const IVehicleRepo = Container.get(config.repos.vehicle.name) as
                IVehicleRepo;
            const vehicleResult = await VehicleMap.toDomain(vehicleDTO);
            await IVehicleRepo.save(vehicleResult);
            const IVehicleDTOResult = VehicleMap.toDTO(vehicleResult) as IVehicleDTO;
            return Result.ok<IVehicleDTO>(IVehicleDTOResult)
        } catch (e) {
            throw e;
        }
    }

    public async deleteVehicle(vehicleId: string): Promise<Result<Boolean>> {
        try {
            const IVehicleRepo = Container.get(config.repos.vehicle.name) as
                IVehicleRepo;
            let flag = await IVehicleRepo.delete(VehicleCode.create(vehicleId).getValue());
            return Result.ok<Boolean>(flag)
        } catch (e) {
            throw e;
        }
    }

    public async getAllVehicles(order: string): Promise<Result<Array<IVehicleDTO>>> {
        const IVehicleRepo = Container.get(config.repos.vehicle.name) as IVehicleRepo
        const result = await IVehicleRepo.findAll(order);

        var listVehicleDTO = new Array<IVehicleDTO>()
        await result.forEach(elem => listVehicleDTO.push(VehicleMap.toDTO(elem)))
        return Result.ok<Array<IVehicleDTO>>(listVehicleDTO)
    }
}

