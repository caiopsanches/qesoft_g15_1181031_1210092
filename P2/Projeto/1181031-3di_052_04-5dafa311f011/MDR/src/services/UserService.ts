import { Service, Inject } from 'typedi';
import config from "../config";
import { Container } from 'typedi';
import { User } from '../domain/user/User';
import IUserDTO from '../dto/IUserDTO';
import { UserMapper } from '../mappers/UserMapper';
import IUserService from './IServices/IUserService';
import { Result } from '../core/logic/Result';
import IUserRepo from '../repos/IRepos/IUserRepo';
import UserRepo from '../repos/UserRepo';
import ILoginDTO from '../dto/ILoginDTO';
import jwt from 'jsonwebtoken';
import { LoginDTO } from '../dto/LoginDTO';
import { Username } from '../domain/user/Username';
import bcrypt from 'bcryptjs';


@Service()
export default class UserService implements IUserService {
    constructor(
        @Inject(config.repos.user.name) private userRepo: IUserRepo
    ) { }


    public async criarCliente(userDTO: IUserDTO): Promise<Result<IUserDTO>> {
        try {
            const IUserRepo = Container.get(config.repos.user.name) as IUserRepo
            const userResult = await UserMapper.toDomain(userDTO)
            await IUserRepo.save(userResult);
            const IUserDTOResult = UserMapper.toDTO(userResult) as IUserDTO;
            return Result.ok<IUserDTO>(IUserDTOResult)
        } catch (e) {
            throw e;
        }
    }

    public async getAllClientes(): Promise<Result<Array<IUserDTO>>> {
        const IUserRepo = Container.get(config.repos.user.name) as IUserRepo
        const result = await IUserRepo.findAll();

        var listUserDTO = new Array<IUserDTO>()
        await result.forEach(elem => listUserDTO.push(UserMapper.toDTO(elem)))
        return Result.ok<Array<IUserDTO>>(listUserDTO)
    }


    public async getUserByUsername(username: string, order: string): Promise<Result<Array<IUserDTO>>> {
        const IUserRepo = Container.get(config.repos.user.name) as IUserRepo
        const result = await IUserRepo.findByUsername(username, order);

        var ret = new Array<IUserDTO>()

        await result.forEach(elem => ret.push(UserMapper.toDTO(elem)))

        return Result.ok<Array<IUserDTO>>(ret)
    }


    public async getUserByUsername1(username: string): Promise<IUserDTO> {
        const IUserRepo = Container.get(config.repos.user.name) as IUserRepo
        var clienteRetornado: User = await IUserRepo.findByUsername1(username);
        const clienteRetDTO: IUserDTO = UserMapper.toDTO(clienteRetornado)

        return clienteRetDTO;
    }



    public async comparePasswords(passIntroduzida: string, username: string): Promise<ILoginDTO> {
        const userDTO: IUserDTO = await this.getUserByUsername1(username);
        const match: boolean = await bcrypt.compare(passIntroduzida, userDTO.password);
        if (match) {
            const payload = { user: userDTO.username };
            const options = { expiresIn: '2d' };
            const secret = "fe1a1915a379f3be5394b64d14794932";
            const token = jwt.sign(payload, secret, options);
            const logDTO: ILoginDTO = new LoginDTO(token, userDTO);
            return logDTO;
        } else {
            throw new Error("Password inválida");
        }
    }


    public async deleteCliente(username: string): Promise<Result<Boolean>> {
    
        try {
            const IUserRepo = Container.get(config.repos.user.name) as
                IUserRepo;
            let flag = await IUserRepo.deleteCliente(Username.create(username).getValue());
          
            return Result.ok<Boolean>(flag)
        } catch (e) {
            throw e;
        }
    }

}
