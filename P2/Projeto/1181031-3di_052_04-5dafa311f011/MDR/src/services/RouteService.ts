import { Service, Inject } from 'typedi';
import IRouteService from "./IServices/IRouteService"
import IRouteRepo from "../repos/IRepos/IRouteRepo"
import ILineRepo from "../repos/IRepos/ILineRepo"
import { RouteMap } from "../mappers/RouteMap"
import IRouteDTO from "../dto/IRouteDTO"
import config from "../config";
import { Result } from "../core/logic/Result";
import { Container } from 'typedi';
import routeRoutes from '../routes/routeRoutes';
import { Route } from "../domain/Route/Route"
import e from 'express';
import INodeRepo from '../repos/IRepos/INodeRepo';
import { ShortName } from '../domain/network/valueObjects/shortName';
import { NetworkSegment } from '../domain/Route/NetworkSegment';
import { SegmentId } from '../domain/Route/SegmentId';
import nodeRoutes from '../routes/nodeRoutes';
import { Segments } from 'celebrate';
import { RouteId } from '../domain/Route/RouteId';


@Service()
export default class RouteService implements IRouteService {
    constructor(
        @Inject(config.repos.route.name) private routeRepo: IRouteRepo
    ) { }

    public async createRoute(routeDTO: IRouteDTO): Promise<Result<IRouteDTO>> {
        try {
            const IRouteRepo = Container.get(config.repos.route.name) as
                IRouteRepo;
            const routeResult = await RouteMap.toDomain(routeDTO);


            if (await routeResult.networkSegments.length > 0) {
                // verifica se o nodes existem 
                if (!await this.verifyNodes(routeResult)) {
                    console.log(routeResult);
                    throw ("Erro ao inserir nós. Estes não existem")
                }

                if (await routeResult.networkSegments.length > 1) {

                    // //verifica se o primeiro no do segmento é igual ao no de inicio do percurso
                    // if (!await this.CheckInicio(routeResult)) {
                    //     throw ("Erro! O primeiro nó de segmento tem de ser o nó de inicio do percurso")
                    // }

                    // //verifica se o ultimo no do segmento é igual ao no de termino do percurso
                    // if (!await this.CheckFim(routeResult)) {
                    //     throw ("Erro! O ultimo nó do segmento tem de ser o nó de termino do percurso")
                    // }


                    // verificar se o nó de Fim do segmento é o no inicial do outro Segmento
                    if (!await this.CheckId1Id2(routeResult)) {
                        console.log(routeResult);
                        throw ("Erro ao inserir segmentos. O nó de fim de segmento tem de ser o inicial do outro segmento");
                    }
                }
            }

            await IRouteRepo.save(routeResult);
            const IRouteDTOResult = RouteMap.toDTO(routeResult) as IRouteDTO;
            return Result.ok<IRouteDTO>(IRouteDTOResult)
        } catch (e) {
            throw e;
        }
    }

    public async deleteRoute(routeId: string): Promise<Result<Boolean>> {
        try {
            const IRouteRepo = Container.get(config.repos.route.name) as
                IRouteRepo;
            let flag = await IRouteRepo.delete(RouteId.create(routeId).getValue());
            return Result.ok<Boolean>(flag)
        } catch (e) {
            throw e;
        }
    }

    private async verifyNodes(route: Route) {
        const INodeRepo = Container.get(config.repos.node.name) as INodeRepo;
        var segments = route.networkSegments;

        for (let index = 0; index < segments.length; index++) {
            if (!await (INodeRepo.exists(segments[index].idNo1.value.valueOf()) && (INodeRepo.exists(segments[index].idNo2.value.valueOf())))) {
                return false;
            }
        }
        return true;
    }

    public async getAllRoutesbyLineId(filter: string): Promise<Result<Array<IRouteDTO>>> {
        const ILineRepo = Container.get(config.repos.line.name) as ILineRepo
        const IRouteRepo = Container.get(config.repos.route.name) as IRouteRepo
        const result = await ILineRepo.findOneByLineId(filter);
        var ret = new Array<IRouteDTO>()
        for (let elem of result.paths) {
            let a1 = await IRouteRepo.findByRouteId(elem.props.routeId.value)
            ret.push(RouteMap.toDTO(a1))
        }
        return Result.ok<Array<IRouteDTO>>(ret)
    }

    public async getAllRoutes(): Promise<Result<Array<IRouteDTO>>> {
        const IRouteRepo = Container.get(config.repos.route.name) as IRouteRepo
        const result = await IRouteRepo.findAll();
        var listRouteDTO = new Array<IRouteDTO>()
        await result.forEach(elem => listRouteDTO.push(RouteMap.toDTO(elem)))
        return Result.ok<Array<IRouteDTO>>(listRouteDTO)
    }



    // private async CheckInicio(route: Route) {

    //     if (await route.networkSegments[0].idNo1.value.toLowerCase().trim() == route.idNoInicio.value.toLowerCase().trim()) {
    //         return true;
    //     }
    //     return false;
    // }

    // private async CheckFim(route: Route) {
    //     let i = route.networkSegments.length;
    //     if (await route.networkSegments[i - 1].idNo2.value.toLowerCase().trim() == route.idNoFim.value.toLowerCase().trim()) {
    //         return true;
    //     }
    //     return false;
    // }



    private async CheckId1Id2(route: Route) {
        let segment = route.networkSegments;

        for (var i = 1; i < segment.length; i++) {
            if (!await (segment[i].idNo1.value.toLocaleLowerCase().trim() == segment[i - 1].idNo2.value.toLowerCase().trim() && segment[i].idNo1.value.toLowerCase().trim() != segment[i].idNo2.value.toLowerCase().trim())) {
                return false;
            }
        }
        return true;
    }




}