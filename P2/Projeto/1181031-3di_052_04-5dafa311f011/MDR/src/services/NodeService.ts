import { Service, Inject } from 'typedi';
import config from "../config";
import INodeDTO from '../dto/INodeDTO';
import INodeService from './IServices/INodeService';
import { NodeMap } from "../mappers/NodeMap";
import INodeRepo from '../repos/IRepos/INodeRepo';
import { Result } from "../core/logic/Result";
import { Container } from 'typedi';
import { ShortName } from '../domain/network/valueObjects/shortName';

@Service()
export default class NodeService implements INodeService {
    constructor(
        @Inject(config.repos.node.name) private nodeRepo: INodeRepo
    ) { }

    public async createNode(nodeDTO: INodeDTO): Promise<Result<INodeDTO>> {
        try {
            const INodeRepo = Container.get(config.repos.node.name) as INodeRepo
            const nodeResult = await NodeMap.toDomain(nodeDTO)
            await INodeRepo.save(nodeResult);
            const INodeDTOResult = NodeMap.toDTO(nodeResult) as INodeDTO;
            return Result.ok<INodeDTO>(INodeDTOResult)
        } catch (e) {
            throw e;
        }
    }

    public async getAllNodes(order: string): Promise<Result<Array<INodeDTO>>> {
        const INodeRepo = Container.get(config.repos.node.name) as INodeRepo
        const result = await INodeRepo.findAll(order);

        var listNodeDTO = new Array<INodeDTO>()
        await result.forEach(elem => listNodeDTO.push(NodeMap.toDTO(elem)))
        return Result.ok<Array<INodeDTO>>(listNodeDTO)
    }

    public async getNodeByShortName(filter: string, order: string): Promise<Result<Array<INodeDTO>>> {
        const INodeRepo = Container.get(config.repos.node.name) as INodeRepo
        const result = await INodeRepo.findByShortName(filter, order);

        var ret = new Array<INodeDTO>()
        
        await result.forEach(elem => ret.push(NodeMap.toDTO(elem)))
        
        return Result.ok<Array<INodeDTO>>(ret)
    }

    public async getNodeByNodeName(filter: string, order: string): Promise<Result<Array<INodeDTO>>> {
        const INodeRepo = Container.get(config.repos.node.name) as INodeRepo
        const result = await INodeRepo.findByName(filter, order);

        var ret = new Array<INodeDTO>()
        
        await result.forEach(elem => ret.push(NodeMap.toDTO(elem)))
        
        return Result.ok<Array<INodeDTO>>(ret)
    }

    public async deleteNode(shortName: string): Promise<Result<Boolean>> {
        try {
            const INodeRepo = Container.get(config.repos.node.name) as
            INodeRepo;
            let flag = await INodeRepo.deleteNode(ShortName.create(shortName).getValue());
            return Result.ok<Boolean>(flag)
        } catch (e) {
            throw e;
        }
    }
}
