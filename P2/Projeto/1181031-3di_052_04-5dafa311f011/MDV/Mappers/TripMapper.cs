using Microsoft.AspNetCore.Mvc;
using DDDSample1.Domain.Trips;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Mappers
{

    public class TripMapper
    {
        /*public async Task<TripDto> toDTO(Trip trip)
        {
            return new TripDto
            {
                Id = trip.Id.AsGuid(),
                LineId = trip.LineId.lineId,
                RouteId = trip.RouteId.routeId,
                IsEmpty = trip.IsEmpty,
                PassingTimes = Array.ConvertAll(trip.PassingTimes, x => new PassingTimeDto(x.Time.time, x.NodeId.nodeId, x.ReliefP)),
                Orinetation = trip.Orinetaiton
            };
        }*/
        
        public async Task<Trip> toDomain(TripDto dto)
        {
            return new Trip
            {
                Id = new TripId(Guid.NewGuid()),
                TripName = new TripName(dto.TripName),
                Orinetaiton = new Orientation(dto.Orinetation),
                LineId = new LineId(dto.LineId),
                RouteId = new RouteId(dto.RouteId),
                IsEmpty = false,
                PassingTimes = Array.ConvertAll(dto.PassingTimes, x => new PassingTime(new Time(x.Time), new NodeId(x.NodeId), x.ReliefP))
            };
        }
    }
}