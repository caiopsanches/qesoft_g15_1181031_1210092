using DDDSample1.Domain.CrewmemberDuties;
using System.Linq;

namespace DDDSample1.Mappers
{

    public class CrewmemberDutyMapper
    {
        private readonly WorkBlockMap WBmapper;

        public CrewmemberDutyMapper()
        {
            WBmapper = new WorkBlockMap();
        }

        public CrewmemberDutyDto toDTO(CrewmemberDuty cat)
        {
            var aux = cat.workblockList.ToList().ConvertAll(e => WBmapper.toDTO(e));
            return new CrewmemberDutyDto { Id = cat.Id.AsGuid(), Identifier = cat.Identifier.id, Workblocks = aux };
        }

        public CrewmemberDuty toDomain(CrewmemberDutyDto dto)
        {
            return new CrewmemberDuty
            {
                Identifier = new Identifier(dto.Identifier),
                workblockList = dto.Workblocks.ToList().ConvertAll(e => WBmapper.toDomain(e))
            };

        }
    }

}