using Microsoft.AspNetCore.Mvc;
using DDDSample1.Domain.Vehicle;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Mappers
{

    public class VehicleMapper
    {
        public async Task<Vehicle> toDomain(VehicleDto dto)
        {
            return new Vehicle
            {
                Id = new VehicleId(Guid.NewGuid()),
                VehicleTypeId = new VehicleTypeId(dto.VehicleTypeId),
                Matricula = new Matricula(dto.Matricula),
                Vin = new Vin(dto.Vin),
                ServiceDate = new ServiceDate(DateTime.Parse(dto.ServiceDate))
            };
        }

    }
}