using Microsoft.AspNetCore.Mvc;
using DDDSample1.Domain.WorkBlocks;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using DDDSample1.Domain.Shared;


namespace DDDSample1.Mappers
{

    public class WorkBlockMap
    {


        public WorkBlockDto toDTO(WorkBlock cat)
        {
            List<string> tripAux = new List<string>();
            if (cat.Trips != null)
            {
                tripAux = cat.Trips.ToList().ConvertAll<string>(aux => aux.TripId.AsString());
            }
            else
            {
                tripAux = null;
            }
            return new WorkBlockDto
            {
                Id = cat.Id.AsGuid(),
                name = cat.workblockName.name,
                StartTime = cat.StartTime.time,
                FinishTime = cat.FinishTime.time,
                FinishNode = cat.FinishNode.nodeId,
                BeginNode = cat.BeginNode.nodeId,
                Trips = tripAux
            };
        }

        public WorkBlock toDomain(WorkBlockDto dto)
        {
            return new WorkBlock
            {
                workblockName = new WorkblockName(dto.name),
                StartTime = new Time(dto.StartTime),
                FinishTime = new Time(dto.FinishTime),
                FinishNode = new NodeId(dto.FinishNode),
                BeginNode = new NodeId(dto.BeginNode)
            };

        }
    }

}