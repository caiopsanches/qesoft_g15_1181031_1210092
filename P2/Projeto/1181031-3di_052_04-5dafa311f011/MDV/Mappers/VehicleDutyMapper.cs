using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.VehicleDuties;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Mappers
{

    public class VehicleDutyMapper
    {
        public async Task<OutputVehicleDutyDTO> toDTO(VehicleDuty vehicleDuty, List<int> list)
        {

            return new OutputVehicleDutyDTO(vehicleDuty.Id.AsString(), vehicleDuty.description.description,
                             vehicleDuty.beginDutyDate.time, vehicleDuty.endDutyDate.time,
                              vehicleDuty.beginNode.beginNode, vehicleDuty.endNode.endNode, list);
        }

        public async Task<VehicleDuty> toDomain(InputVehicleDutyDTO dto, ICollection<Trip> tripList)
        {
           return new VehicleDuty(new Description(dto.description), new Time(dto.beginDutyDate),
                        new Time(dto.endDutyDate), new BeginNode(dto.beginNode), new EndNode(dto.endNode), tripList);

        }
    }
}
