using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Vehicle;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace MDV.Tests
{
    [TestClass]
    public class VehicleControllerTest
    {
        [TestMethod]
        [Fact]
        public async Task controller_create_true()
        {
            VehicleMDRDto v1 = new VehicleMDRDto("Autocarro", "Diesel", 50000, 10, 30, 30);
            
            VehicleMDRDto[] vehicleAux = new VehicleMDRDto[2];
            vehicleAux[0] = v1;
            vehicleAux[1] = v1;

            VehicleDto vehicle = new VehicleDto("Autocarro", "96-AS-63", "4Y1SL65848Z411439", "2021-01-12");

            InputVehicleDto input = new InputVehicleDto("Autocarro", "96-AS-63", "4Y1SL65848Z411439", "2021-01-12");

            var client = new HttpClient();

            var serviceMock = new Mock<IVehicleService>();
            serviceMock.Setup(serv => serv.GetVehicleAsync(input,client)).ReturnsAsync(v1);
            serviceMock.Setup(serv => serv.defineVehicle(input, v1)).ReturnsAsync(vehicle);
        
            Assert.AreEqual(vehicle, serviceMock.Object.defineVehicle(input, v1).Result);

        }
    }
}