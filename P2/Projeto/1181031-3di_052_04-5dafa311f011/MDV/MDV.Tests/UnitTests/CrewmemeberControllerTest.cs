using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DDDSample1.Domain.CrewmemberDuties;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;
using BadRequestResult = Microsoft.AspNetCore.Mvc.BadRequestResult;

namespace MDV.Tests
{
    [TestClass]
    public class CrewmemeberDutyControllerTest
    {
        [TestMethod]
        [Fact]
        public async Task controller_create_true()
        {

            var wb = new WorkBlock(new WorkblockName(1), new Time(400), new Time(500), new NodeId("N2"), new NodeId("N1"));
            var wb1 = new WorkBlock(new WorkblockName(1), new Time(500), new Time(1000), new NodeId("N1"), new NodeId("N2"));
             List<WorkBlock>  list = new List<WorkBlock>() ;
            list.Add(wb);
            list.Add(wb1) ;
            List<string> listIds = new List<string>();
            listIds.Add(wb.Id.AsString());
            listIds.Add(wb1.Id.AsString());
            CreatingCrewmemberDuty ccd = new CreatingCrewmemberDuty("12", listIds);

            var serviceMock = new Mock<ICrewmemberDutyService>();
            serviceMock.Setup(serv => serv.CreateCrewmemeber(ccd))
                  .ReturnsAsync(true);

            var controller = new CrewmemberDutyController(serviceMock.Object);

            var actionResult = await controller.Create(ccd);

            Assert.IsInstanceOfType(actionResult, typeof(OkResult));

           }
       }
}