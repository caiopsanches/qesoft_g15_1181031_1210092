using System;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Vehicle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MDV.Tests
{

    [TestClass]
    public class VehicleTest
    {
        [TestMethod]
        public void canExist()
        {
            var dv = new Vehicle(new VehicleTypeId("Autocarro"), new Matricula("23-AA-65"), new Vin("4Y1SL65848Z411439"), new ServiceDate(new DateTime(2021, 01, 12)));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
    "Matricula is required")]
        public void cantExistWithInvalidMatricula()
        {
            var dv = new Vehicle(new VehicleTypeId("Autocarro"), new Matricula("23-65-65"), new Vin("4Y1SL65848Z411439"), new ServiceDate(new DateTime(2021, 01, 12)));

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
    "Vin is not correct")]
        public void cantExistWithInvalidVin()
        {
            var dv = new Vehicle(new VehicleTypeId("Autocarro"), new Matricula("23-VF-65"), new Vin("4Y1SL65848Z411439123123"), new ServiceDate(new DateTime(2021, 01, 12)));

        }
    }
}

