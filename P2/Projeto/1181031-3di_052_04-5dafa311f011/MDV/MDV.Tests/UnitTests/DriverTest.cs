using System;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Drivers;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MDV.Tests
{
    [TestClass]
    public class DriverTest
    {

        [TestMethod]
        public void canExist()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990, 10, 12),
            new DateTime(2018, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "The Driver's Citizan Card is required.")]
        public void cantExistWithoutDriverCC()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(0, new DateTime(1990, 10, 12),
            new DateTime(2018, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "The Driver's Citizan Card is invalid [8].")]
        public void cantExistWithInvalidDriverCC()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(1234567, new DateTime(1990, 10, 12),
            new DateTime(2018, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's Date of Birth is invalid [at least 18 years old].")]
        public void cantExistWithInvalidDriverDate()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(2020,10,23),
            new DateTime(2018, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's Date of entry into the company is invalid [Company was created 5 years ago].")]
        public void cantExistWitInvalidDriverEntry()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2010, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's Mechanographic Number is required.")]
        public void cantExistWithoutDriverMecNumber()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            null, "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's Mechanographic Number is invalid [9].")]
        public void cantExistWithInvalidMecNumber()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abdc", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's Name is required.")]
        public void cantExistWithoutDriverName()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", null, 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's NIF is required.")]
        public void cantExistWithoutDriverNIF()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 0,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's NIF is invalid [9].")]
        public void cantExistWithInvalidDriverNIF()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 12345678,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's License Number is required.")]
        public void cantExistWithoutDriverLicenseNumber()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), null, auxL);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
           "Driver's License Number is invalid [P-[0-9]{7} [0-9]].")]
        public void cantExistWithInvalidDriverLicenseNumber()
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            var Wb = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 1234,
            new DateTime(2029, 10, 07), "P-12345678", auxL);

        }

    }
}