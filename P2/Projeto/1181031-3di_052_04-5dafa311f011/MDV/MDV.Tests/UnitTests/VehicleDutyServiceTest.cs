using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;
using System.Net.Http;
using DDDSample1.Domain.VehicleDuties;

using DDDSample1.Domain.Vehicle;

namespace MDV.Tests
{
    [TestClass]
    public class VehicleDutyServiceTest
    {
        [TestMethod]
        [Fact]
        public async Task serviceCreate()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));

            var list = new List<Trip>();
            list.Add(trip);

            var vehicleDuty = new VehicleDuty(new Description(123), new Time(200), new Time(300), new BeginNode("VVCAR"), new EndNode("VVCAR"), list);
            var list1 = new List<String>();
            list1.Add("idTrip1");
            list1.Add("idTrip2");

            InputVehicleDutyDTO dto = new InputVehicleDutyDTO("idTest", 123, 200,
                     300, "VVCAR", "VVCAR", list1);
            var client = new HttpClient();

            var vehicleDutyServiceMock = new Mock<IVehicleDutyService>();
            var vehicleServiceMock = new Mock<IVehicleService>();
            var repoMock = new Mock<IVehicleDutyRepository>();
            var vunit = new Mock<IUnitOfWork>();
            
            vehicleDutyServiceMock.Setup(service => service.CreateVehicleDutyAsync(dto)).ReturnsAsync(vehicleDuty);

            
            Assert.AreEqual(vehicleDuty, vehicleDutyServiceMock.Object.CreateVehicleDutyAsync(dto).Result);
        }
    }
}
