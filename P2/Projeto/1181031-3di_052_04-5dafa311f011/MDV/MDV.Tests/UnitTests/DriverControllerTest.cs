using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Drivers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace MDV.Tests
{
    [TestClass]
    public class DriverControllerTest
    {
        [TestMethod]
        [Fact]
        public async Task controller_create_true()
        {
            List<string> laux = new List<string>();
            laux.Add("23");
            CreatingDriverDto d1 = new CreatingDriverDto(12345678, "1990-10-23", "2019-04-21", "2022-10-11",
            "abcde1234", "Alberto", 123456789, "2029-10-07", "P-1234567 8", laux);
            

            List<DriverTypeId> auxL = new List<DriverTypeId>();
            auxL.Add(new DriverTypeId("23"));
            Driver driver1 = new Driver(12345678, new DateTime(1990,10,23),
            new DateTime(2019, 04, 21), new DateTime(2022, 10, 11),
            "abcde1234", "Alberto", 123456789,
            new DateTime(2029, 10, 07), "P-1234567 8", auxL);


            var serviceMock = new Mock<IDriverService>();
            serviceMock.Setup(serv => serv.CreateDriverAsync(d1)).ReturnsAsync(driver1);
            var controller = new DriverController(serviceMock.Object);
            var result = await controller.Create(d1);
            //Assert.IsInstanceOfType(result, typeof(OkObjectResult));

        
            Assert.AreEqual(driver1, serviceMock.Object.CreateDriverAsync(d1).Result);

        }
    }
}