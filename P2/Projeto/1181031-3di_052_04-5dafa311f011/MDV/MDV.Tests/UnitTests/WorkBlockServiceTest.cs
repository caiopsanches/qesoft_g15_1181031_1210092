using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace MDV.Tests
{
    [TestClass]
    public class WorkBlockServiceTest
    {
        [TestMethod]
        [Fact]
        public async Task service_add()
        {


            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));
            CreatingWorkBlockDto dto = new CreatingWorkBlockDto(1, 7200, "a");
            List<Trip> tripList = new List<Trip>();
            tripList.Add(trip);
            WorkBlock wb = new WorkBlock(new WorkblockName(1), new Time(500), new Time(1000), new NodeId("cete"), new NodeId("PARED"));
            WorkBlockMap mapper = new WorkBlockMap();

            List<WorkBlock> lista = new List<WorkBlock>();
            lista.Add(wb);
            var repoMock = new Mock<IWorkBlockRepository>();
            var tripServiceMock = new Mock<ITripService>();

            repoMock.Setup(workblockRepo => workblockRepo.GetAllAsync()).ReturnsAsync(lista);
                  repoMock.Setup(workblockRepo => workblockRepo.GetAllAsyncWithTrips()).ReturnsAsync(lista);

            var vunit = new Mock<IUnitOfWork>();
            tripServiceMock.Setup(tripService => tripService.getAllFromVehicleDuty(dto.vehicleduty))
                .ReturnsAsync(tripList);
            tripServiceMock.Setup(tripService => tripService.getById(trip.Id))
               .ReturnsAsync(trip);


            tripServiceMock.Setup(tripService => tripService.saveChanges())
                        .ReturnsAsync(15);

            var service = new WorkBlockService(vunit.Object, repoMock.Object, tripServiceMock.Object);
            var result = service.createWorkBlocks(dto);
            Assert.AreEqual(result.Result, dto.number);

        }


    }
}


