using System;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MDV.Tests
{
    [TestClass]
    public class TripTest
    {
        [TestMethod]
        public void createTrip()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessRuleValidationException),
   "Every Trip requires a line id.")]
        public void cantExistWithNullLineId()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), null, new RouteId("1"), false, arr, new Orientation("Go"));
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessRuleValidationException),
   "Every Trip requires a route id.")]
        public void cantExistWithNullRouteId()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), null, false, arr, new Orientation("Go"));
        }
    }
}