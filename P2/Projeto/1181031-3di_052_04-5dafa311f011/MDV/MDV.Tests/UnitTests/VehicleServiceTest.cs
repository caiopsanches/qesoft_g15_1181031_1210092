using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Vehicle;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace MDV.Tests
{
    [TestClass]
    public class VehicleServiceTest
    {
        [TestMethod]
        [Fact]
        public async Task service_add()
        {
            VehicleDto vehicle = new VehicleDto("Autocarro", "96-AS-63", "4Y1SL65848Z411439", "2021-01-12");
            InputVehicleDto input = new InputVehicleDto("Autocarro", "96-AS-63", "4Y1SL65848Z411439", "2021-01-12");
            VehicleMDRDto dtoAux = new VehicleMDRDto("Autocarro", "Diesel", 50000, 10, 30, 30);

            var vehicleServiceMock = new Mock<IVehicleService>();
            var repoMock = new Mock<IVehicleRepository>();
            var vunit = new Mock<IUnitOfWork>();

            vehicleServiceMock.Setup(vehicleService => vehicleService.defineVehicle(input, dtoAux)).ReturnsAsync(vehicle);
            var result = vehicleServiceMock.Object.defineVehicle(input, dtoAux);
            Assert.AreEqual(vehicle, result.Result);

        }
    }
}