using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;
using BadRequestResult = Microsoft.AspNetCore.Mvc.BadRequestResult;

namespace MDV.Tests
{
    [TestClass]
    public class WorkBlockControllerTest
    {
        [TestMethod]
        [Fact]
        public async Task controller_create_true()
        {

            WorkBlock wb = new WorkBlock(new WorkblockName(1), new Time(500), new Time(1000), new NodeId("cete"), new NodeId("PARED"));

            CreatingWorkBlockDto dto = new CreatingWorkBlockDto(2, 7200, "id");

            var serviceMock = new Mock<IWorkBlockService>();
            serviceMock.Setup(serv => serv.createWorkBlocks(dto))
                  .ReturnsAsync(2);

            var controller = new WorkBlockController(serviceMock.Object);

            var actionResult = await controller.Create(dto);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));

        }

        [TestMethod]
        [Fact]
        public async Task controller_create_false()
        {

            WorkBlock wb = new WorkBlock(new WorkblockName(5), new Time(500), new Time(1000), new NodeId("cete"), new NodeId("PARED"));

            CreatingWorkBlockDto dto = new CreatingWorkBlockDto(2, 7200, "id");

            var serviceMock = new Mock<IWorkBlockService>();
            serviceMock.Setup(serv => serv.createWorkBlocks(dto))
                  .ReturnsAsync(0);

            var controller = new WorkBlockController(serviceMock.Object);

            var actionResult = await controller.Create(dto);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));

        }

        [TestMethod]
        [Fact]
        public async Task controller_get_all()
        {

            //Arrange 
            WorkBlock wb = new WorkBlock(new WorkblockName(10), new Time(500), new Time(1000), new NodeId("CETE"), new NodeId("PARED"), null);
            WorkBlock wb1 = new WorkBlock(new WorkblockName(14), new Time(200), new Time(500), new NodeId("SOBRO"), new NodeId("ALMA"), null);
            List<WorkBlock> lista = new List<WorkBlock>();
            lista.Add(wb);
            lista.Add(wb1);
            List<WorkBlockDto> listDto = lista.ConvertAll<WorkBlockDto>(cat => new WorkBlockDto
            {
                Id = cat.Id.AsGuid(),
                StartTime = cat.StartTime.time,
                FinishTime = cat.FinishTime.time,
                FinishNode = cat.FinishNode.nodeId,
                BeginNode = cat.BeginNode.nodeId,
                Trips = null
            });


            List<WorkBlockDto> listaDTO = new List<WorkBlockDto>();
            var serviceMock = new Mock<IWorkBlockService>();
            serviceMock.Setup(serv => serv.GetAllAsync())
                  .ReturnsAsync(listDto);
            var repoMock = new Mock<IWorkBlockRepository>();
            repoMock.Setup(_ => _.GetAllAsyncWithTrips()).ReturnsAsync(lista);

            var controller = new WorkBlockController(serviceMock.Object);

            //Act
            var result = await controller.GetAll();
            var aux = result.Value;

            //Assert
            Assert.IsTrue(aux.ToList().All(isItem => listDto.Any(shouldItem => isItem == shouldItem)));



        }



    }
}


