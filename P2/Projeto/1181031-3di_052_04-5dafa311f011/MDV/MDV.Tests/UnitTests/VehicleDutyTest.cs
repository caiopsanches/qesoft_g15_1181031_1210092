using System;
using DDDSample1.Domain.Drivers;
using DDDSample1.Domain.Shared;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDSample1.Domain.VehicleDuties;
using DDDSample1.Domain.Vehicle;
using DDDSample1.Domain.Trips;

namespace MDV.Tests
{
    [TestClass]
    public class VehicleDutyTest
    {

        [TestMethod]
        public void canExist()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));

            var list = new List<Trip>();
            list.Add(trip);

            var Wb = new VehicleDuty(new Description(123), new Time(200), new Time(300), new BeginNode("VVCAR"), new EndNode("VVCAR"), list);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessRuleValidationException),
           "The begin date of the Vehicle Duty has to be before the end date.")]
        public void cantExistWithChangedDates()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));

            var list = new List<Trip>();
            list.Add(trip);

            var Wb = new VehicleDuty(new Description(123), new Time(300), new Time(200), new BeginNode("VVCAR"), new EndNode("VVCAR"), list);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessRuleValidationException),
           "The Vehicle Duty can not be longer the 16 hours.")]
        public void cantExistWithInvalidDriverDate()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));

            var list = new List<Trip>();
            list.Add(trip);

            var Wb = new VehicleDuty(new Description(123), new Time(0), new Time(90000), new BeginNode("VVCAR"), new EndNode("VVCAR"), list);
        }
    }
}