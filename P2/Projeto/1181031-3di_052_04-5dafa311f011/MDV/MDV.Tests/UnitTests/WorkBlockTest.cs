using System;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MDV.Tests
{
    [TestClass]
    public class WorkBlockTest
    {
        [TestMethod]
        [ExpectedException(typeof(BusinessRuleValidationException),
   "Every paramater is needed")]
        public void cantExistWithNullParam()
        {
            var Wb = new WorkBlock(new WorkblockName(1),new Time(123), new Time(500), null, new NodeId("N1"));

        }

        [TestMethod]
        public void canExist()
        {
            var Wb = new WorkBlock(new WorkblockName(1),new Time(123), new Time(500), new NodeId("N2"), new NodeId("N1"));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
   "Valid Time is required")]
        public void cantExistWithInvalidTime()
        {
            var Wb = new WorkBlock(new WorkblockName(1),new Time(-10), new Time(500), new NodeId("N2"), new NodeId("N1"));

        }

    }
}


