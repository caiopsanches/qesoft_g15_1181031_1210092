using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace MDV.Tests
{
    [TestClass]
    public class TripServiceTest
    {
        [TestMethod]
        [Fact]
        public async Task serviceCreate()
        {
            PassingTimeDto[] arr = new PassingTimeDto[2];
            PassingTimeDto p1 = new PassingTimeDto(123, "1", true);
            PassingTimeDto p2 = new PassingTimeDto(130, "2", true);
            arr[0] = p1;
            arr[1] = p2;
            TripDto trip = new TripDto(10,"line1", "1", false, arr, "Go");

            InputTripDto input = new InputTripDto(10,"line1", "1", 28800, 36000, 4, 1800, 0);

            SegmentDto[] networkSegments = new SegmentDto[1];
            SegmentDto s = new SegmentDto("seg1", "REC", "SOB", 200, 150);
            networkSegments[0] = s;
            RouteDto r1 = new RouteDto("q1", "r1", "route1", "REC", "SOB", networkSegments);
            RouteDto r2 = new RouteDto("q2", "r2", "route2", "SOB", "DEF", networkSegments);
            RouteDto[] routeAux = new RouteDto[2];
            routeAux[0] = r1;
            routeAux[1] = r2;

            NodeDto n1 = new NodeDto("recai", "REC", 12, 23, 12.12, 12.34, true, true);
            NodeDto n2 = new NodeDto("sobre", "SOB", 12, 23, 12.33, 22.34, false, false);
            NodeDto n3 = new NodeDto("defre", "DEF", 22, 33, 11.33, 22.34, false, true);
            NodeDto[] nodeAux = new NodeDto[3];
            nodeAux[0] = n1;
            nodeAux[1] = n2;
            nodeAux[2] = n3;

            PathDto p = new PathDto("r1", "Go");
            PathDto[] pAux = new PathDto[1];
            pAux[0] = p;

            VehicleDto v = new VehicleDto("v1");
            VehicleDto[] vAux = new VehicleDto[1];
            vAux[0] = v;

            DriverDto d = new DriverDto("d1");
            DriverDto[] dAux = new DriverDto[1];
            dAux[0] = d;

            LineDto l = new LineDto("1", "ln1", "line1", "blue", pAux, vAux, dAux);

            var tripServiceMock = new Mock<ITripService>();
            var repoMock = new Mock<ITripRepository>();
            var pMock = new Mock<IPassingTimeRepository>();
            var vunit = new Mock<IUnitOfWork>();

            /*tripServiceMock.Setup(tripService => tripService.ComputeTimes(18000, routeAux[0], nodeAux))
                        .ReturnsAsync(arr);

            tripServiceMock.Setup(tripService => tripService.AddAsync(trip))
                        .ReturnsAsync(trip);
                        */
            tripServiceMock.Setup(tripService => tripService.CreateTripsAsync(10,input, routeAux, nodeAux, l))
                        .ReturnsAsync(trip);

            var result = tripServiceMock.Object.CreateTripsAsync(10, input, routeAux, nodeAux, l);

            Assert.AreEqual(trip, result.Result);
        }

        [TestMethod]
        [Fact]
        public async Task service_get_all()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;

            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));

            List<Trip> tripList = new List<Trip>();
            tripList.Add(trip);

            var repoMock = new Mock<ITripRepository>();
            var passingRepoMock = new Mock<IPassingTimeRepository>();
            var vunit = new Mock<IUnitOfWork>();
            
            repoMock.Setup(repo => repo.GetAllTripsAsync())
                .ReturnsAsync(tripList);

            var service = new TripService(vunit.Object, repoMock.Object, passingRepoMock.Object);
            var result = service.GetTripsAsync();

            Assert.AreEqual(result.Result, tripList);
        }

        [TestMethod]
        [Fact]
        public async Task service_get_all_by_line()
        {
            PassingTime[] arr = new PassingTime[2];
            PassingTime p1 = new PassingTime(new Time(123), new NodeId("1"), true);
            PassingTime p2 = new PassingTime(new Time(130), new NodeId("2"), true);
            arr[0] = p1;
            arr[1] = p2;
            var trip = new Trip(new TripName(10), new LineId("line1"), new RouteId("1"), false, arr, new Orientation("Go"));

            string lineAux = "line1";

            List<Trip> tripList = new List<Trip>();
            tripList.Add(trip);

            var repoMock = new Mock<ITripRepository>();
            var passingRepoMock = new Mock<IPassingTimeRepository>();
            var vunit = new Mock<IUnitOfWork>();
            
            repoMock.Setup(repo => repo.GetAllTripsByLineAsync(lineAux))
                .ReturnsAsync(tripList);

            var service = new TripService(vunit.Object, repoMock.Object, passingRepoMock.Object);
            var result = service.GetTripsByLineAsync(lineAux);

            Assert.AreEqual(result.Result, tripList);
        }
    }
}
