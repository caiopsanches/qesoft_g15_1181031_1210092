using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using DDDSample1.Controllers;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;
using BadRequestResult = Microsoft.AspNetCore.Mvc.BadRequestResult;

namespace MDV.Tests
{
    [TestClass]
    public class TripIntegrationTest
    {
        [TestMethod]
        [Fact]
        public async Task integration_true()
        {
            PathDto p = new PathDto("3", "Go");
            PathDto[] pAux = new PathDto[1];
            pAux[0] = p;

            VehicleDto v = new VehicleDto("v1");
            VehicleDto[] vAux = new VehicleDto[1];
            vAux[0] = v;

            DriverDto d = new DriverDto("d1");
            DriverDto[] dAux = new DriverDto[1];
            dAux[0] = d;

            LineDto l = new LineDto("1", "Line:1", "line1", "blue", pAux, vAux, dAux);

            SegmentDto[] networkSegments = new SegmentDto[2];
            SegmentDto s1 = new SegmentDto("seg1", "REC", "SOB", 200, 150);
            SegmentDto s2 = new SegmentDto("seg2", "SOB", "DEF", 210, 150);
            networkSegments[0] = s1;
            networkSegments[1] = s2;
            RouteDto r1 = new RouteDto("q1", "3", "route1", "REC", "SOB", networkSegments);
            RouteDto r2 = new RouteDto("q2", "5", "route2", "SOB", "DEF", networkSegments);
            var trip1 = "Line:1";
            RouteDto[] routeAux = new RouteDto[2];
            routeAux[0] = r1;
            routeAux[1] = r2;

            NodeDto n1 = new NodeDto("recai", "REC", 12, 23, 12.12, 12.34, true, true);
            NodeDto n2 = new NodeDto("sobre", "SOB", 12, 23, 12.33, 22.34, false, false);
            NodeDto n3 = new NodeDto("defre", "DEF", 22, 33, 11.33, 22.34, false, true);
            NodeDto[] nodeAux = new NodeDto[3];
            nodeAux[0] = n1;
            nodeAux[1] = n2;
            nodeAux[2] = n3;

            PassingTimeDto[] arr = new PassingTimeDto[2];
            PassingTimeDto p1 = new PassingTimeDto(123, "1", true);
            PassingTimeDto p2 = new PassingTimeDto(130, "2", true);
            arr[0] = p1;
            arr[1] = p2;
            int number = 10;
            TripDto trip = new TripDto(number, "Line:1", "3", false, arr, "Go");


            InputTripDto input = new InputTripDto(number, "Line:1", "3", 28800, 36000, 4, 1800, 0);
            var client = new HttpClient();

            var serviceMock = new Mock<ITripService>();
            serviceMock.Setup(serv => serv.GetLineAsync(input, client))
                  .ReturnsAsync(l);
            serviceMock.Setup(serv => serv.GetRouteAsync(input, client))
                  .ReturnsAsync(routeAux);
            serviceMock.Setup(serv => serv.GetRouteInLine(routeAux, input, l))
                  .ReturnsAsync(routeAux);
            serviceMock.Setup(serv => serv.GetNodesAsync(input, client, routeAux[0]))
                .ReturnsAsync(nodeAux);
            serviceMock.Setup(serv => serv.CreateTripsAsync(number, input, routeAux, nodeAux, l))
                  .ReturnsAsync(trip);

            var repoMock = new Mock<ITripRepository>();
            var pMock = new Mock<IPassingTimeRepository>();
            var vunit = new Mock<IUnitOfWork>();

            /*tripServiceMock.Setup(tripService => tripService.ComputeTimes(18000, routeAux[0], nodeAux))
                        .ReturnsAsync(arr);

            tripServiceMock.Setup(tripService => tripService.AddAsync(trip))
                        .ReturnsAsync(trip);
                        */

            Assert.AreEqual(trip, serviceMock.Object.CreateTripsAsync(number, input, routeAux, nodeAux, l).Result);
        }

    }
}