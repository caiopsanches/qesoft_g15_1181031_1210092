using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Trips;

namespace DDDSample1.Infrastructure.Trips
{
    internal class PassingTimeEntityTypeConfiguration : IEntityTypeConfiguration<PassingTime>
    {
        public void Configure(EntityTypeBuilder<PassingTime> builder)
        {
            builder.ToTable("PassingTimes", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x => x.NodeId);
            builder.OwnsOne(x => x.Time);
        }
    }
}