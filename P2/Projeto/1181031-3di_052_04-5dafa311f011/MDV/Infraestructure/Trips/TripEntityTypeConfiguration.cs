using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Trips;

namespace DDDSample1.Infrastructure.Trips
{
    internal class TripEntityTypeConfiguration : IEntityTypeConfiguration<Trip>
    {
        public void Configure(EntityTypeBuilder<Trip> builder)
        {
            builder.ToTable("Trips", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x =>x.TripName);
            builder.OwnsOne(x => x.RouteId);
            builder.OwnsOne(x => x.LineId);
            builder.OwnsOne(x => x.Orinetaiton);
            builder.HasMany<PassingTime>(x => x.PassingTimes);
        }
    }
}