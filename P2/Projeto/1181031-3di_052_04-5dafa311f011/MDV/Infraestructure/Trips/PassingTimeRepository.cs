using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Trips;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.Trips
{
    public class PassingTimeRepository : BaseRepository<PassingTime, PassingTimeId>, IPassingTimeRepository
    {
        private readonly DDDSample1DbContext _context;
        public PassingTimeRepository(DDDSample1DbContext context) : base(context.PassingTimes)
        {
            _context = context;
        }

        public async Task<List<PassingTime>> getPassingByTripId(string id)
        {

            return await _context.PassingTimes.FromSqlRaw("SELECT * FROM [dbo].[PassingTimes] AS st WHERE st.TripId = '" + id + "'").AsNoTracking().ToListAsync();
          


        }
    }
}