using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Trips;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Infrastructure.Trips
{
    public class TripRepository : BaseRepository<Trip, TripId>, ITripRepository
    {
        private readonly DDDSample1DbContext _context;
        public TripRepository(DDDSample1DbContext context) : base(context.Trips)
        {
            this._context = context;
        }

        public async Task<List<Trip>> GetAllTripsAsync()
        {
            return await this._context.Trips.FromSqlRaw("SELECT t.* FROM [dbo].[Trips] t").Include(w => w.PassingTimes).ToListAsync();
        }

        public async Task<List<Trip>> GetAllTripsByLineAsync(string lineId)
        {
            return await this._context.Trips.Where(f => f.LineId.lineId == lineId).Include(w => w.PassingTimes).ToListAsync();
        }

        public async Task<List<Trip>> GetAllWithoutWBAsync()
        {
            return await this._context.Trips.FromSqlRaw("SELECT t.* FROM [dbo].[Trips] t JOIN [dbo].[PassingTimes] pt on t.Id = pt.TripId WHERE t.Id NOT IN (SELECT TripId FROM[dbo].[WorkTrip]) and t.LineId_lineId = 'Line:1' ORDER BY t.LineId_lineId, pt.Time_time OFFSET 0 ROWS ").Include(w => w.PassingTimes).AsNoTracking().ToListAsync();
        }

        public async Task<int> CommitAsync()
        {
            return await this._context.SaveChangesAsync();
        }

        public async Task<List<Trip>> getByVehicleDuty(string vehicleduty)
        {
            return await this._context.Trips.FromSqlRaw("select * from dbo.Trips t where t.VehicleDutyId = '" + vehicleduty + "'").Include(w => w.PassingTimes).ToListAsync();
        }

        public async Task<List<Trip>> getByWorkblockName(int workblock)
        {
            return await this._context.Trips.FromSqlRaw("select t.* from trips t, worktrip wt, workblocks wb where t.Id= wt.TripId and wt.WorkblockId = wb.Id and wb.workblockName_name=" + workblock).Include(w => w.PassingTimes).ToListAsync();
        }

        public async Task<Trip> getByName(int name)
        {
            return await this._context.Trips.Where(w => w.TripName.name == name).Include(w => w.workblockTrips).Include(w => w.PassingTimes).FirstAsync();
        }
    }
}