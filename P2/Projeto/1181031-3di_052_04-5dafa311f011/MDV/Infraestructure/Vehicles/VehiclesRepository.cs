using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Vehicle;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.Vehicles
{
    public class VehicleRepository : BaseRepository<Vehicle, VehicleId>, IVehicleRepository
    {
        private readonly DDDSample1DbContext _context;
        public VehicleRepository(DDDSample1DbContext context) : base(context.Vehicles)
        {
            this._context = context;
        }


        public async Task<List<Vehicle>> GetAllVehiclesAsync()
        {
            return await this._context.Vehicles.FromSqlRaw("SELECT t.* FROM [dbo].[Vehicles] t").ToListAsync();
        }

        public async Task<int> CommitAsync()
        {
            return await this._context.SaveChangesAsync();
        }
    }
}