using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Vehicle;

namespace DDDSample1.Infrastructure.Vehicles
{
    internal class VehicleEntityTypeConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable("Vehicles", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x => x.VehicleTypeId);
            builder.OwnsOne(x => x.Matricula, p =>
            {
                p.HasIndex(m => m.matricula).IsUnique();
            });
            builder.OwnsOne(x => x.Vin, p =>
            {
                p.HasIndex(m => m.vin).IsUnique();
            });
            builder.OwnsOne(x => x.ServiceDate);
        }
    }
}