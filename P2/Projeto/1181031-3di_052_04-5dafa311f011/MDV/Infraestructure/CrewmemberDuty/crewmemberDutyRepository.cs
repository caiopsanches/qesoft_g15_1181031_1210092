using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.CrewmemberDuties;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.CrewmemberDuties
{
    public class CrewmemberDutyRepository : BaseRepository<CrewmemberDuty, CrewmemberDutyId>, ICrewmemberDutyRepository
    {
        private readonly DbSet<CrewmemberDuty> _objs;
        public CrewmemberDutyRepository(DDDSample1DbContext context) : base(context.CrewmemberDuties)
        {
            this._objs = context.CrewmemberDuties;
        }
        public async Task<List<CrewmemberDuty>> GetAllWithWBs()
        {
            return await this._objs.FromSqlRaw("SELECT * FROM CrewmemberDuty").Include(w => w.workblockList).ToListAsync();
        }
    }
}