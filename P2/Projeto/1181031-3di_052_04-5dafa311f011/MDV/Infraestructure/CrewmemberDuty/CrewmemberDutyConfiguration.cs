using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.CrewmemberDuties;

namespace DDDSample1.Infrastructure.CrewmemberDuties
{

    internal class CrewmemberDutyEntityTypeConfiguration: IEntityTypeConfiguration<CrewmemberDuty> {

        public void Configure(EntityTypeBuilder<CrewmemberDuty> builder) {
            builder.ToTable("CrewmemberDuty", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x => x.Identifier, p =>
            {
                p.HasIndex(m => m.id).IsUnique();
            });
            builder.HasMany<WorkBlock>(x => x.workblockList);
        }
    }
}