using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.VehicleDuties;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.VehicleDuties
{
    public class VehicleDutyRepository : BaseRepository<VehicleDuty, VehicleDutyId>, IVehicleDutyRepository
    {
        private readonly DbSet<VehicleDuty> _objs;
        public VehicleDutyRepository(DDDSample1DbContext context) : base(context.VehicleDuties)
        {
            this._objs = context.VehicleDuties;
        }

        public async Task<List<VehicleDuty>> GetAllAsyncWithVehicleDuties()
        {
            return await this._objs.FromSqlRaw("SELECT * FROM VehicleDuty").Include(w => w.tripList).ThenInclude(w => w.PassingTimes).ToListAsync();
        }
        public async Task<VehicleDuty> getVehicleDutyByWorkblockId(string id)
        {
            return await this._objs.FromSqlRaw("select DISTINCT  v.* from VehicleDuty v, Trips t , WorkBlocks wb , WorkTrip wt where v.Id = t.VehicleDutyId and t.Id = wt.TripId and wt.WorkblockId='" + id + "'").FirstAsync();
        }

    }
}