using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.VehicleDuties;
using DDDSample1.Domain.Vehicle;
using DDDSample1.Domain.Trips;

namespace DDDSample1.Infrastructure.VehicleDuties {

    internal class VehicleDutyEntityTypeConfiguration: IEntityTypeConfiguration<VehicleDuty> {

        public void Configure(EntityTypeBuilder<VehicleDuty> builder) {
            builder.ToTable("VehicleDuty", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x => x.description);
            builder.OwnsOne(x => x.beginDutyDate);
            builder.OwnsOne(x => x.endDutyDate);
            builder.OwnsOne(x => x.beginNode);
            builder.OwnsOne(x => x.endNode);
            builder.HasMany<Trip>(x => x.tripList);
        }
    }
}