using Microsoft.EntityFrameworkCore;
using DDDSample1.Domain.Categories;
using DDDSample1.Domain.Products;
using DDDSample1.Domain.Families;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Trips;
using DDDSample1.Infrastructure.Categories;
using DDDSample1.Infrastructure.Products;
using DDDSample1.Infrastructure.WorkBlocks;
using DDDSample1.Infrastructure.Trips;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Vehicle;
using DDDSample1.Infrastructure.Vehicles;
using DDDSample1.Domain.Drivers;
using DDDSample1.Infrastructure.Drivers;
using DDDSample1.Domain.VehicleDuties;
using DDDSample1.Infrastructure.VehicleDuties;
using DDDSample1.Infrastructure.CrewmemberDuties;
using DDDSample1.Domain.CrewmemberDuties;

namespace DDDSample1.Infrastructure
{
    public class DDDSample1DbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Family> Families { get; set; }

        public DbSet<WorkBlock> WorkBlocks { get; set; }

        public DbSet<PassingTime> PassingTimes { get; set; }

        public DbSet<Trip> Trips { get; set; }

        public DbSet<WorkTrip> WorkTrip { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<Driver> Drivers { get; set; }

        public DbSet<VehicleDuty> VehicleDuties { get; set; }

        public DbSet<DriverLicense> DriverLicense { get; set; }

        public DbSet<CrewmemberDuty> CrewmemberDuties { get; set; }

        public DDDSample1DbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FamilyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WorkBlockEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TripEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PassingTimeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleDutyEntityTypeConfiguration());
             modelBuilder.ApplyConfiguration(new CrewmemberDutyEntityTypeConfiguration());
            modelBuilder.Entity<WorkTrip>().HasKey(sc => new { sc.WorkblockId, sc.TripId });

        }
    }
}