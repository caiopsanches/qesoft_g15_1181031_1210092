using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Trips;
namespace DDDSample1.Infrastructure.WorkBlocks
{
    internal class WorkBlockEntityTypeConfiguration: IEntityTypeConfiguration<WorkBlock>
    {
        public void Configure(EntityTypeBuilder<WorkBlock> builder)
        {
            builder.ToTable("WorkBlocks", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x => x.workblockName);
            builder.OwnsOne(x => x.BeginNode);
            builder.OwnsOne(x => x.FinishNode);
            builder.OwnsOne(x => x.StartTime);
            builder.OwnsOne(x => x.FinishTime);

        }
    }
}