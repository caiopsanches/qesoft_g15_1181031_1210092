using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.WorkBlocks
{
    public class WorkBlockRepository : BaseRepository<WorkBlock, WorkBlockId>, IWorkBlockRepository
    {
        private readonly DbSet<WorkBlock> _objs;
        public WorkBlockRepository(DDDSample1DbContext context): base(context.WorkBlocks)
        {
            this._objs = context.WorkBlocks;
        }


        public async Task<List<WorkBlock>> GetAllAsyncWithTrips()
        {
            return await this._objs.FromSqlRaw("SELECT * FROM WorkBlocks").Include(w => w.Trips).ToListAsync();
        }

        public async Task<WorkBlock> getByName(int name)
        {
            return await this._objs.Where(x => x.workblockName.name==name).Include(w => w.Trips).FirstAsync();
        }
    }
}