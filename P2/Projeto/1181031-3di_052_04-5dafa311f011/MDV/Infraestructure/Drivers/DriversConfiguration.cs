using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Drivers;

namespace DDDSample1.Infrastructure.Drivers
{
    internal class DriverEntityTypeConfiguration : IEntityTypeConfiguration<Driver>
    {
        public void Configure(EntityTypeBuilder<Driver> builder)
        {
            builder.ToTable("Drivers", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.OwnsOne(x => x.DriverCC,p => {
                p.HasIndex(m=> m.driverCC).IsUnique();
            });
            builder.OwnsOne(x => x.DriverDate);
            builder.OwnsOne(x => x.DriverEntry);
            builder.OwnsOne(x => x.DriverExit);
            builder.OwnsOne(x => x.DriverMecNumber,p => {
                p.HasIndex(m=> m.driverMecNumber).IsUnique();
            });
            builder.OwnsOne(x => x.DriverName);
            builder.OwnsOne(x => x.DriverNIF,p => {
                p.HasIndex(m=> m.driverNIF).IsUnique();
            });
            builder.OwnsOne<DriverLicense>(t => t.DriverLicense, a =>
            {
                a.HasKey(a => a.Id);
                a.OwnsOne(a => a.DriverLicenseExpiry);
                a.OwnsOne(a => a.DriverLicenseNumber);
            });
            builder.OwnsMany<DriverTypeId>(x => x.DriverType);
        }
    }
}