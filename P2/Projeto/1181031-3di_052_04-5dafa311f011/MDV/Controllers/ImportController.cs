using Microsoft.AspNetCore.Mvc;
using DDDSample1.Domain.WorkBlocks;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System;
using System.Xml;
using Microsoft.AspNetCore.Cors;
using System.Net;
using DDDSample1.Domain.Import;
using System.Text.RegularExpressions;

namespace DDDSample1.Import
{

    [Route("mdv/[controller]")]
    [ApiController]
    public class ImportController : ControllerBase
    {
        private readonly ImportService _service;

        public ImportController(ImportService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> RawBinaryDataManual()
        {

            using (var ms = new MemoryStream(2048))
            {
                await Request.Body.CopyToAsync(ms);

                string aux = Encoding.UTF8.GetString(ms.ToArray());

                XmlDocument xmlDoc = new XmlDocument();
                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
        
                string aux1 = aux.Substring(aux.IndexOf('<') - 1);
                if (aux1.StartsWith(_byteOrderMarkUtf8))
                {
                   aux1 = aux1.Remove(0, _byteOrderMarkUtf8.Length);
                }
                string aux2 = aux1.Substring(0, aux1.LastIndexOf(Environment.NewLine));


                string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string res = "";
                int numLines = aux2.Split('\n').Length;
                using (StringReader reader = new StringReader(aux2))
                {
                    int count = 1;
                    string line;
                    while ((line = reader.ReadLine()) != null && count < numLines)
                    {
                        count++;
                        res += line;
 }
                }

              
                xmlDoc.LoadXml(res);
                await _service.AddAsync(xmlDoc);

                return Ok();


            }
            
        }

    
    }
}