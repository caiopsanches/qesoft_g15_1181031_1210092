using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.VehicleDuties;
using System.Net.Http;


namespace DDDSample1.Controllers
{
    [Route("mdv/[controller]")]
    [ApiController]
    public class VehicleDutyController : ControllerBase
    {
        private readonly IVehicleDutyService _service;
        private static readonly HttpClient client = new HttpClient();

        public VehicleDutyController(IVehicleDutyService service)
        {
            _service = service;
        }

        // GET: mdv/VehicleDuty
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OutputVehicleDutyDTO>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // POST: mdv/VehicleDuty
        [HttpPost]
        public async Task<ActionResult<VehicleDuty>> Create(InputVehicleDutyDTO dto) {
            return await _service.CreateVehicleDutyAsync(dto);
        }
    }
}