using Microsoft.AspNetCore.Mvc;
using DDDSample1.Domain.WorkBlocks;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace DDDSample1.Controllers
{
    [Route("mdv/[controller]")]
    [ApiController]
    public class WorkBlockController : ControllerBase
    {
        private readonly IWorkBlockService _service;

        public WorkBlockController(IWorkBlockService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreatingWorkBlockDto dto)
        {
            var cat = await _service.createWorkBlocks(dto);
            if (cat > 0)
            {
                return Ok(cat);
            } else {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkBlockDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }


    }
}
