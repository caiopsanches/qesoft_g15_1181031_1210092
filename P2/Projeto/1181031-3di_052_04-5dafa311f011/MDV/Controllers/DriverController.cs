using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Drivers;
using System.Net.Http;
using Newtonsoft.Json;
using System;

namespace DDDSample1.Controllers
{
    [Route("mdv/[controller]")]
    [ApiController]

    public class DriverController : ControllerBase
    {

        private readonly IDriverService _service;

        private static readonly HttpClient client = new HttpClient();

        public DriverController(IDriverService service)
        {
            _service = service;
        }

        // GET: api/Driver
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DriverDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }


        // POST: api/Driver
        [HttpPost]
        public async Task<ActionResult<Driver>> Create(CreatingDriverDto dto)
        {

            Driver res = null;
            //var client = new HttpClient();

            //List<DriverTypeMDRDto> driverAux = await _service.GetDriverTypeAsync(dto, client);

            res = await _service.CreateDriverAsync(dto);

            return res;
        }

    }
}