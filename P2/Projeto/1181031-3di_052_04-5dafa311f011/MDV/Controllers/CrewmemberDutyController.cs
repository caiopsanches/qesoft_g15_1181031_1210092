using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.CrewmemberDuties;

namespace DDDSample1.Controllers
{
    [Route("mdv/[controller]")]
    [ApiController]
    public class CrewmemberDutyController : ControllerBase
    {
        private readonly ICrewmemberDutyService _service;

        public CrewmemberDutyController(ICrewmemberDutyService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrewmemberDutyDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }


        [HttpPost]
        public async Task<ActionResult> Create(CreatingCrewmemberDuty dto)
        {
            var cat = await _service.CreateCrewmemeber(dto);
            if (cat)
                return Ok();

            return BadRequest();
        }


    }
}