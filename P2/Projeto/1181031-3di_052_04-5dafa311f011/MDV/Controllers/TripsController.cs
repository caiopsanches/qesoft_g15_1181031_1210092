using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Trips;
using System.Net.Http;


namespace DDDSample1.Controllers
{
    [Route("mdv/[controller]")]
    [ApiController]
    public class TripsController : ControllerBase
    {
        private readonly ITripService _service;
        private static readonly HttpClient client = new HttpClient();

        public TripsController(ITripService service)
        {
            _service = service;
        }

        // GET: api/Trips
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Trip>>> GetAll()
        {
            return await _service.GetTripsAsync();
        }

        [HttpGet("{lineId}")]
        public async Task<ActionResult<IEnumerable<Trip>>> GetAllByLine(string lineId)
        {
            return await _service.GetTripsByLineAsync(lineId);
        }

        // POST: api/Trips
        [HttpPost]
        public async Task<ActionResult<TripDto>> Create(InputTripDto dto)
        {
            TripDto res = null;
            var client = new HttpClient();
            var ind = 0;
            //GET TRIPS
            List<Trip> trips = await _service.GetTripsAsync();
            ind = trips.Count;
            //GET LINES
            LineDto lineAux = await _service.GetLineAsync(dto, client);
            //GET ROUTES
            RouteDto[] routes = await _service.GetRouteAsync(dto, client);
            //GET ROUTES DA LINHA
            RouteDto[] routeAux = await _service.GetRouteInLine(routes, dto, lineAux);
            //GET NOS PERCURSO
            NodeDto[] nodesAux = await _service.GetNodesAsync(dto, client, routeAux[0]);

            if (dto.NrTripsParalel > 0)
            {
                for (int i = 0; i < dto.NrTripsParalel; i++)
                {
                    res = await _service.CreateTripsAsync(ind, dto, routeAux, nodesAux, lineAux);
                }
            }
            else
            {
                res = await _service.CreateTripsAsync(ind, dto, routeAux, nodesAux, lineAux);
            }
            return res;
        }
        //catch (BusinessRuleValidationException ex)
        //{
        //    return BadRequest(new { Message = ex.Message });
        //}
    }
}