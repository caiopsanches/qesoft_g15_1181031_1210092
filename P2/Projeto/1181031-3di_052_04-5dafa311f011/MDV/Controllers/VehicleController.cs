using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Vehicle;
using System.Net.Http;
using Newtonsoft.Json;
using System;

namespace DDDSample1.Controllers
{
    [Route("mdv/[controller]")]
    [ApiController]

    public class VehicleController : ControllerBase
    {

        private readonly IVehicleService _service;

        private static readonly HttpClient client = new HttpClient();

        public VehicleController(IVehicleService service)
        {
            _service = service;
        }

        // GET: api/Vehicle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vehicle>>> GetAll()
        {
            return await _service.GetVehiclesAsync();
        }


        // POST: api/Vehicle
        [HttpPost]
        public async Task<ActionResult<VehicleDto>> Create(InputVehicleDto dto)
        {

            VehicleDto res = null;
            var client = new HttpClient();
            //GET Vehicle Type
            VehicleMDRDto vehicleAux = await _service.GetVehicleAsync(dto, client);
            res = await _service.defineVehicle(dto, vehicleAux);

            return res;
        }
    }
}