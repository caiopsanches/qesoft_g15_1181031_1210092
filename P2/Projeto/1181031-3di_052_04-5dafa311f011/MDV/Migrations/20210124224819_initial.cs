﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Categories",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active1 = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CrewmemberDuty",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Identifier_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CrewmemberDuty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Drivers",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DriverCC_driverCC = table.Column<long>(type: "bigint", nullable: true),
                    DriverDate_driverDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DriverEntry_driverEntry = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DriverExit_driverExit = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DriverMecNumber_driverMecNumber = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    DriverName_driverName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DriverNIF_driverNIF = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Families",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active1 = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Families", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleDuty",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    description_description = table.Column<int>(type: "int", nullable: true),
                    beginDutyDate_time = table.Column<int>(type: "int", nullable: true),
                    endDutyDate_time = table.Column<int>(type: "int", nullable: true),
                    beginNode_beginNode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    endNode_endNode = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleDuty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    VehicleTypeId_vehicleTypeId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Matricula_matricula = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ServiceDate_serviceDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Vin_vin = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkBlocks",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    workblockName_name = table.Column<int>(type: "int", nullable: true),
                    StartTime_time = table.Column<int>(type: "int", nullable: true),
                    FinishTime_time = table.Column<int>(type: "int", nullable: true),
                    FinishNode_nodeId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BeginNode_nodeId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CrewmemberDutyId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkBlocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkBlocks_CrewmemberDuty_CrewmemberDutyId",
                        column: x => x.CrewmemberDutyId,
                        principalSchema: "dbo",
                        principalTable: "CrewmemberDuty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverLicense",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DriverLicenseExpiry_driverLicenseExpiry = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DriverLicenseNumber_driverLicenseNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DriverId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverLicense", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverLicense_Drivers_DriverId",
                        column: x => x.DriverId,
                        principalSchema: "dbo",
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DriverTypeId",
                columns: table => new
                {
                    DriverId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    driverTypeID = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverTypeId", x => new { x.DriverId, x.Id });
                    table.ForeignKey(
                        name: "FK_DriverTypeId_Drivers_DriverId",
                        column: x => x.DriverId,
                        principalSchema: "dbo",
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Trips",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TripName_name = table.Column<int>(type: "int", nullable: true),
                    LineId_lineId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RouteId_routeId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsEmpty = table.Column<bool>(type: "bit", nullable: false),
                    Orinetaiton_Orient = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VehicleDutyId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trips_VehicleDuty_VehicleDutyId",
                        column: x => x.VehicleDutyId,
                        principalSchema: "dbo",
                        principalTable: "VehicleDuty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PassingTimes",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Time_time = table.Column<int>(type: "int", nullable: true),
                    NodeId_nodeId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReliefP = table.Column<bool>(type: "bit", nullable: false),
                    TripId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PassingTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PassingTimes_Trips_TripId",
                        column: x => x.TripId,
                        principalSchema: "dbo",
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkTrip",
                columns: table => new
                {
                    WorkblockId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TripId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTrip", x => new { x.WorkblockId, x.TripId });
                    table.ForeignKey(
                        name: "FK_WorkTrip_Trips_TripId",
                        column: x => x.TripId,
                        principalSchema: "dbo",
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkTrip_WorkBlocks_WorkblockId",
                        column: x => x.WorkblockId,
                        principalSchema: "dbo",
                        principalTable: "WorkBlocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CrewmemberDuty_Identifier_id",
                schema: "dbo",
                table: "CrewmemberDuty",
                column: "Identifier_id",
                unique: true,
                filter: "[Identifier_id] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_DriverLicense_DriverId",
                schema: "dbo",
                table: "DriverLicense",
                column: "DriverId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_DriverCC_driverCC",
                schema: "dbo",
                table: "Drivers",
                column: "DriverCC_driverCC",
                unique: true,
                filter: "[DriverCC_driverCC] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_DriverMecNumber_driverMecNumber",
                schema: "dbo",
                table: "Drivers",
                column: "DriverMecNumber_driverMecNumber",
                unique: true,
                filter: "[DriverMecNumber_driverMecNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_DriverNIF_driverNIF",
                schema: "dbo",
                table: "Drivers",
                column: "DriverNIF_driverNIF",
                unique: true,
                filter: "[DriverNIF_driverNIF] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PassingTimes_TripId",
                schema: "dbo",
                table: "PassingTimes",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_VehicleDutyId",
                schema: "dbo",
                table: "Trips",
                column: "VehicleDutyId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_Matricula_matricula",
                schema: "dbo",
                table: "Vehicles",
                column: "Matricula_matricula",
                unique: true,
                filter: "[Matricula_matricula] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_Vin_vin",
                schema: "dbo",
                table: "Vehicles",
                column: "Vin_vin",
                unique: true,
                filter: "[Vin_vin] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_CrewmemberDutyId",
                schema: "dbo",
                table: "WorkBlocks",
                column: "CrewmemberDutyId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkTrip_TripId",
                table: "WorkTrip",
                column: "TripId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Categories",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "DriverLicense",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "DriverTypeId");

            migrationBuilder.DropTable(
                name: "Families",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "PassingTimes",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Products",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Vehicles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "WorkTrip");

            migrationBuilder.DropTable(
                name: "Drivers",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Trips",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "WorkBlocks",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "VehicleDuty",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "CrewmemberDuty",
                schema: "dbo");
        }
    }
}
