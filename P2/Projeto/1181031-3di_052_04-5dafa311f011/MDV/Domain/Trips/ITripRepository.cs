using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public interface ITripRepository : IRepository<Trip, TripId>
    {
        Task<List<Trip>> GetAllTripsAsync();
        Task<List<Trip>> GetAllTripsByLineAsync(string lineId);
        Task<List<Trip>> GetAllWithoutWBAsync();
        Task<int> CommitAsync();
        Task<List<Trip>> getByVehicleDuty(string vehicleduty);
        Task<List<Trip>> getByWorkblockName(int workblock);

        Task<Trip> getByName(int name);
    }
}