using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Domain.Trips
{
    public class Trip : Entity<TripId>, IAggregateRoot
    {
        public TripId Id { get; set; }
        public TripName TripName { get; set; }
        public LineId LineId { get; set; }
        public RouteId RouteId { get; set; }
        public bool IsEmpty { get; set; }
        public ICollection<PassingTime> PassingTimes { get; set; }
        public Orientation Orinetaiton { get; set; }
        public ICollection<WorkTrip> workblockTrips { get; set; }

        public Trip()
        {
        }
        public Trip(TripName name, LineId lineId, RouteId routeId, bool isEmpty, ICollection<PassingTime> passingTimes, Orientation orientation)
        {
            if(name == null)
                throw new BusinessRuleValidationException("Every Trip requires a name.");
            if (lineId == null)   
                throw new BusinessRuleValidationException("Every Trip requires a line id.");
            if (routeId == null)
                throw new BusinessRuleValidationException("Every Trip requires a route id.");

            this.Orinetaiton = orientation;
            this.Id = new TripId(Guid.NewGuid());
            this.TripName = name;
            this.LineId = lineId;
            this.RouteId = routeId;
            this.IsEmpty = isEmpty;
            this.PassingTimes = passingTimes;
        }

        public Trip(TripDto dto)
        {
            this.Orinetaiton = new Orientation(dto.Orinetation);
            this.Id = new TripId(Guid.NewGuid());
            this.TripName = new TripName(dto.TripName);
            this.LineId = new LineId(dto.LineId);
            this.RouteId = new RouteId(dto.RouteId);
            this.IsEmpty = false;
            this.PassingTimes = Array.ConvertAll(dto.PassingTimes, x => new PassingTime(new Time(x.Time), new NodeId(x.NodeId), x.ReliefP));
        }
        public Trip(CreatingTripDto dto)
        {
            this.Orinetaiton = new Orientation(dto.Orientation);
            this.Id = new TripId(Guid.NewGuid());
            this.TripName = new TripName(dto.TripName);
            this.LineId = new LineId(dto.LineId);
            this.RouteId = new RouteId(dto.RouteId);
            this.IsEmpty = false;
            this.PassingTimes = Array.ConvertAll(dto.PassingTimes, x => new PassingTime(new Time(x.Time), new NodeId(x.NodeId), x.ReliefP));
        }
    }
}