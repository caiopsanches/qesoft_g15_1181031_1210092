using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public class Orientation : IValueObject
    {
        public string Orient { get; set; }

        public Orientation()
        {
        }
            public Orientation(string orientation)
        {
            if (orientation == null) throw new Exception("Orientation is required");
            if(orientation != "Go" && orientation != "Return") throw new Exception("Orientation is not valid");
            this.Orient = orientation;
        }
    }
}