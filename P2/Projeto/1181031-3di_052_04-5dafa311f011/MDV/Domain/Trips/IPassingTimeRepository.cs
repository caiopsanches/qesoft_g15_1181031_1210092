using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public interface IPassingTimeRepository : IRepository<PassingTime, PassingTimeId>
    {
        Task<List<PassingTime>> getPassingByTripId(string id);
    }
}