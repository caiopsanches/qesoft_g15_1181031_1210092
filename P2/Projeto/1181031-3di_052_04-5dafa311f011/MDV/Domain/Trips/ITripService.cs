using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using System.Net.Http;

namespace DDDSample1.Domain.Trips
{
    public interface ITripService
    {
        Task<TripDto> CreateTripsAsync(int ind, InputTripDto dto, RouteDto[] routeAux, NodeDto[] nodesAux, LineDto lineAux);
        Task<PassingTimeDto[]> ComputeTimes(int inicialHour, RouteDto route, NodeDto[] nodes);
        Task<List<Trip>> getAllFromVehicleDuty(string vehicleduty);
        Task<List<PassingTime>> getAllPassingTimes();
        Task<Trip> getById(TripId id);
        Task<RouteDto[]> GetRouteInLine(RouteDto[] routes, InputTripDto dto, LineDto line);
        Task<LineDto> GetLineAsync(InputTripDto dto, HttpClient client);
        Task<List<Trip>> GetAllWithoutWorkBlock();
        Task<RouteDto[]> GetRouteAsync(InputTripDto dto, HttpClient client);
        Task<List<PassingTime>> getPassingTimesById(TripId id);
        Task<NodeDto[]> GetNodesAsync(InputTripDto dto, HttpClient client, RouteDto route);
        Task<List<Trip>> GetTripsByLineAsync(string lineId);
        Task<List<Trip>> GetTripsAsync();
        Task<TripDto> AddAsync(TripDto dto);
        Task<Trip> AddAsync1(Trip currentTrip);
        Task<int> saveChanges();
        Task<List<Trip>> getByWorkblockName(int workblock);
        Task<Trip> getByName(int id);

    }
}