using System;

namespace DDDSample1.Domain.Trips
{
    public class TripDto
    {
        public Guid Id { get; set; }
        public int TripName { get; set; }
        public string Orinetation { get; set; }
        public string LineId { get; set; }
        public string RouteId { get; set; }
        public bool IsEmpty { get; set; }
        public PassingTimeDto[] PassingTimes { get; set; }

        public TripDto( int name,string ln, string pt, bool v, PassingTimeDto[] passingTimeDtos, string orinetation)
        {
            this.TripName = name;
            this.LineId = ln;
            this.RouteId = pt;
            this.IsEmpty = v;
            this.PassingTimes = passingTimeDtos;
            this.Orinetation=orinetation;
        }

        public TripDto(Guid id, int name, string ln, string pt, bool v, PassingTimeDto[] passingTimeDtos, string orientation)
        {
            this.Id = id;
            this.TripName = name;
            this.LineId = ln;
            this.RouteId = pt;
            this.IsEmpty = v;
            this.PassingTimes = passingTimeDtos;
            this.Orinetation=orientation;
        }
    }
}