using DDDSample1.Domain.Trips;

namespace DDDSample1.Domain.Trips
{
    public class InputTripDto
    {
        public int TripName { get; set; }
        public string LineId { get; set; }
        public string RouteId { get; set; }
        public int StartTime { get; set; }
        public int FinishTime { get; set; }
        public int NrTrips { get; set; }
        public int Frequency { get; set; }
        public int NrTripsParalel { get; set; }

        public InputTripDto(int name, string lineId, string routeId, int startTime, int finishTime, int nrTrips, int frequency, int nrTripsParalel)
        {
            this.TripName = name;
            this.LineId = lineId;
            this.RouteId = routeId;
            this.StartTime = startTime;
            this.FinishTime = finishTime;
            this.NrTrips = nrTrips;
            this.Frequency = frequency;
            this.NrTripsParalel = nrTripsParalel;
        }
    }
}