namespace DDDSample1.Domain.Trips
{
    public class NodeDto
    {
        public string shortName { get; set; }
        public string nodeName { get; set; }
        public int maxTimeStop { get; set; }
        public int vehicleCapacity { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public bool isDepot { get; set; }
        public bool isReliefPoint { get; set; }

        public NodeDto(string shortName,string nodeName, int maxTimeStop, int vehicleCapacity, double latitude, double longitude, bool isDepot, bool isReliefPoint){
            this.shortName = shortName;
            this.nodeName = nodeName;
            this.maxTimeStop = maxTimeStop;
            this.vehicleCapacity = vehicleCapacity;
            this.latitude = latitude;
            this.longitude = longitude;
            this.isDepot = isDepot;
            this.isReliefPoint = isReliefPoint;
        }
    }
}