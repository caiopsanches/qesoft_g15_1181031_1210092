using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public class RouteId : IValueObject
    {
        public string routeId { get; set; }

        public RouteId()
        {
        }
            public RouteId(string id)
        {
            if (id == null) throw new Exception("Route Id is required");

            routeId = id;
        }
    }
}