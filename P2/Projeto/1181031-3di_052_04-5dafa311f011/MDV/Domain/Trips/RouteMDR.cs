using System;

namespace DDDSample1.Domain.Trips
{
    public class RouteDto
    {
        public string id { get; set; }
        public string routeId { get; set; }
        public string name { get; set; }
        public string idNoInicio { get; set; }
        public string idNoFim { get; set; }
        public SegmentDto[] networkSegments { get; set; }

        public RouteDto(string id,string routeId, string name, string idNoInicio, string idNoFim,SegmentDto[] networkSegments){
            this.id = id;
            this.routeId = routeId;
            this.name = name;
            this.idNoInicio = idNoInicio;
            this.idNoFim = idNoFim;
            this.networkSegments = networkSegments;
        }
    }

    public class SegmentDto
    {
        public string segmentId { get; set; }
        public string idNo1 { get; set; }
        public string idNo2 { get; set; }
        public int distance { get; set; }
        public int deslocationTime { get; set; }
        
        public SegmentDto(string segmentId,string idNo1,string idNo2, int distance, int deslocationTime){
            this.segmentId = segmentId;
            this.idNo1 = idNo1;
            this.idNo2 = idNo2;
            this.distance = distance;
            this.deslocationTime = deslocationTime;
        }
    }
}