using System;
using DDDSample1.Domain.Shared;
using System.Collections.Generic;

namespace DDDSample1.Domain.Trips
{
    public class PassingTime : Entity<PassingTimeId>
    {
        public Time Time { get; set; }
        public NodeId NodeId { get; set; }
        public bool ReliefP { get; set; }
        public PassingTime()
        {
        }
        public PassingTime(Time time, NodeId nodeId, bool reliefP)
        {
            if (time == null) throw new BusinessRuleValidationException("Time is required");
            if (nodeId == null)
                throw new BusinessRuleValidationException("Every Passing Time requires a node id.");

            this.Id = new PassingTimeId(Guid.NewGuid());
            this.Time = time;
            this.NodeId = nodeId;
            this.ReliefP = reliefP;
        }

        public PassingTime(PassingTimeDto dto)
        {
            this.Id = new PassingTimeId(Guid.NewGuid());
            this.Time = new Time(dto.Time);
            this.NodeId = new NodeId(dto.NodeId);
            this.ReliefP = dto.ReliefP;
        }

    }
}