using System;

namespace DDDSample1.Domain.Trips
{
    public class CreatingTripDto
    {
        public int TripName { get; set; }
        public string LineId { get; set; }
        public string RouteId { get; set; }
        public string Orientation { get; set; }
        public bool IsEmpty { get; set; }
        public PassingTimeDto[] PassingTimes { get; set; }

        public CreatingTripDto(int name, string lineId, string routeId, bool isEmpty, PassingTimeDto[] passingTimes, string orientation)
        {
            this.TripName = name;
            this.LineId = lineId;
            this.RouteId = routeId;
            this.IsEmpty = isEmpty;
            this.PassingTimes = passingTimes;
            this.Orientation=orientation;
        }
    }
}