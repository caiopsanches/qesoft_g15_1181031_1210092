using System;

namespace DDDSample1.Domain.Trips
{
    public class LineDto
    {
        public string id { get; set; }
        public string lineId { get; set; }
        public string name { get; set; }
        public string colour { get; set; }
        public PathDto[] paths { get; set; }
        public VehicleDto[] vehicles { get; set; }
        public DriverDto[] drivers { get; set; }

        public LineDto(string id,string lineId,string name,string colour,PathDto[] paths,VehicleDto[] vehicles,DriverDto[] drivers){
            this.id = id;
            this.lineId = lineId;
            this.name = name;
            this.colour = colour;
            this.paths = paths;
            this.vehicles = vehicles;
            this.drivers = drivers;
        }
    }

    public class PathDto
    {
        public string routeId { get; set; }
        public string orientation { get; set; }

        public PathDto(string routeId, string orientation){
            this.routeId = routeId;
            this.orientation = orientation;
        }
    }

    public class VehicleDto
    {
        public string vId { get; set; }

        public VehicleDto(string vId){
            this.vId = vId;
        }
    }

    public class DriverDto
    {
        public string dId { get; set; }

        public DriverDto(string dId){
            this.dId = dId;
        }
    }
}