using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public class TripName : IValueObject
    {
        public int name { get; set; }
        public TripName()
        {
        }
        public TripName(int name)
        {
            this.name = name;
        }
    }
}