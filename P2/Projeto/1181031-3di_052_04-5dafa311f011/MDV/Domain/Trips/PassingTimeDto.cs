using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public class PassingTimeDto
    {
        public Guid Id { get; set; }
        public int Time { get; set; }
        public string NodeId { get; set; }
        public bool ReliefP { get; set; }

        public PassingTimeDto(int t, string node, bool isRpoint)
        {
            this.Time = t;
            this.NodeId = node;
            this.ReliefP = isRpoint;
        }
        public PassingTimeDto(Time t, NodeId node, bool isRpoint)
        {
            this.Time = t.time;
            this.NodeId = node.nodeId;
            this.ReliefP = isRpoint;
        }
    }
}