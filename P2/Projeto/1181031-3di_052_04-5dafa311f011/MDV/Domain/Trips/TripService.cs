using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using System.Net.Http;
using Newtonsoft.Json;
using DDDSample1.Mappers;

namespace DDDSample1.Domain.Trips
{
    public class TripService : ITripService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITripRepository _repo;
        private readonly TripMapper mapper;
        private readonly IPassingTimeRepository _passingrepo;
        public TripService(IUnitOfWork unit, ITripRepository rep, IPassingTimeRepository passingrepo)
        {
            this._unitOfWork = unit;
            this._repo = rep;
            this._passingrepo = passingrepo;
            mapper = new TripMapper();
        }

        public async Task<TripDto> CreateTripsAsync(int ind, InputTripDto dto, RouteDto[] routeAux, NodeDto[] nodesAux, LineDto lineAux)
        {
            TripDto trip = null;
            var aux = dto.StartTime;
            for (int i = 0; i < dto.NrTrips; i++)
            {
                if (aux < dto.FinishTime)
                {
                    if (i % 2 == 0)
                    {
                        aux = dto.StartTime + (dto.Frequency * (i / 2));
                        PassingTimeDto[] timesDto = await ComputeTimes(aux, routeAux[0], nodesAux);
                        TripDto tripDto = new TripDto(ind, lineAux.lineId, routeAux[0].routeId, false, timesDto, "Go");
                        trip = await AddAsync(tripDto);
                        aux = timesDto[timesDto.Length - 1].Time;
                        ind++;
                    }
                    else
                    {
                        var nodesAux1 = nodesAux;
                        Array.Reverse(nodesAux1);
                        PassingTimeDto[] timesDto = await ComputeTimes(aux, routeAux[1], nodesAux1);
                        TripDto tripDto = new TripDto(ind, lineAux.lineId, routeAux[1].routeId, false, timesDto, "Return");
                        trip = await AddAsync(tripDto);
                        ind++;
                    }
                }
            }
            return trip;
        }

        public async Task<PassingTimeDto[]> ComputeTimes(int inicialHour, RouteDto route, NodeDto[] nodes)
        {
            PassingTimeDto[] listTimes = new PassingTimeDto[route.networkSegments.Length + 1];
            for (int i = 0; i < route.networkSegments.Length; i++)
            {
                if (i == 0)
                {
                    PassingTimeDto p1 = new PassingTimeDto(new Time(inicialHour), new NodeId(route.networkSegments[i].idNo1), nodes[0].isReliefPoint);
                    listTimes[i] = p1;
                }
                else
                {
                    PassingTimeDto p = new PassingTimeDto(new Time(listTimes[i - 1].Time + route.networkSegments[i - 1].deslocationTime), new NodeId(route.networkSegments[i].idNo1), nodes[i].isReliefPoint);
                    listTimes[i] = p;

                    if (i == route.networkSegments.Length - 1)
                    {
                        var p2 = new PassingTimeDto(new Time(listTimes[i].Time + route.networkSegments[i].deslocationTime), new NodeId(route.networkSegments[i].idNo2), nodes[i + 1].isReliefPoint);
                        listTimes[i + 1] = p2;
                    }
                }
            }
            return listTimes;
        }

        public Task<List<Trip>> getAllFromVehicleDuty(string vehicleduty)
        {
            return this._repo.getByVehicleDuty(vehicleduty);
        }

        public async Task<List<PassingTime>> getAllPassingTimes()
        {
            return this._passingrepo.GetAllAsync().Result;
        }
        public async Task<Trip> getById(TripId id)
        {
            return this._repo.GetByIdAsync(id).Result;
        }

        public async Task<RouteDto[]> GetRouteInLine(RouteDto[] routes, InputTripDto dto, LineDto line)
        {
            RouteDto[] resAux = new RouteDto[line.paths.Length];
            RouteDto[] res = new RouteDto[2];

            //Get Route ids of the Line
            var i = 0;
            foreach (PathDto p in line.paths)
            {
                foreach (RouteDto r in routes)
                {
                    if (p.routeId == r.routeId)
                    {
                        resAux[i] = r;
                        i++;
                    }
                    if (r.routeId == dto.RouteId)
                    {
                        res[0] = r;
                    }
                }
            }

            //Get route go and return speciificed
            foreach (RouteDto r in resAux)
            {
                if (r.idNoInicio == res[0].idNoFim && r.idNoFim == res[0].idNoInicio)
                {
                    res[1] = r;
                }
            }

            return res;
        }

        public async Task<LineDto> GetLineAsync(InputTripDto dto, HttpClient client)
        {
            LineDto lineAux = null;
            var taskLine = await client.GetAsync("https://opt52.herokuapp.com/api/line");
            var jsonStringLine = await taskLine.Content.ReadAsStringAsync();
            //Console.WriteLine(jsonStringLine);
            List<LineDto> lines = JsonConvert.DeserializeObject<List<LineDto>>(jsonStringLine);
            foreach (LineDto element in lines)
            {
                if (element.lineId == dto.LineId)
                {
                    lineAux = element;
                }

            }
            return lineAux;
        }


        public Task<List<Trip>> GetAllWithoutWorkBlock()
        {
            return this._repo.GetAllWithoutWBAsync();
        }


        public async Task<RouteDto[]> GetRouteAsync(InputTripDto dto, HttpClient client)

        {
            var taskRoute = await client.GetAsync("https://opt52.herokuapp.com/api/route");
            var jsonStringRoute = await taskRoute.Content.ReadAsStringAsync();
            RouteDto[] routes = JsonConvert.DeserializeObject<RouteDto[]>(jsonStringRoute);
            return routes;
        }

        public async Task<List<PassingTime>> getPassingTimesById(TripId id)
        {
            return await this._passingrepo.getPassingByTripId(id.AsString());
        }

        public async Task<NodeDto[]> GetNodesAsync(InputTripDto dto, HttpClient client, RouteDto route)
        {
            NodeDto[] nodes = new NodeDto[route.networkSegments.Length + 1];
            var taskNode = await client.GetAsync("https://opt52.herokuapp.com/api/node");
            var jsonStringNode = await taskNode.Content.ReadAsStringAsync();
            List<NodeDto> nodesAux = JsonConvert.DeserializeObject<List<NodeDto>>(jsonStringNode);

            for (int i = 0; i < route.networkSegments.Length; i++)
            {
                foreach (NodeDto element in nodesAux)
                {
                    if (element.shortName == route.networkSegments[i].idNo1)
                    {
                        nodes[i] = element;
                    }
                    if (i == route.networkSegments.Length - 1)
                    {
                        if (element.shortName == route.networkSegments[i].idNo2)
                        {
                            nodes[i + 1] = element;
                        }
                    }
                }
            }
            return nodes;
        }

        public async Task<List<Trip>> GetTripsByLineAsync(string lineId){
            var list = await this._repo.GetAllTripsByLineAsync(lineId);
            return list;
        }

        public async Task<List<Trip>> GetTripsAsync()
        {
            var list = await this._repo.GetAllTripsAsync();
            return list;
        }

        public async Task<TripDto> AddAsync(TripDto dto)
        {
            Trip trip = mapper.toDomain(dto).Result;

            await this._repo.AddAsync(trip);
            await this._unitOfWork.CommitAsync();
            return dto;
        }
        public async Task<Trip> AddAsync1(Trip currentTrip)
        {
            return await this._repo.AddAsync(currentTrip);
        }

        public async Task<int> saveChanges()
        {
            var res = await this._repo.CommitAsync();
            return res;

        }

        public async Task<List<Trip>> getByWorkblockName(int workblock)
        {
          return  await this._repo.getByWorkblockName(workblock);
        }

        public async Task<Trip> getByName(int name)
        {
          return  await this._repo.getByName(name);
        }
    }

}
