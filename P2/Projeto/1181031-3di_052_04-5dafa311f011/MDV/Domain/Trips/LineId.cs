using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public class LineId : IValueObject
    {
        public string lineId { get; set; }

        public LineId()
        {
        }

        public LineId(string id)
        {
            if (id == null) throw new Exception("Line Id is required");

            lineId = id;
        }
    }
}