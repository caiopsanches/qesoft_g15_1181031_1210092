using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using System;
using System.Xml;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Trips;
using System.Collections.Generic;
using System.Xml.Linq;
using DDDSample1.Domain.CrewmemberDuties;
using DDDSample1.Domain.VehicleDuties;

namespace DDDSample1.Domain.Import
{
    public class ImportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkBlockService _workblockService;
        private readonly ITripService _tripService;
        private readonly ICrewmemberDutyService _crewmemeberDutyService;
        private readonly IVehicleDutyService _vehicleDutyService;
        private XmlNodeList nodes;

        public ImportService(IUnitOfWork unitOfWork, IWorkBlockService workblockService, ITripService tripService, ICrewmemberDutyService crewmemeberDutyService, IVehicleDutyService vehicleDutyService)
        {
            this._workblockService = workblockService;
            this._unitOfWork = unitOfWork;
            this._tripService = tripService;
            this._crewmemeberDutyService = crewmemeberDutyService;
            this._vehicleDutyService = vehicleDutyService;
        }


        internal async Task<int> AddAsync(XmlDocument xmlDoc)
        {

            nodes = xmlDoc.GetElementsByTagName("Node");

            var trips = xmlDoc.GetElementsByTagName("Trip");
             await processTrips(trips);

            var workBlocks = xmlDoc.GetElementsByTagName("WorkBlock");
             await processWorkblocks(workBlocks);


            var vehicles = xmlDoc.GetElementsByTagName("VehicleDuty");
             await ProcessVehicleDuty(vehicles);


            var drivers = xmlDoc.GetElementsByTagName("DriverDuty");
           await ProcessCrewmemeberDuty(drivers);

            return 1;
        }

        private WorkBlock firstWB;
        private WorkBlock lastWB;
        private async Task<bool> ProcessVehicleDuty(XmlNodeList vehicles)
        {

            foreach (XmlElement element in vehicles)
            {

                string key = element.GetAttribute("key");
                int description = GetVehicleDutyName(key);
                int vehicleName = GetVehicleDutyName(element.GetAttribute("key"));
                var wblocks = element.GetElementsByTagName("ref");

                List<Trip> trips = new List<Trip>();
                foreach (XmlElement element1 in wblocks)
                {
                    int name = GetWorkblockName(element1.GetAttribute("key"));
                    List<Trip> tripsAux = await this._tripService.getByWorkblockName(name);
                    tripsAux.ForEach(trip => trips.Add(trip));
                }

                if (await CanGetFirstAndLastWorkblockFromXmlElementList(wblocks))
                {
                    var vDuty = new VehicleDuty(new Description(vehicleName), new Time(firstWB.StartTime.time), new Time(lastWB.FinishTime.time), new BeginNode(firstWB.BeginNode.nodeId), new EndNode(lastWB.FinishNode.nodeId), trips);
                    await this._vehicleDutyService.AddAsync(vDuty);
                }

            }
            await this._unitOfWork.CommitAsync();
            return true;
        }
        private async Task<bool> CanGetFirstAndLastWorkblockFromXmlElementList(XmlNodeList wblocks)
        {
            try
            {
                var firstWorkblock = (XmlElement)wblocks.Item(0);
                var lastWorkblock = (XmlElement)wblocks.Item(wblocks.Count - 1);
                int firstName = GetWorkblockName(firstWorkblock.GetAttribute("key"));
                int secondName = GetWorkblockName(lastWorkblock.GetAttribute("key"));
                firstWB = await GetWorkblockByName(firstName);
                lastWB = await GetWorkblockByName(secondName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private async Task<bool> ProcessCrewmemeberDuty(XmlNodeList crewmemebers)
        {

            foreach (XmlElement element in crewmemebers)
            {
                string key = element.GetAttribute("Name");
                var wblocks = element.GetElementsByTagName("ref");
                List<WorkBlock> workblocks = new List<WorkBlock>();

                foreach (XmlElement element1 in wblocks)
                {
                    int name = GetWorkblockName(element1.GetAttribute("key"));
                    WorkBlock wb = await GetWorkblockByName(name);
                    if (wb != null)
                    {
                        workblocks.Add(wb);
                    }
                }
                await this._crewmemeberDutyService.AddAsync(new CrewmemberDuty(Int32.Parse(key), workblocks));
            }
            await this._unitOfWork.CommitAsync();
            return true;
        }

        private int GetVehicleDutyName(string vehicleDuty)
        {
            return Int32.Parse(vehicleDuty.Replace("VehicleDuty:", "").Trim());
        }

        private int GetWorkblockName(string wbKey)
        {
            return Int32.Parse(wbKey.Replace("WorkBlock:", "").Trim());
        }

        private int GetTripName(string tripKey)
        {
            return Int32.Parse(tripKey.Replace("Trip:", "").Trim());
        }

        private int GetCrewmemeberName(string tripKey)
        {
            return Int32.Parse(tripKey.Replace("DriverDuty:", "").Trim());
        }


        private async Task<bool> processTrips(XmlNodeList trips)
        {

            foreach (XmlElement element in trips)
            {
                string ke = element.GetAttribute("key");
                string ie = element.GetAttribute("IsEmpty");
                string or = element.GetAttribute("Orientation");
                string ln = element.GetAttribute("Line");
                string pt = element.GetAttribute("Path").Replace("Path:","");
                string ct = element.GetAttribute("IsCrewTravelTime");
                var passingTimes = element.GetElementsByTagName("PassingTime");
                var listPassingTimes = new List<PassingTimeDto>();

                foreach (XmlElement element1 in passingTimes)
                {
                    string node = FindNodeNameByKey(element1.GetAttribute("Node"));
                    listPassingTimes.Add(new PassingTimeDto(Int32.Parse(element1.GetAttribute("Time")), node, bool.Parse(element1.GetAttribute("IsReliefPoint"))));
                }

                var currentTrip = new Trip(new TripDto(GetTripName(ke), ln, pt, false, listPassingTimes.ToArray(), or));
                currentTrip.workblockTrips = new List<WorkTrip>();
                await this._tripService.AddAsync1(currentTrip);
            }
            await this._tripService.saveChanges();
            return true;
        }

        private async Task<WorkBlock> GetWorkblockByName(int name)
        {
            try
            {
                return await this._workblockService.getByName(name);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }
        private async Task<Trip> GetTripByName(int name)
        {
            try
            {
                return await this._tripService.getByName(name);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }
        private async Task<int> processWorkblocks(XmlNodeList works)
        {

            foreach (XmlElement elem in works)
            {
                string wtk = elem.GetAttribute("key");
                string st = elem.GetAttribute("StartTime");
                string et = elem.GetAttribute("EndTime");
                string sn = elem.GetAttribute("StartNode");
                string en = elem.GetAttribute("EndNode");
                string ct1 = elem.GetAttribute("IsCrewTravelTime");
                var trips = elem.GetElementsByTagName("ref");
                int workblockName = GetWorkblockName(wtk);

                WorkBlock wb = await GetWorkblockByName(workblockName);
                if (wb == null)
                {
                    wb = new WorkBlock(new WorkblockName(workblockName), new Time(Int32.Parse(st)), new Time(Int32.Parse(et)), new NodeId(FindNodeNameByKey(en)), new NodeId(FindNodeNameByKey(sn)));
                }


                foreach (XmlElement elem1 in trips)
                {
                    int key = GetTripName(elem1.GetAttribute("key"));
                    Trip trip = await GetTripByName(key);
                    if (trip != null)
                    {
                        trip.workblockTrips.Add(new WorkTrip(wb.Id, wb, trip.Id, trip));
                    }
                }

            }
            await this._unitOfWork.CommitAsync();
            return 1;
        }

        private string FindNodeNameByKey(string sn)
        {

            foreach (XmlElement element in nodes)
            {
                if (element.GetAttribute("key") == sn)
                {
                    return element.GetAttribute("ShortName");
                }
            }
            return null;
        }
    }
}