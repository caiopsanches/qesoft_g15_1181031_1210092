using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Vehicle
{
    public interface IVehicleRepository : IRepository<Vehicle, VehicleId>
    {

        Task<List<Vehicle>> GetAllVehiclesAsync();
        Task<int> CommitAsync();
    }
}