using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Vehicle
{

    public class VehicleTypeId : IValueObject
    {

        public string vehicleTypeId { get; set; }

        public VehicleTypeId() { }
        public VehicleTypeId(string vTypeId)
        {
            if (vTypeId == null) throw new Exception("Vehicle Type is required");
            vehicleTypeId = vTypeId;
        }

    }
}