using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Vehicle
{
    public class Matricula : IValueObject
    {


        public string matricula { get; set; }

        public bool isMatriculaValida(String matricula)
        {
            //valida o tamanho e a posição do caracter "-"
            if (matricula.Length != 8 || matricula[2] != '-' || matricula[5] != '-')
            {
                return false;
            }
            int numeros = 0, letras = 0;

            for (int contador = 0; contador < matricula.Length; contador++)
            {

                //verifica se é letra 
                if (char.IsLetter(matricula[contador]))
                {

                    //verifica se é letra Maiuscula se sim incretenta se não, nao incrementa
                    if (matricula[contador] == (matricula.ToUpper()[contador]))
                    {
                        ++letras;
                        continue;
                    }
                }
                //verifica se é digito e se sim, incrementa
                if (char.IsNumber(matricula[contador]))
                {
                    ++numeros;
                }


                if (contador == 2)
                {
                    if (numeros != 2 && letras != 2)
                    {
                        return false;
                    }
                }
                if (contador == 4)
                {
                    if ((numeros != 2 || letras != 2) && (numeros != 4))
                    {
                        return false;
                    }
                }

            }
            //verifica se a matricula contem os 4 numeros e 2 letras maiusculas ou 4 letras e 2 numeros
            if (numeros == 4 && letras == 2 || numeros == 2 && letras == 4)
            {
                return true;
            }
            return false;
        }
        public Matricula()
        {
        }


        public Matricula(String value)
        {

            if (isMatriculaValida(value) == false)

                throw new Exception("Matricula is required");
            matricula = value;
        }
    }
}
