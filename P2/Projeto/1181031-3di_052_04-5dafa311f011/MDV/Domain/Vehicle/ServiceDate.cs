using System;
using DDDSample1.Domain.Shared;
using Newtonsoft.Json;


namespace DDDSample1.Domain.Vehicle
{
    public class ServiceDate : IValueObject
    {
        public DateTime serviceDate { get; set; }
        public ServiceDate()
        {
        }

        public ServiceDate(DateTime sd)
        {
            if (sd == null) throw new Exception("Service Date is required");

            serviceDate = sd;
        }
    }
}