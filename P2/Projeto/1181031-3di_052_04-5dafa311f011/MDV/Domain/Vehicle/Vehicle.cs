using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Domain.Vehicle
{
    public class Vehicle : Entity<VehicleId>, IAggregateRoot
    {
        public VehicleId Id { get; set; }
        public VehicleTypeId VehicleTypeId { get; set; }
        public Matricula Matricula { get; set; }
        public ServiceDate ServiceDate { get; set; }
        public Vin Vin { get; set; }

        public Vehicle()
        {
        }

        public Vehicle(VehicleTypeId vehicleTypeId, Matricula matricula, Vin vin, ServiceDate serviceDate)
        {
            if (matricula == null)
                throw new BusinessRuleValidationException("Every Vehicle requires a matricula.");
            if (vehicleTypeId == null)
                throw new BusinessRuleValidationException("Every Vehicle requires a type.");
            if (vin == null)
                throw new BusinessRuleValidationException("Every Vehicle requires a vin.");

            this.Id = new VehicleId(Guid.NewGuid());
            this.ServiceDate = serviceDate;
            this.Matricula = matricula;
            this.VehicleTypeId = vehicleTypeId;
            this.Vin = vin;

        }

        public Vehicle(VehicleDto dto)
        {
            Console.WriteLine("---------" + dto.Matricula);
            this.Id = new VehicleId(Guid.NewGuid());
            this.VehicleTypeId = new VehicleTypeId(dto.VehicleTypeId);
            this.Matricula = new Matricula(dto.Matricula);
            this.Vin = new Vin(dto.Vin);
            this.ServiceDate = new ServiceDate(DateTime.Parse(dto.ServiceDate));


        }
    }
}
