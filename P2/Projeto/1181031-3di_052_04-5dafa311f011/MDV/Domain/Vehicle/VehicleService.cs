using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using DDDSample1.Mappers;

namespace DDDSample1.Domain.Vehicle
{

    public class VehicleService : IVehicleService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleRepository _repo;
        private readonly VehicleMapper mapper;

        public VehicleService(IUnitOfWork unit, IVehicleRepository rep)
        {
            this._unitOfWork = unit;
            this._repo = rep;
            mapper = new VehicleMapper();
        }

        public async Task<VehicleMDRDto> GetVehicleAsync(InputVehicleDto dto, HttpClient client)
        {
            VehicleMDRDto vehicleTypeAux = null;
            var taskVehicle = await client.GetAsync("https://opt52.herokuapp.com/api/vehicle");
            var jsonStringVehicle = await taskVehicle.Content.ReadAsStringAsync();
            //Console.WriteLine(jsonStringVehicle);
            List<VehicleMDRDto> vehiclesTypes = JsonConvert.DeserializeObject<List<VehicleMDRDto>>(jsonStringVehicle);
            foreach (VehicleMDRDto element in vehiclesTypes)
            {

                if (element.code == dto.VehicleTypeId)
                {
                    vehicleTypeAux = element;
                }

            }

            return vehicleTypeAux;
        }

        public async Task<List<Vehicle>> GetAllAsync()
        {
            var list = await this._repo.GetAllVehiclesAsync();
            return list;
        }


        public async Task<List<Vehicle>> GetVehiclesAsync()
        {
            var list = await this._repo.GetAllVehiclesAsync();
            return list;
        }


        internal async Task<Vehicle> getById(VehicleId id)
        {
            return this._repo.GetByIdAsync(id).Result;
        }

        internal async Task<VehicleDto> AddAsync(VehicleDto dto)
        {
            Vehicle vehicle = mapper.toDomain(dto).Result;

            await this._repo.AddAsync(vehicle);
            await this._unitOfWork.CommitAsync();
            return dto;
        }
        internal async Task<Vehicle> AddAsync1(Vehicle currentVehicle)
        {
            return await this._repo.AddAsync(currentVehicle);
        }

        internal async Task<int> saveChanges()
        {
            var res = await this._unitOfWork.CommitAsync();
            return res;

        }

        public async Task<VehicleDto> defineVehicle(InputVehicleDto dto, VehicleMDRDto vehicleTypeAux)
        {
            VehicleDto vehicle = null;
            VehicleDto vehicleDTO = new VehicleDto(vehicleTypeAux.code, dto.Matricula, dto.Vin, dto.ServiceDate);
            vehicle = await AddAsync(vehicleDTO);

            return vehicle;
        }
    }


}


