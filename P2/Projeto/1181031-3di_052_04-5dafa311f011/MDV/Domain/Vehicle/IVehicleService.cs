
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Domain.Vehicle
{

    public interface IVehicleService
    {
        Task<VehicleMDRDto> GetVehicleAsync(InputVehicleDto dto, HttpClient client);
        Task<List<Vehicle>> GetVehiclesAsync();
        Task<VehicleDto> defineVehicle(InputVehicleDto dto, VehicleMDRDto vehicleTypeAux);
    }
}