using System;

namespace DDDSample1.Domain.Vehicle
{
    public class VehicleDto
    {

        public VehicleDto(string VehicleTypeId, string Matricula, string Vin, string ServiceDate)
        {

            this.VehicleTypeId = VehicleTypeId;
            this.Matricula = Matricula;
            this.Vin = Vin;
            this.ServiceDate = ServiceDate;

        }

        public Guid Id { get; set; }
        public string Matricula { get; set; }
        public string VehicleTypeId { get; set; }
        public string ServiceDate { get; set; }
        public string Vin { get; set; }

    }
}