using System;
using System.ComponentModel.DataAnnotations;
using DDDSample1.Domain.Shared;


namespace DDDSample1.Domain.Vehicle
{
    public class Vin : IValueObject
    {
        [RegularExpression("[A-HJ-NPR-Z0-9]{13}[0-9]{4}", ErrorMessage = "Invalid Vehicle Identification Number Format.")]
        public string vin { get; set; }

        public Vin()
        { }
        public static bool ValidateVin(string vin)
        {
            var c = true;
            if (vin.Length != 17)
            {
                c = false;
            }
            return c;
        }
        public Vin(string v)
        {
            if (ValidateVin(v) == false) throw new Exception("Vin is not correct");
            vin = v;

        }
    }
}