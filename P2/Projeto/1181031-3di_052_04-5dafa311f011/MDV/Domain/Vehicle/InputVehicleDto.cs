using DDDSample1.Domain.Vehicle;


namespace DDDSample1.Domain.Vehicle
{

    public class InputVehicleDto
    {

        public string Matricula { get; set; }
        public string VehicleTypeId { get; set; }

        public string ServiceDate { get; set; }

        public string Vin { get; set; }


        public InputVehicleDto(string vehicleTypeId, string matricula, string vin, string serviceDate)
        {
           
            this.VehicleTypeId = vehicleTypeId;
            this.Matricula = matricula;
            this.ServiceDate = serviceDate;
            this.Vin = vin;

        }

    }

}