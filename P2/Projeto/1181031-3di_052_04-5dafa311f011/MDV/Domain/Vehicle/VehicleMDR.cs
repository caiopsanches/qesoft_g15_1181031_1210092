using System;

namespace DDDSample1.Domain.Vehicle
{
    public class VehicleMDRDto
    {
        public string id { get; set; }
        public string code { get; set; }
        public string energy { get; set; }
        public int autonomy { get; set; }
        public int kmCost { get; set; }
        public int avgConsumption { get; set; }
        public int avgSpeed { get; set; }

        public VehicleMDRDto(string code, string energy, int autonomy, int kmCost, int avgConsumption, int avgSpeed)
        {

            this.code = code;
            this.energy = energy;
            this.autonomy = autonomy;
            this.kmCost = kmCost;
            this.avgConsumption = avgConsumption;
            this.avgSpeed = avgSpeed;

        }

    }

}