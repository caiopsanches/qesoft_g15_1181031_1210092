
using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.WorkBlocks
{
    public class WorkBlockDto
    {
        public Guid Id { get; set; }
        public int name { get; set; }

        public int StartTime { get; set; }
        public int FinishTime { get; set; }
        public string FinishNode { get; set; }
        public string BeginNode { get; set; }

        public List<string> Trips { get; set; }
    }
}