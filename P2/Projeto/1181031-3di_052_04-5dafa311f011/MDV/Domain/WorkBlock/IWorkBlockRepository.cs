
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.WorkBlocks
{
    public interface IWorkBlockRepository : IRepository<WorkBlock, WorkBlockId>
    {
        Task<List<WorkBlock>> GetAllAsyncWithTrips();
        Task<WorkBlock> getByName(int name);
    }
}