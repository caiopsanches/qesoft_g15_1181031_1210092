using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.WorkBlocks
{
    public class WorkblockName : IValueObject
    {
        public int name { get; set; }
        public WorkblockName()
        {
        }
        public WorkblockName(int name)
        {
            this.name = name;
        }
    }
}