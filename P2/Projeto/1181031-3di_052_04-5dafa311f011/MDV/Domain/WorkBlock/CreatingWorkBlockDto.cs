using DDDSample1.Domain.Trips;
namespace DDDSample1.Domain.WorkBlocks
{
    public class CreatingWorkBlockDto
    {
        public int number { get; set; }
        public int duration { get; set; }
        public string vehicleduty { get; set; }

        public CreatingWorkBlockDto(int number, int duration, string vehicleduty)
        {
            this.number = number;
            this.duration = duration;
            this.vehicleduty = vehicleduty;


        }
    }
}