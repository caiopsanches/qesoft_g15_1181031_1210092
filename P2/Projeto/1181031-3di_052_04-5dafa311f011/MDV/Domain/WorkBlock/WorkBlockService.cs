using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using System;

using DDDSample1.Mappers;

namespace DDDSample1.Domain.WorkBlocks
{
    public class WorkBlockService : IWorkBlockService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkBlockRepository _repo;
        private readonly WorkBlockMap mapper;
        private readonly ITripService _serv;
        // Dictionary<Trip, List<WorkBlock>> preparedToPersist = new Dictionary<Trip, List<WorkBlock>>();
        public WorkBlockService(IUnitOfWork unit, IWorkBlockRepository rep, ITripService serv)
        {
            this._serv = serv;
            this._unitOfWork = unit;
            this._repo = rep;
            mapper = new WorkBlockMap();
        }

        public async Task<List<WorkBlockDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsyncWithTrips();

            List<WorkBlockDto> listDto = list.ConvertAll<WorkBlockDto>(cat =>
              mapper.toDTO(cat));

            return listDto;
        }

        public async Task<WorkBlockDto> GetByIdAsync(WorkBlockId id)
        {
            var workblock = await this._repo.GetByIdAsync(id);

            if (workblock == null)
                return null;
            WorkBlockMap mapper = new WorkBlockMap();
            return mapper.toDTO(workblock);
        }


        public async Task<int> saveChanges()
        {
            await this._unitOfWork.CommitAsync();
            return 1;

        }


        public async Task<int> createWorkBlocks(CreatingWorkBlockDto dto)
        {
            List<Trip> trips = await this._serv.getAllFromVehicleDuty(dto.vehicleduty);

            foreach (Trip trip in trips)
            {
                var passing = trip.PassingTimes;
                List<PassingTime> passing_ordered = passing.OrderBy(x => x.Time.time).ToList();
                trip.PassingTimes = passing_ordered;
            }

            List<Trip> trips_ordered = trips.OrderBy(x => x.PassingTimes.ElementAt(0).Time.time).ToList();

            if (trips_ordered.Count > 0)
            {
                return await constructWorkBlocks(trips_ordered, dto);
            }
            else
            {
                return 0;
            }
        }

        public async Task<int> constructWorkBlocks(List<Trip> trips, CreatingWorkBlockDto dto)
        {
            Dictionary<Trip, List<WorkBlock>> preparedToPersist = new Dictionary<Trip, List<WorkBlock>>();
            int generalTripCount = 0;
            var possibleToContinue = true;
            int block_count = 0;


            while (block_count < dto.number && possibleToContinue)
            {
                var currentDuration = 0;
                var haveToExit = false;
                List<Trip> listTrips = new List<Trip>();
                var innerTripCounter = 0;
                while (!haveToExit)
                {
                    if (generalTripCount == trips.Count)
                    {
                        possibleToContinue = false;
                        haveToExit = true;
                    }
                    else
                    {
                        var currentTrip = trips.ElementAt(generalTripCount);
                        var tripDuration = currentTrip.PassingTimes.ElementAt(currentTrip.PassingTimes.Count - 1).Time.time - currentTrip.PassingTimes.ElementAt(0).Time.time;
                        int waitingTime = 0;

                        if (innerTripCounter != 0)
                        {
                            Trip lastTrip = listTrips.ElementAt(innerTripCounter - 1);
                            waitingTime = await computeWaitingTime(lastTrip, currentTrip);
                        }
                        currentDuration = currentDuration + tripDuration + waitingTime;

                        if (currentDuration < dto.duration)
                        {
                            listTrips.Add(currentTrip);
                            innerTripCounter++;
                            generalTripCount++;
                        }
                        else
                        {
                            haveToExit = true;
                        }
                    }
                }
                preparedToPersist = await relateWorkblockWithTrips(listTrips);

                block_count++;
                haveToExit = true;

            }

            foreach (KeyValuePair<Trip, List<WorkBlock>> entry in preparedToPersist)
            {
                var workTrip = new List<WorkTrip>();
                Console.WriteLine(entry.Key.Id.AsString());
                foreach (WorkBlock wbk in entry.Value)
                {
                    workTrip.Add(new WorkTrip(wbk.Id, wbk, entry.Key.Id, entry.Key));
                }
                entry.Key.workblockTrips = workTrip;
            }
            var changes = await this._serv.saveChanges();

            return block_count;
        }
        public async Task<Dictionary<Trip, List<WorkBlock>>> relateWorkblockWithTrips(List<Trip> listTrips)
        {
            Dictionary<Trip, List<WorkBlock>> preparedToPersist = new Dictionary<Trip, List<WorkBlock>>();
            var wbCount = await GetAllAsync();
            var wbAsync = new WorkBlock(new WorkblockName(wbCount.Count + 1), listTrips.ElementAt(0).PassingTimes.ElementAt(0).Time, listTrips.ElementAt(listTrips.Count - 1).PassingTimes.ElementAt(listTrips.ElementAt(listTrips.Count - 1).PassingTimes.Count - 1).Time, listTrips.ElementAt(listTrips.Count - 1).PassingTimes.ElementAt(listTrips.ElementAt(listTrips.Count - 1).PassingTimes.Count - 1).NodeId, listTrips.ElementAt(0).PassingTimes.ElementAt(0).NodeId);

            foreach (Trip trip in listTrips)
            {

                Trip finalTrip = await this._serv.getById(trip.Id);
                if (!preparedToPersist.ContainsKey(finalTrip))
                {
                    var wblist = new List<WorkBlock>();
                    wblist.Add(wbAsync);
                    preparedToPersist.Add(finalTrip, wblist);
                }
                else
                {
                    var oldList = new List<WorkBlock>();
                    preparedToPersist.TryGetValue(finalTrip, out oldList);
                    oldList.Add(wbAsync);
                    preparedToPersist.Add(finalTrip, oldList);
                }
            }
            return preparedToPersist;
        }
        private Task<int> computeWaitingTime(Trip lastTrip, Trip currentTrip)
        {
            return Task.FromResult<int>(currentTrip.PassingTimes.ElementAt(0).Time.time - lastTrip.PassingTimes.ElementAt(lastTrip.PassingTimes.Count - 1).Time.time);
        }

        public async Task<WorkBlock> getByName(int name)
        {
            return await this._repo.getByName(name);
        }

        public async Task<WorkBlockDto> AddAsync(WorkBlock wb)
        {
            var res = await this._repo.AddAsync(wb);
            return mapper.toDTO(res);
        }
    }
}