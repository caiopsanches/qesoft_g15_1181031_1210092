using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using System.ComponentModel.DataAnnotations.Schema;
namespace DDDSample1.Domain.WorkBlocks
{

    public class WorkBlock : Entity<WorkBlockId>, IAggregateRoot
    {
        public WorkblockName workblockName { get; set; }
        public Time StartTime { get; set; }
        public Time FinishTime { get; set; }
        public NodeId FinishNode { get; set; }
        public NodeId BeginNode { get; set; }
        public ICollection<WorkTrip> Trips { get; set; }

        public WorkBlock()
        { }

        public int Duration()
        {
            return FinishTime.time - StartTime.time;
        }
        public WorkBlock(WorkblockName wbName, Time startTime, Time finishTime, NodeId finishNode, NodeId beginNode, List<WorkTrip> list)
        {
            if (startTime == null || finishNode == null || finishTime == null || beginNode == null)
                throw new BusinessRuleValidationException("Every paramater is needed");
            this.workblockName = wbName;
            this.Id = new WorkBlockId(Guid.NewGuid());
            this.StartTime = startTime;
            this.FinishTime = finishTime;
            this.FinishNode = finishNode;
            this.BeginNode = beginNode;
            this.Trips = list;
        }

        public WorkBlock(WorkblockName wbName,Time startTime, Time finishTime, NodeId finishNode, NodeId beginNode)
        {

            if (startTime == null || finishNode == null || finishTime == null || beginNode == null)
                throw new BusinessRuleValidationException("Every paramater is needed");

            this.workblockName = wbName;
            this.Id = new WorkBlockId(Guid.NewGuid());
            this.StartTime = startTime;
            this.FinishTime = finishTime;
            this.FinishNode = finishNode;
            this.BeginNode = beginNode;


        }

        public WorkBlock(WorkBlockDto dto)
        {

            this.Id = new WorkBlockId(dto.Id);
            this.StartTime = new Time(dto.StartTime);
            this.FinishTime = new Time(dto.FinishTime);
            this.FinishNode = new NodeId(dto.FinishNode);
            this.BeginNode = new NodeId(dto.BeginNode);


        }

    }
}