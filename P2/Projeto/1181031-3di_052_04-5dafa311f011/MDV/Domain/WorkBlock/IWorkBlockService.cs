
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.WorkBlocks
{
    public interface IWorkBlockService
    {
         Task<List<WorkBlockDto>> GetAllAsync();
         Task<WorkBlockDto> GetByIdAsync(WorkBlockId id);
          Task<int> saveChanges();
         Task<int> createWorkBlocks(CreatingWorkBlockDto dto);
        Task<WorkBlock> getByName(int name);
Task<WorkBlockDto> AddAsync(WorkBlock wb);
    }
}