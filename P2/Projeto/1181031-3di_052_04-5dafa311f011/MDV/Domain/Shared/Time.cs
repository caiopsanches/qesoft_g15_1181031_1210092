using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Shared
{
    public class Time : IValueObject
    {
        public Time()
        {
        }
        private static int MAX_T = 100000;
        private static int MIN_T = 0;
        public int time { get; set; }
        public Time(int t)
        {
         
            if (t < MIN_T || t > MAX_T) throw new Exception("Valid Time is required");
            time = t;
        }
    }
}