using System;

namespace DDDSample1.Domain.Shared
{
    public class NodeId : IValueObject
    {
         public NodeId()
        {
        }
     
        public string nodeId { get; set; }

       
        public NodeId(string id)
        {
            if (id == null) throw new Exception("Node Id is required");

            nodeId = id;
        }
    }
}