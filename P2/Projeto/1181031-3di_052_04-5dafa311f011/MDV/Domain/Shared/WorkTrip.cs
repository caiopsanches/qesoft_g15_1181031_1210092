using System;
using System.ComponentModel.DataAnnotations;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Domain.Shared
{
    public class WorkTrip 
    {
    
        public WorkTrip()
        {
        }

        public WorkTrip(WorkBlockId id1, WorkBlock wb, TripId id2, Trip t)
        {
            this.WorkblockId = id1;
            this.Workblock = wb;
            this.TripId = id2;
            this.Trip = t;
        }

        public WorkBlockId WorkblockId { get; set; }
        
        public WorkBlock Workblock { get; set; }

        public TripId TripId { get; set; }
        public Trip Trip { get; set; }

      

    }
}