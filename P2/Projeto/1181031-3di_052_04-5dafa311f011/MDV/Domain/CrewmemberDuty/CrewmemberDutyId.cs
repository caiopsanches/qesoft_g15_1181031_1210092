using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using Newtonsoft.Json;

namespace DDDSample1.Domain.CrewmemberDuties
{
        public class CrewmemberDutyId : EntityId
        {
            [JsonConstructor]

            public CrewmemberDutyId(Guid value) : base(value)
            {
            }

            public CrewmemberDutyId(String value) : base(value)
            {
            }

            override
            protected Object createFromString(String text)
            {
                return new Guid(text);
            }

            override
            public String AsString()
            {
                Guid obj = (Guid)base.ObjValue;
                return obj.ToString();
            }


            public Guid AsGuid()
            {
                return (Guid)base.ObjValue;
            }
        }
    }