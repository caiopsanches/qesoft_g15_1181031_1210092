using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.CrewmemberDuties
{
    public class Id : IValueObject
    {


        public string id { get; set; }

        public Id()
        {
        }


        public Id(String id)
        {
            if (!isValid(id))
                throw new Exception("Id is invalid");
            this.id = id;
        }

        public bool isValid(String id)
        {
            if (id == null || id.Length > 10 || id.Length == 0)
            {
                return false;
            }
            return true;
        }
    }
}
