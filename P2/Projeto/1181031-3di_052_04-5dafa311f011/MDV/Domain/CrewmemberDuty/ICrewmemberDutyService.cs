
using System.Collections.Generic;
using System.Threading.Tasks;


namespace DDDSample1.Domain.CrewmemberDuties
{
    public interface ICrewmemberDutyService
    {
        Task<List<CrewmemberDutyDto>> GetAllAsync();
        Task<CrewmemberDutyDto> AddAsync(CrewmemberDuty dto);
        Task<bool> CreateCrewmemeber(CreatingCrewmemberDuty dto);
    }
}