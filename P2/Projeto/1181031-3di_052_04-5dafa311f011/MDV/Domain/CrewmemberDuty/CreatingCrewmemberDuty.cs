using System.Collections.Generic;
using DDDSample1.Domain.Trips;
namespace DDDSample1.Domain.CrewmemberDuties
{
    public class CreatingCrewmemberDuty
    {
        public string id { get; set; }
        public List<string>  wbList { get; set; }


        public CreatingCrewmemberDuty(string id, List<string> wblist)
        {
            this.id = id;
            this.wbList = wblist;
        
        }
    }
}