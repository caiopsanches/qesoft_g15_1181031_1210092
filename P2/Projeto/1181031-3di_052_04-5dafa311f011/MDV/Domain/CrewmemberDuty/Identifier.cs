using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.CrewmemberDuties
{
    public class Identifier : IValueObject
    {


        public int id { get; set; }

        public Identifier()
        {
        }


        public Identifier(int id)
        {
            if (!isValid(id))
                throw new Exception("Identifier is invalid");
            this.id = id;
        }

        public bool isValid(int id)
        {
            if (id == 0)
            {
                return false;
            }
            return true;
        }
    }
}
