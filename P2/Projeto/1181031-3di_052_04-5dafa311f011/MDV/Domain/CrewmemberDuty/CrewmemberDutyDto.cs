
using System;
using System.Collections.Generic;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Domain.CrewmemberDuties
{
    public class CrewmemberDutyDto
    {
        public Guid Id { get; set; }
        public int Identifier { get; set; }
        public List<WorkBlockDto> Workblocks { get; set; }
    }
}