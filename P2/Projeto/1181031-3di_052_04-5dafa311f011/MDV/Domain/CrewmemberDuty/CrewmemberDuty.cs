using System;
using DDDSample1.Domain.Shared;
using Newtonsoft.Json;
using DDDSample1.Domain.WorkBlocks;
using System.Collections.Generic;

namespace DDDSample1.Domain.CrewmemberDuties
{
    public class CrewmemberDuty : Entity<CrewmemberDutyId>, IAggregateRoot
    {
        public const int MAX_TIME = 14400;
        public Identifier Identifier { get; set; }
        public ICollection<WorkBlock> workblockList { get; set; }

        public CrewmemberDuty()
        {
        }
        public CrewmemberDuty(int id, List<WorkBlock> list)
        {
            this.Id = new CrewmemberDutyId(Guid.NewGuid());
            this.Identifier = new Identifier(id);
            this.workblockList = list;
        }

        internal static bool lessThan8h(int totalTime)
        {
            if (totalTime > 28800)
            {
                return false;
            }
            return true;
        }

        internal static bool lessThan4h(int timeCounter)
        {
            if (timeCounter > 14400)
            {
                return false;
            }
            return true;
        }
    }
    } 