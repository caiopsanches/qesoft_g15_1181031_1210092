using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Domain.CrewmemberDuties
{
    public interface ICrewmemberDutyRepository : IRepository<CrewmemberDuty, CrewmemberDutyId>
    {
        Task<List<CrewmemberDuty>> GetAllWithWBs();
    }
}