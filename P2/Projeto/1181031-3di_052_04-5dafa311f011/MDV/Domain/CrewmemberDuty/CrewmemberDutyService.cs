using System.Linq;
using System.Threading.Tasks;
using System;

using DDDSample1.Mappers;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.VehicleDuties;

namespace DDDSample1.Domain.CrewmemberDuties
{

    public class CrewmemberDutyService : ICrewmemberDutyService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ICrewmemberDutyRepository _repo;
        private readonly IWorkBlockRepository _repoWB;
        private readonly IVehicleDutyRepository _vehicleRepo;
        private readonly CrewmemberDutyMapper mapper;
        public CrewmemberDutyService(ICrewmemberDutyRepository repo, IWorkBlockRepository repoWB, IUnitOfWork unitOfWork, IVehicleDutyRepository vehicleRepo)
        {
            _repo = repo;
            _repoWB = repoWB;
            mapper = new CrewmemberDutyMapper();
            _unitOfWork = unitOfWork;
            _vehicleRepo = vehicleRepo;
        }

        public async Task<List<WorkBlock>> GetAllWorkblocks()
        {
            return await _repoWB.GetAllAsyncWithTrips();
        }

        public List<WorkBlock> FindMatchWorkblockAsync(List<string> ids, List<WorkBlock> allWorkblocks)
        {
            List<WorkBlock> WBneeded = new List<WorkBlock>();
            foreach (WorkBlock elem in allWorkblocks)
            {
                if (ids.Contains(elem.Id.AsString()))
                {
                    WBneeded.Add(elem);
                }
            }
            return WBneeded;
        }

        public async Task<string> FindVehicleDutyByWorkblock(WorkBlock wb)
        {
            var res = await _vehicleRepo.getVehicleDutyByWorkblockId(wb.Id.Value);
            return res.Id.Value;
        }

        public async Task<bool> VerifyVehicleDuty(string v, WorkBlock first, WorkBlock second)
        {
            var res = await _vehicleRepo.getVehicleDutyByWorkblockId(first.Id.Value);
            if (res.Id.Value.Equals(v))
            {
                return true;
            }
            else
            {
                if (first.FinishNode == second.BeginNode)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> VerifyContiguous(WorkBlock first, WorkBlock second)
        {
            if (first.FinishTime.time == second.StartTime.time)
            {
                return true;
            }
            return false;
        }


        public async Task<bool> VerifiedToAdd(int id, List<WorkBlock> allWorkblocks)
        {
            var lastWorkblock = new WorkBlock();
            var noExceptionFound = true;
            var wbCounter = 0;
            string vehicleDuty = "";
            int totalTime = 0;
            var timeCounter = 0;
            while (noExceptionFound && wbCounter < allWorkblocks.Count)
            {
                totalTime += timeCounter;
                timeCounter = 0;
                var isContiguous = true;
                noExceptionFound = CrewmemberDuty.lessThan8h(totalTime);
                while (isContiguous && noExceptionFound && wbCounter < allWorkblocks.Count)
                {
                    var localCurrentWorkblock = allWorkblocks.ElementAt(wbCounter);
                    timeCounter += localCurrentWorkblock.Duration();
                    lastWorkblock = localCurrentWorkblock;

                    if (wbCounter == 0)
                    {
                        vehicleDuty = await FindVehicleDutyByWorkblock(localCurrentWorkblock);
                    }
                    else
                    {
                        if (await VerifyVehicleDuty(vehicleDuty, localCurrentWorkblock, lastWorkblock))
                        {
                            isContiguous = await VerifyContiguous(localCurrentWorkblock, lastWorkblock);
                            noExceptionFound = CrewmemberDuty.lessThan4h(timeCounter);
                        }
                        else
                        {
                            noExceptionFound = false;
                        }
                    }
                    wbCounter++;
                }

            }
            if (noExceptionFound)
            {
                CrewmemberDuty res = await this._repo.AddAsync(new CrewmemberDuty(id, allWorkblocks));
                await this._unitOfWork.CommitAsync();
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<CrewmemberDutyDto> AddAsync(CrewmemberDuty crewmember)
        {
            var res = await this._repo.AddAsync(crewmember);
            return mapper.toDTO(res);
        }

        public async Task<List<CrewmemberDutyDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllWithWBs();
            List<CrewmemberDutyDto> res = new List<CrewmemberDutyDto>();
            foreach (CrewmemberDuty cd in list)
            {
                var dto = mapper.toDTO(cd);
                res.Add(dto);
            }
            return res;
        }

        public async Task<bool> CreateCrewmemeber(CreatingCrewmemberDuty dto)
        {
            List<WorkBlock> listaWB = await GetAllWorkblocks();
            listaWB = FindMatchWorkblockAsync(dto.wbList, listaWB);
            if (await VerifiedToAdd(Int32.Parse(dto.id), listaWB))
                return true;

            return false;

        }
    }
}
