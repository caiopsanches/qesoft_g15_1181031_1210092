using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.VehicleDuties {
    
    public interface IVehicleDutyRepository : IRepository<VehicleDuty, VehicleDutyId> {
        Task<List<VehicleDuty>> GetAllAsyncWithVehicleDuties();
          Task<VehicleDuty> getVehicleDutyByWorkblockId(string id);
    }
}