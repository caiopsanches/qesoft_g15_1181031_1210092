using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDDSample1.Domain.VehicleDuties
{
    public interface IVehicleDutyService
    {
        Task<List<OutputVehicleDutyDTO>> GetAllAsync();
        Task<OutputVehicleDutyDTO> GetByIdAsync(VehicleDutyId id);
        Task<VehicleDuty> CreateVehicleDutyAsync(InputVehicleDutyDTO dto);

        Task<VehicleDuty> AddAsync(VehicleDuty vehicleDuty);
    }
}