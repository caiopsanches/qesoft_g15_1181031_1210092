using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.VehicleDuties {

    public class Description : IValueObject {

        public int description { get; set; }
        
        public Description() {}

        public Description(int description) {
          
            this.description = description;
        }
    }
}
