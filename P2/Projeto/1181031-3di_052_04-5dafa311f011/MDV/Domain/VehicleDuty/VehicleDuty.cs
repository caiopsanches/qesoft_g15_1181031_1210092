using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.Vehicle;

namespace DDDSample1.Domain.VehicleDuties
{

    public class VehicleDuty : Entity<VehicleDutyId>, IAggregateRoot
    {

        public Description description { get; set; }

        public Time beginDutyDate { get; set; }

        public Time endDutyDate { get; set; }

        public BeginNode beginNode { get; set; }

        public EndNode endNode { get; set; }

        public ICollection<Trip> tripList { get; set; }

        public VehicleDuty() { }

        public VehicleDuty(Description description, Time beginDutyDate, Time endDutyDate,
                            BeginNode beginNode, EndNode endNode, ICollection<Trip> tripList)
        {

            if (endDutyDate.time < beginDutyDate.time)
                throw new BusinessRuleValidationException("The begin date of the Vehicle Duty has to be before the end date.");

            if (endDutyDate.time - beginDutyDate.time > 24 * 3600)
                throw new BusinessRuleValidationException("The Vehicle Duty can not be longer the 16 hours.");

            this.Id = new VehicleDutyId(Guid.NewGuid());
            this.description = description;
            this.beginDutyDate = beginDutyDate;
            this.endDutyDate = endDutyDate;
            this.beginNode = beginNode;
            this.endNode = endNode;
            this.tripList = tripList;
        }
    }
}