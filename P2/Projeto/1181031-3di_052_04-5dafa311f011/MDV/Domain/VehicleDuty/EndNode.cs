using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.VehicleDuties {

    public class EndNode : IValueObject {

        public String endNode { get; set; }
        
        public EndNode() {}

        public EndNode(String nodeId) {
            if (nodeId == null)
                throw new Exception("End Node is required!");

            this.endNode = nodeId;
        }
    }
}
