using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.VehicleDuties {

    public class OutputVehicleDutyDTO {

        public String id { get; set; }

        public int description { get; set; }

        public int beginDutyDate { get; set; }

        public int endDutyDate { get; set; }

        public String beginNode { get; set; }

        public String endNode { get; set; }

        public List<int> tripList { get; set; }

        public OutputVehicleDutyDTO(String id, int description, int beginDutyDate,
                     int endDutyDate, String beginNode, String endNode, List<int> tripList) {
            this.id = id;
            this.description = description;
            this.beginDutyDate = beginDutyDate;
            this.endDutyDate = endDutyDate;
            this.beginNode = beginNode;
            this.endNode = endNode;
            this.tripList = tripList;
        }
    }
}