using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.VehicleDuties {

    public class BeginNode : IValueObject {

        public String beginNode { get; set; }
        
        public BeginNode() {}

        public BeginNode(String nodeId) {
            if (nodeId == null)
                throw new Exception("Begin Node is required!");

            this.beginNode = nodeId;
        }
    }
}
