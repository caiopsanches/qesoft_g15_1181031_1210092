using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using System.Linq;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.Vehicle;
using DDDSample1.Mappers;

namespace DDDSample1.Domain.VehicleDuties
{

    public class VehicleDutyService : IVehicleDutyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleDutyRepository _repo;
        private readonly IVehicleRepository _vehicleRepo;
        private readonly ITripRepository _tripRepo;
        private readonly IPassingTimeRepository _passingRepo;

        public VehicleDutyService() { }
        public VehicleDutyService(IUnitOfWork unit, IVehicleDutyRepository rep, ITripRepository tripRep, IPassingTimeRepository passingRep)
        {

            this._unitOfWork = unit;
            this._repo = rep;
            this._tripRepo = tripRep;
            this._passingRepo = passingRep;

        }

        public async Task<List<OutputVehicleDutyDTO>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsyncWithVehicleDuties();
            VehicleDutyMapper mapper = new VehicleDutyMapper();

            List<OutputVehicleDutyDTO> listDto = new List<OutputVehicleDutyDTO>();

            foreach (var item in list)
            {
                Trip[] array = item.tripList.ToArray();
                List<int> list1 = new List<int>();

                foreach (var item1 in array)
                {
                    // PassingTime[] array1 = item1.PassingTimes.ToArray();
                    // PassingTimeDto[] list2 = new PassingTimeDto[array1.Length];
                    // foreach (var item2 in array1)
                    // {
                    //     list2.Append(new PassingTimeDto(item2.Time, item2.NodeId, item2.ReliefP));
                    // }
                    // list1.Add(item1.Id.AsString());

                    list1.Add(item1.TripName.name);
                }
                listDto.Add(mapper.toDTO(item, list1).Result);
            }
            return listDto;
        }

        public async Task<OutputVehicleDutyDTO> GetByIdAsync(VehicleDutyId id)
        {
            var vehicleDuty = await this._repo.GetByIdAsync(id);
            VehicleDutyMapper mapper = new VehicleDutyMapper();

            if (vehicleDuty == null)
                return null;

            Trip[] array = vehicleDuty.tripList.ToArray();
            List<int> list = new List<int>();

            foreach (var item in array)
            {
                // PassingTime[] array1 = item.PassingTimes.ToArray();
                // PassingTimeDto[] list1 = new PassingTimeDto[array1.Length];
                // foreach (var item1 in array1)
                // {
                //     list1.Append(new PassingTimeDto(item1.Time, item1.NodeId, item1.ReliefP));
                // }
                // list.Append(item.Id.AsString());

                list.Add(item.TripName.name);
            }

            return mapper.toDTO(vehicleDuty, list).Result;
        }

        public async Task<VehicleDuty> CreateVehicleDutyAsync(InputVehicleDutyDTO dto)
        {
            VehicleDutyMapper mapper = new VehicleDutyMapper();

            TripService tripService = new TripService(this._unitOfWork, this._tripRepo, this._passingRepo);

            ICollection<Trip> tripList = new List<Trip>();

            foreach (var item in dto.tripList)
            {
                Trip trip = await tripService.getById(new TripId(item));
                tripList.Add(trip);
            }

            return await AddAsync1(mapper.toDomain(dto, tripList).Result);
        }

        internal async Task<VehicleDuty> AddAsync1(VehicleDuty vehicleDuty)
        {
            VehicleDuty aux = await this._repo.AddAsync(vehicleDuty);
            await saveChanges();
            return aux;
        }

        internal async Task<int> saveChanges()
        {
            await this._unitOfWork.CommitAsync();
            return 1;
        }

        public async Task<VehicleDuty> AddAsync(VehicleDuty vehicleDuty)
        {
            return await this._repo.AddAsync(vehicleDuty);
        }
    }
}