using System;
using DDDSample1.Domain.Shared;
using System.Text.RegularExpressions;

namespace DDDSample1.Domain.Drivers
{
    public class DriverLicenseNumber : IValueObject
    {       
        public DriverLicenseNumber() 
        {
        }

        Regex rx = new Regex("P-[0-9]{7} [0-9]", RegexOptions.Compiled);
        public string driverLicenseNumber { get; private set; }

        public DriverLicenseNumber(string driLN)
        {

            if (driLN == null) 
            {
            throw new Exception("Driver's License Number is required.");
            }

            if (!rx.IsMatch(driLN))
            {
                throw new Exception("Driver's License Number is invalid [P-[0-9]{7} [0-9]].");
            }

            this.driverLicenseNumber = driLN;
        }

    }
}