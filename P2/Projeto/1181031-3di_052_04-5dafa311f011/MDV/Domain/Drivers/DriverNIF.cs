using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverNIF : IValueObject
    {       
        public DriverNIF() 
        {
        }

        private static long MAX_nif = 999999999;
        private static long MIN_nif = 100000000;
        public long driverNIF { get; private set; }

        public DriverNIF(long dNIF)
        {

            if (dNIF == 0) 
            {
            throw new Exception("Driver's NIF is required.");
            }

            if (dNIF < MIN_nif || dNIF > MAX_nif)
            {
                throw new Exception("Driver's NIF is invalid [9].");
            }

            driverNIF = dNIF;
        }

    }
}