using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverLicenseExpiry : IValueObject
    {       
        public DriverLicenseExpiry() 
        {
        }
        public DateTime driverLicenseExpiry { get; private set; }

        public DriverLicenseExpiry(DateTime dT)
        {

            driverLicenseExpiry = dT;
        }

    }
}