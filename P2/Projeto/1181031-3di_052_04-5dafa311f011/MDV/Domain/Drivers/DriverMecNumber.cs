using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverMecNumber : IValueObject
    {       
        public DriverMecNumber() 
        {
        }
        public string driverMecNumber { get; private set; }

        public DriverMecNumber(string dN)
        {

            if (dN == null) 
            {
            throw new Exception("Driver's Mechanographic Number is required.");
            }

            if (!(dN.Length == 9))
            {
                throw new Exception("Driver's Mechanographic Number is invalid [9].");
            }

            driverMecNumber = dN;
        }

    }
}