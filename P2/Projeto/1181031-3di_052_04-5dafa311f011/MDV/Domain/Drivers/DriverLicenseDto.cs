using System;

namespace DDDSample1.Domain.Drivers
{
    public class DriverLicenseDto
    {        

        public Guid Id { get; set; }
        public string DriverLicenseExpiry { get; set; }
        public string DriverLicenseNumber { get; set; }

        public DriverLicenseDto(string driverLicenseExpiry, string driverLicenseNumber)
        {
            this.DriverLicenseExpiry = driverLicenseExpiry;
            this.DriverLicenseNumber = driverLicenseNumber;
        }

        public DriverLicenseDto(Guid id, string driverLicenseExpiry,  string driverLicenseNumber)
        {
            this.Id = id;
            this.DriverLicenseExpiry = driverLicenseExpiry;
            this.DriverLicenseNumber = driverLicenseNumber;
        }
    }
}