using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverCC : IValueObject
    {       
        public DriverCC() 
        {
        }

        private static long MAX_CC = 99999999;
        private static long MIN_CC = 10000000;
        public long driverCC { get; private set; }

        public DriverCC(long driCC)
        {

            if (driCC == 0) 
            {
            throw new Exception("The Driver's Citizan Card is required.");
            }

            if (driCC < MIN_CC || driCC > MAX_CC)
            {
                throw new Exception("The Driver's Citizan Card is invalid [8].");
            }

            driverCC = driCC;
        }

    }
}