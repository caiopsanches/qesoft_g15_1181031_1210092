using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverExit : IValueObject
    {       
        public DriverExit() 
        {
        }
        public DateTime driverExit { get; private set; }

        public DriverExit(DateTime dT)
        {

            driverExit = dT;
        }

    }
}