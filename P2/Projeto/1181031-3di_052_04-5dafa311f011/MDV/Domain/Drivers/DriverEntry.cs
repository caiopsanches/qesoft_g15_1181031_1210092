using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverEntry : IValueObject
    {
        public DriverEntry()
        {
        }
        public DateTime driverEntry { get; private set; }

        public DriverEntry(DateTime dT)
        {

            DateTime bday = DateTime.Parse(dT.ToString());
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age))
            {
                age--;
            }
            if (age > 5)
            {
                throw new Exception("Driver's Date of entry into the company is invalid [Company was created 5 years ago].");
            }

            driverEntry = dT;
        }

    }
}