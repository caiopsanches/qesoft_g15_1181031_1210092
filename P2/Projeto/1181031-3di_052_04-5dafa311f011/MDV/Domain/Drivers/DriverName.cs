using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverName : IValueObject
    {       
        public DriverName() 
        {
        }
        public string driverName { get; private set; }

        public DriverName(string dN)
        {

            if (dN == null) 
            {
            throw new Exception("Driver's Name is required.");
            }

            driverName = dN;
        }

    }
}