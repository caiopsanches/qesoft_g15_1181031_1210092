using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.Drivers
{
    public class CreatingDriverDto
    {
        public long DriverCC { get; set; }
        public string DriverDate { get; set; }
        public string DriverEntry { get; set; }
        public string DriverExit { get; set; }
        public string DriverMecNumber { get; set; }
        public string DriverName { get; set; }
        public long DriverNIF { get; set; }
        public DriverLicenseDto DriverLicense { get; set; }
        public List<DriverTypeId> listDriverType { get; set; }

        public CreatingDriverDto(long driverCC, string driverDate, string driverEntry, 
        string driverExit, string driverMecNumber,  string driverName, long driverNIF, string driverLicenseExpiry, 
        string driverLicenseNumber, List<string> listDriverType)
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            this.DriverCC = driverCC;
            this.DriverDate = driverDate;
            this.DriverEntry = driverEntry;
            this.DriverExit = driverExit;           
            this.DriverMecNumber = driverMecNumber;
            this.DriverName = driverName;
            this.DriverNIF = driverNIF;
            this.DriverLicense = new DriverLicenseDto(driverLicenseExpiry, driverLicenseNumber);
            foreach(string s in listDriverType){
                var aux = new DriverTypeId(s);
                auxL.Add(aux);
            }
            this.listDriverType = new List<DriverTypeId>(auxL);
        }
    }
}