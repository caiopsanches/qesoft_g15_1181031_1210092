using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;


namespace DDDSample1.Domain.Drivers
{

    public class DriverService : IDriverService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IDriverRepository _repo;

        public DriverService() { }
        public DriverService(IUnitOfWork unit, IDriverRepository rep)
        {

            this._unitOfWork = unit;
            this._repo = rep;

        }

        public async Task<List<DriverDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<DriverDto> listDto = list.ConvertAll<DriverDto>(driver1 =>
              new DriverDto
              {
                  Id = driver1.Id.AsGuid(),
                  DriverCC = driver1.DriverCC.driverCC,
                  DriverDate = driver1.DriverDate.driverDate.ToString(),
                  DriverEntry = driver1.DriverEntry.driverEntry.ToString(),
                  DriverExit = driver1.DriverExit.driverExit.ToString(),
                  DriverMecNumber = driver1.DriverMecNumber.driverMecNumber,
                  DriverName = driver1.DriverName.driverName,
                  DriverNIF = driver1.DriverNIF.driverNIF,
                  DriverLicenseDto = new DriverLicenseDto(driver1.DriverLicense.DriverLicenseExpiry.driverLicenseExpiry.ToString(),
                    driver1.DriverLicense.DriverLicenseNumber.driverLicenseNumber),
                  listDriverType = driver1.DriverType
              });

            return listDto;
        }

        public async Task<DriverDto> GetByIdAsync(DriverId id)
        {
            var driver1 = await this._repo.GetByIdAsync(id);

            if (driver1 == null)
                return null;

            return new DriverDto
            {
                Id = driver1.Id.AsGuid(),
                DriverCC = driver1.DriverCC.driverCC,
                DriverDate = driver1.DriverDate.driverDate.ToString(),
                DriverEntry = driver1.DriverEntry.driverEntry.ToString(),
                DriverExit = driver1.DriverExit.driverExit.ToString(),
                DriverMecNumber = driver1.DriverMecNumber.driverMecNumber,
                DriverName = driver1.DriverName.driverName,
                DriverNIF = driver1.DriverNIF.driverNIF,
                DriverLicenseDto = new DriverLicenseDto(driver1.DriverLicense.DriverLicenseExpiry.driverLicenseExpiry.ToString(),
                  driver1.DriverLicense.DriverLicenseNumber.driverLicenseNumber),
                listDriverType = driver1.DriverType
            };
        }

        /*public async Task<List<DriverTypeMDRDto>> GetDriverTypeAsync(CreatingDriverDto dto, HttpClient client)
        {
            List<DriverTypeMDRDto> listAux = new List<DriverTypeMDRDto>();
            var taskDriver = await client.GetAsync("http://localhost:8080/api/crewmember");
            var jsonStringDriver = await taskDriver.Content.ReadAsStringAsync();
            //Console.WriteLine(jsonStringDriver);
            List<DriverTypeMDRDto> driversTypes = JsonConvert.DeserializeObject<List<DriverTypeMDRDto>>(jsonStringDriver);
            foreach (DriverTypeMDRDto element in driversTypes)
            {

                foreach (string s in dto.DriverType)
                {
                    if (element.code == s)
                    {
                        listAux.Add(element);
                    }
                }
            }
            return listAux;
        }*/

        public async Task<Driver> CreateDriverAsync(CreatingDriverDto dto)
        {
            //List<string> listAux = driverTypeAux.ToList().ConvertAll<string>(aux => aux.code);
            Driver driver1 = null;

            Driver driverDTO = new Driver(dto.DriverCC, DateTime.Parse(dto.DriverDate), DateTime.Parse(dto.DriverEntry),
            DateTime.Parse(dto.DriverExit), dto.DriverMecNumber, dto.DriverName, dto.DriverNIF,
            DateTime.Parse(dto.DriverLicense.DriverLicenseExpiry), dto.DriverLicense.DriverLicenseNumber, dto.listDriverType);
            driver1 = await AddAsync1(driverDTO);
            return driver1;
        }

        internal async Task<Driver> AddAsync1(Driver currentDriver)
        {

            var validateDates = DateTime.Compare(currentDriver.DriverEntry.driverEntry, currentDriver.DriverExit.driverExit);

            if (validateDates >= 0)
            {
                throw new Exception("A data de saída tem de ser depois da data de entrada.");
            }

            Driver aux = await this._repo.AddAsync(currentDriver);
            await saveChanges();
            return aux;
        }

        internal async Task<int> saveChanges()
        {
            await this._unitOfWork.CommitAsync();
            return 1;

        }

    }
}