using System;
using DDDSample1.Domain.Shared;
using System.Collections.Generic;

namespace DDDSample1.Domain.Drivers
{
    public class Driver : Entity<DriverId>, IAggregateRoot
    {
        public DriverCC DriverCC { get; private set; }

        public DriverDate DriverDate { get; private set; }

        public DriverEntry DriverEntry { get; private set; }

        public DriverExit DriverExit { get; private set; }

        public DriverMecNumber DriverMecNumber { get; private set; }

        public DriverName DriverName { get; private set; }

        public DriverNIF DriverNIF { get; private set; }

        public DriverLicense DriverLicense { get; private set; }

        public List<DriverTypeId> DriverType { get; private set; }

        public Driver()
        {
        }

        public Driver(long driverCC, DateTime driverDate, DateTime driverEntry,
        DateTime driverExit, string driverMecNumber, string driverName, long driverNIF,
        DateTime driverLicenseExpiry, string driverLicenseNumber, List<DriverTypeId> listDriverType)
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            this.Id = new DriverId(Guid.NewGuid());
            this.DriverCC = new DriverCC(driverCC);
            this.DriverDate = new DriverDate(driverDate);
            this.DriverEntry = new DriverEntry(driverEntry);
            this.DriverExit = new DriverExit(driverExit);
            this.DriverMecNumber = new DriverMecNumber(driverMecNumber);
            this.DriverName = new DriverName(driverName);
            this.DriverNIF = new DriverNIF(driverNIF);
            this.DriverLicense = new DriverLicense(driverLicenseExpiry, driverLicenseNumber);
            this.DriverType = new List<DriverTypeId>(listDriverType);
        }


        public Driver(long driverCC, DateTime driverDate, DateTime driverEntry,
                string driverMecNumber, string driverName, long driverNIF,
                DateTime driverLicenseExpiry, string driverLicenseNumber, List<string> listDriverType)
        {
            List<DriverTypeId> auxL = new List<DriverTypeId>();
            this.Id = new DriverId(Guid.NewGuid());
            this.DriverCC = new DriverCC(driverCC);
            this.DriverDate = new DriverDate(driverDate);
            this.DriverEntry = new DriverEntry(driverEntry);
            this.DriverMecNumber = new DriverMecNumber(driverMecNumber);
            this.DriverName = new DriverName(driverName);
            this.DriverNIF = new DriverNIF(driverNIF);
            this.DriverLicense = new DriverLicense(driverLicenseExpiry, driverLicenseNumber);
            foreach(string s in listDriverType){
                var aux = new DriverTypeId(s);
                auxL.Add(aux);
            }
            this.DriverType = new List<DriverTypeId>(auxL);
        }
    }
}