
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Domain.Drivers
{

    public interface IDriverService
    {
        Task<DriverDto> GetByIdAsync(DriverId id);
        Task<List<DriverDto>> GetAllAsync();
        Task<Driver> CreateDriverAsync(CreatingDriverDto dto);
    }
}