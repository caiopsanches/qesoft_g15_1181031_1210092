using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.Drivers
{
    public class DriverDto
    {

        public Guid Id { get; set; }
        public long DriverCC { get; set; }
        public string DriverDate { get; set; }
        public string DriverEntry { get; set; }
        public string DriverExit { get; set; }
        public string DriverMecNumber { get; set; }
        public string DriverName { get; set; }
        public long DriverNIF { get; set; }
        public DriverLicenseDto DriverLicenseDto { get; set; }
        public List<DriverTypeId> listDriverType { get; set; }

    }
}