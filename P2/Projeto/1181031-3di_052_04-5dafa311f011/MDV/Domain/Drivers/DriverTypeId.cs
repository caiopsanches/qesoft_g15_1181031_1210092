using System;
using DDDSample1.Domain.Shared;
using Newtonsoft.Json;

namespace DDDSample1.Domain.Drivers
{
    public class DriverTypeId 
    {
        public string driverTypeID { get; private set; }
        public DriverTypeId() { }

        public DriverTypeId(string driverTypeID)
        {
            if (string.IsNullOrEmpty(driverTypeID))
            {
                throw new BusinessRuleValidationException("invalid: Driver Type ID invalid");
            }
            this.driverTypeID = driverTypeID;
        }
    }
}