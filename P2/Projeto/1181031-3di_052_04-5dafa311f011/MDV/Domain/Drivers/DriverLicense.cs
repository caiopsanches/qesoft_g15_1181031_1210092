using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverLicense : Entity<DriverLicenseId>, IAggregateRoot
    {

        public DriverLicenseExpiry DriverLicenseExpiry { get; private set; }

        public DriverLicenseNumber DriverLicenseNumber { get; private set; }

        public DriverLicense()
        {
        }

        public DriverLicense(DateTime driverLicenseExpiry, string driverLicenseNumber)
        {
            this.Id = new DriverLicenseId(Guid.NewGuid());
            this.DriverLicenseExpiry = new DriverLicenseExpiry(driverLicenseExpiry);
            this.DriverLicenseNumber = new DriverLicenseNumber(driverLicenseNumber);

        }


    }
}