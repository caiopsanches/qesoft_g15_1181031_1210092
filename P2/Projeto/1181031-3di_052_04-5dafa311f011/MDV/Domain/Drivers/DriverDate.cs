using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class DriverDate : IValueObject
    {
        public DriverDate()
        {
        }
        public DateTime driverDate { get; private set; }

        public DriverDate(DateTime dT)
        {

            DateTime bday = DateTime.Parse(dT.ToString());
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age))
            {
                age--;
            }
            if (age < 18)
            {
                throw new Exception("Driver's Date of Birth is invalid [at least 18 years old].");
            }

            driverDate = dT;
        }

    }
}